# k.LAB Integrated Development Environment

This project contains the code for the Eclipse k.LAB environment (formerly Thinkcap) and other modules to support the k.LAB IDE building strategy using Maven and Tycho. 

## Building

Building the IDE distribution is a complex dance around the intricacies of Eclipse and Maven, made necessary by limitations in the Maven Tycho plug-in (which handles the build of Eclipse artifacts). The main limitation is the need to build an intermediate (necessarily network-based) p2 repository if OSGI artifacts not packaged as Eclipse plug-ins are used. Of course we are in this situation (because the core k.LAB stack is not and should not be Eclipse based), so the build strategy has to create the OSGI bundle first, then create a temporary networked update site that provides it, then build the rest of the system with the temporary site in the target platform. Of course in the time it took to figure this out, each of my colleagues published 7 papers.

I have chosen to use a temporary update site on integratedmodelling.org instead of using a local Jetty instance, as the latter strategy proved unstable (ports need to be free) and hard to use with continuous development. If you have better ideas for the build let me (ferdinando.villa@bc3research.org) know. In the meantime, the full build strategy is included in the script scripts/build.sh, to be run from the directory (usually ~/git) where both klab-core and klab-ide have been checked out. The script builds and installs the distribution in the official site, so it should only be run by authorized administrators (it requires ssh access to integratedmodelling.org). Other users can adapt the script as needed.

At the end of the build, the org.integratedmodelling.klab.updatesite/target directory will contain the p2 repository in repository/ and three lovingly packaged distributions in the respective zips. All I cared about were the 64-bit versions for Intel MacOsX, Windows and Linux, but others can be easily added in the parent pom.xml. 

## Developing (Eclipse)

Once the klab-core, klab-kim and klab-ide projects are in the Eclipse workspace with all the nested projects loaded, the configuration works for development, capturing any changes made locally to either project. This seemingly obvious property relies on a very non-obvious setup that costed a few more paper-equivalents:

* the klab-common and klab-api have Eclipse manifests, and they are seen as plug-ins (as well as regular Maven modules with 'jar' packaging) within an Eclipse environment;
* the klab-common plugin manifest re-exports all the packages in klab-api, so that the klab IDE plugin can find all needed packages by only importing klab-common;
* the klab-common plugin manifest declares the plugin as org.integratedmodelling.klab, which is the same name as the bundle built by Maven in the klab-ide/build.site module. 
 
So during development, the org.integratedmodelling.klab name is resolved to the plugin in the workspace, and during Maven build, it is resolved to the OSGI bundle in the temporary update site. Both include all dependencies in a possibly inelegant but very effective configuration; for the Eclipse plugin, this relies on having the jars in target/dependency (built by Maven dependency:copy-dependencies goal); the jars need to be updated in the klab-common plugin manifest when any dependency changes.

Changing anything in this setup (including letting Eclipse see the "hidden" build.site module of klab-ide, loading it as a project in the environment) will interfere with this strategy and likely make development impossible without Maven recompilation of any changes.