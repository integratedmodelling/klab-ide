# run this in your git root directory after cloning the desired versions of klab-core and klab-ide.
export HAIZEA="www.integratedmodelling.org"
cd klab-core
mvn clean install
cd ../klab-ide/build.site
mvn clean install p2:site
rm -f /e/klabk/klab/plugins/org.integratedmodelling.klab_*.jar
cp target/repository/plugins/org.integratedmodelling.klab*.jar /e/klabk/klab/plugins/