# run this in your git root directory after cloning the desired versions of klab-core and klab-ide.
export HAIZEA="www.integratedmodelling.org"
cd klab-core
mvn clean install
cd ../klab-ide/build.site
mvn clean install p2:site
cd ..
scp -r build.site/target/repository/* bc3@$HAIZEA:/var/www/klab/updates/tmp/
mvn clean install