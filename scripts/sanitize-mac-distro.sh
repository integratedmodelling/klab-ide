#!/bin/sh
# by Ioannis Athanasiadis, May 5th, 2016
#
# Cleanup, unzip, apply patch
LOC="dmgdir"
rm -rf $LOC klab.dmg klab-macosx.x86_64.zip
# Unzip
mkdir -p $LOC/klab.app/Contents
unzip -qo klab-macosx.cocoa.x86_64.zip -d $LOC/klab.app/Contents/
# Apply patch
sed -i -e  's#<array>#<array><string>--launcher.ini</string><string>$APP_PACKAGE/Contents/klab/klab.ini</string>#' $LOC/klab.app/Contents/Info.plist 
#
# Create dmg distribution Works only on MacOS
#hdiutil create -srcfolder $LOC/klab.app/ klab.dmg
#
# Create dmg distribution (Unix version, untested)
genisoimage -V klab -D -R -apple -no-pad -o klab.dmg $LOC
# if the above wont work, use this one:
#zip -rq klab-macosx.x86_64.zip $LOC/klab.app
# Cleanup
rm -rf $LOC