# run this in your git root directory after cloning the desired versions of klab-core and klab-ide.
export HAIZEA="www.integratedmodelling.org"
cd klab-core
mvn clean install
cd ../klab-ide/build.site
mvn clean install p2:site
cd ..
scp -r build.site/target/repository/* bc3@$HAIZEA:/var/www/klab/updates/tmp/
mvn clean install
scp -r org.integratedmodelling.klab.updatesite/target/repository/* bc3@$HAIZEA:/var/www/klab/updates/0.9.9/
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-linux.gtk.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-linux.gtk.x86_64.zip
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-macosx.cocoa.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-macosx.cocoa.x86_64.zip
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-win32.win32.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-win32.win32.x86_64.zip
