#!/bin/bash
export HAIZEA="www.integratedmodelling.org"
export SRCDIR="${HOME}/srctmp"
rm -rf $SRCDIR
mkdir $SRCDIR
cd $SRCDIR
git clone -b master https://bitbucket.org/ariesteam/klab-kim.git 
git clone -b master https://bitbucket.org/ariesteam/klab-core.git 
git clone -b master https://bitbucket.org/ariesteam/klab-ide.git 
cd $SRCDIR/klab-kim
mvn clean install
cd $SRCDIR/klab-core
mvn clean dependency:copy-dependencies install 
cd ../klab-ide/build.site
mvn clean install p2:site
cd ..
scp -r build.site/target/repository/* bc3@$HAIZEA:/var/www/klab/updates/tmp/
mvn clean install
scp -r org.integratedmodelling.klab.updatesite/target/repository/* bc3@$HAIZEA:/var/www/klab/updates/0.9.10/
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-linux.gtk.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-linux.gtk.x86_64.zip
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-macosx.cocoa.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-macosx.cocoa.x86_64.zip
scp org.integratedmodelling.klab.updatesite/target/products/org.integratedmodelling.thinkcap.product-win32.win32.x86_64.zip bc3@$HAIZEA:/var/www/klab/klab-win32.win32.x86_64.zip
