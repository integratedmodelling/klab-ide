<h2>Thinklab cookbook</h2>

<p>This section is meant to support a "learn by example" paradigm for task of common occurrence. These examples are mostly meant to show expressions and their use, and use simple semantics that should not be taken as indicative of good semantic modelin practices. In semantic modeling, more than anywhere else, it is fundamental to use examples to learn, not to cut and paste from - the practice is bad everywhere, but particularly so in a paradigm that is founded on what things <em>mean</em>: cutting and pasting is the opposite of thinking!</p>

<h3>Computing states</h3>

<p>The examples shown below illustrate how to build analytic (i.e., algebraic) models using the Thinklab language. The keys to this are two clauses:</p>

<p>  on definition set to [some formula of the dependent concepts]</p>

<p>  on definition change to [some formula of the dependent concepts]</p>

<p>The "set to" clause assigns the value to be a function of the dependent concepts referenced in the observing clause. The "change to" clause uses the dependent concepts to modify the state of the top-level observable.</p>

<p>Importantly, if "change to" is used, the system will expect a <em>previous</em> value for the observable to be defined at the time the expression is evaluated, and will try to resolve it using data or other models; the formula may contain the model name to refer to the unmodified observable. If "set to" is used instead, the model is expected to produce the value <em>directly</em> and will work as long as all the dependencies are resolved, and the use of the model name in the formula is an error.</p>

<p>Note that within the square brackets [] any valid Groovy code may be evaluated, provided that it returns a value that matches the model type. For more information on the Groovy language, see http://groovy.codehaus.org</p>

<p>To model a concept as an analytical function of the values of one or more other concepts:</p>

<p>  model newConcept
    as measure newConcept in <unit>
   observing
      (inputConcept1 as measure inputConcept1SourceConcept in <unit>) named input-concept1
      (inputConcept2 as measure inputConcept2SourceConcept in <unit>) named input-concept2
    on definition set to [input-concept1 + input-concept2];</p>

<p>Example using the above model statement to express the equation:
TotalCarbonStorage = VegetationCarbonStorage + SoilCarbonStorage</p>

<p>  model TotalCarbonStorage
      as measure aries.carbon:TotalCarbonStorage in t/ha
      observing
        (VegetationCarbonStorage as measure aries.carbon:VegetationCarbonStorage in t/ha) named vegetation-c,
        (SoilCarbonStorage as measure aries.carbon:SoilCarbonStorage in t/ha) named soil-c
      on definition set to [vegetation-c + soil-c];</p>

<p>Another example which expresses the equation:
GreenhouseGasEmissions = PopulationDensity * 0.04</p>

<p>  model GreenhouseGasEmissions
      as measure im.policy:GreenhouseGasEmissions in t/ha*year
      observing
        (PopulationDensity as count im.policy:Population per km^2) named population-density
      on definition set to [population-density * 0.04];</p>

<p>Another example which expresses the equation:
YieldUse = PopulationDensity * YieldUseCoefficient</p>

<p>  model YieldUse
    as measure ia.agriculture:YieldUse in kg/year
    observing
      (ia.agriculture:PopulationDensity as count im.policy:Population per km^2) named population-density,
      (YieldUseCoefficient as ratio YieldUsePerPerson) named yield-use-coefficient
    on definition set to [population-density * yield-use-coefficient];</p>

<p>Another example which expresses the equation: LiveStockWaterUse = CowDensity * WaterUseCoefficientCattle + SheepDensity * WaterUseCoefficientSheep + GoatDensity * WaterUseCoefficientGoat</p>

<p>  model LivestockWaterUse
    as measure aries.water:LivestockWaterUse in mm
    observing
      (CowDensity as count im.agriculture:Cattle per km^2) optional named cow-density,
      (SheepDensity as count im.agriculture:Sheep per km^2) optional named sheep-density,
      (GoatDensity as count im.agriculture:Goat per km^2) optional named goat-density,
      (WaterUseCoefficientCattle as ratio aries.water:WaterUsePerHeadOfCattle) optional named water-use-coefficient-cattle,
      (WaterUseCoefficientSheep as ratio aries.water:WaterUsePerSheep) optional named water-use-coefficient-sheep,
      (WaterUseCoefficientGoat as ratio aries.water:WaterUsePerGoat) optional named water-use-coefficient-goat
    on definition set to [(cow-density * water-use-coefficient-cattle) +
                        (sheep-density * water-use-coefficient-sheep) +
                        (goat-density * water-use-coefficient-goat)];</p>

<p>A tricky example which expresses the piecewise equation: IrrigationWaterUse = {2000 if LandCover = AgriculturalArea, 0 otherwise} Note the use of "change to" rather than "set to" in this model statement.</p>

<p>  model 2000 named default-irrigation-amount
    as measure aries.water:IrrigationWaterUse in mm
    observing
      (LandCover as classify im.landcover:LandCoverType) named land-cover
    on definition change to 0 unless [land-cover == im.landcover:AgriculturalArea];</p>

<p>An example using the Normal distribution. This expresses the piecewise equation:
Elevation = {0 if Normal(mean=542.0,std=223.3) &lt; 500, Normal(mean=542.0,std=223.3) otherwise}
Note the use of "change to" rather than "set to" in this model statement.</p>

<p>  model rand.normal(mean = 542.0, std = 223.3) named random-elevation-filtered
      as measure im.geo:Elevation in m
      on definition change to 0 if [random-elevation-filtered &lt; 500];</p>

<p>An example using the Poisson distribution. This expresses the piecewise equation:
ResidentialWaterUse = {0 if Poisson(lambda=12) &lt;= 22, Poisson(lambda=12) otherwise}
Note the use of "change to" rather than "set to" in this model statement.</p>

<p>  model rand.poisson(lambda = 12) named random-scattered-residential-users
    as measure aries.water:ResidentialWaterUse in mm
    on definition change to 0 if [random-scattered-residential-users &lt;= 22];</p>

<p>An example using the || operator to merge a River and Spring layer together.
WaterPresence = true if RiverPresence or SpringPresence is true</p>

<p>  model WaterPresence
    as presence of im.hydrology:WaterBody
    observing
      (RiverPresence as presence of im.hydrology:River) named stream,
      (SpringPresence as presence of im.hydrology:Spring) named spring
      // "presence of" is a boolean value, so we use the OR (||) operator here, true if either or both are true.
    on definition set to [stream || spring];</p>

<p>An example using a rank (unitless quantification) observation type.
WildlifeSpeciesRichness = (AmphibianRichness + BirdRichness + MammalRichness + ReptileRichness) * 0.25</p>

<p>  model WildlifeSpeciesRichness
    as rank aries.recreation:WildlifeSpeciesRichness
    observing
      (im.ecology:AmphibianRichness as rank im.ecology:AmphibianRichness) named amphibian-richness,
      (im.ecology:BirdRichness as rank im.ecology:BirdRichness) named bird-richness,
      (im.ecology:MammalRichness as rank im.ecology:MammalRichness) named mammal-richness,
      (im.ecology:ReptileRichness as rank im.ecology:ReptileRichness) named reptile-richness
    on definition set to [(amphibian-richness + bird-richness + mammal-richness + reptile-richness) * 0.25];</p>
