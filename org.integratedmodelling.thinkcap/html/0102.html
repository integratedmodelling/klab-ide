<p>﻿# Module 2. Models as observations: subjects, qualities and traits.</p>

<p>Anything that happens in Thinklab is the result of choosing a model and running it in order to create an observation. A <em>model</em> always stands between the intent to observe a concept and the successful observation of that concept. Models require a <em>subject</em> in order to observe anything, so a model begins with the <em>observe</em> keyword, which can be thought of as the declaration of the root subject. While most of the examples discussed in the text present the root subject as a region of space, this is only due to the nature of the examples. Space and time are special observation types, also known as <em>extents</em>; one or more extents create a <em>scale</em>. Special observation types will be discussed in greater detail in <a href="0105.html">Module 5</a>.</p>

<p>Because the observe statement does not specify <em>how</em> the subject should be observed, Thinklab will identify an appropriate model that will, if necessary, initiate any computation(s) required to create a semantically consistent subject. In the case of subjects, a model does not need to exist in the model base; if there is no model, a default set of actions will be taken, according to the concept that the subject incarnates. At a minimum, a simple subject will be created; this, as discussed in <a href="0101.html">Module 1</a>, is what will happen for example for a im.geography:Region, which is simply expected to <em>be there</em> and not required to have specific qualities. When, on the contrary, the semantics of the subject defines specific constraints, more observations may happen automatically. For example, observing a watershed subject would trigger a basic hydrological characterization of the watershed, consisting in observations of some qualities such as the flow direction and the elevation. </p>

<p>In contrast to subjects, models of a quality, must exist in the knowledge base: it is not possible to create a “default” elevation measurement or land cover classification. The simplest (and, from an epistemological point of view, also the best) model of a quality is <em>evidence</em> in the form of data. So when observing a quality, the model base is first searched for a data model (also called a <em>data annotation</em>) that matches that quality. If a data annotation does not exist for the quality, Thinklab will look up a model that can <em>compute</em> it based on an algorithm or other process. </p>

<p>Modeling in Thinklab entails writing <em>model</em> instructions that produce observations of subjects and qualities. This includes <em>annotating</em> data sources to turn them into models of shared, recognized concepts. An important part of this process is understanding the criteria with which Thinklab ranks models when more than one is found for a particular concept. These criteria, discussed in detail in <a href="0105.html">Module 5</a>, ensure that the most appropriate model will be selected to observe a concept in a specified context.</p>

<p>A <code>model</code> statement can be run manually in Thinkcap, by dragging and dropping it onto a context. While this is an appropriate way to test models, it is important to remember that the ultimate purpose of a model is to be considered during the <em>resolution</em> of the concept(s) it describes, so that Thinklab can choose the most suitable model for an observation in the specified context. In a collaborative effort, it is normal to have more than one model for a concept (e.g., different data sources with different resolution, coverage, currency). The criteria that negotiate the model selection process selected are the subject of <a href="0105.html">Module 5</a>.</p>

<h2>Concepts and observables</h2>

<p>At this point, it is important to understand the details of the <em>concepts</em> that are used to specify the <em>observable</em> of a model, i.e., the concept that the model will produce an observation of. Concepts live in <em>ontologies</em> and Thinklab can be used to define them (a Thinklab <em>namespace</em> is indeed an <em>ontology</em>, which can define semantics both for concepts and models). A substantial set of concepts come with the integratedmodelling.org certificate. The process of creating ontologies (or even new concepts in an existing ontology) has its difficulties. Creating new concepts or ontologies (as discussed in <a href="">Module XXX</a>), requires a deep understanding of the contents of existing ontologies and a cautious approach which ensures semantic consistency and avoids unintended consequences to the collaborative modeling environment. Ontologies are, the fundamental building blocks of the semantic modeling approach. The remainder of this section describes how to <em>use</em> the knowledge that exists within the ontology library.</p>

<p>Three fundamental aspects must be correctly specified to define an <em>observable</em>:</p>

<ol>
<li>The <strong><em>primary observable</em></strong>: it is a thing, process, quality or event.</li>
<li>Any <strong><em>traits</em></strong> that further specify the primary observable and influence the type of observation made;</li>
<li>The <strong><em>inherency</em></strong> of a concept to a subject, which further informs the matching of concepts to models during resolution.</li>
</ol>

<h3>The primary observable</h3>

<p>There are two fundamental kinds of observables: </p>

<ol>
<li>Those that specify entities that can be thought of as existing autonomously (or, as some philosophers say, have <em>unity</em>): those include entities that “are”, such as regions, human beings, animals or plants, but also entities that “happen”, such as events (e.g. an earthquake) or processes (e.g. “carbon sequestration”). In the following, we use the convention of referring to all of these with the term <strong>things</strong>.</li>
<li>Those that specify <strong>qualities</strong>, which must refer to a <em>thing</em> in order to exist as a meaningful concept for modeling. Those include “properties” such as color, temperature, or density.</li>
</ol>

<p>We say that an observable is <em>concrete</em> when it can be directly observed without further specification: for example, “nitrogen amount” would be concrete while “amount” would be <em>abstract</em>, the opposite of concrete. An “amount” cannot be identified without specifying an <em>identity</em> for it that “grounds” it to the physical world.</p>

<p>Because an identity clearly is also a concept but cannot be observed without an observable to refer it to, we need to bring in another class of concepts, which we call in general <strong>traits</strong>. Those are not observables: so you cannot write a model for, say, a plant species or a chemical element. Rather, they are used to <em>qualify</em> observables so that their observation becomes unambiguous. We recognize three types of traits:</p>

<ol>
<li><strong>Identities</strong>, which have the property of being able to turn a compatible abstract observable into concrete. An observable can have one and only one identity – for example, a “cat individual” or a “gold weight”. You cannot observe gold, but you can observe <em>its</em> weight: to annotate this observation, you use the abstract observable (weight, a quality) identified with “gold” (a chemical identity).</li>
<li><strong>Attributes</strong> that can only apply to concrete observables and further specify them so that there is no ambiguity in annotations. For example “annual rainfall amount” would be observable without the attribute “annual”, but it would be incorrect to mix annual measurements with monthly ones, so we use the attribute to specify the annual character. There is a vast taxonomy of attributes, many of which are collected in our core ontologies so that the need for “inventing the wheel” is minimized (along with the risk of making the wheel square). An observable can have many attributes, but only one per category: so we can see “annual average rainfall amount” but not “annual monthly rainfall amount”.</li>
<li><strong>Realms</strong> are special attributes that are very common in modeling, so we have decided to give them a status of their own. A realm refers to a broad subdivision of the physical world (not necessarily geographically delimited) where a particular observable is expected to be observed; for example, geographical realms such as land, ocean, atmosphere, soil, or biogeographical ones such as ecozones. They are conceptually not very different from attributes and they work the same way – many realms, but only one per category. Because modelers often use realms, we conceptualize them separately so they can be more easily catalogued and located.</li>
</ol>

<p>Thinklab provides base semantics for several other kinds of observables, such as basic physical properties (see <a href="0103.html">Module 3</a>. Still, the distinction between <em>things</em> and <em>qualities</em>, complemented by <em>traits</em>, remains the fundamental conceptual skeleton for the process of annotation. Any confusion over what is a thing vs. what is a quality will lead to trouble. When possible Thinklab will validate concepts so it should not be allowable to use a thing concept when a quality concept is needed (or vice versa). This is not universally possible, and observations resulting in strange model behaviors will arise unless the distinction is clearly understood.</p>

<p>Observing things and qualities works differently, and the observation of each produces different outcomes. </p>

<ul>
<li><p>The observation of a thing, also known as a <strong>direct observation</strong>, produces a virtual representation of one or more things in a context. So for example, when observing a watershed, an "object" tagged with the watershed concept is created in the computer’s memory. Within the context of the watershed it is possible to observe households, rivers or bridges. The observation of those concepts will create new objects, under the "ownership" of the containing watershed.</p></li>
<li><p>The observation of a quality is an <strong>indirect observation</strong>. An indirect observation produces an output (numbers, for example) that indirectly describes the <em>state</em> of that quality in the context. For example, consider the concept describing the amount of annual rainfall in a watershed. Let’s assume that the watershed context is distributed over an interpretation of space that conceptualizes it as multiple cells or polygons (In Thinklab, the temporal and spatial qualities of observations depend on the specific characteristics of the context. This will be discussed in greater detail in <a href="0105.html">Module 5</a>.) A successful observation of this concept creates an observation of rainfall in the watershed, most likely expressed in mm (expressing the density of the water volume over space). The observation will take the form of a map, attributing a number <em>value</em> to each subdivision of the context. Each value expresses the rainfall amount indirectly, referring to a known scale (how many mm, defined in the SI system). </p></li>
</ul>

<p>The interpretation of numbers (or other data types, e.g., categories) in an indirect observation will depend on the defined <em>observation semantics</em> (for example, a <em>measurement</em> in a particular <em>unit</em>). <em>Units</em> and <em>observation semantics</em> are described in <a href="0103.html">Module 3</a>. For now, it is important to understand that the observation of a quality can only happen within the <em>context</em> of a thing. Temperature, a quality, cannot exist alone but only in reference to the thing whose temperature is being measured. Choosing semantics to match the desired level of detail enables semantic shortcuts with respect to the physical world. For example, it is possible to measure the "atmospheric temperature" of a "region" (a thing) instead of observing the "atmosphere" in that region and contextualizing the temperature to it. </p>

<p>Thinklab offers the user extensive granularity when defining concepts with its suite of existing ontologies. Things can be inanimate or reactive (<em>agents</em>); <em>processes</em> and <em>events</em> are specialized things that will be discussed in more detail later. For now, understand that most of what is referred to as "data" are qualities in the Thinklab language, and a "dataset" (a collection of different data relative to the same context) is what Thinklab considers the observation of the <em>thing</em> that has been chosen as the context, including all of its observed qualities.</p>

<h3>Keeping ontologies simple</h3>

<p>We have discussed how traits work as "descriptors" to avoid confusion with other incompatible concept. For example, when modeling rainfall on an annual time scale, the results are presented as "annual rainfall" and annual measurements are not mixed with monthly or daily measurements. In a semantic world, it is also possible to represent such distinctions be represented by using a separate concept: for example <code>AnnualRainfall</code> and <code>MonthlyRainfall</code>, which could be specialized cases of <code>im.climate:Rainfall</code>. it is important to minimize the size of ontologies if they are to be used by wide communities, and because there are many attributes that apply equally to concepts for many disciplines, traits offer a way out of a potential semantic explosion that can lead to a complete lack of interoperability. If it were necessary to define a version of all the qualities that can be measured annually, the ontologies would rapidly increase in both size and complexity (e.g., monthly measurements for each month, daily measurements for each Julian date). Instead what is needed is the specialization of a concept that uses a general "adjective", such as:
    -finite/infinite, 
-vulnerable/invulnerable, or 
-high/medium/low.</p>

<p>When looking up models, Thinklab prioritizes models that share the same trait as the concept that is being observed, and ignoring those models that feature a different trait of the same type. Traits can be added to concepts simply by writing them along with the main observable, as it would be done in English: for example adding the Annual descriptor to the context (<code>im.hydrology:Watershed</code>) in the observe statement:</p>

<pre><code>observe im:Annual im.hydrology:Watershed
    over space(...)
model ... as measure im.climate:Rainfall in mm;
</code></pre>

<p>An "annual watershed" doesn't make much sense by itself, but the Annual trait refers to the <em>observation</em> of the watershed, ensuring that any model or data selected to observe a quality in this watershed will be annual. So when observing rainfall in this watershed using the model statement, only observations of annual rainfall will be made. If a model is chosen to resolve a <em>process concept</em>, for example "river flow," only models with an annual time step will be chosen. In other words, traits "percolate" through the resolution process and influence the choice of models made so that the result is always consistent with the observable concept.</p>

<p>Of course the observe statement above does not <em>require</em> any traits. They can be added to the model statement instead. The process will work the same way, and the resolution of an annual model will ensure that all observations are made on annual data (or processes with an annual time step). And, of course, it is critical to use appropriate traits when annotating data. For example, some traits detail the data reduction choices made when collecting data, such as average, minimum or maximum measurements. As many traits as necessary should be used: restrictions on the types and number of traits have been detailed before, but in general, only one identity is admitted, and as many attributes or realms can be present as long as each one belongs to a different abstract category. The following model statement illustrates how to assign traits for average annual rainfall data:</p>

<pre><code>model .... as measure im:Annual im:Average im.climate:Rainfall in mm;
</code></pre>

<p>Traits are also useful when classifying observations. They can be used to classify continuous data into discrete categories, which proves useful for certain types of modeling (e.g., Bayesian modeling). In this case, classifications can be done using the <code>by</code> keyword followed by the type of trait: </p>

<pre><code>model Elevation as classify (measure im.geography:Elevation in m) by im:Level into
    im:Low if 0 to 350,
    im:Medium if 350 to 1000,
    im:High if 1000 to 8000;
</code></pre>

<p>Additional examples of this will be shown in detail later. For now, consider the benefits of using the general trait im:Level, known to Thinklab as a subjective trait, to classify a continuous quantity according to an interpretation of the values. This style of model specification eliminates the need for defining concepts like "HighElevation" and "LowElevation.". There are many predefined traits in the <code>im</code> ontology describing general attributes such as regularity of occurrence (regular/irregular), frequency of occurrence (ephemeral, rare, common or continuous) and origin (endogenous/exogenous). Learning to use traits appropriately is the best way to ensure data and model compatibility.</p>

<h3>Identities managed by authorities</h3>

<p>In most cases when an identity is used, there are many – and sometimes infinite – possibile concepts, and the attribution of identity is normally subject to great debate and change. For example, biological species have a many-to-many relationship with the taxonomic <em>concept</em> they describe: individuals of the same species may have been attributed to different ones before realizing that they were two growth stages of the same, and there is an enormous amount of species that grows every day. Chemical “species” work similarly: the periodic table of elements is relatively stable, but molecules are certainly not something we can classify in a single ontology, and even if so, we certainly would not want the humongous chemical ontology only to describe water and carbon dioxide.</p>

<p>Fortunately, we are not the only ones to need a stable reference framework for this kind of “open-ended” identities, and organizations such as the <a href="http://gbif.org">Global Biodiversity Information Facility</a> have been established to provide exactly such framework. Such organizations provide a vocabulary and a process to assign a specific, stable identifier to a concept, so that it can be “tracked” unambiguously throughout the changes that it has undergone during its use in scientific practice. Similar organization promote unique ways to define molecular structures, agricultural terms, etc. Thinklab provides a way to define <em>identities</em> based on these identifiers, and the software we provide links some authorities in so that annotation becomes very simple and efficient. Authorities are identified by an uppercase string, such as GBIF, and the syntax for an authority-backed identity is as follows:</p>

<pre><code>&lt;abstract observable&gt; identified as “&lt;key&gt;” by &lt;authority&gt;
</code></pre>

<p>For example, to annotate a model that observes the number of individual of the fish species Argyrosomus hololepidotus, you can refer to the GBIF identifier for the species and write</p>

<pre><code>count im.ecology:Individual identified as "5212442" by GBIF
</code></pre>

<p>The part starting at <code>identified</code> counts as the definition of an identity trait, which is not specified directly as a concept, but by referring to an identifier managed by the GBIF authority. The authority name must be recognized and correspond to a plug-in installed in the language; use of authorities comes with the integratedmodelling.org certificate. Thinklab provides interactive search facilities and translation for some authorities: for example, the specification above will create a concept that displays (for example in data legends) as the common name of the species. Authorities will be developed and made available to suit the user communities; at the moment the three authorities available are</p>

<ol>
<li><a href="http://gbif.org">GBIF</a> for taxonomic identifiers, as above;</li>
<li><a href="http://aims.fao.org/standards/agrovoc/functionalities/search">AGROVOC</a> (from FAO) for agricultural identifiers; and</li>
<li>IUPAC for chemical species identified by an <a href="http://en.wikipedia.org/wiki/International_Chemical_Identifier">InChl</a> string.</li>
</ol>

<p>Only the GBIF authority has search facilities associated for the time being. For all others, identifiers can be retrieved by using the institutions’ web sited. Requests for supporting other authorities can be sent at integrated.modelling@gmail.com.</p>

<h3>Inherent qualities and subjects</h3>

<p>In ontologies, <em>properties</em> are used to classify the type of relationship that exists between concepts. The "specialization" property is frequently used (e.g., a <code>Car</code> is-a (type of) <code>TransportationVehicle</code>) but there are many others, both very general (e.g., <code>Individual part-of Population</code>) and specialized (<code>StreamReach has-slope Slope</code>). Although properties can be explicitly detailed in a concept specification, Thinklab can create and validate properties automatically, to reduce the complexity for the modeler and encourage shorter, more readable specifications. Yet, there are some important logical implications to consider before connecting concepts and making observations. One of which is determining the legal and illegal properties of a subject: a Watershed has Rainfall and Elevation but no Liver or Heart. Without having to explicitly create such constraints, which would be difficult, it is possible to specify the allowable subject observables to refer to, thereby avoiding improper usage in the model resolution process. This type of specification is referred to as an <em>inherency</em> specification, which in Thinklab is created using the keyword <code>within</code>. For example:</p>

<pre><code>model ... as measure im.geography:Elevation within im.geography:Region in m;
</code></pre>

<p>Assume there is a dataset specification (as we will see in Section 2) instead of an ellipsis, and consider only the <em>observable</em> of the model: a quality (elevation) with an inherent subject (a geographical region). This guarantees that any observation of <code>Elevation</code>  will use data only if the context of the observation is a <code>Region</code>. Note that this works for <em>any kind</em> of region: for example, both a Watershed and a Country are specialized types of Regions, so the data will satisfy a request for observing elevation in both of these instances. Observing the elevation of a tree, however, would not be possible using the data produced by the model above. Inherent subjects are used to restrict the semantics of the model to an appropriate set of applications.</p>

<p>Inherent subjects are used automatically during resolution: if there are two datasets for a concept, one is inherent to a Region and the other is not, the dataset inherent to the region will be given precedence during model resolution (see <a href="0105.html">Module 5</a>) for full details on prioritization in resolution). In general, there should always be an inherent subject for all observables; but the choice should be made intelligently, as it is always possible to choose a subject so restricted that models become almost useless. For example, identifying a <code>LowerAtmosphere Region</code> as the inherent subject for Temperature data may make them less useful than simply using a Region, given that the “default” meaning of temperature data refers to the Earth’s surface. Both specifications, and probably many others, may be conceptually correct, but the <em>conceptual resolution</em> of a model requires careful thinking: over-specification of semantics should be avoided as much as under-specification. If in doubt, remember that the same data can be annotated as many times as necessary, and there is no reason not to create both models if both observables have enough generality to be useful and sufficiently distinct meanings to be <em>both</em> useful.</p>

<h3>Putting everything together</h3>

<p>To summarize, an observable is composed of:</p>

<ol>
<li>one and only one <em>observable</em> concept - a quality, thing, process or event;</li>
<li>if the observable is abstract, one and only one <em>identity</em> that grounds it to reality. For example, a species for an individual or group: <code>im.agriculture:Cattle im.core:Group</code>.</li>
<li>zero or more <em>attributes</em> and/or <em>realms</em> to complete the meaning of the observable if necessary;</li>
<li>zero or one <em>inherent subject type</em>, which specifies the most general kind of subject that this observable may refer to.</li>
</ol>

<p>It is important that all the necessary concepts, and not one too many, are included in each model. Thinklab, as illustrated in the examples throughout this documentation, provides syntax to simplify the definition of an observable (in a nutshell: just string together trait concepts with their observable concept, and use the <code>within</code> keyword to introduce the inherent subject type if one is present). Consider the observable, in its three conceptual dimensions (observable/traits/inherency), as the "semantic fingerprint" of the model or data being described. Decomposing the observable keeps the ontology small - a parsimony principle which is crucial to the usability of a collaborative modeling infrastructure. The smaller the ontologies, the more useful and powerful they will be. Using separate concepts instead of specialization to capture the key meaning of observations avoids the explosion of the knowledge base and facilitates opportunities for more modelers to contribute to it.</p>
