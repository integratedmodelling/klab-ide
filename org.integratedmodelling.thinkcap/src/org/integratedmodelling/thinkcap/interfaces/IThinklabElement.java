/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.interfaces;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;

public interface IThinklabElement extends IAdaptable {

    public Object[] NO_CHILDREN = new IThinklabElement[0];

    /*
     * type constants. An element must be one of these, returned by getElementType().
     */
    public static final int THINKLAB_WORKSPACE   = 1;
    public static final int THINKLAB_PROJECT     = 2;
    public static final int THINKLAB_NAMESPACE   = 3;
    public static final int THINKLAB_MODEL       = 4;
    public static final int THINKLAB_CONTEXT     = 5;
    public static final int THINKLAB_OBSERVER    = 6;
    public static final int THINKLAB_CONCEPT     = 7;
    public static final int THINKLAB_PROPERTY    = 8;
    public static final int THINKLAB_RESTRICTION = 9;
    public static final int THINKLAB_DEFINITION  = 10;
    public static final int THINKLAB_CONNECTION  = 11;
    public static final int THINKLAB_DATA_SOURCE = 12;
    public static final int THINKLAB_ONTOLOGY    = 13;
    public static final int THINKLAB_SCRIPT      = 14;

    /**
     * Return the containing Thinklab model. Never null.
     * 
     * @return workspace
     */
    public IThinklabWorkspace getThinklabWorkspace();

    /**
     * Returns whether this Thinklab element exists physically in the model and can be
     * changed by user action. Elements for which this returns false may be imported
     * concepts or models from external packages.
     * 
     * <p>
     * Thinklab elements are handle objects that may or may not be backed by an
     * actual element. Thinklab elements that are backed by an actual element are
     * said to "exist", and this method returns <code>true</code>.
     * </p>
     *
     * @return <code>true</code> if this element exists in the Thinklab model, and
     * <code>false</code> if this element does not exist
     */
    boolean exists();

    public int getElementType();

    public String getElementName();

    /**
     * Return the containing project, or null if this element is not contained
     * in any project.
     * 
     * @return project
     */
    public IThinklabProject getThinklabProject();

    /**
     * The element directly containing this one, or null if we have no parent.
     * 
     * @return parent
     */
    public IThinklabElement getParent();

    /**
     * Returns the path to the innermost resource enclosing this element.
     * If this element is not included in an external library,
     * the path returned is the full, absolute path to the underlying resource,
     * relative to the workbench.
     * If this element is included in an external library,
     * the path returned is the absolute path to the archive or to the
     * folder in the file system.
     *
     * @return the path to the innermost resource enclosing this element
     */
    public IPath getPath();

    /**
     * Returns the innermost resource enclosing this element.
     * If this element is included in an archive and this archive is not external,
     * this is the underlying resource corresponding to the archive.
     * If this element comes from an external source, <code>null</code>
     * is returned.
     *
     * @return the innermost resource enclosing this element, <code>null</code> if this
     * element is included in an external archive
     */
    public IResource getResource();

    /**
     * Return the most direct ancestor of this element that is of the passed type.
     * 
     * @param ancestorType
     * @return ancestor of type
     */
    public IThinklabElement getAncestor(int ancestorType);

    /**
     * Return either NO_CHILDREN or an array of children. These should be IThinklabElement
     * when appropriate, or other elements that CNF can handle when not. Do not
     * store the child array - this should be created every time it is requested,
     * as quickly as possible.
     * 
     * @return children
     */
    public Object[] getChildren();

    /**
     * Should be able to check without having to create the full child hierarchy.
     *   
     * @return true if has children
     */
    public boolean hasChildren();

    /**
     * Potentially longer description, for the status bar. NOTE: at the moment this is used
     * to provide a unique label for comparison, so it should be unique across the model
     * tree. This is obviously silly.
     *  
     * @return description
     */
    public String getElementDescription();

}
