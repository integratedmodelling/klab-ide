/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.interfaces;

import org.eclipse.core.resources.IResource;

/**
 * The thinklab model, i.e. the (single) cache of active Thinklab project in a workspace. 
 * Can be retrieved from IThinklabElement.getThinklabModel(), never null. Should contain 
 * methods like copy, rename, delete for other elements.
 *
 * @author Ferd
 */
public interface IThinklabWorkspace extends IThinklabElement {

	/**
	 * Returns whether this model contains an <code>IThinklabModel</code> whose
	 * resource is the given resource or a non-Java resource which is the given resource.
	 * <p>
	 * @param resource the resource to check
	 * @return true if the resource is accessible through the model
	 */
	boolean contains(IResource resource);

	public IResource getResourceForURL(String resourceUrl);
}
