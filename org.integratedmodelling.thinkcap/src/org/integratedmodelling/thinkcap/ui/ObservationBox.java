/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.referencing.KnowledgeSearch;
import org.integratedmodelling.common.client.referencing.KnowledgeSearch.SearchType;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;

/**
 * A smart text field supporting an assisted observation workflow. Provides abstract
 * functions to (asynchronously) show alternatives and execute observations.
 * 
 * @author Ferd
 *
 */
public abstract class ObservationBox extends Composite {

    private Text            searchField;
    private Label           searchModeButton;
    private Label           btnNewButton_2;
    private boolean         searching        = false;
    private boolean         resultShown      = false;

    private KnowledgeSearch search           = null;

    /*
     * for cycling the search mode
     */
    int                     currentModeIndex = 0;
    static List<SearchType> searchTypeOrder  = new ArrayList<>();

    static {
        searchTypeOrder.add(SearchType.OBSERVATION);
        searchTypeOrder.add(SearchType.CONCEPT);
        searchTypeOrder.add(SearchType.QUALITY);
        searchTypeOrder.add(SearchType.TRAIT);
        searchTypeOrder.add(SearchType.SUBJECT);
        searchTypeOrder.add(SearchType.PROCESS);
        searchTypeOrder.add(SearchType.EVENT);
    }

    private Composite           composite;
    private Label               label;
    private SearchResultsViewer resultViewer;
    private Composite           resultArea;
    private Point               size;
    private ViewPart            view;

    public ObservationBox(Composite parent, ViewPart view, int style) {

        super(parent, style);

        setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
        GridLayout gl_ccombo = new GridLayout(1, false);
        gl_ccombo.horizontalSpacing = 2;
        gl_ccombo.verticalSpacing = 0;
        gl_ccombo.marginWidth = 0;
        gl_ccombo.marginHeight = 0;
        setLayout(gl_ccombo);
        draw();
        this.view = view;
    }

    protected void draw() {

        for (Control c : getChildren()) {
            c.dispose();
        }

        composite = new Composite(this, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        GridLayout gl_composite = new GridLayout(3
                + (search == null ? 0 : search.getCurrentChoices().size()), false);
        gl_composite.verticalSpacing = 0;
        gl_composite.marginWidth = 0;
        gl_composite.marginHeight = 0;
        gl_composite.horizontalSpacing = 1;
        composite.setLayout(gl_composite);

        searchModeButton = new Label(composite, SWT.NONE);
        searchModeButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        searchModeButton.setAlignment(SWT.CENTER);
        searchModeButton.setToolTipText("Query the network for a new context");
        searchModeButton.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/Database.png"));
        searchModeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                searchMode(!searching);
            }
        });

        if (search != null) {

            setIcon(getSearchType());

            for (PaletteItem p : search.getCurrentChoices()) {
                
                if (p.getNamespace() != null) {
                    continue;
                }
                
                /*
                 * Add small badge with name
                 */
                Label badge = new Label(composite, SWT.BORDER_SOLID);
                badge.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
                badge.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
                badge.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
                badge.setText(p.getName());
            }
        }

        searchField = new Text(composite, SWT.NONE);
        searchField.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        searchField.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
        searchField.setForeground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
        searchField.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        searchField.addListener(SWT.Traverse, new Listener() {
            @Override
            public void handleEvent(Event event) {

            }
        });

        searchField.addTraverseListener(new TraverseListener() {
            @Override
            public void keyTraversed(TraverseEvent e) {
                if (e.detail == SWT.TRAVERSE_TAB_PREVIOUS) {
                    currentModeIndex = (currentModeIndex == 0 ? (searchTypeOrder.size() - 1)
                            : currentModeIndex - 1);
                    setSearchMode(searchTypeOrder.get(currentModeIndex));
                    e.doit = false;
                } else if (e.detail == SWT.TRAVERSE_TAB_NEXT) {

                    /*
                     * if there's only one result, this selects it
                     */
                    if (search.getOnlyResult() != null) {
                        addChoice(search.getOnlyResult());

                    } else {

                        currentModeIndex = (currentModeIndex == (searchTypeOrder.size() - 1)) ? 0
                                : currentModeIndex + 1;
                        setSearchMode(searchTypeOrder.get(currentModeIndex));
                    }
                    e.doit = false;
                } else if (e.detail == SWT.TRAVERSE_RETURN) {
                    handleReturn();
                }
            }
        });

        searchField.addKeyListener(new KeyAdapter() {
            private boolean backspacePressed;

            @Override
            public void keyReleased(KeyEvent ke) {

                if (ke.stateMask == SWT.CTRL) {
                    switch (ke.keyCode) {
                    case 't':
                        setSearchMode(SearchType.TRAIT);
                        break;
                    case 'p':
                        setSearchMode(SearchType.PROCESS);
                        break;
                    case 'e':
                        setSearchMode(SearchType.EVENT);
                        break;
                    case 'q':
                        setSearchMode(SearchType.QUALITY);
                        break;
                    case 's':
                        setSearchMode(SearchType.SUBJECT);
                        break;
                    case 'o':
                        setSearchMode(SearchType.OBSERVATION);
                        break;
                    case 'c':
                        setSearchMode(SearchType.CONCEPT);
                        break;
                    }
                } else if (ke.keyCode == SWT.ESC) {
                    cancelSearch();
                } else if (ke.keyCode == ' ') {
                    // check current field contents; if OK,
                    // accept current (or set as wrong) prepare for next; otherwise
                    // beep. We cannot insert a space.
                    ke.doit = false;
                } else if (ke.keyCode == SWT.BS) {

                    // remove last modifier and redraw after two backspaces on an empty
                    // field.
                    if (searchField.getText().isEmpty()) {
                        if (backspacePressed) {
                            search.removeLastChoice();
                            draw();
                            if (resultViewer != null) {
                                resultViewer.clear();
                            }
                            packView();
                            ke.doit = false;
                            backspacePressed = false;
                            setFocus();
                        } else {
                            backspacePressed = true;
                        }
                        setFocus();
                    }
                } else {
                    backspacePressed = false;
                    setAutoCompletion(searchField, searchField.getText(), ObservationBox.this.getSize());
                }
            }

        });
        final Composite btnNewButton = new Composite(composite, SWT.NONE);
        btnNewButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
        btnNewButton.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
        FillLayout fl_btnNewButton = new FillLayout(SWT.HORIZONTAL);
        fl_btnNewButton.spacing = 2;
        btnNewButton.setLayout(fl_btnNewButton);
        {
            btnNewButton_2 = new Label(btnNewButton, SWT.NONE);
            btnNewButton_2.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", "icons/download-gray.png"));
        }

        label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
        GridData gd_label = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_label.heightHint = 4;
        label.setLayoutData(gd_label);

    }

    protected void addChoice(PaletteItem item) {
        String s = search.addChoice(item);
        if (s == null) {
            draw();
        }
        if (resultViewer != null) {
            resultViewer.clear();
        }
        if (s == null) {
            packView();
            setFocus();
        } else {
            searchField.setText(s + ":");
        }
    }

    private void packView() {
        getParent().pack();
    }

    public SearchType getSearchType() {
//        if (search != null) {
//            setSearchMode(search.getType());
//        }
        return searchTypeOrder.get(currentModeIndex);
    }

    protected void handleReturn() {

        if (getSearchType() == SearchType.OBSERVATION) {
            /*
             * we shouldn't get here with a URN but I may be wrong
             */
            search();
        } else {
            PaletteItem item = search.getObservableTarget();
            if (item != null) {
                Activator.engine().handleObservationAction(item, false);
            }
        }
    }

    /**
     * Called when user presses ESC.
     */
    protected abstract void cancelSearch();

    @Override
    public boolean setFocus() {
        return searchField.setFocus();
    }

    protected abstract Composite showResultArea(boolean show);

    public void searchMode(boolean on) {

        searchField.setText("");
        searchField.setEnabled(on);
        searchField.setEditable(on);
        if (on) {
            searchField.setFocus();
        }

        searchModeButton.setToolTipText(Environment.get().getTargetSubject() == null
                ? "Query the network for a new context"
                : "Search for observables to observe in " + Environment.get().getTargetSubject().getName());

        setSearchMode(Environment.get().getContext() == null ? SearchType.OBSERVATION
                : SearchType.CONCEPT);

        searching = on;
    }

    private void setAutoCompletion(Text text, String value, Point parentSize) {

        /*
         * if null (we have an empty field) and search results are visible, remove search
         * results.
         */
        if ((value == null || value.isEmpty()) && resultShown) {
            resultShown = false;
            this.resultArea = showResultArea(false);
            for (Control c : resultArea.getChildren()) {
                c.dispose();
            }
            this.resultViewer = null;
            return;
        } else if (value != null && !value.isEmpty() && !resultShown) {

            new UIJob(getDisplay(), "") {

                @Override
                public IStatus runInUIThread(IProgressMonitor monitor) {
                    resultArea = showResultArea(true);
                    resultArea.setLayout(new GridLayout(1, true));
                    resultArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
                    resultViewer = new SearchResultsViewer(resultArea, size);
                    resultViewer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
                    try {
                        resultViewer.setResults(search.run(value), parentSize);
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    resultArea.getParent().getParent().pack();
                    resultShown = true;
                    return Status.OK_STATUS;
                }
            }.schedule();

        } else if (!value.isEmpty() && resultViewer != null) {

            new UIJob(getDisplay(), "") {

                @Override
                public IStatus runInUIThread(IProgressMonitor monitor) {
                    try {
                        resultViewer.setResults(search.run(value), parentSize);
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    resultArea.getParent().getParent().pack();
                    return Status.OK_STATUS;
                }
            }.schedule();
        }
    }

    public void setSearchMode(SearchType type) {

        if (searchTypeOrder.get(currentModeIndex) != type) {
            this.currentModeIndex = searchTypeOrder.indexOf(type);
        }
        search = new KnowledgeSearch(type);
        setIcon(type);
    }

    private void setIcon(SearchType type) {

        switch (type) {
        case EVENT:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/e_yellow.png"));
            break;
        case PROCESS:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/p_gold.png"));
            break;
        case QUALITY:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/q_green.png"));
            break;
        case SUBJECT:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/s_orange.png"));
            break;
        case TRAIT:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/t_blue.png"));
            break;
        case CONCEPT:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/objects.gif"));
            break;
        case OBSERVATION:
            searchModeButton
                    .setImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif"));
        default:
            break;
        }
    }

    public void setContext(IContext c) {
        Environment.get().setContext(c);
        searchMode(false);
    }

    protected List<IModelObject> search() {

        List<IModelObject> matches = new ArrayList<>();
        String text = searchField.getText();

        IKnowledgeIndex i = KLAB.KM.getIndex();
        if (i != null) {
            if (text.trim().length() > 0) {
                // NAH this searches the Observation kbox local and remote
                // try {
                // for (IMetadata m : i.search(text.trim())) {
                // Object o = ((KnowledgeIndex) i).retrieveObject(m);
                // IModelObject b = use(o);
                // if (b != null)
                // matches.add(b);
                // }
                // } catch (KlabException e) {
                // Eclipse.handleException(e);
                // }
            }
        }
        return matches;
    }

    protected IModelObject use(Object o) {

        if (o == null)
            return null;

        IModelObject ret = null;
        if (o instanceof IConcept) {
            // if (filter != null && !filter.match((IConcept) o)) {
            // return null;
            // }
            ret = KLAB.MMANAGER.findModelObject(o.toString());
        }
        return ret;
    }
}
