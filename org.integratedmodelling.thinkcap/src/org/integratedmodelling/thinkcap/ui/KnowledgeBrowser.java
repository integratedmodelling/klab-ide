/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.common.client.referencing.Bookmark;
import org.integratedmodelling.common.client.referencing.BookmarkManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.indexing.KnowledgeIndex;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.OntologyFocusEvent;
import org.integratedmodelling.thinkcap.views.OntologyView;

/**
 * Knowledge search form that allows filtering and browsing.
 */
public class KnowledgeBrowser extends Composite {

    private Text                   text;
    private TreeViewer             treeViewer;
    boolean                        showParents      = false;
    private Filter<IKnowledge>     _filter          = null;

    private boolean                _displayModels   = false;
    private boolean                _displayConcepts = true;
    private Collection<IKnowledge> _preselected     = null;
    private Observables            _queries         = new Observables();

    /**
     * the following three hold the observable being built in the search bar.
     */
    private List<IConcept> currentTraits = new ArrayList<>();
    private IConcept currentObservable = null;
    private String currentConceptText = "";
    
    /*
     * edit position in search bar to start search
     */
    private int searchStartPosition = 0;
    
    private List<Bookmark>         matches          = new ArrayList<>();
    private Combo filters;

    private Image getBookmarkImage(Bookmark element, String bclass) {

        IConcept concept = null;

        if (bclass.equals(BookmarkManager.CLASS_CHECK)) {
            // TODO proper gif
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
        } else if (bclass.equals(BookmarkManager.CLASS_CHECK)) {
            // TODO proper gif
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
        } else {
            switch (element.objectType) {
            case CONCEPT:
                concept = KLAB.KM.getConcept(element.objectName);
                return Eclipse.getConceptImage(concept, NS.isNothing(concept));
            case MODEL:
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, element.agent ? "/icons/agent.gif"
                        : "/icons/model.gif");
            case SUBJECT:
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
            default:
                break;
            }
        }

        return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
    }

    class BookmarkNode extends Bookmark {

        IKnowledge concept;

        BookmarkNode(IModelObject o) {
            super(o);
        }

        BookmarkNode(IKnowledge o) {
            IModelObject obj = KLAB.MMANAGER.findModelObject(o.toString());
            objectName = id = o.toString();
            lineNumber = obj == null ? -1 : obj.getFirstLineNumber();
            file = obj == null ? null : obj.getNamespace().getLocalFile().toString();
            namespace = o.getOntology().getConceptSpace();
            inheritance = new String[] { o.toString() };
            objectType = Type.CONCEPT;
            concept = o;
        }

        private static final long serialVersionUID = -957881730982613445L;

        List<BookmarkNode>        children         = new ArrayList<>();
        BookmarkNode              parent           = null;
    }

    public class KnowledgeBookmarkContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parent) {
            if (parent instanceof List<?>) {
                return ((List<?>) parent).toArray();
            } else if (parent instanceof BookmarkNode) {

                if (((BookmarkNode) parent).objectType == Bookmark.Type.CONCEPT) {

                    // FIXME we don't really need to store children
                    IKnowledge c = ((BookmarkNode) parent).concept;
                    ((BookmarkNode) parent).children.clear();
                    if (c != null) {
                        if (c instanceof IProperty) {
                            if (showParents) {
                                for (IProperty p : ((IProperty) c).getParents()) {
                                    BookmarkNode bp = new BookmarkNode(p);
                                    ((BookmarkNode) parent).children.add(bp);
                                    bp.parent = (BookmarkNode) parent;
                                }
                            } else {
                                for (IProperty p : ((IProperty) c).getChildren()) {
                                    BookmarkNode bp = new BookmarkNode(p);
                                    ((BookmarkNode) parent).children.add(bp);
                                    bp.parent = (BookmarkNode) parent;
                                }
                            }
                        } else {
                            if (showParents) {
                                for (IConcept p : ((IConcept) c).getParents()) {
                                    BookmarkNode bp = new BookmarkNode(p);
                                    ((BookmarkNode) parent).children.add(bp);
                                    bp.parent = (BookmarkNode) parent;
                                }
                            } else {
                                for (IConcept p : ((IConcept) c).getChildren()) {
                                    BookmarkNode bp = new BookmarkNode(p);
                                    ((BookmarkNode) parent).children.add(bp);
                                    bp.parent = (BookmarkNode) parent;
                                }
                            }
                        }
                        return ((BookmarkNode) parent).children.toArray();
                    }
                }
                /*
                 * TODO see what we want to do with other things.
                 */
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {

            if (element instanceof BookmarkNode) {
                if (((BookmarkNode) element).parent == null)
                    return matches;
                return ((BookmarkNode) element).parent;
            }

            return null;
        }

        @Override
        public boolean hasChildren(Object element) {

            if (element instanceof List<?>) {
                return ((List<?>) element).size() > 0;
            } else if (element instanceof BookmarkNode
                    && ((BookmarkNode) element).objectType == Bookmark.Type.CONCEPT) {

                IKnowledge c = ((BookmarkNode) element).concept;
                return nChildren(c) > 0;
            }
            return false;
        }

        private int nChildren(IKnowledge k) {
            return k == null ? 0 : (k instanceof IConcept ? ((IConcept) k).getChildren().size()
                    : ((IProperty) k).getChildren().size());
        }
    }

    class KnowledgeBookmarkLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (columnIndex == 0 && element instanceof BookmarkNode) {
                return getBookmarkImage((Bookmark) element, BookmarkManager.CLASS_BOOKMARK);
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            if (element instanceof BookmarkNode) {
                if (((BookmarkNode) element).concept != null) {
                    switch (columnIndex) {
                    case 0:
                        String localName = ((BookmarkNode) element).concept.getLocalName();
                        String ret = NS.getDisplayName(((BookmarkNode) element).concept);
                        return ret + (ret.equals(localName) ? "" : (" (") + localName + ")");
                    case 1:
                        return ((BookmarkNode) element).concept.getConceptSpace();
                    case 2:
                        return getComment(((BookmarkNode) element).concept);
                    }
                }
                return columnIndex == 0 ? ((BookmarkNode) element).objectName
                        : ((BookmarkNode) element).message;
            }
            return null;
        }

        private String getComment(IKnowledge concept) {
            return concept.getMetadata().getString(IMetadata.DC_COMMENT);
        }
    }

    public KnowledgeBrowser(Composite parent, int style) {
        super(parent, style);
        setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        GridLayout gridLayout = new GridLayout(1, false);
        gridLayout.verticalSpacing = 0;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        setLayout(gridLayout);

        Composite composite = new Composite(this, SWT.NONE);
        composite.setLayout(new GridLayout(7, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        
        Label lblNewLabel_1 = new Label(composite, SWT.NONE);
        lblNewLabel_1.setToolTipText("No current observable set");
        lblNewLabel_1.setImage(ResourceManager.getPluginImage("org.integratedmodelling.thinkcap", "icons/target_gray.png"));
        final DragSource osource = new DragSource(lblNewLabel_1,  DND.DROP_DEFAULT);
        osource.addDragListener(new DragSourceAdapter() {
            @Override
            public void dragSetData(DragSourceEvent event) {
                Object element = buildCurrentObservable();
                if (element instanceof BookmarkNode) {
                    element = new Bookmark((BookmarkNode) element).getSignature();
                }
                event.data = element;
            }
        });
        Label lblNewLabel = new Label(composite, SWT.NONE);
        lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblNewLabel.setText("Search:");

        text = new Text(composite, SWT.BORDER);
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        text.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.stateMask == SWT.CTRL) {
                    switch (e.keyCode) {
                    case 'q':
                        setFilter("Qualities");
                        filters.select(1);
                        break;
                    case 's':
                        setFilter("Subjects");
                        filters.select(2);
                        break;
                    case 'a':
                        setFilter("Attributes");
                        filters.select(3);
                        break;
                    case 'i':
                        setFilter("Identities");
                        filters.select(4);
                        break;
                    case 'r':
                        setFilter("Realms");
                        filters.select(5);
                        break;
                    case 'p':
                        setFilter("Processes");
                        filters.select(6);
                        break;
                    case 'e':
                        setFilter("Events");
                        filters.select(7);
                        break;
                    case 'x':
                        setFilter("nothing");
                        filters.select(0);
                        break;                    }
                } else {
                    String t = text.getText();
                    search(t);
                }
            }
        });

        filters = new Combo(composite, SWT.READ_ONLY);
        filters.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setFilter(filters.getText());
            }
        });

        treeViewer = new TreeViewer(this, SWT.BORDER);
        Tree tree = treeViewer.getTree();
        treeViewer.addDoubleClickListener(new IDoubleClickListener() {
            @Override
            public void doubleClick(DoubleClickEvent event) {

                Object o = ((TreeSelection) (event.getSelection())).getFirstElement();
                final Bookmark bookmark = o instanceof Bookmark ? (Bookmark) o : null;
                if (bookmark == null) {
                    return;
                }
                IConcept c = bookmark.getConcept();
                if (c != null) {
                    if (!Eclipse.openConceptDefinition(c)) {
                        Eclipse.beep();
                    }
                }

//
//                if (o instanceof BookmarkNode)
//                    o = ((BookmarkNode) o).concept;
//
//                if (o != null) {
//                    addToSelection(o);
//                }
            }
        });
        
        final DragSource source = new DragSource(tree, DND.DROP_COPY
                | DND.DROP_MOVE | DND.DROP_DEFAULT);
        source.addDragListener(new DragSourceAdapter() {
            @Override
            public void dragSetData(DragSourceEvent event) {
                Object element = ((TreeSelection) (treeViewer.getSelection()))
                        .getFirstElement();
                if (element instanceof BookmarkNode) {
                    element = new Bookmark((BookmarkNode) element).getSignature();
                }
                event.data = element;
            }
        });
        source.setTransfer(new Transfer[] { /* BookmarkTransfer.getInstance(), */ TextTransfer.getInstance() });
        tree.setHeaderVisible(true);
        tree.setLinesVisible(true);
        tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        treeViewer.setLabelProvider(new KnowledgeBookmarkLabelProvider());
        treeViewer.setContentProvider(new KnowledgeBookmarkContentProvider());

        TreeColumn treeColumn = new TreeColumn(tree, SWT.NONE);
        treeColumn.setWidth(240);
        treeColumn.setText("Name");

        TreeColumn treeColumn_1 = new TreeColumn(tree, SWT.NONE);
        treeColumn_1.setWidth(160);
        treeColumn_1.setText("Namespace");

        TreeColumn treeColumn_2 = new TreeColumn(tree, SWT.NONE);
        treeColumn_2.setWidth(420);
        treeColumn_2.setText("Description");
        
        Menu menu = new Menu(tree);
        tree.setMenu(menu);
        
        MenuItem mntmNewItem = new MenuItem(menu, SWT.NONE);
        mntmNewItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        mntmNewItem.setText("Add to selection");
        
        MenuItem mntmNewItem_1 = new MenuItem(menu, SWT.NONE);
        mntmNewItem_1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {

                Object o = tree.getSelection()[0].getData();

                if (o instanceof BookmarkNode)
                    o = ((BookmarkNode) o).concept;

                if (o != null)
                    showOntology(o);
            }
        });
        mntmNewItem_1.setText("Open in ontology viewer");
        
        MenuItem mntmNewItem_2 = new MenuItem(menu, SWT.NONE);
        mntmNewItem_2.setText("Observe now");

        filters.add("No filter");
        filters.add("Qualities");
        filters.add("Subjects");
        filters.add("Attributes");
        filters.add("Identities");
        filters.add("Realms");
        filters.add("Processes");
        filters.add("Events");

        filters.select(0);

        final Button btnNewButton = new Button(composite, SWT.TOGGLE);
        btnNewButton.setToolTipText("Include concepts in results");
        btnNewButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                _displayConcepts = btnNewButton.getSelection();
                reset();
            }
        });
        btnNewButton.setSelection(true);
        btnNewButton.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/concept.gif"));

        final Button btnNewButton_1 = new Button(composite, SWT.TOGGLE);
        btnNewButton_1.setToolTipText("Include models in results");
        btnNewButton_1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                _displayModels = btnNewButton_1.getSelection();
                reset();
            }
        });
        btnNewButton_1.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/model.gif"));

        Button btnNewButton_2 = new Button(composite, SWT.NONE);
        btnNewButton_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                KLAB.KM.getIndex().reindex();
            }
        });
        btnNewButton_2.setToolTipText("Rebuild knowledge index");
        btnNewButton_2.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/refresh.gif"));

        addControlListener(new ControlListener() {

            @Override
            public void controlResized(ControlEvent e) {
            }

            @Override
            public void controlMoved(ControlEvent e) {
            }
        });
    }

    protected void addToSelection(Object o) {

        /*
         * add to selection, set text + space for next observable
         */
        
        /*
         * if observable set observable
         */
        
        /*
         * if trait add trait
         */
        
        /*
         * append to current text and set current point of edit at
         * end 
         */
        
        
    }

    protected IConcept buildCurrentObservable() {
        // TODO Auto-generated method stub
        return null;
    }

    private void setFilter(String text) {

        switch (text) {
        case "Qualities":
            _preselected = _queries.getAllFundamentalQualities();
            _filter = _queries.getAllFundamentalQualitiesFilter();
            break;
        case "Subjects":
            _preselected = _queries.getAllFundamentalSubjects();
            _filter = _queries.getAllFundamentalSubjectsFilter();
            break;
        case "Attributes":
            _preselected = _queries.getAllFundamentalAttributes();
            _filter = _queries.getAllFundamentalAttributesFilter();
            break;
        case "Identities":
            _preselected = _queries.getAllFundamentalIdentities();
            _filter = _queries.getAllFundamentalIdentitiesFilter();
            break;
        case "Realms":
            _preselected = _queries.getAllFundamentalRealms();
            _filter = _queries.getAllFundamentalRealmsFilter();
            break;
        case "Processes":
            _preselected = _queries.getAllRootProcesses();
            _filter = _queries.getAllRootProcessesFilter();
            break;
        case "Events":
            _preselected = _queries.getAllRootEvents();
            _filter = _queries.getAllRootEventsFilter();
            break;
        default:
            _preselected = null;
            _filter = null;
        }

        reset();

    }

    private void showOntology(Object o) {

        try {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(OntologyView.ID);
            Activator.getDefault().fireEvent(new OntologyFocusEvent(o));
        } catch (PartInitException e) {
            Activator.engine().getMonitor().error(e);
        }

    }

    protected void search(final String text) {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                Set<Object> hash = new HashSet<>();
                IKnowledgeIndex i = KLAB.KM.getIndex();
                if (i != null) {
                    matches = new ArrayList<>();
                    if (text.trim().length() > 0) {
                        try {
                            for (IMetadata m : i.search(text.trim())) {
                                Object o = ((KnowledgeIndex) i).retrieveObject(m);
                                // FIXME there should be no duplications in returned objects.
                                if (o == null || hash.contains(o)) {
                                    continue;
                                }
                                hash.add(o);
                                Bookmark b = use(o);
                                if (b != null) {
                                    matches.add(b);
                                }
                            }
                        } catch (KlabException e) {
                            Activator.engine().getMonitor().error(e);
                        }
                    } else {
                        reset();
                    }
                }
                treeViewer.setInput(matches);
            }
        });
    }

    protected Bookmark use(Object o) {

        if (o == null)
            return null;

        BookmarkNode ret = null;

        /*
         * check against search settings
         */
        if (o instanceof IConcept && _displayConcepts) {
            if (_filter != null && !_filter.match((IConcept) o)) {
                return ret;
            }
            ret = new BookmarkNode((IConcept) o);
        } else if (o instanceof IModel && _displayModels) {
            Set<IKnowledge> cc = getObservableConcepts((IModel) o);
            boolean ok = _filter == null;
            if (_filter != null) {
                ok = false;
                for (IKnowledge c : cc) {
                    if (_filter.match(c)) {
                        ok = true;
                        break;
                    }
                }
            }
            if (ok) {
                ret = new BookmarkNode((IModelObject) o);
            }
        }
        return ret;
    }

    private Set<IKnowledge> getObservableConcepts(IModel o) {
        Set<IKnowledge> ret = new HashSet<>();
        ret.add(o.getObservable().getType());
        for (IConcept t : Traits.getTraits(o.getObservable().getType())) {
            ret.add(t);
        }
//        if (o.getObservable().getInherentType() != null) {
//            ret.add(o.getObservable().getInherentType());
//        }
//        if (o.getObservable().getTraitType() != null) {
//            ret.add(o.getObservable().getTraitType());
//        }
        return ret;
    }

    private void reset() {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                /*
                 * perform current search if we have text, otherwise behave according to
                 * options and selections.
                 */
                if (text.getText() != null && !text.getText().isEmpty()) {
                    search(text.getText());
                } else if (_preselected != null) {
                    treeViewer.setInput(matches = selectCollection(_preselected));
                } else {
                    treeViewer.setInput(null);
                }
            }
        });
    }

    protected List<Bookmark> selectCollection(Collection<IKnowledge> concepts) {
        ArrayList<Bookmark> ret = new ArrayList<>();
        for (IKnowledge c : concepts) {
            ret.add(new BookmarkNode(c));
        }
        return ret;
    }

    public void setFilter(Filter filter) {
        _filter = filter;
    }
}
