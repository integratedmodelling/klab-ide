///*******************************************************************************
// *  Copyright (C) 2007, 2015:
// *  
// *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
// *    - integratedmodelling.org
// *    - any other authors listed in @author annotations
// *
// *    All rights reserved. This file is part of the k.LAB software suite,
// *    meant to enable modular, collaborative, integrated 
// *    development of interoperable data and model components. For
// *    details, see http://integratedmodelling.org.
// *    
// *    This program is free software; you can redistribute it and/or
// *    modify it under the terms of the Affero General Public License 
// *    Version 3 or any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but without any warranty; without even the implied warranty of
// *    merchantability or fitness for a particular purpose.  See the
// *    Affero General Public License for more details.
// *  
// *     You should have received a copy of the Affero General Public License
// *     along with this program; if not, write to the Free Software
// *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// *     The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
//package org.integratedmodelling.thinkcap.ui;
//
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//
//import org.eclipse.swt.dnd.ByteArrayTransfer;
//import org.eclipse.swt.dnd.TransferData;
//import org.integratedmodelling.common.client.referencing.Bookmark;
//
//public class BookmarkTransfer extends ByteArrayTransfer {
//
//	public static String MIME_TYPE = "custom/bookmark";
//
//	final int MIME_TYPE_ID = registerType(MIME_TYPE);
//
//	static BookmarkTransfer _instance = null;
//	
//	@Override
//    protected int[] getTypeIds() {
//		return new int[] { MIME_TYPE_ID };
//	}
//
//	@Override
//    protected String[] getTypeNames() {
//		return new String[] { MIME_TYPE };
//	}
//	
//	public static BookmarkTransfer getInstance() {
//		if (_instance == null) {
//			_instance = new BookmarkTransfer();
//		}
//		return _instance;
//	}
//
//	@Override
//    public void javaToNative(Object object, TransferData transferData) {
////		if (!checkBookmark(object) || !isSupportedType(transferData)) {
////			DND.error(DND.ERROR_INVALID_DATA);
////		}
//		Bookmark Bookmark = (Bookmark) object;
//		byte[] bytes = convertToByteArray(Bookmark);
//		if (bytes != null) {
//			super.javaToNative(bytes, transferData);
//		}
//	}
//
//	@Override
//    public Object nativeToJava(TransferData transferData) {
////		if (!isSupportedType(transferData))
////			return null;
//		byte[] bytes = (byte[]) super.nativeToJava(transferData);
//		try {
//			return bytes == null ? null : restoreFromByteArray(bytes);
//		} catch (Exception e) {
//			return null;
//		}
//	}
//
//	@Override
//    protected boolean validate(Object object) {
//		return object instanceof Bookmark;
//	}
//
//	/* shared methods for converting instances of Bookmark <-> byte[] */
//	static byte[] convertToByteArray(Bookmark type) {
//		try {
//			ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
//			ObjectOutputStream out = new ObjectOutputStream(byteOutStream);
//			out.writeObject(type);
//			return byteOutStream.toByteArray();
//		} catch (IOException e) {
//			return null;
//		}
//	}
//
//	static Bookmark restoreFromByteArray(byte[] bytes) throws Exception {
//		ByteArrayInputStream bis = null;
//		ObjectInputStream in = null;
//		try {
//			bis = new ByteArrayInputStream(bytes);
//			in = new ObjectInputStream(bis);
//			return (Bookmark) in.readObject();
//		} catch (Exception ex) {
//			return null;
//		} finally {
//            if (bis != null) {
//                bis.close();
//            }
//            if (in != null) {
//                in.close();
//            }
//		}
//	}
//
//}
