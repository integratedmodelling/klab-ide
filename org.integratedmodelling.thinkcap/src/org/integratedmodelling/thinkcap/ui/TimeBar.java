/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Progress bar that also serves as Youtube-like time scrubber and quick
 * time setter.
 * 
 * @author ferdinando.villa
 *
 */
public class TimeBar extends Composite {

    public ITemporalExtent    time;
    public long               computed   = 0;
    public long               current    = 0;
    long                      start      = 0;
    long                      step       = 0;
    long                      total      = 0;
    // private Color remainingColor;
    // private Color expiredColor;
    private Color             computedColor;
    Canvas                    canvas;
    Listener                  listener;
    private DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd/MM/YYYY hh:mm");

    /**
     * Callback for 
     * @author Ferd
     *
     */
    public static interface Listener {
        void timeSelected(long time);
    }

    public TimeBar(Composite parent, int style) {
        super(parent, style);
        setLayout(new FillLayout(SWT.HORIZONTAL));
        canvas = new Canvas(this, SWT.NONE);
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                long t = (long) (total * ((double) (e.x) / (double) (canvas.getSize().x)));
                if (t <= computed) {
                    setVisualizedTime(t + start);
                }
            }
        });
        computedColor = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);

        addControlListener(new ControlListener() {

            @Override
            public void controlResized(ControlEvent e) {
                reset();
            }

            @Override
            public void controlMoved(ControlEvent e) {
                // TODO Auto-generated method stub

            }
        });
    }

    public void addListener(Listener listener) {
        this.listener = listener;
    }

    public void setScale(ITemporalExtent time) {
        this.time = time;
        if (time != null && time.getMultiplicity() > 0) {
            this.computed = 0;
            this.start = this.time.getExtent(0).getStart().getMillis();
            this.step = this.time.getExtent(0).getEnd().getMillis() - this.start;
            this.total = time.getEnd().getMillis() - time.getStart().getMillis();
            this.current = start;
        }
        reset();
    }

    public void setComputedTime(long time) {

        if (this.computed <= (time - start)) {
            this.computed = time - start;
            reset();
        }
    }

    public void setVisualizedTime(long time) {
        this.current = time;
        reset();
        if (listener != null) {
            listener.timeSelected(time);
        }
    }

    private void drawTransition(GC gc, long time) {

        gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));

        int at = (int) (getSize().x * ((double) (time - start) / (double) total));
        if (at <= (getSize().x - 4)) {
            gc.fillPolygon(new int[] { at, getSize().y, at, 0, at + 4, 0 });
        } else {
            gc.fillPolygon(new int[] { at - 4, 0, at, 0, at, getSize().y });
        }
    }

    private void reset() {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {

                GC gc = new GC(canvas);
                if (time != null) {

                    int avail = (int) (((double) computed / (double) total) * getSize().x);
                    if (avail > 0) {
                        gc.setBackground(computedColor);
                        gc.fillRectangle(0, 0, avail, getSize().y);
                    }

                    drawTransition(gc, current);

                    if (current == start) {
                        canvas.setToolTipText(time.getMultiplicity() + " steps from "
                                + new DateTime(time.getStart().getMillis()).toString(dateFormat)
                                + " to " + new DateTime(time.getEnd().getMillis()).toString(dateFormat));
                    } else {
                        canvas.setToolTipText(new DateTime(current).toString(dateFormat));
                    }

                } else {
                    gc.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WIDGET_BACKGROUND));
                    gc.fillRectangle(0, 0, getSize().x, getSize().y);

                    canvas.setToolTipText("Context is not temporally explicit");
                }
            }
        });
    }
}
