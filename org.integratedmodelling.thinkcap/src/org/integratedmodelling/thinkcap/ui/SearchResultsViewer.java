package org.integratedmodelling.thinkcap.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.referencing.KnowledgeSearch.SearchType;
import org.integratedmodelling.common.utils.StringUtils;

public class SearchResultsViewer extends Composite {

    private List<Pair<SearchType, List<PaletteItem>>> results;
    private int colSelection = -1, rowSelection = -1;
    private Point parentSize;

    private static Map<SearchType, String> typeDescriptions = new HashMap<>();
    
    static {
        typeDescriptions.put(SearchType.OBSERVATION, "Observations");
        typeDescriptions.put(SearchType.CONCEPT, "Concepts");
        typeDescriptions.put(SearchType.PROCESS, "Processes");
        typeDescriptions.put(SearchType.SUBJECT, "Subjects");
        typeDescriptions.put(SearchType.NAMESPACE, "Namespaces");
        typeDescriptions.put(SearchType.QUALITY, "Qualities");
        typeDescriptions.put(SearchType.TRAIT, "Traits");
        typeDescriptions.put(SearchType.EVENT, "Events");
        typeDescriptions.put(SearchType.ROLE, "Roles");
        typeDescriptions.put(SearchType.MODIFIER, "Concept modifiers");
    }
    
    public SearchResultsViewer(Composite parent, Point size) {
        super(parent, SWT.NONE);
        setLayout(new GridLayout(1, false));
        this.addControlListener(new ControlAdapter() {
            @Override
            public void controlResized(ControlEvent event) {
                // System.out.println("ZIO CAROGNA 0 = " + getSize());
                // System.out.println("ZIO CAROGNA 1 = " + parent.getSize());
                // System.out.println("ZIO CAROGNA 2 = " + parent.getParent().getSize());
                // System.out.println("ZIO CAROGNA 3 = " +
                // parent.getParent().getParent().getSize());
                /*
                 * crazy, but this so far is the only way to not make controls turn small
                 * when the observation bar is explicitly redrawn. I may understand the
                 * actual reason and fix it for good one day, but an entire sunday and
                 * monday morning spent on this sounds like enough this far.
                 */
                parent.setSize(parent.getParent().getSize());
                // parent.pack(true);
            }
        });
        this.parentSize = size;
    }

    public void setResults(List<Pair<SearchType, List<PaletteItem>>> results, Point parentSize) {
        this.results = results;
        this.parentSize = parentSize;
        draw();
    }

    /*
     * Select the shape that corresponds to the pressed key in the layout.
     */
    public PaletteItem select(int arrowKey) {
        return null;
    }
    
    private void draw() {
           
        clear();
                
        /*
         * compute how many columns we can afford. Min we can give to a badge is 280 pixels. Use 
         * parent size as we're supposed to fill a composite and the first time we get here our size is still
         * 0.
         */
        int n = parentSize.x/280;
        if (n < 1) {
            n = 1;
        }

        ((GridLayout)getLayout()).numColumns = n;
        ((GridLayout)getLayout()).makeColumnsEqualWidth = true;
        
        for (Pair<SearchType, List<PaletteItem>> pp : this.results) {
            
            /*
             * label taking n columns
             */
            CLabel titleLabel = new CLabel(this, SWT.NONE);
            titleLabel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NONE));
            titleLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, n, 1));
            titleLabel.setText(typeDescriptions.get(pp.getFirst()));

            /*
             * separator filling n columns
             */
            Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
            GridData gd_label = new GridData(SWT.FILL, SWT.CENTER, true, false, n, 1);
            label.setLayoutData(gd_label);
            
            for (PaletteItem item : pp.getSecond()) {
                
                /*
                 * add item, fill in any holes in 
                 */
                Badge badge = new Badge(this, Badge.MULTILINE | Badge.SELECTABLE, SWT.NONE);
                badge.addTitle(item.getName());
                badge.setBackground(getColor(pp.getFirst()));
                badge.setText(item.getDescription());
                badge.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
                
            }
            
            /*
             * complete row
             */
            for (int i = 0; i < pp.getSecond().size() % n; i ++) {
                new Label(this, SWT.NONE);
            }
            
        }
        
        redraw();
    }

    private Color getColor(SearchType type) {
        switch (type) {
        case CONCEPT:
            return SWTResourceManager.getColor(SWT.COLOR_GRAY);
        case EVENT:
            return SWTResourceManager.getColor(SWT.COLOR_YELLOW);
        case MODIFIER:
            return SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND);
        case NAMESPACE:
            return SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT);
        case OBSERVATION:
            return SWTResourceManager.getColor(SWT.COLOR_GRAY);
        case PROCESS:
            return SWTResourceManager.getColor(SWT.COLOR_RED);
        case QUALITY:
            return SWTResourceManager.getColor(SWT.COLOR_GREEN);
        case ROLE:
            return SWTResourceManager.getColor(SWT.COLOR_GRAY);
        case SUBJECT:
            return SWTResourceManager.getColor(SWT.COLOR_GRAY);
        case TRAIT:
            return SWTResourceManager.getColor(SWT.COLOR_GRAY);
        default:
            break;
        
        }
        return null;
    }

    public void clear() {
        for (Control c : getChildren()) {
            c.dispose();
        }
    }
    
}
