/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.visualization.IViewer.Display;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.model.runtime.Structure;

public class DataButtonPanel extends Composite {

    /*
     * if this changes, the full logics of display selection must change - i.e., 
     * don't change it unless you're prepared to review the code for generality.
     */
    int MAX_DISPLAYS = 2;

    Display[] visualizations = new Display[MAX_DISPLAYS];

    private Label btnPlay;
    private Label separator1;
    private Label btnMap;
    private Label btnPlot;
    private Label separator2;
    private Label btnStructure;
    private Label btnFlow;
    private Label btnProvenance;
    private Label separator3;
    private Label btnPan;
    private Label btnInfo;
    private Label btnCoordinates;
    private Label btnTransect;
    private Label separator4;
    private Label btnExport;

    private boolean haveTime         = false;
    private boolean haveSpace        = true;
    private boolean stateDisplayed   = false;
    private boolean haveStructure    = false;
    private boolean haveFlows        = false;
    private boolean haveProvenance   = false;
    private boolean haveContext      = false;
    private Cursor  cursor           = null;
    private boolean isRunning        = false;
    private boolean isContextualized = false;

    private Interaction currentInteraction = Interaction.PAN;
    // private Visualization currentVisualization = Visualization.MAP;

    private Listener listener;

    public static interface Listener {

        /**
         * User selects displays. A minimum of one display and a maximum of MAX_DISPLAYS may be passed. 
         * @param displays
         */
        void onDisplayChanged(Display[] displays);

        /**
         * User has changed interaction mode. Only one at a time is selected - essentially a radio
         * button.
         * 
         * @param interation
         */
        void onInteractionChanged(Interaction interaction);

        /**
         * User has pressed the Run button to cause the passed status Only happens in temporal contexts that have been
         * initialized.
         */
        void onRunSelected(Run runStatus);

        /**
         * User wants to interrupt a run, only happening when a run is ongoing.
         * 
         */
        void onStopSelected();

        /**
         * Show report.
         */
        void onReportSelected();

        void onAdditionalMapClick();

        void onMapDoubleClick();
    }

    public enum Interaction {
        PAN,
        INFO,
        TRANSECT,
        COORDINATES
    }

    public enum Run {
        RUN,
        STOP
    }

    public DataButtonPanel(Composite parent, int style, Listener listener) {
        super(parent, style);
        this.cursor = new Cursor(parent.getDisplay(), SWT.CURSOR_HAND);
        this.setLayout(new GridLayout(15, false));
        this.listener = listener;
        visualizations[0] = Display.MAP;
        display();
    }

    @Override
    public void dispose() {
        cursor.dispose();
        super.dispose();
    }

    public boolean isVisualized(Display vis) {
        return (visualizations[0] != null && visualizations[0] == vis) ||
                (visualizations[1] != null && visualizations[1] == vis);
    }

    public void display() {

        int bSoFar = 0;
        this.clear();

        if (haveTime) {
            btnPlay = new Label(this, SWT.NONE);

            if (!(isRunning || isContextualized)) {
                btnPlay.setToolTipText("Run temporal transitions");
                btnPlay.setImage(ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", "icons/launch_run.gif"));
                btnPlay.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseUp(MouseEvent e) {
                        setRunning();
                    }
                });
            } else {
                btnPlay.setImage(ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", isContextualized
                                ? "icons/complete_status.gif"
                                : "icons/stop.gif"));
                btnPlay.setToolTipText(isContextualized ? "Contextualization is complete."
                        : "Temporal contextualization ongoing. Click to abort.");

            }
            if (!isContextualized) {
                handCursor(btnPlay);
            }
            bSoFar++;
        }

        if (bSoFar > 0) {

            separator1 = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
            GridData gd_label = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_label.heightHint = 16;
            separator1.setLayoutData(gd_label);
            bSoFar = 0;
        }

        if (haveSpace) {
            btnMap = new Label(this, SWT.NONE);
            btnMap.setToolTipText("Geographical map (click to change view)");
            btnMap.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", isVisualized(Display.MAP)
                            ? "icons/gmaps.png"
                            : "icons/gmaps-dg.png"));
            handCursor(btnMap);
            btnMap.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    if ((e.stateMask & SWT.CTRL) != 0) {
                        listener.onMapDoubleClick();
                    } else {
                        setVisualization(Display.MAP);
                    }

                }

            });
            bSoFar++;
        }

        if (haveTime && (isRunning || isContextualized)) {
            btnPlot = new Label(this, SWT.NONE);
            btnPlot.setToolTipText("Timeseries plot");
            btnPlot.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", isVisualized(Display.TIMEPLOT)
                            ? "icons/Graph-red.png"
                            : "icons/Graph-lg.png"));
            handCursor(btnPlot);
            btnPlot.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setVisualization(Display.TIMEPLOT);
                }
            });
            bSoFar++;
        }

        if (bSoFar > 0) {
            separator2 = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
            GridData gd_label_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_label_1.heightHint = 16;
            separator2.setLayoutData(gd_label_1);
            bSoFar = 0;
        }

        if (haveStructure) {
            btnStructure = new Label(this, SWT.NONE);
            btnStructure.setToolTipText("Structure graph");
            btnStructure
                    .setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", isVisualized(Display.STRUCTURE)
                                    ? "icons/Structure-green.png"
                                    : "icons/Structure-dg.png"));
            btnStructure.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setVisualization(Display.STRUCTURE);
                }
            });
            handCursor(btnStructure);
            bSoFar++;
        }

        if (haveFlows) {
            btnFlow = new Label(this, SWT.NONE);
            btnFlow.setToolTipText("Flow diagram");
            btnFlow.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", isVisualized(Display.FLOWS)
                            ? "icons/Flow-green.png"
                            : "icons/Flow-dg.png"));
            handCursor(btnFlow);
            btnFlow.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setVisualization(Display.FLOWS);
                }
            });
            bSoFar++;
        }

        if (haveProvenance) {
            btnProvenance = new Label(this, SWT.NONE);
            btnProvenance.setToolTipText("Provenance graph");
            btnProvenance
                    .setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", isVisualized(Display.PROVENANCE)
                                    ? "icons/Provenance-green.png"
                                    : "icons/Provenance-dg.png"));
            btnProvenance.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setVisualization(Display.PROVENANCE);
                }
            });
            handCursor(btnProvenance);
            bSoFar++;
        }

        if (bSoFar > 0) {
            separator3 = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
            GridData gd_label_4 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_label_4.heightHint = 16;
            separator3.setLayoutData(gd_label_4);
            bSoFar = 0;

        }

        if (haveSpace) {
            btnPan = new Label(this, SWT.NONE);
            btnPan.setToolTipText("Pan the map");
            btnPan.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", currentInteraction == Interaction.PAN
                            ? "icons/Hand-blue.png"
                            : "icons/Hand-dg.png"));
            handCursor(btnPan);
            btnPan.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setInteraction(Interaction.PAN);
                }
            });
            bSoFar++;

            btnCoordinates = new Label(this, SWT.NONE);
            btnCoordinates.setToolTipText("Show latitude and longitude at click");
            btnCoordinates
                    .setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", currentInteraction == Interaction.COORDINATES
                                    ? "icons/Target-blue.png"
                                    : "icons/Target-dg.png"));
            handCursor(btnPan);
            btnCoordinates.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    setInteraction(Interaction.COORDINATES);
                }
            });
            bSoFar++;

            if (stateDisplayed) {
                btnInfo = new Label(this, SWT.NONE);
                btnInfo.setToolTipText("Show data at click location");
                btnInfo.setImage(ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", currentInteraction == Interaction.INFO
                                ? "icons/Info-blue.png"
                                : "icons/Info-dg.png"));
                btnInfo.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseUp(MouseEvent e) {
                        setInteraction(Interaction.INFO);
                    }
                });
                handCursor(btnInfo);

                bSoFar++;
                if (haveTime && (isRunning || isContextualized)) {
                    btnTransect = new Label(this, SWT.NONE);
                    btnTransect.setToolTipText("Historical value plot");
                    btnTransect
                            .setImage(ResourceManager
                                    .getPluginImage("org.integratedmodelling.thinkcap", currentInteraction == Interaction.TRANSECT
                                            ? "icons/Transect-blue.png"
                                            : "icons/Transect-dg.png"));
                    handCursor(btnTransect);
                    btnTransect.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                            setInteraction(Interaction.TRANSECT);
                        }
                    });

                    bSoFar++;
                }
            }
        }
        if (bSoFar > 0) {
            separator4 = new Label(this, SWT.SEPARATOR | SWT.VERTICAL);
            GridData gd_label_7 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_label_7.heightHint = 16;
            separator4.setLayoutData(gd_label_7);
            bSoFar = 0;
        }

        btnExport = new Label(this, SWT.NONE);
        btnExport.setToolTipText("Show contextualization report");
        btnExport.setImage(ResourceManager.getPluginImage("org.integratedmodelling.thinkcap", "icons/report.png"));
        btnExport.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                listener.onReportSelected();
            }
        });
        handCursor(btnExport);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
//        new Label(this, SWT.NONE);
        // layout(true);
        getParent().layout(true);
    }

    private void clear() {
        for (Control c : this.getChildren()) {
            c.dispose();
        }
        // layout(true);
        getParent().layout(true);
    }

    private void handCursor(final Control c) {
        c.setCursor(cursor);
    }

    private void setInteraction(Interaction interaction) {
        currentInteraction = interaction;
        listener.onInteractionChanged(interaction);
        display();
    }

    private void setVisualization(Display visualization) {

        /*
         * provenance and structure are incompatible.
         */
        if (visualization == Display.PROVENANCE && isVisualized(Display.STRUCTURE)) {
            removeVisualization(Display.STRUCTURE);
        } else if (visualization == Display.STRUCTURE && isVisualized(Display.STRUCTURE)) {
            removeVisualization(Display.PROVENANCE);
        }
        if (!isVisualized(visualization)) {
            visualizations[1] = visualizations[0];
            visualizations[0] = visualization;
        } else if (getCurrentVisualizationsCount() > 1) {
            removeVisualization(visualization);
        } else if (visualization == Display.MAP) {
            // clicked on map icon when shown, which we can use to switch view.
            listener.onAdditionalMapClick();
        }
        listener.onDisplayChanged(visualizations);
        display();
    }

    private void removeVisualization(Display visualization) {

        Display[] nv = new Display[MAX_DISPLAYS];
        int n = 0;
        for (Display v : visualizations) {
            if (v != visualization) {
                nv[n++] = v;
            }
        }
        visualizations = nv;
    }

    private int getCurrentVisualizationsCount() {
        int ret = 0;
        for (Display v : visualizations) {
            if (v != null) {
                ret++;
            }
        }
        ;
        return ret;
    }

    /**
     * Set the status for a context in a particular location, according to whether we have time, space,
     * structure and flows.
     * 
     * @param context
     */
    public void setForContext(IContext context, IScale.Locator... locators) {

        if (context == null || context.isEmpty()) {
            resetToEmpty();
            return;
        }

        IScale scale = context.getSubject().getScale();
        haveTime = scale.getTime() != null && scale.getTime().getMultiplicity() > 1;
        haveSpace = scale.getSpace() != null;
        haveStructure = context.getSubject().getStructure(locators).getRelationships().size() > 0;
        haveProvenance = true;
        haveFlows = ((Structure) (context.getSubject().getStructure(locators))).hasFlows();
        haveContext = true;
        stateDisplayed = false;
        isRunning = context.isRunning();
        isContextualized = context.isFinished();

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                display();
            }
        });

    }

    /**
     * Pass a state being visualized or null to say we're not visualizing any state.
     * @param state
     */
    public void setForState(IState state) {
        stateDisplayed = state != null;
        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                display();
            }
        });
    }

    private void setRunning() {
        listener.onRunSelected(isRunning ? Run.STOP : Run.RUN);
    }

    public void resetToEmpty() {

        haveTime = false;
        haveSpace = true;
        stateDisplayed = false;
        haveStructure = false;
        haveFlows = false;
        haveProvenance = false;
        haveContext = false;

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                display();
            }
        });
    }

    public void runFinished() {

        isContextualized = true;
        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                display();
            }
        });
    }
}
