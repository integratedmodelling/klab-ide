///*******************************************************************************
// *  Copyright (C) 2007, 2015:
// *  
// *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
// *    - integratedmodelling.org
// *    - any other authors listed in @author annotations
// *
// *    All rights reserved. This file is part of the k.LAB software suite,
// *    meant to enable modular, collaborative, integrated 
// *    development of interoperable data and model components. For
// *    details, see http://integratedmodelling.org.
// *    
// *    This program is free software; you can redistribute it and/or
// *    modify it under the terms of the Affero General Public License 
// *    Version 3 or any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but without any warranty; without even the implied warranty of
// *    merchantability or fitness for a particular purpose.  See the
// *    Affero General Public License for more details.
// *  
// *     You should have received a copy of the Affero General Public License
// *     along with this program; if not, write to the Free Software
// *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// *     The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
//package org.integratedmodelling.thinkcap.ui;
//
//import org.eclipse.jface.viewers.DoubleClickEvent;
//import org.eclipse.jface.viewers.IDoubleClickListener;
//import org.eclipse.jface.viewers.LabelProvider;
//import org.eclipse.jface.viewers.Viewer;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.custom.SashForm;
//import org.eclipse.swt.events.ControlEvent;
//import org.eclipse.swt.events.ControlListener;
//import org.eclipse.swt.events.SelectionAdapter;
//import org.eclipse.swt.events.SelectionEvent;
//import org.eclipse.swt.graphics.Image;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.layout.GridLayout;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Control;
//import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.ToolBar;
//import org.eclipse.swt.widgets.ToolItem;
//import org.eclipse.ui.forms.widgets.FormToolkit;
//import org.eclipse.wb.swt.ResourceManager;
//import org.eclipse.wb.swt.SWTResourceManager;
//import org.eclipse.zest.core.viewers.GraphViewer;
//import org.eclipse.zest.core.viewers.IGraphContentProvider;
//import org.eclipse.zest.core.widgets.ZestStyles;
//import org.eclipse.zest.layouts.LayoutAlgorithm;
//import org.eclipse.zest.layouts.algorithms.GridLayoutAlgorithm;
//import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
//import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
//import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
//import org.integratedmodelling.api.modelling.IDirectObservation;
//import org.integratedmodelling.api.modelling.IRelationship;
//import org.integratedmodelling.api.modelling.ISubject;
//import org.integratedmodelling.common.visualization.GraphVisualization;
//import org.integratedmodelling.thinkcap.Activator;
//
///**
// * ZEST graph display widget. This will display both subject structure and 
// * generic graph visualizations. Should make GraphBrowser obsolete.
// * 
// * @author ferdinando.villa
// *
// */
//public class StructureBrowser extends Composite {
//
//    private final static int  LAYOUT_SPRING = 0;
//    private final static int  LAYOUT_GRID   = 1;
//    private final static int  LAYOUT_HTREE  = 2;
//    private final static int  LAYOUT_VTREE  = 3;
//    private final static int  LAYOUT_RADIAL = 4;
//    private final FormToolkit toolkit       = new FormToolkit(Display.getCurrent());
//    private GraphViewer       graphViewer;
//    Object                    subject       = null;
//
//    public class GraphLabelProvider extends LabelProvider {
//
//        @Override
//        public String getText(Object element) {
//            if (element instanceof ISubject) {
//                return ((IDirectObservation) element).getName();
//            } else if (element instanceof GraphVisualization.Node) {
//                return ((GraphVisualization.Node) element).label;
//            } else if (element instanceof GraphVisualization.Edge) {
//                return ((GraphVisualization.Edge) element).label;
//            }
//            /*
//             * TODO relationships for now don't get an ID
//             */
//            return null;
//        }
//
//        @Override
//        public Image getImage(Object element) {
//            if (element instanceof ISubject) {
//                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
//            } else if (element instanceof GraphVisualization.Node) {
//
//                String type = ((GraphVisualization.Node) element).type;
//
//                if (type.equals("accessor")) {
//                    // TODO different accessor types
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/sigma.gif");
//                } else if (type.equals("amodel")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/agent.gif");
//                } else if (type.equals("dmodel")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/model.gif");
//                } else if (type.equals("observer")) {
//                    // TODO different observer types
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/observer.png");
//                } else if (type.equals("datasource")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/datasource.gif");
//                } else if (type.equals("state")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/dataset.gif");
//                } else if (type.equals("networknode")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/server.gif");
//                } else if (type.equals("clientnode")) {
//                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/Computer.png");
//                }
//            }
//            return super.getImage(element);
//        }
//    }
//
//    public class GraphContentProvider implements IGraphContentProvider {
//
//        @Override
//        public void dispose() {
//        }
//
//        @Override
//        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
//        }
//
//        @Override
//        public Object[] getElements(Object inputElement) {
//            if (inputElement instanceof ISubject) {
//                return ((ISubject) inputElement).getStructure().getRelationships().toArray();
//            } else if (inputElement instanceof GraphVisualization) {
//                return ((GraphVisualization) inputElement).edgeSet().toArray();
//            }
//            return null;
//        }
//
//        @Override
//        public Object getSource(Object rel) {
//            if (rel instanceof IRelationship) {
//                return ((IRelationship) rel).getSource();
//            } else if (rel instanceof GraphVisualization.Edge) {
//                return ((GraphVisualization.Edge) rel).getSourceNode();
//            }
//            return null;
//        }
//
//        @Override
//        public Object getDestination(Object rel) {
//            if (rel instanceof IRelationship) {
//                return ((IRelationship) rel).getTarget();
//            } else if (rel instanceof GraphVisualization.Edge) {
//                return ((GraphVisualization.Edge) rel).getTargetNode();
//            }
//            return null;
//        }
//    }
//
//    public StructureBrowser(Composite parent, int style) {
//        super(parent, SWT.BORDER);
//        setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//        setLayout(new GridLayout(1, false));
//
//        // if resized, layout from scratch.
//        this.addControlListener(new ControlListener() {
//
//            @Override
//            public void controlResized(ControlEvent e) {
//                graphViewer.setInput(subject);
//                graphViewer.applyLayout();
//            }
//
//            @Override
//            public void controlMoved(ControlEvent e) {
//            }
//        });
//
//        SashForm sashForm = new SashForm(this, SWT.NONE);
//        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//        toolkit.adapt(sashForm);
//        toolkit.paintBordersFor(sashForm);
//
//        graphViewer = new GraphViewer(sashForm, SWT.NONE);
//        Control control = graphViewer.getControl();
//        control.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
//        graphViewer.setLabelProvider(new GraphLabelProvider());
//        graphViewer.setContentProvider(new GraphContentProvider());
//        graphViewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
//        graphViewer.setNodeStyle(ZestStyles.NODES_NO_LAYOUT_RESIZE | ZestStyles.NODES_FISHEYE);
//
//        graphViewer.addDoubleClickListener(new IDoubleClickListener() {
//
//            @Override
//            public void doubleClick(DoubleClickEvent event) {
//                System.out.println("ZIRGUL " + event.getSelection());
//            }
//        });
//
//        Composite composite = new Composite(sashForm, SWT.NONE);
//        toolkit.adapt(composite);
//        toolkit.paintBordersFor(composite);
//        composite.setLayout(new GridLayout(1, false));
//
//        ToolBar toolBar = new ToolBar(composite, SWT.FLAT | SWT.RIGHT | SWT.VERTICAL);
//        toolBar.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, false, 1, 1));
//        toolBar.setSize(115, 22);
//        toolkit.adapt(toolBar);
//        toolkit.paintBordersFor(toolBar);
//
//        ToolItem toolItem = new ToolItem(toolBar, SWT.RADIO);
//        toolItem.addSelectionListener(new SelectionAdapter() {
//            @Override
//            public void widgetSelected(SelectionEvent e) {
//                setLayoutEngine(LAYOUT_RADIAL);
//            }
//        });
//        toolItem.setImage(ResourceManager
//                .getPluginImage("org.integratedmodelling.thinkcap", "icons/radial_layout.gif"));
//
//        ToolItem toolItem_1 = new ToolItem(toolBar, SWT.RADIO);
//        toolItem_1.addSelectionListener(new SelectionAdapter() {
//            @Override
//            public void widgetSelected(SelectionEvent e) {
//                setLayoutEngine(LAYOUT_GRID);
//            }
//        });
//        toolItem_1.setImage(ResourceManager
//                .getPluginImage("org.integratedmodelling.thinkcap", "icons/grid_layout.gif"));
//
//        ToolItem toolItem_2 = new ToolItem(toolBar, SWT.RADIO);
//        toolItem_2.addSelectionListener(new SelectionAdapter() {
//            @Override
//            public void widgetSelected(SelectionEvent e) {
//                setLayoutEngine(LAYOUT_SPRING);
//            }
//        });
//        toolItem_2.setImage(ResourceManager
//                .getPluginImage("org.integratedmodelling.thinkcap", "icons/spring_layout.gif"));
//
//        ToolItem toolItem_3 = new ToolItem(toolBar, SWT.RADIO);
//        toolItem_3.setSelection(true);
//        toolItem_3.addSelectionListener(new SelectionAdapter() {
//            @Override
//            public void widgetSelected(SelectionEvent e) {
//                setLayoutEngine(LAYOUT_VTREE);
//            }
//        });
//        toolItem_3.setImage(ResourceManager
//                .getPluginImage("org.integratedmodelling.thinkcap", "icons/tree_layout.gif"));
//
//        ToolItem toolItem_4 = new ToolItem(toolBar, SWT.RADIO);
//        toolItem_4.addSelectionListener(new SelectionAdapter() {
//            @Override
//            public void widgetSelected(SelectionEvent e) {
//                setLayoutEngine(LAYOUT_HTREE);
//            }
//        });
//        toolItem_4.setImage(ResourceManager
//                .getPluginImage("org.integratedmodelling.thinkcap", "icons/horizontal_tree_layout.gif"));
//        sashForm.setWeights(new int[] { 433, 0 });
//        toolkit.paintBordersFor(this);
//        setLayoutEngine(LAYOUT_VTREE);
//
//    }
//
//    private void setLayoutEngine(int lcode) {
//
//        LayoutAlgorithm layout = null;
//
//        switch (lcode) {
//        case LAYOUT_SPRING:
//            layout = new SpringLayoutAlgorithm();
//            break;
//        case LAYOUT_VTREE:
//            layout = new TreeLayoutAlgorithm();
//            break;
//        case LAYOUT_GRID:
//            layout = new GridLayoutAlgorithm();
//            break;
//        case LAYOUT_HTREE:
//            layout = new TreeLayoutAlgorithm();
//            break;
//        case LAYOUT_RADIAL:
//            layout = new RadialLayoutAlgorithm();
//            break;
//        }
//
//        graphViewer.setLayoutAlgorithm(layout, true);
//        graphViewer.applyLayout();
//    }
//
//    public void setGraph(Object s) {
//        this.subject = s;
//        show();
//    }
//
//    public void show() {
//        graphViewer.setInput(subject);
//        graphViewer.applyLayout();
//    }
//}
