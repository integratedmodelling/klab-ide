package org.integratedmodelling.thinkcap.ui;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.IHistogram;
import org.integratedmodelling.api.modelling.visualization.IView;
import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.requests.ViewerNotification;
import org.integratedmodelling.common.beans.responses.ValueSummary;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.viewer.WebViewer;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.geospace.SpatialServices;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.model.ShapeRecord;
import org.integratedmodelling.thinkcap.wizards.AddFeatureWizard;

import com.vividsolutions.jts.geom.Coordinate;

/**
 * Eclipse widget which delegates all viewer functions to the default session WebViewer.
 * Also accepts drop actions on maps to make observations according to scale and location.
 * 
 * @author Ferd
 */
public class EclipseViewer extends Composite implements IViewer {

    boolean           isBrowser;
    Browser           sbrowser;
    Composite         gbrowser;
    Viewport          viewport = new Viewport(800, 800);
    private SashForm  sashForm;
    WebViewer         viewer;
    private Composite searchResults;
    private Composite browserForm;
    protected AddFeatureWizard featureWizard;
	private double SCALE_FACTOR;

    /**
     * @param parent
     *            SWT widget
     * @param flags
     *            DRAW_MODE etc; 0 for normal operation
     * @param style
     *            SWT style flags
     */
    public EclipseViewer(Composite parent, int flags, int style) {
        super(parent, style);
        GridLayout gridLayout = new GridLayout(1, false);
        gridLayout.horizontalSpacing = 0;
        gridLayout.verticalSpacing = 0;
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        setLayout(gridLayout);

        sashForm = new SashForm(this, SWT.SMOOTH | SWT.VERTICAL);
        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        // sbrowser = new Browser(sashForm, SWT.NONE);
        browserForm = new Composite(sashForm, SWT.NONE);
        GridLayout gl_browserForm = new GridLayout(1, false);
        gl_browserForm.verticalSpacing = 0;
        gl_browserForm.marginWidth = 0;
        gl_browserForm.marginHeight = 0;
        gl_browserForm.horizontalSpacing = 0;
        browserForm.setLayout(gl_browserForm);
        searchResults = new Composite(browserForm, SWT.NONE);
        // searchResults.setLayout(new FillLayout());
        GridData gd_searchResults = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_searchResults.exclude = true;
        searchResults.setLayoutData(gd_searchResults);
        searchResults.setVisible(false);
        sbrowser = new Browser(browserForm, SWT.NONE);
        sbrowser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        gbrowser = new Composite(sashForm, SWT.NONE);
        sashForm.setWeights(new int[] { 250, 60 });
        isBrowser = true;
        
        this.SCALE_FACTOR = Double.parseDouble(System.getProperty("org.eclipse.swt.internal.deviceZoom", "100"));
        
        this.addControlListener(new ControlAdapter() {
            @Override
            public void controlResized(ControlEvent event) {
//                 System.out.println("ZIO CAROGNA 0 = " + getSize());
//                 System.out.println("ZIO CAROGNA 1 = " + parent.getSize());
//                 System.out.println("ZIO CAROGNA 2 = " + parent.getParent().getSize());
//                 System.out.println("ZIO CAROGNA 3 = " +
//                 parent.getParent().getParent().getSize());
                /*
                 * crazy, but this so far is the only way to not make controls turn small
                 * when the observation bar is explicitly redrawn. I may understand the
                 * actual reason and fix it for good one day, but an entire sunday and
                 * monday morning spent on this sounds like enough this far.
                 */
                // TODO boh - seems OK now, cross fingers  
//                parent.setSize(parent.getParent().getSize());
                // parent.pack(true);
            }
        });
        // map looks cool at startup
        sashForm.setMaximizedControl(browserForm);

        sbrowser.addControlListener(new ControlListener() {

            @Override
            public void controlResized(ControlEvent e) {
                
            	viewport.setSize(sbrowser.getSize().x, sbrowser.getSize().y);
                
                /*
                 * TODO needs to take DPI scaling into account, or switch to DPI-aware 
                 * openlayers if that's possible.
                 */
                sbrowser.execute("document.getElementById('geomap').style.width= "
                        + (int)(sbrowser.getSize().x * (SCALE_FACTOR/100.0)) + ";");
                sbrowser.execute("document.getElementById('geomap').style.height= "
                        + sbrowser.getSize().y * (int)(SCALE_FACTOR/100.0) + ";");
            }

            @Override
            public void controlMoved(ControlEvent e) {
            }
        });

        setWaiting();

        loadSession(Environment.get().getSession());
        viewport.setSize(sbrowser.getSize().x, sbrowser.getSize().y);
    }

    /**
     * Switch the view to the search results, overlaying the browser, or the other way
     * around. In all circumstances return the search result composite, which has a
     * 1-column grid layout.
     * 
     * @param show
     * @param point
     */
    public Composite showSearchResults(boolean show, Point point) {
        searchResults.setVisible(show);
        sbrowser.setVisible(!show);
        ((GridData) searchResults.getLayoutData()).exclude = !show;
        ((GridData) sbrowser.getLayoutData()).exclude = show;
        browserForm.getParent().pack();
        return searchResults;
    }

    private void setWaiting() {

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    sbrowser.setUrl(FileLocator
                            .toFileURL(Activator.getDefault().getBundle()
                                    .getEntry("/html/wait.html"))
                            .toString());
                } catch (IOException e1) {
                    // f'ock
                }
            }
        });

    }

    /**
     * React to the establishment of an engine session by connecting to that engine's
     * dataviewer. If the session is null, show an explanatory page.
     * 
     * @param session
     */
    public void loadSession(ClientSession session) {
        if (session != null) {

            session.setViewer(viewer = new WebViewer(session) {

                @Override
                public void acceptNotification(ViewerNotification notification) {
                    switch (notification.getCommand()) {
                    case "on-click":
                        handleLocationClick(((Number) notification.getArguments().get(0))
                                .doubleValue(), ((Number) notification.getArguments()
                                        .get(1)).doubleValue());
                        break;
                    case "shape-added":
                        String shape = notification.getArguments().get(0).toString();
                        if (shape != null) {
                            shape.replaceAll("\\+", ",");
                        }
                        recordShape(shape);
                        break;
                        
                    case "tool-drop":

                        String path = notification.getArguments().get(0).toString();
                        double lat = ((Number) notification.getArguments().get(1)).doubleValue();
                        double lon = ((Number) notification.getArguments().get(2)).doubleValue();
                        double minx = ((Number) notification.getArguments().get(3)).doubleValue();
                        double maxx = ((Number) notification.getArguments().get(4)).doubleValue();
                        double miny = ((Number) notification.getArguments().get(5)).doubleValue();
                        double maxy = ((Number) notification.getArguments().get(6)).doubleValue();

                        String keys = notification.getArguments().get(7).toString();
                        PaletteItem item = PaletteManager.get().getItem(path);

                        boolean done = false;
                        if (keys.contains("ctrl")) {
                            done = addShape(item.getModelObject(), lat, lon);
                        }
                        if (!done) {
                            /*
                             * set areas of preference and extent if we have no context
                             */
                            Environment.get().getSpatialForcing().setRegionOfInterest(minx, maxx, miny, maxy);
                            Environment.get().getSpatialForcing().setLocationOfInterest(lat, lon);

                            Activator.engine().handleObservationAction(item, keys.contains("ctrl"));
                        }
                        
                        break;

                    case "object-drop":

                        path = notification.getArguments().get(0).toString();
                        lat = ((Number) notification.getArguments().get(1)).doubleValue();
                        lon = ((Number) notification.getArguments().get(2)).doubleValue();
                        minx = ((Number) notification.getArguments().get(3)).doubleValue();
                        maxx = ((Number) notification.getArguments().get(4)).doubleValue();
                        miny = ((Number) notification.getArguments().get(5)).doubleValue();
                        maxy = ((Number) notification.getArguments().get(6)).doubleValue();

                        keys = notification.getArguments().get(7).toString();


                        done = false;
                        if (keys.contains("ctrl")) {
                            done = addShape(path, lat, lon);
                        }
                        if (!done) {
                            /*
                             * set areas of preference and extent if we have no context
                             */
                            Environment.get().getSpatialForcing().setRegionOfInterest(minx, maxx, miny, maxy);
                            Environment.get().getSpatialForcing().setLocationOfInterest(lat, lon);

                            Activator.engine().handleObservationAction(path, keys.contains("ctrl"));
                        }
                        
                        break;

                    case "on-view-change":

                        minx = ((Number) notification.getArguments().get(0)).doubleValue();
                        maxx = ((Number) notification.getArguments().get(1)).doubleValue();
                        miny = ((Number) notification.getArguments().get(2)).doubleValue();
                        maxy = ((Number) notification.getArguments().get(3)).doubleValue();

                        /*
                         * set areas of preference and extent if we have no context
                         */
                        Environment.get().getSpatialForcing().setRegionOfInterest(minx, maxx, miny, maxy);

                        break;
                    default:
                        super.acceptNotification(notification);
                        break;
                    }
                }
            });
            String url = KLAB.ENGINE.getUrl() + "/dataviewer/index.html?sessionId="
                    + session.getId();

            Coordinate location = SpatialServices.geolocate();
            if (location == null) {
                url += "&lat=" + WebViewer.DEFAULT_LAT + "&lon=" + WebViewer.DEFAULT_LON;
            } else {
                url += "&lat=" + location.y + "&lon=" + location.x;
            }

            final boolean useExternal = KLAB.CONFIG.getProperties()
                    .getProperty("klab.browser.external", "false").equals("true");
            final String zurl = url;
            org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!useExternal) {
                        sbrowser.setJavascriptEnabled(true);
                        sbrowser.setUrl(zurl);
                    } else {
                        viewer.start();
                    }
                }
            });

        } else {
            /*
             * TODO load some error page saying what the problem is and that it will be
             * resolved as soon as a session is established.
             */
            setWaiting();
        }
    }

    /*
     * -----------------------------------------------------------------------------------
     * --- handlers for user events, sent back through WebViewer.
     * -----------------------------------------------------------------------------------
     * ---
     */

    protected boolean addShape(String dropped, double lat, double lon) {

        class WDialog extends WizardDialog {
            public WDialog(Shell parentShell, IWizard newWizard) {
                super(parentShell, newWizard);
                // skip the modal flag
                setShellStyle(SWT.CLOSE | SWT.MAX | SWT.TITLE | SWT.BORDER
                        | SWT.RESIZE | getDefaultOrientation());
            }
        }

        final IConcept observable = KLAB.KM.getConcept(dropped);
        if (observable == null || !(NS.isObservable(observable) || NS.isRole(observable))) {
            return false;
        }

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                featureWizard = new AddFeatureWizard(EclipseViewer.this, observable, lat, lon);
                WizardDialog dialog = new WDialog(gbrowser.getShell(), featureWizard);
                dialog.setBlockOnOpen(true);
                dialog.create();
                if (dialog.open() == Window.OK) {
                    String shape = featureWizard.getShape();
                    if (shape != null) {
                        /*
                         * Create shape and add to ctx
                         */
                        // TODO if concept was a role, find the most suitable object; check compat with region
                        ShapeRecord record = new ShapeRecord(shape, observable);
                        record.setName(featureWizard.getName());
                        // TODO if concept was a role, must assign the role to the new object
                        Activator.engine().handleObservationAction(record.makeSubjectObserver(), true);
                    }
                }
                
                viewer.setContextEditMode(false);
                featureWizard = null;
                
            }
        });
        return true;
    }

    /*
     * do-nothing version: override for functions. Shape WKT will be in "shape" argument.
     */
    protected void recordShape(String shape) {
    }

    /*
     * do-nothing version: override for functions.
     */
    protected void handleLocationClick(double lat, double lon) {
    }

    /*
     * -----------------------------------------------------------------------------------
     * --- delegate methods for IViewer below
     * -----------------------------------------------------------------------------------
     * ---
     */

    @Override
    public void show(IContext context) {
        viewer.show(context);
    }

    @Override
    public void show(IObservation observation) {
        viewer.show(observation);
    }

    @Override
    public void focus(IDirectObservation observation) {
        viewer.focus(observation);
    }

    @Override
    public void hide(IObservation observation) {
        viewer.hide(observation);
    }

    @Override
    public boolean equals(Object obj) {
        return viewer.equals(obj);
    }

    @Override
    public ISession getSession() {
        return viewer.getSession();
    }

    @Override
    public Display getCurrentDisplay() {
        return viewer.getCurrentDisplay();
    }

    @Override
    public Display setDisplay(Display display) {
        return viewer.setDisplay(display);
    }

    @Override
    public void cycleView() {
        viewer.cycleView();
    }

    @Override
    public IState getVisibleSpatialState() {
        return viewer.getVisibleSpatialState();
    }

    @Override
    public void setOpacity(IObservation observation, double opacity) {
        viewer.setOpacity(observation, opacity);
    }

    @Override
    public boolean setContextEditMode(boolean on) {
        return viewer.setContextEditMode(on);
    }

    @Override
    public void clearAllStates() {
        viewer.clearAllStates();
    }

    @Override
    public IView getView(Display display) {
        return viewer.getView(display);
    }

    @Override
    public IView getView() {
        return viewer.getView();
    }

    @Override
    public String getLabel(IObservation observation) {
        return viewer.getLabel(observation);
    }

    @Override
    public void locate(Locator... locators) {
        viewer.locate(locators);
    }

    @Override
    public void resetView() {
        viewer.resetView();
    }

    @Override
    public List<IState> getVisibleSpatialStates() {
        return viewer.getVisibleSpatialStates();
    }

    @Override
    public void clear() {
        if (viewer != null) {
            viewer.clear();
        }
    }

    @Override
    public Pair<IColormap, IHistogram> getStateSummary(IState state) {
        return viewer.getStateSummary(state);
    }

    public ValueSummary getValueDescriptor(IState state, long offset) {
        return viewer.getValueSummary(state, offset);
    }

    @Override
    public void setContextEditMode(DrawMode mode) {
        viewer.setContextEditMode(mode);
    }

    @Override
    public void show(IObservation observation, Display display) {
        viewer.show(observation, display);
    }
    
    public void showNames(boolean b) {
        viewer.showNames(b);
    }

}
