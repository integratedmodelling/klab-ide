/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.mylyn.internal.commons.notifications.ui.popup.NotificationPopup;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.integratedmodelling.thinkcap.Eclipse;

public class Notification extends NotificationPopup {

    private String title;
    private String message;
    private String link;

    @SuppressWarnings("restriction")
    public Notification(Shell display) {
        super(display);
    }

    // TODO add message
    @SuppressWarnings("restriction")
    public Notification(String title, String message, String link) {
        super(Eclipse.getShell());
        this.title = title;
        this.message = message;
        this.link = link;
    }

    @Override
    protected void createContentArea(Composite composite) {

        composite.setLayout(new GridLayout(1, true));

        /*
         * TODO needs title, date and text. Also should be able to use a list of announcements
         * and allow paging between them.
         */

        if (this.link != null) {
            Link linkGoogleNews = new Link(composite, 0);
            String googlenewsLink = "<a href=\"" + this.link + "\">Learn more...</a>";
            linkGoogleNews.setText(googlenewsLink);
            linkGoogleNews.setSize(400, 100);

            linkGoogleNews.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    try {
                        PlatformUI.getWorkbench().getBrowserSupport()
                                .getExternalBrowser().openURL(new URL(link));
                    } catch (PartInitException e1) {
                        e1.printStackTrace();
                    } catch (MalformedURLException e1) {
                        e1.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected String getPopupShellTitle() {
        return title;
    }

    @Override
    protected Image getPopupShellImage(int maximumHeight) {
        return null;
    }
}
