/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.auth.KlabCertificate;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.launcher.EngineLauncher;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.kim.ui.contentassist.KimProposalProvider;
import org.integratedmodelling.kim.ui.elink.FunctionParameterHelper;
import org.integratedmodelling.kim.ui.elink.KimLinkDetector;
import org.integratedmodelling.kim.ui.elink.KimLinkDetector.LinkOpenListener;
import org.integratedmodelling.thinkcap.dialogs.CertificateDialog;
import org.integratedmodelling.thinkcap.dialogs.EngineDialog;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ModelModifiedEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

    public static final String                 PLUGIN_ID                        = "org.integratedmodelling.thinkcap"; //$NON-NLS-1$

    /*
     * preferences for storage
     */
    public static final String                 THINKLAB_EXPORT_PATH_PREFERENCE  = "thinklab.install.path";
    public static final String                 WEBAPPS_INSTALL_PATH_PREFERENCE  = "webapps.install.path";
    public static final String                 THINKLAB_JRE_PATH_PREFERENCE     = "thinklab.jre.path";
    public static final String                 THINKLAB_LICENSE_TEXT            = "thinklab.license.text";
    public static final String                 THINKLAB_MEMORY_LIMIT_PREFERENCE = "thinklab.memory.limit";

    public static final String                 DEFAULT_THINKLAB_MEMORY          = "2048M";

    // The shared instance
    private static Activator                   plugin;

    private EclipseEngine                      engine;
    private Throwable                          error;
    private IUser                              user;
    private boolean                            updateAvailable                  = false;
    private volatile Boolean                   guiActive                        = false;
    private boolean                            initializationFinished;
    volatile ArrayList<IThinklabEventListener> listeners                        = new ArrayList<>();
    AtomicBoolean                              refreshing                       = new AtomicBoolean(false);
    KlabCertificate                            certificate;

    private EngineLauncher                     engineLauncher;

    /**
     * The constructor
     */
    public Activator() {
    }

    public IEclipsePreferences getPreferences() {
        return InstanceScope.INSTANCE.getNode(PLUGIN_ID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework. BundleContext
     * )
     */
    @Override
    public void start(BundleContext context) throws Exception {

        KLAB.CONFIG = new Configuration(true);

        this.certificate = getCertificate();
        if (certificate.isValid()) {
            engine = new EclipseEngine(certificate.getFile());
            engine.performLocalBoot();
            this.addThinklabEventListener(engine);
        } else {
            Eclipse.error(certificate.getCause());
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    MessageDialog dialog = new MessageDialog(null, "Certificate error", null, certificate
                            .getCause(), MessageDialog.QUESTION, new String[] { "Exit", "Continue" }, 0);
                    if (dialog.open() == 0) {
                        System.exit(255);
                    }
                }
            });
        }

        super.start(context);
        plugin = this;

        // apparently, you never know
        getPreferences().sync();

        /*
         * establish if we have a new version of the plugin; if so, force update of the
         * server distribution.
         */
        boolean updateServer = false;
        try {
            File thinkcapv = new File(KLAB.CONFIG.getDataPath() + File.separator + ".tcversion");
            Version lastversion = new Version(0, 0, 0);
            if (thinkcapv.exists()) {
                String s = FileUtils.readFileToString(thinkcapv);
                if (!s.contains("qualifier")) {
                    // not sure why the first time there is "qualifier" left in
                    // it.
                    lastversion = Version.parseVersion(s);
                }
            }

            // if (this.getBundle().getVersion().compareTo(lastversion) > 0) {
            //
            // /*
            // * update
            // */
            // updateServer = true;
            // if (!this.getBundle().getVersion().toString().contains("qualifier")) {
            // FileUtils.writeStringToFile(thinkcapv,
            // this.getBundle().getVersion().toString());
            // }
            // }
            //
            // if (!this.getBundle().getVersion().toString().contains("qualifier")
            // && NetUtilities.urlResponds(UPDATE_SITE)) {
            // /*
            // * check if the update site does not have a plugin with our
            // * version. If not, we take it as a sign that an update is
            // * available, given that the site responds.
            // */
            // String purl = UPDATE_SITE + "/plugins/org.integratedmodelling.thinkcap_" +
            // this.getBundle().getVersion()
            // + ".jar";
            // updateAvailable = !NetUtilities.urlResponds(purl);
            // }

        } catch (Throwable e) {
            error = e;
        }

        /**
         * ensure we have an engine running.
         */
        this.engineLauncher = new EngineLauncher(certificate.getFile(), KLAB.CONFIG
                .getDataPath("engine"), engine.getClientSignature()) {

            @Override
            protected void error(String error) {
                Eclipse.error(error);
                Display.getDefault().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        MessageDialog dialog = new MessageDialog(null, "Engine error", null, error, MessageDialog.QUESTION, new String[] {
                                "Exit",
                                "Continue" }, 0);
                        if (dialog.open() == 0) {
                            System.exit(255);
                        }
                    }
                });
            }

        };

        /**
         * Ensure Mac users know they need to provide a route for multicasting to self.
         */
        Eclipse.macOsAlert();

        if (!EngineDialog.initializeEngine(engineLauncher, certificate.getGroups())) {
            System.exit(0);
        }

        /*
         * set up cross-references for concepts and other model objects.
         */
        KimLinkDetector.setListener(new LinkOpenListener() {

            @Override
            public void openLink(String text) {

                if (!Eclipse.openRefDefinition(text)) {
                    Eclipse.beep();
                }
            }
        });

        KimProposalProvider.setFunctionParameterHelper(new FunctionParameterHelper() {

            @Override
            public Collection<Collection<String>> getParameterProposals(String function) {
                List<Collection<String>> ret = new ArrayList<>();
                if (engine() != null) {
                    IPrototype prototype = engine().getFunctionPrototype(function);
                    if (prototype != null) {
                        for (String arg : prototype.getArgumentNames()) {
                            ret.add(documentParameter(prototype, arg, false));
                        }
                        for (String opt : prototype.getOptionNames()) {
                            ret.add(documentParameter(prototype, opt, true));
                        }
                    }
                }
                return ret;
            }

            private Collection<String> documentParameter(IPrototype prototype, String parameter, boolean option) {
                List<String> ret = new ArrayList<>();
                ret.add(parameter);
                String desc = prototype.getArgumentDescription(parameter);
                String type = prototype.getArgumentTypeDescription(parameter);
                ret.add(parameter + " (" + type + "): " + ((desc == null || desc.isEmpty()) ? "[no description given]" : desc));
                return ret;
            }
        });

        //
        final boolean upserv = updateServer;

        Job init = new Job("Booting k.LAB") {

            @Override
            public IStatus run(IProgressMonitor monitor) {

                if (error == null) {

                    synchronized (guiActive) {
                        guiActive = true;
                    }

                    try {
                        engine.start();
                        user = engine.getUser();

                        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
                            Eclipse.updateMarkersForNamespace(ns, Eclipse.getNamespaceIFile(ns.getId()));
                        }

                    } catch (Exception e) {
                        error = e;
                    }
                }

                if (error != null) {
                    Eclipse.alert("Error: "
                            + (error instanceof KlabException || error instanceof KlabRuntimeException
                                    ? error.getMessage() : MiscUtilities.throwableToString(error)));
                }

                // try {
                //
                //// Eclipse.setRepositories(UPDATE_SITE);
                // // fireEvent(new AuthenticationEvent(user));
                // // fireEvent(new ModelModifiedEvent(null));
                //
                // // client.startNetworkPolling();
                // //
                // // if (!client.startEngine(upserv)) {
                // // throw new KlabRuntimeException("Cannot upgrade the engine
                // // because
                // // it failed to stop
                // // before update. Please terminate the engine before
                // // attempting
                // // boot.");
                // // }
                // //
                // // if (client.connectToThinklabNetwork()) {
                // // Activator.client().fireEvent(new
                // // AuthenticationEvent(user));
                // // int nremoteprojs = client.synchronizeNetworkAssets();
                // // if (nremoteprojs > 0) {
                // // client.info("loaded " + nremoteprojs + " projects from
                // // the
                // // network");
                // // client.reindexKnowledge();
                // // }
                // // }
                //
                // } catch (Exception e) {
                // // client.error(e);
                // return Status.CANCEL_STATUS;
                // }

                /*
                 * after first setup is done, register a change listener for whatever the
                 * user will do.
                 */
                ResourcesPlugin.getWorkspace()
                        .addResourceChangeListener(new ResourceChangeReporter(), IResourceChangeEvent.PRE_CLOSE
                                | IResourceChangeEvent.PRE_DELETE
                                | IResourceChangeEvent.POST_CHANGE);

                initializationFinished = true;

                Eclipse.displayNotifications(user);

                return Status.OK_STATUS;
            }
        };

        init.schedule();
    }

    public boolean isInitializationFinished() {
        return initializationFinished;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework. BundleContext
     * )
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
        plugin = null;
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }

    /**
     * Get the client.
     * 
     * @return the client engine
     */
    public static EclipseEngine engine() {
        return plugin == null ? null : plugin.engine;
    }

    /**
     * Returns an image descriptor for the image file at the given plug-in relative path
     *
     * @param path the path
     * @return the image descriptor
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }

    public static void log(IStatus status) {
        plugin.getLog().log(status);
    }

    public boolean isGUIActive() {
        synchronized (guiActive) {
            return guiActive;
        }
    }

    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    /**
     * Ensure a certificate exists and if so, that is valid and current. If no certificate
     * is found, import one from the dialog and return it.
     * 
     * @return a valid certificate or null if user pressed cancel.
     */
    public KlabCertificate getCertificate() {

        if (certificate == null) {

            File certificate = KlabCertificate.getCertificateFile();
            if (!certificate.exists()) {
                CertificateDialog cdialog = new CertificateDialog(Eclipse.getShell());
                if (cdialog.open() == Dialog.OK) {
                    this.certificate = cdialog.getCertificate();
                } else {
                    System.exit(0);
                }
            } else {
                this.certificate = new KlabCertificate(certificate);
            }

        }

        return this.certificate;
    }

    public void addThinklabEventListener(final IThinklabEventListener listener) {
        Job job = new Job("") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                synchronized (listeners) {
                    listeners.add(listener);
                }
                return Status.OK_STATUS;
            }
        };
        job.setUser(false);
        job.schedule();
    }

    /**
     * 
     * @param listener
     */
    public synchronized void removeThinklabEventListener(final IThinklabEventListener listener) {

        Job job = new Job("") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                synchronized (listeners) {
                    listeners.remove(listener);
                }
                return Status.OK_STATUS;
            }
        };
        job.setUser(false);
        job.schedule();
    }

    /**
     * Send an event to all listening views.
     * 
     * @param e
     */
    public synchronized void fireEvent(final ThinklabEvent e) {

        if (!(e instanceof EngineEvent && ((EngineEvent) e).type == EngineEvent.HEARTBEAT)) {
            System.out.println("# " + e);
        }

        Job job = new Job("") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                synchronized (listeners) {

                    for (IThinklabEventListener l : listeners) {
                        l.handleThinklabEvent(e);
                    }
                }
                return Status.OK_STATUS;
            }
        };
        job.setUser(false);
        job.schedule();
    }

    /**
     * Should only be used after project-scope events; for file-scoped changes, we should
     * just let the standard filesystem listener do its work.
     * 
     * @param full
     */
    public synchronized void reloadProjects(boolean full) {

        refreshing.set(true);
        try {
            List<INamespace> namespaces = KLAB.PMANAGER.load(full, KLAB.MFACTORY.getRootParsingContext());
            for (INamespace namespace : namespaces) {
                if (namespace == null) {
                    // in error
                    continue;
                }
                IFile file = Eclipse.getNamespaceIFile(namespace.getId());
                if (file.exists()) {
                    Eclipse.updateMarkersForNamespace(namespace, file);
                }
            }
            Activator.getDefault().fireEvent(new ModelModifiedEvent(namespaces));
        } catch (Exception e) {
            engine.getMonitor().error(e);
        } finally {
            refreshing.set(false);
        }
    }

    public boolean isRefreshing() {
        return refreshing.get();
    }

    public EngineLauncher getEngineLauncher() {
        return engineLauncher;
    }
}
