/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.navigator.IDescriptionProvider;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.interfaces.IThinklabConcept;
import org.integratedmodelling.thinkcap.interfaces.IThinklabDefinition;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModel;
import org.integratedmodelling.thinkcap.interfaces.IThinklabNamespace;
import org.integratedmodelling.thinkcap.interfaces.IThinklabOntology;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabScript;
import org.integratedmodelling.thinkcap.interfaces.IThinklabSubjectGenerator;
import org.integratedmodelling.thinkcap.interfaces.IThinklabUserKnowledge;

public class TLViewLabelProvider extends LabelProvider implements ILabelProvider, IDescriptionProvider {

    WorkbenchLabelProvider delegate = new WorkbenchLabelProvider();

    @Override
    public Image getImage(Object element) {

        /*
         * TODO stop.gif inserted where we need a new image
         */
        if (element instanceof IThinklabProject) {
            if (((IThinklabProject) element).getTLProject().hasErrors())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "/icons/k-lab-icon-16.gif"), ResourceManager
                                .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"), SWTResourceManager.BOTTOM_LEFT);

            if (((IThinklabProject) element).getTLProject().isTainted())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "/icons/k-lab-icon-16.gif"), ResourceManager
                                .getPluginImage(Activator.PLUGIN_ID, "/icons/default_tsk.png"), SWTResourceManager.TOP_LEFT);
            
            if (((IThinklabProject) element).getTLProject().hasWarnings())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "/icons/k-lab-icon-16.gif"), ResourceManager
                                .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/warning_co.gif"), SWTResourceManager.BOTTOM_LEFT);
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/k-lab-icon-16.gif");
            

        } else if (element instanceof IThinklabUserKnowledge) {

            INamespace ns = ((IThinklabNamespace) element).getNamespace();
            String base = "icons/icon_o.png";

            if (ns != null && ns.hasErrors())
                return ResourceManager
                        .decorateImage(ResourceManager
                                .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                        .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"), SWTResourceManager.BOTTOM_LEFT);

            if (ns != null && ns.isTainted())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                .getPluginImage(Activator.PLUGIN_ID, "/icons/default_tsk.png"), SWTResourceManager.TOP_LEFT);

            if (ns != null && ns.hasWarnings())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/warning_co.gif"), SWTResourceManager.BOTTOM_LEFT);


            
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, base);

        } else if (element instanceof IThinklabNamespace) {

            INamespace ns = ((IThinklabNamespace) element).getNamespace();

            if (ns == null) {
                return null;
            }

            String base = ns.isInactive() ? "/icons/project_inactive.gif" : (ns.isPrivate() ? "icons/project_private.gif" : "/icons/project.gif");
            if (ns.isScenario()) {
                base = ns.isInactive() ? "/icons/scenario_inactive.gif" : "/icons/scenario.gif";
            } else if (ns.getDomain() != null) {
                base = "/icons/icon_d.jpg";
            }

            if (ns != null && ns.hasErrors())
                return ResourceManager
                        .decorateImage(ResourceManager
                                .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                        .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"), SWTResourceManager.BOTTOM_LEFT);

            if (ns != null && ns.isTainted())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                .getPluginImage(Activator.PLUGIN_ID, "/icons/default_tsk.png"), SWTResourceManager.TOP_LEFT);

            
            if (ns != null && ns.hasWarnings())
                return ResourceManager.decorateImage(ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, base), ResourceManager
                                .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/warning_co.gif"), SWTResourceManager.BOTTOM_LEFT);

            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, base);

        } else if (element instanceof IThinklabModel) {
            
            IThinklabModel model = ((IThinklabModel) element);

            Image ret = null;
            if (model.getModel().isInstantiator()) {

                if (model.getModel().isInactive()) {
                    ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/ivoid16.png");
                } else {
                    if (NS.isRelationship(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/irelationship16.png");
                    } else if (NS.isEvent(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/ievent16.png");
                    } else {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/isubject16.png");
                    }
                }
            } else {
                if (model.getModel().isInactive()) {
                    ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/cvoid16.png");
                } else {
                    if (NS.isQuality(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/cquality16.png");
                    } else if (NS.isProcess(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/cprocess16.png");
                    } else if (NS.isRelationship(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/crelationship16.png");
                    } else if (NS.isEvent(model.getModel().getObservable())) {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/cevent16.png");
                    } else {
                        ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/csubject16.png");
                    }
                }
            }

            if (model.getModel().getErrorCount() > 0) {
                ret = ResourceManager.decorateImage(ret, ResourceManager
                        .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"), SWTResourceManager.BOTTOM_LEFT);
            }
            if (((KIMModel) model.getModel()).isAbstract()) {
                ret = ResourceManager.decorateImage(ret, ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "/icons/class_abs_decorator.gif"), SWTResourceManager.TOP_LEFT);
            }
            return ret;
        } else if (element instanceof IThinklabSubjectGenerator) {
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/observer.gif");
        } else if (element instanceof IThinklabDefinition) {
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/stop.gif");
        } else if (element instanceof IThinklabConcept) {
            return Eclipse.getConceptImage(((IThinklabConcept) element)
                    .getConcept(), ((IThinklabConcept) element).isNothing());
            // } else if (element instanceof IThinklabProperty) {
            // return ResourceManager.getPluginImage(Activator.PLUGIN_ID,
            // "/icons/relationship.png");
            // } else if (element instanceof IThinklabSource) {
            //
            // IThinklabSource tsource = (IThinklabSource) element;
            // return ResourceManager.getPluginImage(Activator.PLUGIN_ID,
            // tsource.getSourceIcon());
            //
            // } else if (element instanceof IThinklabConnection) {
            //
            // IThinklabConnection connection = (IThinklabConnection) element;
            // if (connection.getConnection().hasErrors()) {
            // return
            // ResourceManager.decorateImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID,
            // connection.getSourceIcon()), ResourceManager.getPluginImage(
            // "org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"),
            // SWTResourceManager.BOTTOM_LEFT);
            // }
            // return ResourceManager.getPluginImage(Activator.PLUGIN_ID,
            // connection.getSourceIcon());
            //
        } else if (element instanceof IThinklabOntology) {
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/classes.gif");
        } else if (element instanceof IThinklabScript) {
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/script.gif");
        }
        return delegate.getImage(element);
    }

    @Override
    public String getText(Object element) {
        if (element instanceof IThinklabElement) {
            return ((IThinklabElement) element).getElementName();
        }
        return delegate.getText(element);
    }

    @Override
    public String getDescription(Object element) {
        if (element instanceof IThinklabElement) {
            return ((IThinklabElement) element).getElementDescription();
        }
        return delegate.getText(element);
    }

}
