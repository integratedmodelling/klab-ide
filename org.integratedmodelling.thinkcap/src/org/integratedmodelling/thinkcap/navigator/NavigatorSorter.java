/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import org.eclipse.core.resources.IFolder;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabOntology;
import org.integratedmodelling.thinkcap.model.ThinklabModelObject;

public class NavigatorSorter extends ViewerSorter {

    public NavigatorSorter() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.
     * Viewer, java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Viewer viewer, Object e1, Object e2) {

        /*
         * last place for folders
         */
        if (e1 instanceof IFolder && !(e2 instanceof IFolder))
            return 1;
        if (e2 instanceof IFolder && !(e1 instanceof IFolder))
            return -1;
        if (e2 instanceof IFolder && e1 instanceof IFolder)
            return ((IFolder)e1).getName().compareTo(((IFolder)e2).getName());

        if (e1 instanceof IThinklabElement && e2 instanceof IThinklabElement) {

            /*
             * Declaration order for model objects in a namespace
             */
            if (e1 instanceof ThinklabModelObject && e2 instanceof ThinklabModelObject)
                return ((ThinklabModelObject) e1).order - ((ThinklabModelObject) e2).order;

            /*
             * last place for ontologies (they never appear at the same level of folders).
             */
            if (e1 instanceof IThinklabOntology && !(e2 instanceof IThinklabOntology))
                return 1;
            if (e2 instanceof IThinklabOntology && !(e1 instanceof IThinklabOntology))
                return -1;

            /*
             * alphabetic order for everything else
             */
            return ((IThinklabElement) e1).getElementName()
                    .compareTo(((IThinklabElement) e2).getElementName());
        }

        return super.compare(viewer, e1, e2);
    }

}
