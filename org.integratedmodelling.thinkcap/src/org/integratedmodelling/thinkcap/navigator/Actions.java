/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.ide.IDE;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.metadata.IReport;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.dialogs.BookmarkDialog;
import org.integratedmodelling.thinkcap.dialogs.ReportDialog;
import org.integratedmodelling.thinkcap.events.BookmarksModifiedEvent;
import org.integratedmodelling.thinkcap.interfaces.IThinklabConcept;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModel;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModelObject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabNamespace;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabScript;
import org.integratedmodelling.thinkcap.interfaces.IThinklabSubjectGenerator;
import org.integratedmodelling.thinkcap.wizards.NewNamespaceWizard;
import org.integratedmodelling.thinkcap.wizards.NewScriptWizard;

public class Actions {

    public static class DeleteNamespace extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabNamespace data;

        public DeleteNamespace(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Delete namespace");
            this.setToolTipText("Delete this namespace");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/project.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabNamespace) {
                    data = (IThinklabNamespace) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (data == null)
                return;

            if (Eclipse.confirm("Please confirm deletion of namespace " + data.getNamespace().getId())) {
                IProject project = data.getThinklabProject().getTLProject();
                ((Project) project).deleteNamespace(data.getNamespace().getId());
            }
        }
    }

    public static class ExportNamespace extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabNamespace data;

        public ExportNamespace(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Export as OWL...");
            this.setToolTipText("Export the knowledge in this namespace as an OWL ontology");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/Export-green.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabNamespace) {
                    data = (IThinklabNamespace) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (data == null)
                return;

            FileDialog dialog = new FileDialog(page.getWorkbenchWindow().getShell(), SWT.SAVE);
            dialog.setFilterExtensions(new String[]{"*.owl"});
            File path = new File(System.getProperty("user.home"));
            if (KLAB.CONFIG.getDefaultExportPath() != null) {
                path = KLAB.CONFIG.getDefaultExportPath();
            }
            dialog.setFilterPath(path.getPath());
            String result = dialog.open();
            if (result == null) {
                return;
            }

            try {
                data.getNamespace().getOntology().write(new File(result), true);
            } catch (KlabException e) {
                Eclipse.handleException(e);
            }
            
        }
    }
    
    public static class CheckNamespaceConsistency extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabNamespace data;

        public CheckNamespaceConsistency(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Check consistency...");
            this.setToolTipText("Use a reasoner to check semantic consistency of this namespace");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/Export-green.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabNamespace) {
                    data = (IThinklabNamespace) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {
            // TODO
        }
    }
    
    public static class CreateReport extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabElement   data;

        public CreateReport(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Create report...");
            this.setToolTipText("Create a report on the selected element and show it in a browser window");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/Export-green.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabNamespace) {
                    data = (IThinklabNamespace) (ssel.getFirstElement());
                    return true;
                } else if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabConcept) {
                    data = (IThinklabConcept) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            IReport report = null;

            if (data instanceof IThinklabConcept) {
                report = Observables.describe(((IThinklabConcept)data).getConcept());
            } else if (data instanceof IThinklabNamespace) {
                report = Observables.describe(((IThinklabNamespace)data).getNamespace());
            }
            
            if (report != null) {
                ReportDialog dialog = new ReportDialog(page.getWorkbenchWindow().getShell(), report.asHTML());
                dialog.open();
            }
            
        }
    }
    
    public static class RebuildAll extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabProject   data;

        public RebuildAll(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Rebuild workspace");
            this.setToolTipText("Reparse all projects");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/Refresh.png"));
        }

        @Override
        public boolean isEnabled() {
            return KLAB.PMANAGER.getProjects().size() > 0;
        }

        @Override
        public void run() {
            Activator.getDefault().reloadProjects(true);
        }
    }

    public static class NewNamespace extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabProject   data;

        public NewNamespace(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("New namespace...");
            this.setToolTipText("Create a new namespace file and edit it");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/project.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {
                    data = (IThinklabProject) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (data == null)
                return;

            NewNamespaceWizard wizard = new NewNamespaceWizard(false);
            wizard.setTargetProject(data.getThinklabProject().getTLProject());
            WizardDialog dialog = new WizardDialog(page.getWorkbenchWindow().getShell(), wizard);
            dialog.create();
            dialog.open();
        }
    }

    // public static class DeployProject extends Action {
    //
    // ISelectionProvider provider;
    // IWorkbenchPage page;
    // IThinklabProject data;
    //
    // public DeployProject(IWorkbenchPage p, ISelectionProvider selectionProvider) {
    // this.page = p;
    // this.provider = selectionProvider;
    // this.setText("Deploy to remote server");
    // this.setToolTipText("Create a new namespace file and edit it");
    // this.setImageDescriptor(
    // ResourceManager.getPluginImageDescriptor(
    // Activator.PLUGIN_ID, "/icons/project.gif"));
    // }
    //
    // @Override
    // public boolean isEnabled() {
    //
    // if (!Activator.client().getUser().getGroups().contains("ADMIN")) {
    // return false;
    // }
    //
    // ISelection selection = provider.getSelection();
    // if (!selection.isEmpty()) {
    // IStructuredSelection ssel = (IStructuredSelection) selection;
    // if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {
    // data = (IThinklabProject) (ssel.getFirstElement());
    // return true;
    // }
    // }
    // return false;
    // }
    //
    // @Override
    // public void run() {
    //
    // if (data == null)
    // return;
    //
    // Activator.client().deployToRemote(data.getThinklabProject().getTLProject());
    // }
    // }

    public static class NewScript extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabProject   data;

        public NewScript(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("New script...");
            this.setToolTipText("Create a new Thinklab script and edit it");
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor(Activator.PLUGIN_ID, "/icons/script.gif"));
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {
                    data = (IThinklabProject) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (data == null)
                return;

            NewScriptWizard wizard = new NewScriptWizard();
            wizard.setTargetProject(data.getThinklabProject().getTLProject());
            WizardDialog dialog = new WizardDialog(page.getWorkbenchWindow().getShell(), wizard);
            dialog.create();
            dialog.open();
        }

    }

    // public static class NewSource extends Action {
    //
    // ISelectionProvider provider;
    // IWorkbenchPage page;
    // IThinklabProject data;
    //
    // public NewSource(IWorkbenchPage p, ISelectionProvider selectionProvider) {
    // this.page = p;
    // this.provider = selectionProvider;
    // this.setText("New data connection...");
    // this.setToolTipText("Connect to a new source of data");
    // }
    //
    // @Override
    // public boolean isEnabled() {
    // ISelection selection = provider.getSelection();
    // if (!selection.isEmpty()) {
    // IStructuredSelection ssel = (IStructuredSelection) selection;
    // if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {
    //
    // /*
    // * TODO more checks
    // */
    //
    // data = (IThinklabProject) (ssel.getFirstElement());
    // return true;
    // }
    // }
    // return false;
    // }
    //
    // @Override
    // public void run() {
    //
    // if (data == null)
    // return;
    //
    // NewConnection wizard = new NewConnection();
    // wizard.setTargetProject(data.getThinklabProject().getTLProject());
    // WizardDialog dialog = new WizardDialog(page.getWorkbenchWindow().getShell(), wizard);
    // dialog.create();
    // dialog.open();
    // }
    //
    // }

    public static class DefineImports extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabProject   data;
        Shell              shell;

        public DefineImports(IWorkbenchPage p, ISelectionProvider selectionProvider, Shell shell) {
            this.page = p;
            this.provider = selectionProvider;
            this.shell = shell;
            this.setText("Prerequisite projects...");
            this.setToolTipText("Choose projects in the workspace that this project depends on");
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {

                    /*
                    * TODO more checks
                    */
                    data = (IThinklabProject) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            ArrayList<String> deps = new ArrayList<>();
            List<IProject> seldeps = Eclipse.selectDependencies(data, shell);
            if (seldeps != null) {
                for (IProject dp : seldeps) {
                    deps.add(dp.getId());
                }
                try {
                    ((Project) (data.getTLProject())).setDependencies(deps);
                    Activator.getDefault().reloadProjects(true);
                } catch (KlabException e) {
                    Eclipse.handleException(e);
                }
            }
        }
    }

    public static class CopyDatasource extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;

        // IThinklabSource data;

        public CopyDatasource(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Copy data source definition");
            this.setToolTipText("Copy datasource function to clipboard for pasting in source code");
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                // if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabSource) {
                //
                // /*
                // * TODO more checks
                // */
                //
                // data = (IThinklabSource) (ssel.getFirstElement());
                // return true;
                // }
            }
            return false;
        }

        @Override
        public void run() {

            // if (data != null) {
            // String src = data.getSource().getDefinitionPrototype("tql");
            // Eclipse.copyToClipboard(src);
            // }
        }

    }

    public static class BookmarkObject extends Action {

        ISelectionProvider   provider;
        IWorkbenchPage       page;
        IThinklabModelObject data;
        Shell                shell;

        public BookmarkObject(IWorkbenchPage p, ISelectionProvider selectionProvider, Shell shell) {
            this.page = p;
            this.provider = selectionProvider;
            this.shell = shell;
            this.setText("Bookmark");
            this.setToolTipText("Bookmark this object");
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 &&
                        (ssel.getFirstElement() instanceof IThinklabModel ||
                                ssel.getFirstElement() instanceof IThinklabConcept ||
                                ssel.getFirstElement() instanceof IThinklabSubjectGenerator)) {

                    data = (IThinklabModelObject) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (data != null) {
                BookmarkDialog dialog = new BookmarkDialog(shell);
                if (dialog.open() == Window.OK) {

                    final String afile = dialog.getText().trim();
                    /*
                     * TODO move validation inside the dialog
                     */
                    if (!afile.isEmpty()) {
                        IBookmark bookmark = KLAB.KM.getBookmarkManager()
                                .bookmark(data.getModelObject(), dialog.getText(), dialog
                                        .getDescription());
                        Activator.getDefault().fireEvent(new BookmarksModifiedEvent(bookmark));
                    }
                }
            }
        }

    }

    public static class RunScript extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabScript    data;

        public RunScript(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Run script");
            this.setToolTipText("Import a namespace from a supported external knowledge source");
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabScript) {
                    data = (IThinklabScript) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {
            // Activator.client().run(data.getScriptFile());
        }

    }

    public static class ImportNamespace extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabProject   data;

        public ImportNamespace(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
            this.setText("Import namespace...");
            this.setToolTipText("Import a namespace from a supported external knowledge source");
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabProject) {

                    /*
                     * TODO more checks
                     */

                    data = (IThinklabProject) (ssel.getFirstElement());
                    return true;
                }
            }
            return false;
        }

        @Override
        public void run() {
        }

    }

    public static class OpenAction extends Action {

        ISelectionProvider provider;
        IWorkbenchPage     page;
        IThinklabElement   data           = null;
        IFile              resourceToOpen = null;
        int                lineNumber     = -1;

        public OpenAction(IWorkbenchPage p, ISelectionProvider selectionProvider) {
            this.page = p;
            this.provider = selectionProvider;
        }

        @Override
        public boolean isEnabled() {
            ISelection selection = provider.getSelection();
            if (!selection.isEmpty()) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                if (ssel.size() == 1 && ssel.getFirstElement() instanceof IThinklabElement) {
                    data = (IThinklabElement) (ssel.getFirstElement());

                    if (data instanceof IThinklabNamespace && data.getResource() instanceof IFile) {
                        resourceToOpen = (IFile) data.getResource();
                        return true;
                    } else if (data instanceof IThinklabScript) {
                        resourceToOpen = (IFile) data.getResource();
                        return true;
                    } else if (data instanceof IThinklabModelObject) {
                        IResource r = data.getAncestor(IThinklabElement.THINKLAB_NAMESPACE).getResource();
                        if (r instanceof IFile) {
                            resourceToOpen = (IFile) r;
                            lineNumber = ((IThinklabModelObject) data).getStartLine();
                            return true;
                        }
                    } /* TODO handle sources, context editors etc */
                }
            }
            return false;
        }

        @Override
        public void run() {

            if (resourceToOpen == null)
                return;

            try {
                if (lineNumber > 0) {
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
                    IMarker marker = resourceToOpen.createMarker(IMarker.TEXT);
                    marker.setAttributes(map);
                    IDE.openEditor(page, marker);
                    marker.delete();
                } else {
                    IDE.openEditor(page, resourceToOpen);
                }
            } catch (Exception e) {
                Activator.engine().getMonitor().error(e);
            }
        }
    }
}
