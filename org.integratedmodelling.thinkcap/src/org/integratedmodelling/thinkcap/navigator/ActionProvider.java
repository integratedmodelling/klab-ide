/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.navigator.CommonActionProvider;
import org.eclipse.ui.navigator.ICommonActionConstants;
import org.eclipse.ui.navigator.ICommonActionExtensionSite;
import org.eclipse.ui.navigator.ICommonMenuConstants;
import org.eclipse.ui.navigator.ICommonViewerSite;
import org.eclipse.ui.navigator.ICommonViewerWorkbenchSite;

public class ActionProvider extends CommonActionProvider {

    /*
     * ACTIONS:
     * 
     * on PROJECT
     * 
     * 		Deploy
     * 		Undeploy
     * 		New Namespace...
     * 
     * on NAMESPACE
     * 
     * 		New context...
     * 		Delete
     * 
     * on MODEL
     * 
     * 		Observe in current context
     * 
     * on CONTEXT
     * 
     * 		Set as current context
     * 		Merge with current context
     * 
     * on CONCEPT
     * 
     * 		Observe in current context
     * 		(Create model...)
     * 
     * on Sources
     * 		
     * 		Connect
     * 		Delete
     * 
     * on Source
     *	
     *		(drag function) 
     * 		Copy source statement
     *		Visualize 
     *		(List/Search existing annotations for this source)
     * 
     */

    private ICommonViewerWorkbenchSite wSite;

    public ActionProvider() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void init(ICommonActionExtensionSite aSite) {
        // TODO Auto-generated method stub
        super.init(aSite);
        ICommonViewerSite viewSite = aSite.getViewSite();
        if (viewSite instanceof ICommonViewerWorkbenchSite) {
            this.wSite = (ICommonViewerWorkbenchSite) viewSite;
        }
    }

    @Override
    public void fillContextMenu(IMenuManager menu) {

        super.fillContextMenu(menu);

        /*
         * create actions for the full menu, only add the enabled ones.
         */
        Action deploy = null;
        Action undeploy = null;
        Action newNamespace = new Actions.NewNamespace(wSite.getPage(), wSite.getSelectionProvider());
        // Action deployProject =
        // new Actions.DeployProject(wSite.getPage(), wSite.getSelectionProvider());
        Action deleteNamespace = new Actions.DeleteNamespace(wSite.getPage(), wSite.getSelectionProvider());
        Action exportNamespace = new Actions.ExportNamespace(wSite.getPage(), wSite.getSelectionProvider());
        Action checkConsistency = new Actions.CheckNamespaceConsistency(wSite.getPage(), wSite.getSelectionProvider());
        Action report = new Actions.CreateReport(wSite.getPage(), wSite.getSelectionProvider());
        Action newScript = new Actions.NewScript(wSite.getPage(), wSite.getSelectionProvider());
        Action importNamespace = new Actions.CopyDatasource(wSite.getPage(), wSite.getSelectionProvider());
        // Action newSource =
        // new Actions.NewSource(wSite.getPage(), wSite.getSelectionProvider());
        Action newContext = null;
        // Action deleteObject = new DeleteResourceAction(wSite.getShell());
        // Action renameObject = new RenameResourceAction(Eclipse.getShell());
        Action observeInCurrent = null;
        Action setAsCurrent = null;
        Action mergeWithCurrent = null;
        Action connectSource = null;
        Action copyStatement = null;
        Action visualizeSource = null;
        Action defineDependencies = new Actions.DefineImports(wSite.getPage(), wSite
                .getSelectionProvider(), wSite.getShell());
        Action copyDatasource = new Actions.CopyDatasource(wSite.getPage(), wSite.getSelectionProvider());
        Action bookmarkObject = new Actions.BookmarkObject(wSite.getPage(), wSite
                .getSelectionProvider(), wSite.getShell());
        Action runScript = new Actions.RunScript(wSite.getPage(), wSite.getSelectionProvider());

        boolean ins = false;

        if (newNamespace.isEnabled()) {
            menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, newNamespace);
        }
        if (newScript.isEnabled()) {
            menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, newScript);
        }
        // if (newSource.isEnabled()) {
        // menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, newSource);
        // }

        if (runScript.isEnabled()) {
            menu.add(runScript);
        }

        // if (importNamespace.isEnabled()) {
        // // if (menu.getItems().length == 0)
        // menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, importNamespace);
        // // else
        // // menu.insertBefore(menu.getItems()[0].getId(), importNamespace);
        // // ins = true;
        // }

        if (defineDependencies.isEnabled()) {
            // if (menu.getItems().length == 0)
            menu.appendToGroup(ICommonMenuConstants.GROUP_NEW, defineDependencies);
            // else
            // menu.insertBefore(menu.getItems()[0].getId(), defineDependencies);
            // ins = true;
        }

        if (deleteNamespace.isEnabled()) {
            menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, deleteNamespace);
        }

        // if (copyDatasource.isEnabled()) {
        // menu.add(copyDatasource);
        // }

        if (exportNamespace.isEnabled()) {
            menu.appendToGroup(ICommonMenuConstants.GROUP_NEW, exportNamespace);
        }

        if (checkConsistency.isEnabled()) {
            menu.appendToGroup(ICommonMenuConstants.GROUP_NEW, checkConsistency);
        }
        
        if (report.isEnabled()) {
            menu.appendToGroup(ICommonMenuConstants.GROUP_NEW, report);
        }

        
        if (ins && menu.getItems().length > 0)
            menu.insertBefore(menu.getItems()[0].getId(), new Separator());

        if (bookmarkObject.isEnabled()) {
            menu.add(bookmarkObject);
        }

        menu.add(new Actions.RebuildAll(wSite.getPage(), wSite.getSelectionProvider()));

        // if (deleteObject.isEnabled()) {
        // menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, deleteObject);
        // }

        // if (renameObject.isEnabled()) {
        // menu.prependToGroup(ICommonMenuConstants.GROUP_NEW, renameObject);
        // }

        // if (deployProject.isEnabled()) {
        // menu.add(deployProject);
        // }

    }

    @Override
    public void fillActionBars(IActionBars actionBars) {

        super.fillActionBars(actionBars);

        actionBars.setGlobalActionHandler(ICommonActionConstants.OPEN, new Actions.OpenAction(wSite
                .getPage(), wSite.getSelectionProvider()));

        /*
         * TODO 
         * add package view button (hierarchical/flat), a->z ordering, ...
         */
    }

}
