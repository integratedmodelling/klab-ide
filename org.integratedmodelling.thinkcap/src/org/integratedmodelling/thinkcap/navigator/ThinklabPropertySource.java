/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import java.util.ArrayList;

import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModel;

public class ThinklabPropertySource implements IPropertySource {

    IThinklabElement o = null;

    public ThinklabPropertySource(IThinklabElement o) {
        this.o = o;
    }

    @Override
    public Object getEditableValue() {
        return this;
    }

    @Override
    public IPropertyDescriptor[] getPropertyDescriptors() {

        ArrayList<IPropertyDescriptor> ret = new ArrayList<IPropertyDescriptor>();

        if (o instanceof IThinklabModel) {

            boolean errors = ((IThinklabModel) o).getModel().getErrorCount() > 0;
            ret.add(new TextPropertyDescriptor("errorCount", "N. of Errors"));

            if (!errors) {
                ret.add(new TextPropertyDescriptor("observedType", "Observed Type"));
                ret.add(new TextPropertyDescriptor("observableType", "Observable Type"));
                ret.add(new TextPropertyDescriptor("observationType", "Observation Type"));
                ret.add(new TextPropertyDescriptor("inherentType", "Inherent Subject Type"));

                if (((IThinklabModel) o).getModel().getObserver() != null) {
                    IObserver obs = ((IThinklabModel) o).getModel().getObserver();
                    if (obs != null) {
                        ret.add(new TextPropertyDescriptor("oobservableType", "Observer Type"));
                        if (obs instanceof IClassifyingObserver) {
                            ret.add(new TextPropertyDescriptor("conceptSpace", "Classifies Type"));
                        }
                        if (obs instanceof IMediatingObserver
                                && ((IMediatingObserver) obs).getMediatedObserver() != null) {
                            ret.add(new TextPropertyDescriptor("mediatedType", "Mediated Type"));

                        }
                        // ret.add(new TextPropertyDescriptor("oobservationType", "Observer obs Type"));
                        // ret.add(new TextPropertyDescriptor("oinherentType", "Observer Subject Type"));
                    }
                }
            }
        }

        return ret.size() == 0 ? null : ret.toArray(new IPropertyDescriptor[ret.size()]);
    }

    @Override
    public Object getPropertyValue(Object id) {

        boolean errors = ((IThinklabModel) o).getModel().getErrorCount() > 0;

        if (id.equals("observableType")) {
            return ((IThinklabModel) o).getModel().getObservable().toString();
        } else if (id.equals("errorCount")) {
            return errors ? "> 0" : "0";
        } else if (id.equals("observedType")) {
            IKnowledge c = ((IThinklabModel) o).getModel().getObservable().getType();
            return c == null ? "NULL" : c.toString();
        } else if (id.equals("observationType")) {
            IConcept c = ((IThinklabModel) o).getModel().getObservable().getObservationType();
            return c == null ? "NULL" : c.toString();
        } else if (id.equals("inherentType")) {
            IKnowledge c = Observables.getInherentType(((IThinklabModel) o).getModel().getObservable().getType());
            return c == null ? "NULL" : c.toString();
        } else if (id.equals("conceptSpace")) {
            IObserver ob = ((IThinklabModel) o).getModel().getObserver();
            IConcept c = ((IClassifyingObserver) ob).getClassification().getConceptSpace();
            return c.toString();
        } else if (id.equals("mediatedType")) {
            IObserver ob = ((IThinklabModel) o).getModel().getObserver();
            return ((IMediatingObserver) ob).getMediatedObserver().getObservable().toString();
        } else if (id.equals("oobservableType")) {
            IObserver ob = ((IThinklabModel) o).getModel().getObserver();
            return ob.getObservable().toString();
        } /* else if (id.equals("oobservationType")) {
          IObserver ob = ((IThinklabModel)o).getModel().getObserver();
          IConcept c = ob == null ? null : Observations.getObservationType(ob.getObservableConcept());
          return c == null ? "NULL" : c.toString();
          } else if (id.equals("oinherentType")) {
          IObserver ob = ((IThinklabModel)o).getModel().getObserver();
          IConcept c = ob == null ? null : Observations.getInherentSubjectType(ob.getObservableConcept());
          return c == null ? "NULL" : c.toString();
          } */
        return null;
    }

    @Override
    public boolean isPropertySet(Object id) {
        return false;
    }

    @Override
    public void resetPropertyValue(Object id) {
    }

    @Override
    public void setPropertyValue(Object id, Object value) {
    }

}
