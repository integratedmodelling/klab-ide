/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.navigator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.model.ThinklabModelManager;

public class TLViewContentProvider extends WorkbenchContentProvider {

    protected static final Object[] NO_CHILDREN = new Object[0];

    @Override
    public void dispose() {
    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        if (ThinklabNavigator._viewer == null) {
            ThinklabNavigator._viewer = viewer;
        }
        super.inputChanged(viewer, oldInput, newInput);
    }

    @Override
    public Object[] getElements(Object inputElement) {
        return getChildren(inputElement);
    }

    @Override
    public Object[] getChildren(Object parentElement) {

        try {
            if (parentElement instanceof IWorkspaceRoot) {
                return ThinklabModelManager.get().getChildren(parentElement);
            } else if (parentElement instanceof IThinklabElement) {
                return ((IThinklabElement) parentElement).getChildren();
            }
        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }

        return super.getChildren(parentElement);
    }

    @Override
    public Object getParent(Object element) {

        if (element instanceof IProject) {
            return ((IProject) element).getWorkspace().getRoot();
        } else if (element instanceof IThinklabElement) {
            return ((IThinklabElement) element).getParent();
        }
        return super.getParent(element);
    }

    @Override
    public boolean hasChildren(Object element) {

        if (IWorkspaceRoot.class.isInstance(element)) {
            return ((IWorkspaceRoot) element).getProjects().length > 0;
        } else if (element instanceof IThinklabElement) {
            return ((IThinklabElement) element).hasChildren();
        }
        return super.hasChildren(element);
    }

    // @Override
    // protected void processDelta(IResourceDelta delta) {
    //
    // IProject p = Eclipse.isProjectConfigurationChange(delta);
    // if (p != null) {
    // org.integratedmodelling.api.project.IProject project = Env.PMANAGER.getProject(p.getName());
    // try {
    // ((Project) project).setDependencies(Eclipse.getReferencedProjects(p.getName()));
    // } catch (ThinklabException e) {
    // Activator.client().error(e);
    // }
    // } else {
    //
    // if (Eclipse.isProjectLevelChange(delta)) {
    // Activator.client().rescanProjects(false, _viewer);
    // } else {
    //
    // /*
    // * file change
    // */
    // for (IResource r : Eclipse.getRelevantChangedResources(delta)) {
    // if (r instanceof IFile) {
    // Activator.client().rescanProjects(false, _viewer);
    // break;
    // }
    // }
    // }
    // }
    //
    // super.processDelta(delta);
    // }

}
