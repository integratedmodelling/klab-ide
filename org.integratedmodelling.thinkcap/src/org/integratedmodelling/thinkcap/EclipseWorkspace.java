/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.common.utils.MiscUtilities;

public class EclipseWorkspace extends Workspace {

    org.eclipse.core.resources.IWorkspace eclipseWorkspace;

    public EclipseWorkspace(org.eclipse.core.resources.IWorkspace workspace) {
        super(workspace.getRoot().getLocation().toFile(), false);
        eclipseWorkspace = workspace;
        load();
    }

    public void load() {
        pDirs.clear();
        for (org.eclipse.core.resources.IProject p : eclipseWorkspace.getRoot().getProjects()) {
            if (p.isOpen()) {
                IPath rl = p.getRawLocation();
                if (rl == null) {
                    continue;
                }
                File f = rl.toFile();
                if (ProjectManager.isKlabProject(f) && ProjectManager.isOpen(f)) {
                    pDirs.add(f);
                }
            }
        }

    }

    @Override
    public File getDefaultFileLocation() {
        return eclipseWorkspace.getRoot().getLocation().toFile();
    }

    @Override
    public IProject create(String project) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(String project) {
        // TODO Auto-generated method stub

    }

    /**
     * Return only the projects that are visible to the Eclipse user as 
     * k.LAB projects - i.e. those that are open, in the workspace and not 
     * using a remote version.
     * 
     * @return all user-visible projects
     */
    public Collection<IProject> getUserProjects() {
        List<IProject> ret = new ArrayList<>();
        return ret;
    }
    
    public static IProject addProject(String src) {

        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        org.eclipse.core.resources.IProject eproj = root.getProject(src);

        try {
            eproj.create(null);
            eproj.open(null);
            IProject proj = KLAB.PMANAGER.createProject(eproj.getLocation().toFile(), null);
            requireNature(eproj, "org.integratedmodelling.thinkcap.thinklab");
            return proj;

        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }

        ((EclipseWorkspace) KLAB.WORKSPACE).load();

        return null;
    }

    public static void requireNature(org.eclipse.core.resources.IProject project, String nature) {

        boolean hasNature;
        try {
            hasNature = project.hasNature(nature);
            if (!hasNature) {
                IProjectDescription desc = project.getDescription();
                String[] natures = new String[desc.getNatureIds().length + 1];
                int i = 0;
                for (String s : desc.getNatureIds()) {
                    natures[i++] = s;
                }
                natures[i] = nature;
                desc.setNatureIds(natures);
                project.setDescription(desc, null);
            }
        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }

    }

    public static IProject getProjectForTarget(Object target) {

        IProject ret = null;

        if (target instanceof INamespace) {
            ret = ((INamespace) target).getProject();
        } else if (target instanceof IModelObject) {
            ret = ((IModelObject) target).getNamespace().getProject();
        } else if (target instanceof IProject) {
            ret = (IProject) target;
        }
        return ret;
    }

    public static IFile getResourceForNamespace(IProject project, INamespace namespace) {

        String rpath = namespace.getId().replace('.', '/');
        rpath += "." + MiscUtilities.getFileExtension(namespace.getLocalFile().toString());
        rpath = namespace.getProject().getSourceRelativePath() + "/" + rpath;

        IFile file = ResourcesPlugin.getWorkspace().getRoot().getProject(project.getId()).getFile(rpath);

        if (!file.exists())
            return null;

        return file;
    }

}
