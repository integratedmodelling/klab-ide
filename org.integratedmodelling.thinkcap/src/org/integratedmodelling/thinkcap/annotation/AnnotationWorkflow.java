/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.annotation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.thinkcap.annotation.AnnotationActions.Action;

/**
 * TODO this may go in common after ensuring steps hav no GUI elements in it.
 * @author Ferd
 *
 */
public class AnnotationWorkflow {

    /**
     * The workflow history. Last step is the current one. Should always start with
     * a beginning step.
     */
    LinkedList<AnnotationStep> _wf = new LinkedList<>();

    /**
     * Workflow template will depend on the type of model we want to annotate.
     * 
     * @author Ferd
     *
     */
    enum Type {
        DATASOURCE_MODEL,
        OBJECTSOURCE_MODEL,
        QUALITY_MODEL,
        SUBJECT_MODEL;
    }

    /**
     * this is actually describing each step, not the workflow.
     */
    public enum ActionType {
        CHOOSE_YN,
        CHOOSE_ELEMENT,
        ENTER_ID,
        EXPLAIN;
    }

    /**
     * Declaration being built. Starts empty.
     */
//    ObservableDeclaration _declaration    = new ObservableDeclaration();
    Listener              _choiceListener = null;

    /**
     * Listener installed in the workflow and called by the default AnnotationStep to filter any choice made by user
     * and allow the interface to react.
     * 
     * @author Ferd
     *
     */
    public static interface Listener {

        /**
         * Passed the annotation step, the choice and the choice type at every user choice. If
         * this returns false, the actual choice handler in the annotation step isn't called. Only
         * works if the annotation step is a child of AnnotationActions.Action.
         * 
         * @param action
         * @param choice
         * @param choiceType
         * @return poh
         */
        boolean onChoice(AnnotationStep action, Object choice, String choiceType);

        void onAdvance(AnnotationStep currentStep);

        boolean afterChoice(AnnotationStep action, Object choice, String choiceType);

    }

    public static interface AnnotationStep {

        /**
         * We want this static, so must have a pointer to the workflow that holds it.
         * 
         * @return bah
         */
        AnnotationWorkflow getWorkflow();

        /**
         * Get the choices given to the user; based on the type, allow a user action that will
         * process them properly.
         * 
         * @return buh
         */
        Object[] getChoices();

        /**
         * Called when accept() is chosen for this step.Returns the next step, or null if
         * no further steps are possible.
         * 
         */
        AnnotationStep action();

        /**
         * Callback when the user makes a choice relevant to this step (relative to the object(s)
         * returned by choice).
         * 
         * If this returns a step, immediately move on to it in the workflow. Otherwise just 
         * record the step and wait for action().
         * 
         * @param choice
         * @param choiceType = "choice" for one of getChoices(), "option" for one of getOptions(), and custom for
         *        other choices made through the interface.
         */
        AnnotationStep onChoice(Object choice, String choiceType);

        /**
         * Shown to user to guide them in going beyond this step.
         * 
         * @return nah
         */
        String getMessage();

        /**
         * Options that are relevant to the step, or null.
         * 
         * @return beh
         */
        String[] getOptions();

        /**
         * A filter to select the knowledge of interest for this step
         * 
         * @return uh
         */
        Filter getFilter();

        /**
         * description in simplified HTML. Should fit in a couple paragraphs.
         */
        String getDescription();

        /**
         * Documentation URL that will be used by a GUI to link to further docs.
         * @return ah
         */
        String webURL();

        /**
         * Locator of correspondent section in help system, or null.
         * 
         * @return oh
         */
        String helpSection();
    }

    public AnnotationWorkflow() {
        _wf.add(new AnnotationActions.BeginModel(this));
    }

    public int size() {
        return _wf.size();
    }

//    public ObservableDeclaration getDeclaration() {
//        return _declaration;
//    }

    private List<Pair<String, Object>> declaration() {
        List<Pair<String, Object>> ret = new ArrayList<>();
        for (AnnotationStep s : _wf) {
            ((Action) s).collectData(ret);
        }
        return ret;
    }

    public void setListener(Listener listener) {
        _choiceListener = listener;
    }

    /*
     * return a string definition of the model observable so far, wrapped in a model template
     * reflecting the workflow type. Use bold keywords in HTML fashion if so requested.
     */
    public String getTemplate(boolean html) {

        List<Pair<String, Object>> d = declaration();
        String ret = "";

        // TODO
        for (Pair<String, Object> z : d) {
            ret += "<b>" + z.getFirst() + "</b>" + " = " + z.getSecond() + "\n";
        }

        return ret;
    }

    /**
     * Remove the last accepted step.
     */
    public void undo() {
        _wf.removeLast();
    }

    public AnnotationStep getCurrentStep() {
        return _wf.peekLast();
    }

    public void advance() {
        _wf.add(getCurrentStep().action());
        if (_choiceListener != null) {
            _choiceListener.onAdvance(getCurrentStep());
        }
    }

    public void advance(AnnotationStep step) {
        _wf.add(step);
        if (_choiceListener != null) {
            _choiceListener.onAdvance(getCurrentStep());
        }
    }

}
