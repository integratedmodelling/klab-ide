/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.annotation;

import java.io.IOException;
import java.net.URL;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.HTMLStyledTextParser;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.annotation.AnnotationWorkflow.AnnotationStep;
import org.integratedmodelling.thinkcap.ui.KnowledgeBrowser;

public class AnnotationAssistant extends ViewPart {

    public static final String ID        = "org.integratedmodelling.thinkcap.annotation.AnnotationAssistant";
    private final FormToolkit  toolkit   = new FormToolkit(Display.getCurrent());

    KnowledgeBrowser           searchWidget;

    AnnotationWorkflow         _workflow = new AnnotationWorkflow();
    private Label              stepInstructionLine;
    private Composite          choicesArea;
    private StyledText         helpText;
    private Button             helpButton;
    private Button             webButton;
    private Table              table;
    private StyledText templateText;

    public AnnotationAssistant() {
    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        GridLayout gl_container = new GridLayout(1, false);
        gl_container.verticalSpacing = 0;
        gl_container.marginWidth = 0;
        gl_container.marginHeight = 0;
        container.setLayout(gl_container);
        {
            Composite titleArea = new Composite(container, SWT.NONE);
            titleArea.setLayout(new GridLayout(5, false));
            titleArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            toolkit.adapt(titleArea);
            toolkit.paintBordersFor(titleArea);
            titleArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
            {
                Label lblNewLabel = new Label(titleArea, SWT.NONE);
                lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
                lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI Semibold", 12, SWT.NORMAL));
                toolkit.adapt(lblNewLabel, true, true);
                lblNewLabel.setText("Semantic annotation assistant");
                lblNewLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
                lblNewLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
            }
            {
                Button undoButton = new Button(titleArea, SWT.NONE);
                undoButton.setToolTipText("Cancel this step and go back to previous");
                undoButton.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        undo();
                    }
                });
                toolkit.adapt(undoButton, true, true);
                undoButton.setText("Undo");
            }
            {
                Button startOverButton = new Button(titleArea, SWT.NONE);
                startOverButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseUp(MouseEvent e) {

                        boolean ok = true;
                        if (_workflow.size() > 1) {
                            /*
                             * TODO ask for confirmation 
                             */
                        }
                        if (ok) {
                            _workflow = new AnnotationWorkflow();
                            display();
                        }
                    }
                });
                startOverButton.setToolTipText("Start annotation from scratch");
                toolkit.adapt(startOverButton, true, true);
                startOverButton.setText("Start Over");
            }
            {
                Button abandonButton = new Button(titleArea, SWT.NONE);
                abandonButton.setToolTipText("Abandon annotation and close view");
                toolkit.adapt(abandonButton, true, true);
                abandonButton.setText("Cancel");
            }
            {
                Button finishButton = new Button(titleArea, SWT.NONE);
                finishButton.setToolTipText("Copy template to clipboard, send any requests and close view");
                toolkit.adapt(finishButton, true, true);
                finishButton.setText("Finish");
            }
        }
        {
            SashForm workArea = new SashForm(container, SWT.VERTICAL);
            workArea.setSashWidth(1);
            workArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            toolkit.adapt(workArea);
            toolkit.paintBordersFor(workArea);
            workArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
            {
                SashForm choiceDocArea = new SashForm(workArea, SWT.NONE);
                choiceDocArea.setSashWidth(2);
                toolkit.adapt(choiceDocArea);
                toolkit.paintBordersFor(choiceDocArea);
                choiceDocArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
                {
                    Composite chooseArea = new Composite(choiceDocArea, SWT.NONE);
                    toolkit.adapt(chooseArea);
                    toolkit.paintBordersFor(chooseArea);
                    chooseArea.setLayout(new GridLayout(1, false));
                    {
                        stepInstructionLine = new Label(chooseArea, SWT.NONE);
                        GridData gd_stepInstructionLine = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
                        gd_stepInstructionLine.heightHint = 14;
                        stepInstructionLine.setLayoutData(gd_stepInstructionLine);
                        toolkit.adapt(stepInstructionLine, true, true);
                        stepInstructionLine.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_RED));
                        stepInstructionLine.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
                    }
                    {
                        Composite actionArea = new Composite(chooseArea, SWT.NONE);
                        GridLayout gl_actionArea = new GridLayout(2, false);
                        gl_actionArea.horizontalSpacing = 1;
                        gl_actionArea.verticalSpacing = 0;
                        gl_actionArea.marginWidth = 0;
                        gl_actionArea.marginHeight = 0;
                        actionArea.setLayout(gl_actionArea);
                        actionArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
                        toolkit.adapt(actionArea);
                        toolkit.paintBordersFor(actionArea);
                        {
                            choicesArea = new Composite(actionArea, SWT.NONE);
                            GridData gd_choicesArea = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
                            gd_choicesArea.heightHint = 24;
                            choicesArea.setLayoutData(gd_choicesArea);
                            RowLayout rl_choicesArea = new RowLayout(SWT.HORIZONTAL);
                            rl_choicesArea.justify = true;
                            rl_choicesArea.marginTop = 0;
                            rl_choicesArea.marginRight = 0;
                            rl_choicesArea.marginLeft = 0;
                            rl_choicesArea.marginBottom = 0;
                            choicesArea.setLayout(rl_choicesArea);
                            toolkit.adapt(choicesArea);
                            toolkit.paintBordersFor(choicesArea);
                        }
                        {
                            Button btnNewButton = new Button(actionArea, SWT.NONE);
                            btnNewButton
                                    .setToolTipText("Move to the next step. Press \"Finish\" instead if you're done.");
                            btnNewButton.addMouseListener(new MouseAdapter() {
                                @Override
                                public void mouseUp(MouseEvent e) {
                                    loadNextStep();
                                }
                            });
                            btnNewButton.setImage(ResourceManager
                                    .getPluginImage("org.integratedmodelling.thinkcap", "icons/dplay.png"));
                            toolkit.adapt(btnNewButton, true, true);
                        }
                    }
                    {
                        searchWidget = new KnowledgeBrowser(chooseArea, SWT.NONE);
                        searchWidget.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
                        toolkit.adapt(searchWidget);
                        toolkit.paintBordersFor(searchWidget);
                    }
                }
                {
                    Composite infoArea = new Composite(choiceDocArea, SWT.NONE);
                    toolkit.adapt(infoArea);
                    toolkit.paintBordersFor(infoArea);
                    GridLayout gl_infoArea = new GridLayout(1, false);
                    gl_infoArea.horizontalSpacing = 0;
                    gl_infoArea.verticalSpacing = 1;
                    gl_infoArea.marginWidth = 0;
                    gl_infoArea.marginHeight = 0;
                    infoArea.setLayout(gl_infoArea);

                    helpText = new StyledText(infoArea, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
                    helpText.setAlwaysShowScrollBars(false);
                    helpText.setWordWrap(true);
                    helpText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
                    toolkit.adapt(helpText);
                    toolkit.paintBordersFor(helpText);
                    helpText.setBackground(SWTResourceManager.getColor(SWT.COLOR_INFO_BACKGROUND));

                    Composite composite = new Composite(infoArea, SWT.NONE);
                    GridLayout gl_composite = new GridLayout(3, false);
                    gl_composite.horizontalSpacing = 1;
                    gl_composite.verticalSpacing = 1;
                    gl_composite.marginWidth = 0;
                    gl_composite.marginHeight = 0;
                    composite.setLayout(gl_composite);
                    composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                    toolkit.adapt(composite);
                    toolkit.paintBordersFor(composite);
                    {
                        Label lblNewLabel_3 = new Label(composite, SWT.NONE);
                        lblNewLabel_3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                        lblNewLabel_3.setBounds(0, 0, 55, 15);
                        toolkit.adapt(lblNewLabel_3, true, true);
                    }

                    helpButton = new Button(composite, SWT.NONE);
                    helpButton.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                            PlatformUI.getWorkbench().getHelpSystem()
                                    .displayHelp(_workflow.getCurrentStep().helpSection());
                        }
                    });
                    helpButton.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));
                    helpButton.setBounds(0, 0, 75, 25);
                    toolkit.adapt(helpButton, true, true);
                    helpButton.setText("Help");

                    webButton = new Button(composite, SWT.NONE);
                    webButton.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                            try {
                                IWebBrowser browser = PlatformUI.getWorkbench().getBrowserSupport()
                                        .createBrowser("annotationhelp");
                                browser.openURL(new URL(_workflow.getCurrentStep().webURL()));
                            } catch (Exception e1) {
                                Eclipse.beep();
                            }
                        }
                    });
                    webButton.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));
                    toolkit.adapt(webButton, true, true);
                    webButton.setText("Web");
                }
                choiceDocArea.setWeights(new int[] { 4, 2 });
            }
            {
                SashForm templateLogArea = new SashForm(workArea, SWT.VERTICAL);
                templateLogArea.setSashWidth(1);
                toolkit.adapt(templateLogArea);
                toolkit.paintBordersFor(templateLogArea);
                templateLogArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
                {
                    Composite templateArea = new Composite(templateLogArea, SWT.NONE);
                    toolkit.adapt(templateArea);
                    toolkit.paintBordersFor(templateArea);
                    templateArea.setLayout(new GridLayout(1, false));

                    Label lblNewLabel_1 = new Label(templateArea, SWT.NONE);
                    lblNewLabel_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
                    lblNewLabel_1.setBounds(0, 0, 55, 15);
                    toolkit.adapt(lblNewLabel_1, true, true);
                    lblNewLabel_1.setText("Code Template");

                    templateText = new StyledText(templateArea, SWT.BORDER | SWT.FULL_SELECTION
                            | SWT.READ_ONLY);
                    templateText.setBottomMargin(2);
                    templateText.setTopMargin(2);
                    templateText.setRightMargin(1);
                    templateText.setLeftMargin(4);
                    templateText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
                    toolkit.adapt(templateText);
                    toolkit.paintBordersFor(templateText);
                    templateText.setBackground(SWTResourceManager
                            .getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
                    templateText.setFont(SWTResourceManager.getFont("Consolas", 10, SWT.NORMAL));
                }
                {
                    Composite variableArea = new Composite(templateLogArea, SWT.NONE);
                    toolkit.adapt(variableArea);
                    toolkit.paintBordersFor(variableArea);
                    variableArea.setLayout(new FillLayout(SWT.HORIZONTAL));
                    {
                        table = new Table(variableArea, SWT.BORDER | SWT.FULL_SELECTION);
                        toolkit.adapt(table);
                        toolkit.paintBordersFor(table);
                        table.setLinesVisible(true);
                        {
                            TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
                            tblclmnNewColumn.setWidth(100);
                            tblclmnNewColumn.setText("New Column");
                        }
                        {
                            TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
                            tblclmnNewColumn_1.setWidth(100);
                            tblclmnNewColumn_1.setText("New Column");
                        }
                    }
                }
                templateLogArea.setWeights(new int[] { 1, 0 });
            }
            workArea.setWeights(new int[] { 3, 2 });
        }

        createActions();
        initializeToolBar();
        initializeMenu();
        
        _workflow.setListener(new AnnotationWorkflow.Listener() {
			
			@Override
			public boolean onChoice(AnnotationStep action, Object choice,
					String choiceType) {

		        Display.getDefault().asyncExec(new Runnable() {
		            @Override
		            public void run() {
		                defineTemplate();
		            }

		        });
				return true;
			}
			
			@Override
			public boolean afterChoice(AnnotationStep action, Object choice,
					String choiceType) {

		        Display.getDefault().asyncExec(new Runnable() {
		            @Override
		            public void run() {
		                defineTemplate();
		            }

		        });
				return true;
			}
			
			@Override
			public void onAdvance(AnnotationStep currentStep) {
			}
		});
        
        display();
    }

    protected void undo() {
        _workflow.undo();
        display();
    }

    protected void loadNextStep() {
        _workflow.advance();
        display();
    }

    public void dispose() {
        toolkit.dispose();
        super.dispose();
    }

    public void display() {

        searchWidget.setFilter(_workflow.getCurrentStep().getFilter());

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                createStepChoices();
                printStepInfo();
                defineTemplate();
            }

        });
    }

    protected void defineTemplate() {
    	templateText.setText(_workflow.getTemplate(true));
        try {
            new HTMLStyledTextParser(templateText).parse();
        } catch (IOException e) {
        }
    }

    protected void printStepInfo() {
        AnnotationStep step = _workflow.getCurrentStep();
        stepInstructionLine.setText(step.getMessage());
        helpText.setText(step.getDescription());
        try {
            new HTMLStyledTextParser(helpText).parse();
        } catch (IOException e) {
        }
        webButton.setEnabled(step.webURL() != null);
        helpButton.setEnabled(step.helpSection() != null);
    }

    private void createStepChoices() {

        final AnnotationStep step = _workflow.getCurrentStep();

        for (Control control : choicesArea.getChildren()) {
            control.dispose();
        }

        Object[] choices = step.getChoices();
        String[] options = step.getOptions();

        boolean empty = true;

        if (choices != null) {

            for (final Object c : choices) {
                if (c instanceof IConcept) {
                    // we want to choose a concept out of many
                } else if (c instanceof String) {

                    if (((String) c).startsWith("#")) {
                        // string starting with # is a radio option
                        Button b = new Button(choicesArea, SWT.RADIO);
                        b.setText(((String) c).substring(1));
                        b.setSelection(empty);
                        b.addSelectionListener(new SelectionAdapter() {
                            @Override
                            public void widgetSelected(SelectionEvent e) {
                                AnnotationStep res = step.onChoice(((String) c).substring(1), "choice");
                                if (res != null) {
                                    _workflow.advance(res);
                                    display();
                                }
                            }
                        });
                    } else if (((String) c).startsWith("?")) {
                        // string starting with ? is a question: if empty, it's a Yes/No one
                        if (((String) c).substring(1).isEmpty()) {
                            Button b = new Button(choicesArea, SWT.NONE);
                            b.setText("Yes");
                            b.addSelectionListener(new SelectionAdapter() {
                                @Override
                                public void widgetSelected(SelectionEvent e) {
                                    AnnotationStep res = step.onChoice(true, "choice");
                                    if (res != null) {
                                        _workflow.advance(res);
                                        display();
                                    }
                                }
                            });
                            b = new Button(choicesArea, SWT.NONE);
                            b.setText("No");
                            b.addSelectionListener(new SelectionAdapter() {
                                @Override
                                public void widgetSelected(SelectionEvent e) {
                                    AnnotationStep res = step.onChoice(false, "choice");
                                    if (res != null) {
                                        _workflow.advance(res);
                                        display();
                                    }
                                }
                            });
                        } else {
                            // label and text field
                            Label l = new Label(choicesArea, SWT.NONE);
                            l.setText(((String) c).substring(1));
                            Text t = new Text(choicesArea, SWT.BORDER);
                        }
                    } else if (((String) c).startsWith(">")) {
                        // button
                        Button b = new Button(choicesArea, SWT.NONE);
                        b.setText(((String) c).substring(1));
                        b.addSelectionListener(new SelectionAdapter() {
                            @Override
                            public void widgetSelected(SelectionEvent e) {
                                AnnotationStep res = step.onChoice(((String) c).substring(1), "choice");
                                if (res != null) {
                                    _workflow.advance(res);
                                    display();
                                }
                            }
                        });

                    }

                }
                empty = false;
            }

        }

        if (options != null) {
            empty = false;
            final Combo combo = new Combo(choicesArea, SWT.READ_ONLY);
            combo.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    step.onChoice(combo.getText(), "option");
                }
            });

            for (String o : options) {
                combo.add(o);
            }

            combo.select(0);
        }

        if (!empty) {
            choicesArea.pack();
        }

        // searchWidget.setEnabled(step.getFilter() != null);

        if (step.getFilter() == null) {
        }
    }

    /**
    * Create the actions.
    */
    private void createActions() {
        // Create the actions
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

	@Override
	public Object getAdapter(Class arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
