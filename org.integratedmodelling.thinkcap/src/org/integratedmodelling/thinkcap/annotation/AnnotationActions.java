/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.annotation;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.annotation.AnnotationWorkflow.AnnotationStep;

/**
 * Static container for annotation steps. Move to common package when done.
 * 
 * @author Ferd
 *
 */
public class AnnotationActions {

    public abstract static class Action implements AnnotationWorkflow.AnnotationStep {

        protected AnnotationWorkflow _workflow;
        private String               _message;
        private String               _description;

        protected Filter             _cmatch = new Filter<IConcept>() {
                                                 @Override
                                                 public boolean match(IConcept o) {
                                                     // TODO Auto-generated method stub
                                                     return o instanceof IConcept
                                                             && !NS.isDerived(o);
                                                 }
                                             };
        protected Filter             _qmatch = new Filter() {
                                                 @Override
                                                 public boolean match(Object o) {
                                                     // TODO Auto-generated method stub
                                                     return o instanceof IConcept
                                                             && NS.isQuality((IConcept) o)
                                                             && !NS.isDerived((IKnowledge) o);
                                                 }
                                             };
        protected Filter             _pmatch = new Filter() {
                                                 @Override
                                                 public boolean match(Object o) {
                                                     // TODO Auto-generated method stub
                                                     return o instanceof IConcept
                                                             && NS.isProcess((IConcept) o)
                                                             && !NS.isDerived((IKnowledge) o);
                                                 }
                                             };
        protected Filter             _smatch = new Filter() {
                                                 @Override
                                                 public boolean match(Object o) {
                                                     // TODO Auto-generated method stub
                                                     return o instanceof IConcept
                                                             && NS.isThing((IConcept) o)
                                                             && !NS.isDerived((IKnowledge) o);
                                                 }
                                             };
        protected Filter             _ematch = new Filter() {
                                                 @Override
                                                 public boolean match(Object o) {
                                                     // TODO Auto-generated method stub
                                                     return o instanceof IConcept
                                                             && NS.isEvent((IConcept) o)
                                                             && !NS.isDerived((IKnowledge) o);
                                                 }
                                             };

        Action(AnnotationWorkflow workflow, String message, String description) {
            _workflow = workflow;
            _message = message;
            _description = description;
        }

        public abstract void collectData(List<Pair<String, Object>> map);

        @Override
        public AnnotationWorkflow getWorkflow() {
            return _workflow;
        }

        @Override
        public String getMessage() {
            return _message;
        }

        @Override
        public String getDescription() {
            return _description;
        }

        @Override
        final public AnnotationStep onChoice(Object choice, String choiceType) {

            AnnotationStep ret = null;
            if (_workflow._choiceListener == null
                    || (_workflow._choiceListener != null && _workflow._choiceListener
                            .onChoice(this, choice, choiceType))) {
                ret = handleChoice(choice, choiceType);
                if (_workflow._choiceListener != null) {
                    _workflow._choiceListener.afterChoice(this, choice, choiceType);
                }
            }
            return ret;
        }

        protected abstract AnnotationStep handleChoice(Object choice, String choiceType);

        @Override
        public String webURL() {
            return null;
        }

        @Override
        public String helpSection() {
            return null;
        }
    }

    public static class BeginModel extends Action {

        String mType = "Quality";
        String mRole = "For annotating data";

        BeginModel(AnnotationWorkflow workflow) {
            super(workflow, "Choose the fundamental observable type and the purpose of this annotation", BEGIN_MODEL_DESCRIPTION);
        }

        @Override
        public AnnotationStep action() {
            return mType.equals("Quality") ? new ChooseObserver(_workflow, true)
                    : new ChooseSubjectType(_workflow);
        }

        @Override
        public String[] getOptions() {

            return new String[] {
                    "For annotating data",
                    "For a computed model",
                    "For a dependency",
                    "For a quick observation"
            };
        }

        @Override
        public Filter getFilter() {
            return _cmatch;
        }

        @Override
        public Object[] getChoices() {

            return new String[] {
                    "#Quality",
                    "#Subjects",
            };
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {

            if (choiceType.equals("choice")) {
                mType = choice.toString();
            } else if (choiceType.equals("option")) {
                mRole = choice.toString();
            }
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("mType", mType));
            data.add(new Pair<String, Object>("mRole", mRole));
        }

    }

    public static class ChooseObserver extends Action {

        String observer = "measure";

        ChooseObserver(AnnotationWorkflow workflow, boolean forData) {
            super(workflow, "Choose the observer you will use to interpret the data resulting from observation", CHOOSE_OBSERVER_DESCRIPTION);
        }

        @Override
        public Object[] getChoices() {
            return new String[] {
                    "#measure",
                    "#rank",
                    "#classify",
                    "#count",
                    "#ratio",
                    "#presence",
                    "#percentage",
                    "#proportion",
                    "#value"
            };
        }

        @Override
        public AnnotationStep action() {
            return new ChoosePrimaryObservable(_workflow, observer);
        }

        @Override
        public String[] getOptions() {
            return null;
        }

        @Override
        public Filter getFilter() {
            return _qmatch;
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {
            observer = choice.toString();
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("observer", observer));
        }

    }

    public static class ChooseSubjectType extends Action {

        String stype = "Subject";

        ChooseSubjectType(AnnotationWorkflow workflow) {
            super(workflow, "Choose the fundamental type of subject observed", CHOOSE_SUBJECT_TYPE_DESCRIPTION);
        }

        @Override
        public Object[] getChoices() {
            return new String[] {
                    "#Subject",
                    "#Process",
                    "#Event",
            };
        }

        @Override
        public AnnotationStep action() {
            return new ChoosePrimaryObservable(_workflow, stype);
        }

        @Override
        public String[] getOptions() {
            return null;
        }

        @Override
        public Filter getFilter() {
            return _qmatch;
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {
            stype = choice.toString();
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("stype", stype));
        }

    }

    public static class ChoosePrimaryObservable extends Action {

        String   forwhat;

        IConcept primary;

        ChoosePrimaryObservable(AnnotationWorkflow workflow, String forWhat) {
            super(workflow, "Choose the primary observable concept", CHOOSE_OBSERVER_DESCRIPTION);
            forwhat = forWhat;
        }

        @Override
        public Object[] getChoices() {
            return new String[] {
                    "#Search for a concrete type",
                    "#Build from abstract + identity",
            };
        }

        @Override
        public AnnotationStep action() {
            return new CheckForTraits(_workflow, primary);
        }

        @Override
        public String[] getOptions() {
            return null;
        }

        @Override
        public Filter getFilter() {
            return _qmatch;
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {
            if (choice instanceof IConcept) {
                if (NS.isTrait((IConcept) choice)) {

                }
            } else if (choice.toString().startsWith("Search")) {

            } else if (choice.toString().startsWith("Build")) {

            }
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("primary", primary));
        }

    }

    public static class CheckForTraits extends Action {

        String observer = "measure";

        CheckForTraits(AnnotationWorkflow workflow, IConcept which) {
            super(workflow, "Select any attribute traits for " + which + "; continue when done", CHOOSE_OBSERVER_DESCRIPTION);
        }

        @Override
        public Object[] getChoices() {
            return new String[] {
                    "?Trait concept: ",
            };
        }

        @Override
        public AnnotationStep action() {
            return null;
        }

        @Override
        public String[] getOptions() {
            return null;
        }

        @Override
        public Filter getFilter() {
            return _qmatch;
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {
            observer = choice.toString();
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("observer", observer));
        }

    }

    public static class ChooseTraits extends Action {

        String observer = "measure";

        ChooseTraits(AnnotationWorkflow workflow, boolean forData) {
            super(workflow, "Choose the observer you will use to interpret the data resulting from observation", CHOOSE_OBSERVER_DESCRIPTION);
        }

        @Override
        public Object[] getChoices() {
            return new String[] {
                    "#measure",
                    "#rank",
                    "#classify",
                    "#count",
                    "#ratio",
                    "#presence",
                    "#percentage",
                    "#proportion",
                    "#value"
            };
        }

        @Override
        public AnnotationStep action() {
            return null;
        }

        @Override
        public String[] getOptions() {
            return null;
        }

        @Override
        public Filter getFilter() {
            return _qmatch;
        }

        @Override
        protected AnnotationStep handleChoice(Object choice, String choiceType) {
            observer = choice.toString();
            return null;
        }

        @Override
        public void collectData(List<Pair<String, Object>> data) {
            data.add(new Pair<String, Object>("observer", observer));
        }

    }

    public static final String BEGIN_MODEL_DESCRIPTION         = "Please choose the type of model you want to annotate.\n\n"
                                                                       + "A <b>quality model with data source</b> is for a constant value or for a data source that "
                                                                       + "produces an indirect observation, like a measurement or classification. It can "
                                                                       + "also be producing data from an object source like a vector file, if you are "
                                                                       + "using an attribute. Data models annotate values that are usually numbers or categories,"
                                                                       + "and require you to choose an <b>observer</b>, for example a measurement or ranking, "
                                                                       + "later on.\n\n"
                                                                       + "A <b>subject model with object source</b> annotates direct observations, like a"
                                                                       + " vector file of roads or countries. You use this when you want to produce the "
                                                                       + "subjects themselves, not an attribute of them, like the land cover type. The "
                                                                       + "observable for this model will be a <b>subject</b>; it can also be a <b>process</b>"
                                                                       + " or, less commonly, an <b>event</b>\n\n"
                                                                       + "The <b>computed</b> version of the same models is for when you want to specify a way to"
                                                                       + " create qualities or subjects based on the value of other observations, by computing them"
                                                                       + " directly (for example using an equation) or having them computed by an external function "
                                                                       + "(for example a Bayesian network).\n\n"
                                                                       + "You can search for concepts in the search bar on the left, but it won't have any effect on the "
                                                                       + "results for now. As you define the observable, your choice of concepts in the search "
                                                                       + "will be restricted to results compatible with your choices.";

    public static final String CHOOSE_OBSERVER_DESCRIPTION     = "You will be annotating a quality, whose states are expressed by data values such as numbers or categories. "
                                                                       + "Please choose the type of observer you will use to annotate the data.\n\n"
                                                                       + "A <b>quality model with data source</b> is for a constant value or for a data source that "
                                                                       + "produces an indirect observation, like a measurement or classification. It can "
                                                                       + "also be producing data from an object source like a vector file, if you are "
                                                                       + "using an attribute. Data models annotate values that are usually numbers or categories,"
                                                                       + "and require you to choose an <b>observer</b>, for example a measurement or ranking, "
                                                                       + "later on.\n\n"
                                                                       + "A <b>subject model with object source</b> annotates direct observations, like a"
                                                                       + " vector file of roads or countries. You use this when you want to produce the "
                                                                       + "subjects themselves, not an attribute of them, like the land cover type. The "
                                                                       + "observable for this model will be a <b>subject</b>; it can also be a <b>process</b>"
                                                                       + " or, less commonly, an <b>event</b>\n\n"
                                                                       + "The <b>computed</b> version of the same models is for when you want to specify a way to"
                                                                       + " create qualities or subjects based on the value of other observations, by computing them"
                                                                       + " directly (for example using an equation) or having them computed by an external function "
                                                                       + "(for example a Bayesian network).\n\n"
                                                                       + "You can search for concepts in the search bar on the left, but it won't have any effect on the "
                                                                       + "results for now. As you define the observable, your choice of concepts in the search "
                                                                       + "will be restricted to results compatible with your choices.";

    public static final String CHOOSE_SUBJECT_TYPE_DESCRIPTION =
                                                                       "You will be annotating a quality, whose states are expressed by data values such as numbers or categories. "
                                                                               + "Please choose the type of observer you will use to annotate the data.\n\n"
                                                                               + "A <b>quality model with data source</b> is for a constant value or for a data source that "
                                                                               + "produces an indirect observation, like a measurement or classification. It can "
                                                                               + "also be producing data from an object source like a vector file, if you are "
                                                                               + "using an attribute. Data models annotate values that are usually numbers or categories,"
                                                                               + "and require you to choose an <b>observer</b>, for example a measurement or ranking, "
                                                                               + "later on.\n\n"
                                                                               + "A <b>subject model with object source</b> annotates direct observations, like a"
                                                                               + " vector file of roads or countries. You use this when you want to produce the "
                                                                               + "subjects themselves, not an attribute of them, like the land cover type. The "
                                                                               + "observable for this model will be a <b>subject</b>; it can also be a <b>process</b>"
                                                                               + " or, less commonly, an <b>event</b>\n\n"
                                                                               + "The <b>computed</b> version of the same models is for when you want to specify a way to"
                                                                               + " create qualities or subjects based on the value of other observations, by computing them"
                                                                               + " directly (for example using an equation) or having them computed by an external function "
                                                                               + "(for example a Bayesian network).\n\n"
                                                                               + "You can search for concepts in the search bar on the left, but it won't have any effect on the "
                                                                               + "results for now. As you define the observable, your choice of concepts in the search "
                                                                               + "will be restricted to results compatible with your choices.";

}
