/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.util.ArrayList;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.interfaces.IThinklabConcept;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabNamespace;

/**
 * Represents a concept; if that comes from a definition in the language, we store that and
 * give it the line numbers it deserves.
 * 
 * @author Ferd
 *
 */
public class ThinklabConcept extends ThinklabModelObject implements IThinklabConcept {

    private IKnowledgeObject concept;

    public ThinklabConcept(IKnowledgeObject o, IThinklabElement parent) {
        this.concept = o;
        this.parent = parent;
        INamespace ns = ((IThinklabNamespace) (getAncestor(THINKLAB_NAMESPACE))).getNamespace();
        // conceptDefinition = (IKnowledgeObject) ns.getModelObject(concept.getLocalName());
        // if (conceptDefinition != null) {
        this._startLine = concept.getFirstLineNumber();
        this._endLine = concept.getLastLineNumber();
        // }
    }

    @Override
    public int getElementType() {
        return THINKLAB_CONCEPT;
    }

    @Override
    public String getElementName() {
        return concept.getType().getLocalName();
    }

    @Override
    public Object[] getChildren() {
        ArrayList<IThinklabElement> ret = new ArrayList<IThinklabElement>();
        for (IModelObject c : concept.getChildren()) {
            if (c instanceof IKnowledgeObject) {
                ret.add(new ThinklabConcept((IKnowledgeObject) c, this));
            }
        }
        return ret.toArray();
    }

    @Override
    public boolean hasChildren() {
        return getChildren().length > 0;
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinkcap.interfaces.IThinklabElement#getElementDescription()
     */
    @Override
    public String getElementDescription() {
        return this.concept.toString();
    }

    @Override
    public IConcept getConcept() {
        return concept.getConcept();
    }

    @Override
    public String getUniqueID() {
        return this.concept + "$concept";
    }

    @Override
    public IModelObject getModelObject() {
        return concept;
    }

    @Override
    public boolean isNothing() {
        return NS.isNothing(concept);
    }

}
