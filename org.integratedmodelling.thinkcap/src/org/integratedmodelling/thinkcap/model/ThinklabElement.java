/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import org.eclipse.core.runtime.PlatformObject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabWorkspace;

public abstract class ThinklabElement 
	extends PlatformObject implements IThinklabElement {
	
	// set appropriately in implementing classes - may be null.
	protected IThinklabElement parent;

	@Override
	public String getElementName() {
		return "";
	}
	
	@Override
	public IThinklabElement getAncestor(int ancestorType) {
		IThinklabElement element = this;
		while (element != null) {
			if (element.getElementType() == ancestorType)  
				return element;
			element= element.getParent();
		}
		return null;
	}

	@Override
	public IThinklabWorkspace getThinklabWorkspace() {
		IThinklabElement current = this;
		do {
			if (current instanceof IThinklabWorkspace) return (IThinklabWorkspace) current;
		} while ((current = current.getParent()) != null);
		return null;
	}
	
	@Override
	public IThinklabProject getThinklabProject() {
		IThinklabElement current = this;
		do {
			if (current instanceof IThinklabProject) return (IThinklabProject) current;
		} while ((current = current.getParent()) != null);
		return null;
	}
	
	@Override
	public IThinklabElement getParent() {
		return parent;
	}
	
	public int hashCode() {
		return getUniqueID().hashCode();
	}
	
	public boolean equals(Object o) {
		return 
				o instanceof ThinklabElement && 
				getUniqueID().equals(((ThinklabElement)o).getUniqueID());
	}
	
	/*
	 * ID returned by this one must uniquely represent the name of the the Thinklab object
	 * wrapped by this. There must be no duplicates across the model, but modifying the
	 * object (except for its name) should not change the ID.
	 */
	public abstract String getUniqueID();
	
	
	
}
