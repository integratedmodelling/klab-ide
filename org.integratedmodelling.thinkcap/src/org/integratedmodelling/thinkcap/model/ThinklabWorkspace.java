/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabWorkspace;

/**
 * Cache of active Thinklab project in a workspace. Can be retrieved from IThinklabElement.getThinklabModel()
 * @author Ferd
 *
 */
public class ThinklabWorkspace extends ThinklabElement implements IThinklabWorkspace {

    private IWorkspaceRoot workspace;

    /**
     * if true, children are arranged in a flat, sorted hierarchy. If not, the model is
     * fully hierarchical and different children may represent the same object if multiple
     * inheritances are seen. This affects the behavior of getChildren() for most elements.
     */
    protected boolean      isFlat = true;

    public ThinklabWorkspace(IWorkspaceRoot adaptableObject) {
        this.workspace = adaptableObject;
    }

    @Override
    public IThinklabWorkspace getThinklabWorkspace() {
        return this;
    }

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public int getElementType() {
        return THINKLAB_WORKSPACE;
    }

    /**
     * Returns the workbench associated with this object.
     */
    public IWorkspaceRoot getWorkspace() {
        return this.workspace;
    }

    @Override
    public IThinklabProject getThinklabProject() {
        return null;
    }

    @Override
    public IPath getPath() {
        return Path.ROOT;
    }

    @Override
    public IResource getResource() {
        return ResourcesPlugin.getWorkspace().getRoot();
    }

    @Override
    public IThinklabElement[] getChildren() {

        ArrayList<IThinklabElement> pret = new ArrayList<IThinklabElement>();
        // FIXME this stuff should be handled uniformly using the new IWorkspace

        if (KLAB.PMANAGER != null) {
            for (IProject p : workspace.getProjects()) {
                if (ThinklabModelManager.get().isThinklabProject(p)
                        && ProjectManager.isOpen(p.getLocation().toFile())) {
                    IThinklabElement proj = ThinklabModelManager.get().getProject(p, this);
                    if (proj != null) {
                        pret.add(proj);
                    }
                }
            }
        }

        return pret.toArray(new IThinklabProject[pret.size()]);
    }

    @Override
    public boolean hasChildren() {
        for (IProject p : workspace.getProjects()) {
            if (ThinklabModelManager.get().isThinklabProject(p)
                    && ProjectManager.isOpen(p.getLocation().toFile())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(IResource resource) {
        return workspace.getResourceAttributes() != null;
    }

    @Override
    public IResource getResourceForURL(String resourceUrl) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinkcap.interfaces.IThinklabElement#getElementDescription()
     */
    @Override
    public String getElementDescription() {
        return "";
    }

    @Override
    public String getUniqueID() {
        return "workspace$workspace";
    }

}
