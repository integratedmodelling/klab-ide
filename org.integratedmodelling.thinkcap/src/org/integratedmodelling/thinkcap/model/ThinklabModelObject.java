/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModelObject;

public abstract class ThinklabModelObject extends ThinklabElement implements IThinklabModelObject {

    static int __mo = 0;

    int next() {
        return __mo++;
    }

    int        _startLine, _endLine;
    public int order = next();

    @Override
    public int getStartLine() {
        return _startLine;
    }

    @Override
    public int getEndLine() {
        return _endLine;
    }

    @Override
    public boolean exists() {
        // TODO check what to do - line numbers etc for adapting to resource fragment
        return false;
    }

    @Override
    public IPath getPath() {
        /*
         * FIXME this should be left implemented but the AdapterFactory must be fixed first, otherwise
         * it will adapt each model object to a file.
         */
        return null; // getAncestor(THINKLAB_NAMESPACE).getPath();
    }

    @Override
    public IResource getResource() {
        /*
         * FIXME this should be left implemented but the AdapterFactory must be fixed first, otherwise
         * it will adapt each model object to a file.
         */
        return null; // getAncestor(THINKLAB_NAMESPACE).getResource();
    }

    public static IThinklabElement getModelObject(IModelObject o, IThinklabElement thinklabNamespace) {

        ThinklabModelObject ret = null;

        if (o instanceof IModel) {
            ret = new ThinklabModel((IModel) o, thinklabNamespace);
        } else if (o instanceof IDirectObserver) {
            ret = new ThinklabSubjectGenerator((IDirectObserver) o, thinklabNamespace);
        } else if (o instanceof IKnowledgeObject) {
            if (((IKnowledgeObject) o).isConcept()) {
                ret = new ThinklabConcept((IKnowledgeObject) o, thinklabNamespace);
//            } else if (((IKnowledgeObject) o).isProperty()) {
//                ret = new ThinklabProperty((IKnowledgeObject) o, thinklabNamespace);
            }
        } /* TODO defines */

        if (ret != null) {

            ret._startLine = o.getFirstLineNumber();
            ret._endLine = o.getLastLineNumber();

            /*
             * TODO metadata -> properties
             */
        }

        return ret;
    }

}
