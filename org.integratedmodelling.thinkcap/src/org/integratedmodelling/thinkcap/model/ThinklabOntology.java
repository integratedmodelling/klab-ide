/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.thinkcap.model;
//
// import java.util.ArrayList;
// import java.util.List;
//
// import org.eclipse.core.resources.IResource;
// import org.eclipse.core.runtime.IPath;
// import org.integratedmodelling.api.knowledge.IConcept;
// import org.integratedmodelling.api.knowledge.IOntology;
// import org.integratedmodelling.api.modelling.IModelObject;
// import org.integratedmodelling.common.owl.Ontology;
// import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
// import org.integratedmodelling.thinkcap.interfaces.IThinklabOntology;
//
// public class ThinklabOntology extends ThinklabElement implements IThinklabOntology {
//
// IOntology ontology;
//
// /* (non-Javadoc)
// * @see org.integratedmodelling.thinkcap.model.ThinklabElement#getElementName()
// */
// @Override
// public String getElementName() {
// return "Ontology";
// }
//
// public ThinklabOntology(IOntology ontology, ThinklabNamespace namespace) {
// this.ontology = ontology;
// this.parent = namespace;
// }
//
// @Override
// public IOntology getOntology() {
// return ontology;
// }
//
// public static IThinklabElement getThinklabOntology(IOntology ontology, ThinklabNamespace namespace) {
// return new ThinklabOntology(ontology, namespace);
// }
//
// @Override
// public int getStartLine() {
// return 0;
// }
//
// @Override
// public int getEndLine() {
// return 0;
// }
//
// @Override
// public boolean exists() {
// return false;
// }
//
// @Override
// public int getElementType() {
// return THINKLAB_ONTOLOGY;
// }
//
// @Override
// public IPath getPath() {
// return null;
// }
//
// @Override
// public IResource getResource() {
// return null;
// }
//
// @Override
// public Object[] getChildren() {
// ArrayList<IThinklabElement> ret = new ArrayList<IThinklabElement>();
// appendConcepts(ontology, ret, this);
// return ret.toArray();
// }
//
// @Override
// public boolean hasChildren() {
// return ((Ontology) ontology).getTopConcepts(true).size() > 0;
// }
//
// @Override
// public String getElementDescription() {
// return "Ontology: " + ontology.getConceptSpace();
// }
//
// @Override
// public String getUniqueID() {
// return ontology.getConceptSpace() + "$ontology";
// }
//
// /**
// * Append the concept hierarchy from the passed ontology to the given array.
// *
// * @param ontology2
// * @param ret
// */
// public static void appendConcepts(IOntology ontology, List<IThinklabElement> ret, IThinklabElement parent)
// {
//
// for (IConcept c : ((Ontology) ontology).getTopConcepts(true)) {
// ret.add(new ThinklabConcept(c, parent));
// }
// }
//
// @Override
// public IModelObject getModelObject() {
// // TODO Auto-generated method stub
// return null;
// }
//
// }
