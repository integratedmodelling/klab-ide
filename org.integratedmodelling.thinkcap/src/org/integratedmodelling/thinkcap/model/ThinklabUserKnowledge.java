/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.util.ArrayList;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabUserKnowledge;

public class ThinklabUserKnowledge extends ThinklabElement implements IThinklabUserKnowledge {

    INamespace namespace;

    public ThinklabUserKnowledge(INamespace namespace, ThinklabProject thinklabProject) {
        this.namespace = namespace;
        this.parent = thinklabProject;
    }

    @Override
    public INamespace getNamespace() {
        return namespace;
    }

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public int getElementType() {
        return THINKLAB_NAMESPACE;
    }

    @Override
    public String getElementName() {
        return "Project Knowledge";
    }

    @Override
    public IPath getPath() {
        return getResource().getFullPath();
    }

    @Override
    public IResource getResource() {
        return Eclipse.getNamespaceIFile(namespace.getId());
    }

    @Override
    public IThinklabElement[] getChildren() {

        ArrayList<IThinklabElement> ret = new ArrayList<>();

        for (IModelObject o : getNamespace().getModelObjects()) {
            if (o.isFirstClass()) {
                IThinklabElement mo = ThinklabModelObject.getModelObject(o, this);
                if (mo == null) {
                    // the various K/nothing: shouldn't happen after properties are implemented
                    continue;
                }
                ret.add(mo);
            }
        }
        return ret.toArray(new IThinklabElement[ret.size()]);
    }

    @Override
    public boolean hasChildren() {

        IOntology ont = getNamespace().getOntology();
        return getNamespace().getModelObjects().size() > 0
                || (ont != null && (ont.getConceptCount() > 0 || ont.getPropertyCount() > 0));
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinkcap.interfaces.IThinklabElement#getElementDescription()
     */
    @Override
    public String getElementDescription() {
        return getElementName();
    }

    @Override
    public String getUniqueID() {
        return this.namespace + "$namespace";
    }

}
