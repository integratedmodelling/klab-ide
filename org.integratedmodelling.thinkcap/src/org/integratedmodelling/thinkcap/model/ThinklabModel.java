/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModel;

public class ThinklabModel extends ThinklabModelObject implements IThinklabModel {

    private IModel model;

    public ThinklabModel(IModel o, IThinklabElement thinklabNamespace) {
        this.model = o;
        this.parent = thinklabNamespace;
    }

    @Override
    public int getElementType() {
        return THINKLAB_MODEL;
    }

    @Override
    public String getElementName() {
        return model.getId();
    }

    @Override
    public Object[] getChildren() {
        /*
         * TODO this should become the default method, only redefined in namespaces.
         */
//        if (model.getChildren().size() > 0) {
//            ArrayList<Object> ret = new ArrayList<>();
//            for (IModelObject o : model.getChildren()) {
//                ret.add(getModelObject(o, this));
//            }
//            return ret.toArray();
//        }
        return NO_CHILDREN;
    }

    @Override
    public boolean hasChildren() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getElementDescription() {
        return this.model.getName();
    }

    @Override
    public boolean isAgent() {
        return this.model.getObserver() == null;
    }

    @Override
    public IModel getModel() {
        return this.model;
    }

    @Override
    public String getUniqueID() {
        return this.model.getName() + "$model";
    }

    @Override
    public IModelObject getModelObject() {
        return this.model;
    }

}
