package org.integratedmodelling.thinkcap.model;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.common.beans.DirectObservation;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Eclipse;

public class ShapeRecord {

    public ShapeRecord(String shapeDef, IConcept concept) {
        this.setShape(shapeDef);
        this.setConcept(concept);
    }

    public ShapeRecord(IObservationMetadata omd) {
        setName(omd.getId());
        setFqName(omd.getName());
        setConcept(omd.getConcept());
        stored = true;
    }

    private String   shape;
    public boolean   publish;
    private String   fqName;
    private String   name;
    private IConcept concept;
    public boolean   stored;

    /**
     * The endorsed method (use Observations class to convert to whatever necessary
     * afterwards).
     * 
     * @return
     */
    public DirectObservation makeObservationBean() {
        return null;
    }

    public IDirectObserver makeSubjectObserver() {
        if (!publish) {
            return null;
        }
        try {
            return ModelFactory
                    .createDirectObserver(getConcept(), getName(), KLAB.MMANAGER
                            .getLocalNamespace(), KLAB.ENGINE
                                    .getMonitor(), getShape());
        } catch (KlabException e) {
            Eclipse.handleException(e);
        }
        return null;
    }

    public String getKIM() {
        return "observe " + getConcept() + " named " + getName() + "\n   " +
                "over space (shape=\"EPSG:4326 " + getShape() + "\")\n;";
    }

    public boolean isKnowledge() {
        return getName() != null && KLAB.MMANAGER.findModelObject(getName()) != null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public IConcept getConcept() {
        return concept;
    }

    public void setConcept(IConcept concept) {
        /*
         * TODO if this is a role, we need to infer the object type and ensure the
         * role will be added to it.
         */
        this.concept = concept;
    }

    public String getFqName() {
        return fqName;
    }

    public void setFqName(String fqName) {
        this.fqName = fqName;
    }
}
