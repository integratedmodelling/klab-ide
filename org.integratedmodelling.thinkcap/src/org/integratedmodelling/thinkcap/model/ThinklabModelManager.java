/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.integratedmodelling.api.factories.IProjectFactory;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.EclipseWorkspace;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;

public class ThinklabModelManager {

    private static ThinklabModelManager _this       = null;
    private static final Object[]       NO_CHILDREN = new Object[0];

    ThinklabWorkspace _root;
    IProjectFactory   _pfactory = KLAB.PMANAGER;

    ThinklabWorkspace getRoot() {
        if (_root == null) {
            _root = new ThinklabWorkspace(ResourcesPlugin.getWorkspace().getRoot());
        }
        return _root;
    }

    /*
     * TODO twisted call logics - harmless, but straighten eventually
     */
    public Object[] getChildren(Object object) {

        if (object instanceof IWorkspaceRoot) {

            IWorkspaceRoot wroot = (IWorkspaceRoot) object;
            _root = new ThinklabWorkspace(wroot);
            return _root.getChildren();
        }

        return NO_CHILDREN;
    }

    public static ThinklabModelManager get() {
        if (_this == null) {
            _this = new ThinklabModelManager();
        }
        return _this;
    }

    public IThinklabProject addProject(String src) {

        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IProject eproj = root.getProject(src);

        try {
            eproj.create(null);
            eproj.open(null);

            org.integratedmodelling.api.project.IProject proj = EclipseWorkspace.addProject(src);

            /*
             * add thinklab nature; forget about XText for now
             */
            requireNature(eproj, "org.integratedmodelling.thinkcap.thinklab");

            // Client.get().fireEvent(new ProjectModifiedEvent(proj));

            return new ThinklabProject(eproj, _root);

        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }
        return null;
    }

    public void requireNature(IProject project, String nature) {

        boolean hasNature;
        try {
            hasNature = project.hasNature(nature);
            if (!hasNature) {
                IProjectDescription desc = project.getDescription();
                String[] natures = new String[desc.getNatureIds().length + 1];
                int i = 0;
                for (String s : desc.getNatureIds()) {
                    natures[i++] = s;
                }
                natures[i] = nature;
                desc.setNatureIds(natures);
                project.setDescription(desc, null);
            }
        } catch (CoreException e) {
            Eclipse.handleException(e);
        }
    }

    public static org.integratedmodelling.api.project.IProject getProjectForTarget(Object target) {

        org.integratedmodelling.api.project.IProject ret = null;

        if (target instanceof INamespace) {
            ret = ((INamespace) target).getProject();
        } else if (target instanceof IModelObject) {
            ret = ((IModelObject) target).getNamespace().getProject();
        } else if (target instanceof org.integratedmodelling.api.project.IProject) {
            ret = (org.integratedmodelling.api.project.IProject) target;
        }

        return ret;
    }

    public IThinklabProject getProject(String name) {
        try {
            return new ThinklabProject(getRoot().getWorkspace().getProject(name), getRoot());
        } catch (KlabException e) {
        }
        return null;
    }

    public boolean isThinklabProject(IProject p) {
        
        File file = p.getLocation().toFile();
        
        File zio = new File(file + File.separator + "META-INF" + File.separator + "klab.properties");
        if (!zio.exists()) {
            zio = new File(file + File.separator + "META-INF" + File.separator + "thinklab.properties");
        }
        return zio.exists();
    }

    public IThinklabElement getProject(IProject p, ThinklabWorkspace thinklabWorkspace) {

        if (KLAB.PMANAGER.getProject(p.getName()) == null) {
            try {
                KLAB.PMANAGER.registerProject(p.getLocation().toFile());
            } catch (KlabException e) {
                Activator.engine().getMonitor().error(e);
            }
        }
        return new ThinklabProject(p, ((ProjectManager) KLAB.PMANAGER)
                .getProject(p.getName()), thinklabWorkspace);
    }

    /**
     * Check if a modified resource's extension correspond to a change that can
     * affect the model.
     * 
     * @param fileExtension
     * @return true if extension is used for namespaces
     */
    public boolean isNamespaceFileExtension(String fileExtension) {
        return fileExtension != null && KIM.FILE_EXTENSIONS.contains(fileExtension);
    }

    /**
     * Return all Thinklab projects we manage.
     * 
     * @return all projects
     */
    public List<IThinklabProject> getProjects() {

        ArrayList<IThinklabProject> ret = new ArrayList<IThinklabProject>();
        for (Object o : getRoot().getChildren()) {
            ret.add((IThinklabProject) o);
        }
        return ret;

    }

}
