/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.ui.views.properties.IPropertySource;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.navigator.ThinklabPropertySource;

public class ThinklabAdapterFactory implements IAdapterFactory {

    @Override
    public Object getAdapter(Object adaptableObject, Class adapterType) {

        Object ret = null;

        if (adaptableObject instanceof IThinklabElement && adapterType == IPropertySource.class) {
            return new ThinklabPropertySource((IThinklabElement) adaptableObject);
        }

        // if (adaptableObject instanceof ISource && adapterType.equals(String.class)) {
        // ret = ((ISource) adaptableObject).getDefinitionPrototype("tql");
        // } else if (adaptableObject instanceof IThinklabSource && adapterType.equals(String.class)) {
        // ret = ((IThinklabSource) adaptableObject).getSource().getDefinitionPrototype("tql");
        // } else
        if (adaptableObject instanceof IWorkspaceRoot && adapterType.equals(IThinklabElement.class)) {
            ret = new ThinklabWorkspace((IWorkspaceRoot) adaptableObject);
        } /* else if (adaptableObject instanceof IThinklabProject
                && IResource.class.isAssignableFrom(IFile.class)) {
            ret = Eclipse.getWorkspaceRoot().getFile(((ThinklabProject) adaptableObject).getEclipseProject()
                    .getLocation());
          } */else if (adaptableObject instanceof IThinklabElement && adapterType.equals(IFile.class)) {
            IResource r = ((IThinklabElement) adaptableObject).getResource();
            if (r instanceof IFile)
                ret = r;
        } else if (adaptableObject instanceof IThinklabElement && adapterType.equals(IProject.class)) {
            IResource r = ((IThinklabElement) adaptableObject).getResource();
            if (r instanceof IProject)
                ret = r;
        } else if (adaptableObject instanceof IThinklabElement
                && IResource.class.isAssignableFrom(adapterType)) {
            ret = ((IThinklabElement) adaptableObject).getResource();
        } /* else if (adaptableObject instanceof IThinklabWorkspace
                && IResource.class.isAssignableFrom(IWorkspace.class)) {
            ret = Eclipse.getWorkspace();
          } else if (adaptableObject instanceof IThinklabWorkspace
                && IResource.class.isAssignableFrom(IWorkspaceRoot.class)) {
            ret = Eclipse.getWorkspaceRoot();
          } */

        // System.out.println(ret + " << adapt " + adaptableObject + " to " + adapterType);

        return ret;
    }

    @Override
    public Class[] getAdapterList() {
        return new Class[] {
                IFile.class,
                IProject.class,
                IResource.class,
                IThinklabElement.class // ,
        // IThinklabNamespace.class,
        // IThinklabOntology.class,
        // IThinklabConcept.class,
        // IThinklabModel.class,
        // IThinklabProject.class,
        // IThinklabSubjectGenerator.class,
        // IThinklabConnection.class,
        // ISource.class
        };
    }

}
