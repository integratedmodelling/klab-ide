/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabWorkspace;

public class ThinklabProject extends ThinklabElement implements IThinklabProject {

    IProject                                     project;
    org.integratedmodelling.api.project.IProject tproj;

    public ThinklabProject(IProject object, IThinklabWorkspace parent) throws KlabException {
        this.project = object;
        this.parent = parent;
        this.tproj = KLAB.PMANAGER.getProject(object.getName());
    }

    public ThinklabProject(IProject p,
            org.integratedmodelling.api.project.IProject prj,
            ThinklabWorkspace thinklabWorkspace) {
        this.project = p;
        this.parent = thinklabWorkspace;
        this.tproj = prj;
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public int getElementType() {
        return THINKLAB_PROJECT;
    }

    @Override
    public String getElementName() {
        return project.getName();
    }

    @Override
    public IPath getPath() {
        return this.project.getFullPath();
    }

    @Override
    public IResource getResource() {
        return this.project;
    }

    @Override
    public Object[] getChildren() {

        ArrayList<Object> ret = new ArrayList<>();
        String ukid = null;
        INamespace uk = tproj.getUserKnowledge();
        if (uk != null) {
            ukid = uk.getId();
            ret.add(new ThinklabUserKnowledge(uk, this));
        }

        // namespaces
        for (INamespace n : tproj.getNamespaces()) {
            if (ukid == null || !n.getId().equals(ukid)) {
                ret.add(new ThinklabNamespace(n.getId(), this));
            }
        }

        for (File f : tproj.getScripts()) {
            ret.add(new ThinklabScript(f, this));
        }

        // non-managed resource folders
        for (String f : tproj.getUserResourceFolders()) {
            ret.add(project.getFolder(f));
        }

        return ret.toArray(new Object[ret.size()]);
    }

    @Override
    public boolean hasChildren() {
        return tproj.getNamespaces().size() > 0 ||
                tproj.getUserResourceFolders().size() > 0 ||
                tproj.getScripts().size() > 0;
    }

    @Override
    public org.integratedmodelling.api.project.IProject getTLProject() {
        return this.tproj;
    }

    @Override
    public IProject getEclipseProject() {
        return this.project;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.integratedmodelling.thinkcap.interfaces.IThinklabElement#getElementDescription(
     * )
     */
    @Override
    public String getElementDescription() {
        return getElementName();
    }

    @Override
    public String getUniqueID() {
        return this.project.getName() + "$project";
    }

}
