/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.io.File;

import org.eclipse.core.resources.IResource;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.thinkcap.interfaces.IThinklabScript;

public class ThinklabScript extends ThinklabModelObject implements IThinklabScript {

    private File source;

    public ThinklabScript(File f, ThinklabProject thinklabProject) {
        this.parent = thinklabProject;
        source = f;
    }

    @Override
    public IResource getResource() {
        String rpath = getElementName() + ".groovy";
        ThinklabProject p = (ThinklabProject) getAncestor(THINKLAB_PROJECT);
        rpath = ".scripts/" + rpath;
        return p.project.getFile(rpath);
    }

    @Override
    public int getElementType() {
        return THINKLAB_DATA_SOURCE;
    }

    @Override
    public String getElementName() {
        return MiscUtilities.getFileBaseName(source.toString());
    }

    @Override
    public Object[] getChildren() {
        return NO_CHILDREN;
    }

    @Override
    public boolean hasChildren() {
        return false;
    }

    @Override
    public String getElementDescription() {
        return getElementName();
    }

    @Override
    public String getUniqueID() {
        return this.source.getName() + "$script";
    }

    @Override
    public IModelObject getModelObject() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public File getScriptFile() {
        return source;
    }

}
