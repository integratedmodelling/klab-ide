/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.model;

import java.util.ArrayList;

import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabSubjectGenerator;

public class ThinklabSubjectGenerator extends ThinklabModelObject implements IThinklabSubjectGenerator {

    private IDirectObserver context;

    public ThinklabSubjectGenerator(IDirectObserver o, IThinklabElement thinklabNamespace) {
        this.context = o;
        this.parent = thinklabNamespace;
    }

    @Override
    public String getElementName() {
        return context.getId();
    }

    @Override
    public int getElementType() {
        return THINKLAB_CONTEXT;
    }

    @Override
    public Object[] getChildren() {
        if (context.getChildrenDefinition().size() > 0) {
            ArrayList<Object> ret = new ArrayList<>();
            for (IModelObject o : context.getChildrenDefinition()) {
                ret.add(getModelObject(o, this));
            }
            return ret.toArray();
        }
        return NO_CHILDREN;
    }

    @Override
    public boolean hasChildren() {
        return context.getChildrenDefinition().size() > 0;
    }

    @Override
    public String getElementDescription() {
        return this.context.getName();
    }

    @Override
    public IDirectObserver getSubjectGenerator() {
        return this.context;
    }

    @Override
    public String getUniqueID() {
        return this.context.getName() + "$context";
    }

    @Override
    public IModelObject getModelObject() {
        return context;
    }

}
