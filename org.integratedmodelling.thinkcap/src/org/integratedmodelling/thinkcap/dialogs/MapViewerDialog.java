/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.thinkcap.Activator;

public class MapViewerDialog extends TitleAreaDialog {

    private TableViewer tableViewer;
    private Table       dataTable;
    private Map<?, ?>   data;
    private String resourceId;

    class Component {
        File       directory;
        String     id;
        Properties properties;

        public boolean ok() {
            return properties != null;
        }

        Component(File file) {
            this.properties = MiscUtilities
                    .readProperties(new File(file + File.separator + "src" + File.separator + "main"
                            + File.separator
                            + "resources" + File.separator + "META-INF" + File.separator
                            + "klab.properties"));
            if (this.properties != null) {
                this.id = this.properties
                        .getProperty(org.integratedmodelling.common.project.Component.COMPONENT_ID_PROPERTY);
                if (this.id == null) {
                    this.properties = null;
                }
                this.directory = file;
            }
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Component other = (Component) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (id == null) {
                if (other.id != null)
                    return false;
            } else if (!id.equals(other.id))
                return false;
            return true;
        }

        private MapViewerDialog getOuterType() {
            return MapViewerDialog.this;
        }

    }

    class ComponentLabelProvider extends LabelProvider implements ITableLabelProvider {

        @Override
        public Image getImage(Object element) {

            if (element instanceof Component) {
                ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/Plugin.png");
            }
            return null;
        }

        @Override
        public String getText(Object element) {
            return null;
        }

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {
            if (element instanceof Entry) {
                if (columnIndex == 0) {
                    return ((Entry<?, ?>) element).getKey().toString();
                }
                return ((Entry<?, ?>) element).getValue().toString();
            }
            return null;
        }

    }

    class ComponentContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof Map) {
                return data.entrySet().toArray();
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof Entry) {
                return data;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return element instanceof Map && data.size() > 1;
        }

    }

    /**
     * Create the dialog.
     * 
     * @param parent
     */
    public MapViewerDialog(Shell parent, String resourceId, Map<?, ?> data) {
        super(parent);
        this.data = data;
        this.resourceId = resourceId;
    }

    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("Data viewer");
    }

    /**
     * Create contents of the dialog.
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        setTitleImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/logo_white_64.jpg"));

        Composite fileName = (Composite) super.createDialogArea(parent);
        fileName.setLayout(new GridLayout(1, false));
        tableViewer = new TableViewer(fileName, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
        dataTable = tableViewer.getTable();
        dataTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        dataTable.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // removeButton.setEnabled(componentTable.getSelection().length > 0);
            }
        });
        dataTable.setLinesVisible(true);
        dataTable.setHeaderVisible(true);
        TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn tblclmnNewColumn = tableViewerColumn.getColumn();
        tblclmnNewColumn.setWidth(220);
        tblclmnNewColumn.setText("Field");

        TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn tblclmnNewColumn_1 = tableViewerColumn_1.getColumn();
        tblclmnNewColumn_1.setWidth(220);
        tblclmnNewColumn_1.setText("Value");

        tableViewer.setContentProvider(new ComponentContentProvider());
        tableViewer.setLabelProvider(new ComponentLabelProvider());

        this.setTitle("Data viewer");
        this.setMessage("Showing data associated with " + resourceId);

        tableViewer.setInput(data);
        return fileName;
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        super.createButtonsForButtonBar(parent);
        getButton(IDialogConstants.OK_ID).setEnabled(true);
    }
}
