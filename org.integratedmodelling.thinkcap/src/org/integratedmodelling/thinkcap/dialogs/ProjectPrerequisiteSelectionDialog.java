/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.navigator.IDescriptionProvider;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.thinkcap.Activator;

public class ProjectPrerequisiteSelectionDialog extends ListSelectionDialog {

    static class ScenarioLabelProvider extends LabelProvider implements ILabelProvider, IDescriptionProvider {

        @Override
        public Image getImage(Object element) {

            if (element instanceof IProject) {
                if (((IProject)element).isRemote()) {
                    // project image, with Internal decoration if synchronized.
                    return ResourceManager.decorateImage(
                            ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/tl16.gif"),
                            ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "/icons/identity_decorator.gif"),
                            SWTResourceManager.TOP_RIGHT);
                } else {
                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/tl16.gif");
                }
            }

            return null;
        }

        @Override
        public String getText(Object element) {
            if (element instanceof IProject) {
                return ((IProject) element).getId();
            }
            return null;
        }

        @Override
        public String getDescription(Object anElement) {
            return null;
        }

    }

    static class ScenarioContentProvider implements ITreeContentProvider {

        IProject project;

        public ScenarioContentProvider(IProject project) {
            this.project = project;
        }

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof IProject
                    && project.getId().equals(((IProject) parentElement).getId())) {

                List<IProject> ret = new ArrayList<>();
                for (org.integratedmodelling.api.project.IProject p : KLAB.PMANAGER.getProjects()) {
                    if (!project.getId().equals(p.getId())) {
                        ret.add(p);
                    }
                }
                Collections.sort(ret, new Comparator<IProject>() {
                    @Override
                    public int compare(IProject arg0, IProject arg1) {
                        return arg0.getId().compareTo(arg1.getId());
                    }
                });
                return ret.toArray();
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof IProject
                    && !project.getId().equals(((IProject) element).getId())) {
                return project;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return element instanceof IProject && ((IProject) element).getId().equals(project.getId())
                    && KLAB.PMANAGER.getProjects().size() > 1;
        }
    }

    public ProjectPrerequisiteSelectionDialog(Shell parentShell, IProject project) {
        super(parentShell, project, new ScenarioContentProvider(project), new ScenarioLabelProvider(), "Select projects:");
        ArrayList<IProject> sel = new ArrayList<IProject>();
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            if (Arrays.asList(((Project) project).getPrerequisiteIds()).contains(p.getId())) {
                sel.add(p);
            }
        }
        setInitialElementSelections(sel);
        setTitle("Prerequisite projects selection");
    }

}
