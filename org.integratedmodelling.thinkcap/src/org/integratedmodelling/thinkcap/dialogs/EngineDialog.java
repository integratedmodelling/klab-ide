/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.auth.KlabCertificate;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.launcher.EngineLauncher;
import org.integratedmodelling.common.launcher.EngineLauncher.LaunchListener;
import org.integratedmodelling.common.network.NetworkedDistribution;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.thinkcap.Eclipse;

public class EngineDialog extends TitleAreaDialog {

    protected KlabCertificate     result;
    protected Shell               shell;
    private Text                  certFilePath;
    private Text                  dataPath;
    private Text                  localInstallationPath;
    private Text                  text_3;
    private Set<String>           groups;
    private Label                 lblOfficialDistribution;
    private Button                btnUseDeveloperNetwork;
    private Label                 lblLocalInstall;
    private Button                btnStartAutomatically;
    private Button                btnUpgradeAutomatically;
    private Button                btnStopOnExit;
    private Spinner               spinner;
    private Button                btnRunCleaningCycle;
    private Button                btnCleanModelCache;
    private Button                btnCleanWcswfsCache;
    private Button                btnCleanObservationDatabase;
    private Button                btnDropAllSessions;
    private EngineLauncher        launcher;
    private boolean               haveMaven;

    Integer                       lastDevBuild     = null;
    Integer                       lastBuild        = null;

    static final long             ENGINE_SIZE_HINT = 176160768l;
    protected static final String ENGINE_URL       = "http://www.integratedmodelling.org/downloads/kmodeler.jar";
    protected static final String DEV_ENGINE_URL   = "http://www.integratedmodelling.org/downloads/kmodeler_dev.jar";

    private Button                btnRememberSettings;
    private Button                btnDownload;
    private Button                browseMavenInstallButton;
    private Button                btnStopTheEngine;
    private Button                btnJavaDebugMode;
    private Text                  debugPort;
    private Label                 lblPort;
    private Button                btnKlabDebugMode;
    private Button                btnUseMavenInstallation;
    private Button                btnUseDownloadedDistribution;
    private Label                 localComponentChoose;
    private Label                 lblSelectLocalComponent;
    private Composite             composite;
    private Composite             composite_1;
    private Composite             composite_2;
    private Composite             composite_3;
    private Label                 lblNewLabel;
    private Label                 lblNewLabel_1;
    private Label                 lblNewLabel_2;

    /**
     * Create the dialog.
     * 
     * @param parent
     * @param launcher
     * @param groups
     */
    public EngineDialog(Shell parent, EngineLauncher launcher, Set<String> groups) {
        super(parent);
        setHelpAvailable(false);
        this.setBlockOnOpen(true);
        this.groups = groups;
        this.launcher = launcher;
    }

    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("Engine control panel");
    }

    /**
     * Create contents of the dialog.
     * 
     * @return
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        setTitleImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));

        Composite fileName = (Composite) super.createDialogArea(parent);
        GridLayout gridLayout = (GridLayout) fileName.getLayout();
        gridLayout.marginRight = 4;
        gridLayout.marginLeft = 4;
        fileName.setSize(450, 527);

        // GC gc = new GC(parent);
        // gc.setFont(parent.getFont());
        // FontMetrics fm = gc.getFontMetrics();
        // Point extent = gc.textExtent("M");

        // int hb = extent.y + 8;
        // int hm = extent.y + 2;
        // int wm = extent.x + 2;
        // int ht = extent.y + 6;

        Group grpEngineDistribution = new Group(fileName, SWT.NONE);
        grpEngineDistribution.setLayout(new GridLayout(6, false));
        GridData gd_grpEngineDistribution = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_grpEngineDistribution.widthHint = 543;
        gd_grpEngineDistribution.heightHint = 108;
        grpEngineDistribution.setLayoutData(gd_grpEngineDistribution);
        grpEngineDistribution.setText("Engine distribution");
        grpEngineDistribution.setBounds(10, 10, 424, 98);

        btnUseDownloadedDistribution = new Button(grpEngineDistribution, SWT.RADIO);
        btnUseDownloadedDistribution.setSelection(!launcher.isUseLocal());
        btnUseDownloadedDistribution.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setUseLocal(!btnUseDownloadedDistribution.getSelection());
                checkOk();
            }
        });

        lblOfficialDistribution = new Label(grpEngineDistribution, SWT.NONE);
        lblOfficialDistribution.setText("Official distribution");

        btnUseDeveloperNetwork = new Button(grpEngineDistribution, SWT.CHECK);
        btnUseDeveloperNetwork.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
        btnUseDeveloperNetwork.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setUseDeveloper(btnUseDeveloperNetwork.getSelection());
                checkOk();
            }
        });
        btnUseDeveloperNetwork.setEnabled(false);
        btnUseDeveloperNetwork.setSelection(launcher.isDevelop());
        btnUseDeveloperNetwork.setText("Use developer network");
        new Label(grpEngineDistribution, SWT.NONE);

        btnDownload = new Button(grpEngineDistribution, SWT.NONE);
        btnDownload.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        btnDownload.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                try {
                    boolean wasOnline = launcher.isOnline();
                    if (wasOnline) {
                        try {
                            new ProgressMonitorDialog(Eclipse.getShell())
                                    .run(true, true, new IRunnableWithProgress() {

                                        @Override
                                        public void run(IProgressMonitor monitor)
                                                throws InvocationTargetException, InterruptedException {
                                            monitor.beginTask("Stopping engine...", IProgressMonitor.UNKNOWN);
                                            launcher.shutdown(10);
                                            monitor.done();
                                        }
                                    });
                        } catch (InvocationTargetException | InterruptedException ex) {
                            Eclipse.handleException(ex);
                        }
                    }
                    if (!launcher.isOnline()) {
                        downloadFile((btnDownload.getText().equals("Upgrade") ? "Upgrading " : "Downloading")
                                + " engine distribution"
                                + (launcher.isUseDeveloper() ? " (development)"
                                        : ""), launcher, shell, new URL(launcher.isUseDeveloper()
                                                ? DEV_ENGINE_URL
                                                : ENGINE_URL), new File(KLAB.CONFIG.getDataPath("engine")
                                                        + File.separator
                                                        + (launcher.isUseDeveloper() ? "kmodeler_dev.jar"
                                                                : "kmodeler.jar")), ENGINE_SIZE_HINT, EngineDialog.this);
                    }
                    if (wasOnline) {
                        try {
                            new ProgressMonitorDialog(Eclipse.getShell())
                                    .run(true, true, new IRunnableWithProgress() {

                                        @Override
                                        public void run(IProgressMonitor monitor)
                                                throws InvocationTargetException, InterruptedException {
                                            monitor.beginTask("Starting engine...", IProgressMonitor.UNKNOWN);
                                            launcher.launch(true);
                                            monitor.done();
                                        }
                                    });
                        } catch (InvocationTargetException | InterruptedException ex) {
                            Eclipse.handleException(ex);
                        }
                    }
                } catch (MalformedURLException e1) {
                    Eclipse.handleException(e1);
                }
            }
        });
        btnDownload.setText("Download");

        btnUseMavenInstallation = new Button(grpEngineDistribution, SWT.RADIO);
        btnUseMavenInstallation.setSelection(launcher.isUseLocal());
        btnUseMavenInstallation.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                // if (!haveMaven) {
                // Eclipse.error("Cannot locate Maven on your machine: please install it
                // and/or set the MAVEN_HOME variable.");
                // } else {
                launcher.setUseLocal(btnUseMavenInstallation.getSelection());
                checkOk();
                // }
            }
        });

        lblLocalInstall = new Label(grpEngineDistribution, SWT.NONE);
        lblLocalInstall.setEnabled(false);
        lblLocalInstall.setText("Local installation");

        localInstallationPath = new Text(grpEngineDistribution, SWT.BORDER);
        localInstallationPath.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
        localInstallationPath.setEnabled(false);

        browseMavenInstallButton = new Button(grpEngineDistribution, SWT.NONE);
        browseMavenInstallButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        browseMavenInstallButton.setEnabled(false);
        browseMavenInstallButton.setGrayed(true);
        browseMavenInstallButton.setText("Browse");
        browseMavenInstallButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                DirectoryDialog dlg = new DirectoryDialog(fileName.getShell(), SWT.OPEN);
                dlg.setFilterPath(System.getProperty("user.home"));
                String fn = dlg.open();
                if (fn != null) {
                    String file = dlg.getFilterPath();
                    localInstallationPath.setText(file);
                    launcher.setMavenDirectory(new File(file));
                    checkOk();
                }
            }
        });

        composite = new Composite(grpEngineDistribution, SWT.NONE);
        GridLayout gl_composite = new GridLayout(4, false);
        gl_composite.horizontalSpacing = 6;
        composite.setLayout(gl_composite);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));

        btnStartAutomatically = new Button(composite, SWT.CHECK);
        btnStartAutomatically.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        btnStartAutomatically.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setLaunchAutomatically(btnStartAutomatically.getSelection());
                if (btnStartAutomatically.getSelection()) {
                    launcher.setUpgradeAutomatically(true);
                    btnUpgradeAutomatically.setSelection(true);
                }
                btnUpgradeAutomatically.setEnabled(!btnStartAutomatically.getSelection());
            }
        });
        btnStartAutomatically.setSelection(true);
        btnStartAutomatically.setText("Autostart");
        btnStartAutomatically.setSelection(launcher.isLaunchAutomatically());

        btnStopOnExit = new Button(composite, SWT.CHECK);
        btnStopOnExit.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        btnStopOnExit.setText("Stop on exit");
        btnStopOnExit.setSelection(launcher.isStopOnExit());
        // we don't really do this yet.
        btnStopOnExit.setEnabled(false);
        
        btnRememberSettings = new Button(composite, SWT.CHECK);
        btnRememberSettings.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        btnRememberSettings.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setRememberSettings(btnRememberSettings.getSelection());
            }
        });
        btnRememberSettings.setSelection(launcher.isRememberSettings());
        btnRememberSettings.setText("Save settings");

        btnUpgradeAutomatically = new Button(composite, SWT.CHECK);
        btnUpgradeAutomatically.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        btnUpgradeAutomatically.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setUpgradeAutomatically(btnUpgradeAutomatically.getSelection());
            }
        });
        btnUpgradeAutomatically.setText("Autoupgrade");
        btnUpgradeAutomatically.setSelection(launcher.isUpgradeAutomatically());
        btnUpgradeAutomatically.setEnabled(!launcher.isLaunchAutomatically());
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);
        // new Label(grpEngineDistribution, SWT.NONE);

        Group grpSettings = new Group(fileName, SWT.NONE);
        grpSettings.setLayout(new GridLayout(5, false));
        GridData gd_grpSettings = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        gd_grpSettings.widthHint = 543;
        gd_grpSettings.heightHint = 122;
        grpSettings.setLayoutData(gd_grpSettings);
        grpSettings.setText("Settings");
        grpSettings.setBounds(10, 134, 424, 126);

        Label label_1 = new Label(grpSettings, SWT.NONE);
        label_1.setText("RAM limit");
        int mem = launcher.getMemLimitMb();
        spinner = new Spinner(grpSettings, SWT.BORDER);
        spinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                launcher.setMemLimitMb(spinner.getSelection());
            }
        });
        spinner.setPageIncrement(1024);
        spinner.setMaximum(49152);
        spinner.setMinimum(2048);
        spinner.setIncrement(256);
        spinner.setSelection(mem);

        Label lblNetworkPort = new Label(grpSettings, SWT.NONE);
        lblNetworkPort.setText("Network port");

        text_3 = new Text(grpSettings, SWT.BORDER);
        GridData gd_text_3 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_text_3.widthHint = 80;
        text_3.setLayoutData(gd_text_3);
        text_3.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                launcher.setNetworkPort(Integer.parseInt(text_3.getText()));
            }
        });
        text_3.setText(launcher.getNetworkPort() + "");
        new Label(grpSettings, SWT.NONE);

        Label lblDataDir = new Label(grpSettings, SWT.NONE);
        lblDataDir.setText("Data dir:");

        dataPath = new Text(grpSettings, SWT.BORDER);
        dataPath.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));

        Button browseDataDirButton = new Button(grpSettings, SWT.NONE);
        browseDataDirButton.setText("Browse");
        browseDataDirButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                // User has selected to open multiple files
                DirectoryDialog dlg = new DirectoryDialog(fileName.getShell(), SWT.OPEN);
                dlg.setFilterPath(System.getProperty("user.home"));
                String fn = dlg.open();
                if (fn != null) {
                    String file = dlg.getFilterPath();
                    if (file.equals(KLAB.CONFIG.getDataPath().toString())) {
                        setErrorMessage("The engine data path cannot be the same as the IDE's.");
                    } else {
                        dataPath.setText(file);
                        launcher.setDataDir(new File(file));
                        checkOk();
                    }
                }
            }
        });

        Label lblCertFile = new Label(grpSettings, SWT.NONE);
        lblCertFile.setText("Cert file:");

        certFilePath = new Text(grpSettings, SWT.BORDER);
        certFilePath.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 3, 1));
        Button browseCertFileButton = new Button(grpSettings, SWT.NONE);
        browseCertFileButton.setText("Browse");

        Group grpClean = new Group(fileName, SWT.NONE);
        GridLayout gl_grpClean = new GridLayout(5, false);
        gl_grpClean.verticalSpacing = 7;
        grpClean.setLayout(gl_grpClean);
        GridData gd_grpClean = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
        gd_grpClean.heightHint = 180;
        gd_grpClean.widthHint = 543;
        grpClean.setLayoutData(gd_grpClean);
        grpClean.setText("Cleanup and advanced configuration");
        grpClean.setBounds(10, 266, 424, 162);

        btnRunCleaningCycle = new Button(grpClean, SWT.CHECK);
        btnRunCleaningCycle.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
        btnRunCleaningCycle.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setCleanCycle(btnRunCleaningCycle.getSelection());
            }
        });
        btnRunCleaningCycle.setText("Restart engine (drops all sessions)");

        btnJavaDebugMode = new Button(grpClean, SWT.CHECK);
        btnJavaDebugMode.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
        btnJavaDebugMode.setEnabled(false);
        btnJavaDebugMode.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                lblPort.setEnabled(btnJavaDebugMode.getSelection());
                debugPort.setEnabled(btnJavaDebugMode.getSelection());
                launcher.setUseDebug(btnJavaDebugMode.getSelection());
            }
        });
        btnJavaDebugMode.setText("Java debug");

        lblPort = new Label(grpClean, SWT.NONE);
        lblPort.setEnabled(false);
        lblPort.setText("Port");

        debugPort = new Text(grpClean, SWT.BORDER);
        debugPort.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        debugPort.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                launcher.setDebugPort(Integer.parseInt(debugPort.getText().trim()));
            }
        });
        debugPort.setEnabled(false);
        debugPort.setText("" + launcher.getDebugPort());

        composite_1 = new Composite(grpClean, SWT.NONE);
        GridLayout gl_composite_1 = new GridLayout(2, false);
        gl_composite_1.marginHeight = 0;
        composite_1.setLayout(gl_composite_1);
        composite_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

        lblNewLabel = new Label(composite_1, SWT.NONE);
        GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_lblNewLabel.widthHint = 16;
        lblNewLabel.setLayoutData(gd_lblNewLabel);

        btnCleanModelCache = new Button(composite_1, SWT.CHECK);
        btnCleanModelCache.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setCleanModelCache(btnCleanModelCache.getSelection());
                btnRunCleaningCycle.setSelection(launcher.isRestartRequested());
            }
        });
        btnCleanModelCache.setText("Clean model cache");

        btnKlabDebugMode = new Button(grpClean, SWT.CHECK);
        btnKlabDebugMode.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
        btnKlabDebugMode.setText("k.LAB debug mode (slow)");
        btnKlabDebugMode.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setKlabDebug(btnKlabDebugMode.getSelection());
            }
        });

        composite_2 = new Composite(grpClean, SWT.NONE);
        GridLayout gl_composite_2 = new GridLayout(2, false);
        gl_composite_2.marginHeight = 0;
        composite_2.setLayout(gl_composite_2);
        composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

        lblNewLabel_1 = new Label(composite_2, SWT.NONE);
        GridData gd_lblNewLabel_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_lblNewLabel_1.widthHint = 16;
        lblNewLabel_1.setLayoutData(gd_lblNewLabel_1);

        btnCleanWcswfsCache = new Button(composite_2, SWT.CHECK);
        btnCleanWcswfsCache.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setCleanDataCache(btnCleanWcswfsCache.getSelection());
                btnRunCleaningCycle.setSelection(launcher.isRestartRequested());
            }
        });
        btnCleanWcswfsCache.setText("Clean WCS/WFS cache");

        Button btnUseReasoning = new Button(grpClean, SWT.CHECK);
        btnUseReasoning.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
        btnUseReasoning
                .setToolTipText("When selected, the engine will validate the semantics of all namespaces "
                        + "and outputs. Any inconsistency will be flagged. Operation will be slightly slower. "
                        + "It is necessary to pass all consistency checks before publishing a project.");
        btnUseReasoning.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setUseReasoning(btnUseReasoning.getSelection());
                checkOk();
            }
        });
        btnUseReasoning.setSelection(launcher.isUseReasoning());
        btnUseReasoning.setEnabled(false);
        btnUseReasoning.setText("Use machine reasoning");

        composite_3 = new Composite(grpClean, SWT.NONE);
        GridLayout gl_composite_3 = new GridLayout(2, false);
        gl_composite_3.marginHeight = 0;
        composite_3.setLayout(gl_composite_3);
        composite_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

        lblNewLabel_2 = new Label(composite_3, SWT.NONE);
        GridData gd_lblNewLabel_2 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_lblNewLabel_2.widthHint = 16;
        lblNewLabel_2.setLayoutData(gd_lblNewLabel_2);

        btnCleanObservationDatabase = new Button(composite_3, SWT.CHECK);
        btnCleanObservationDatabase.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setCleanObservationCache(btnCleanObservationDatabase.getSelection());
                btnRunCleaningCycle.setSelection(launcher.isRestartRequested());
            }
        });
        btnCleanObservationDatabase.setText("Clean observation database");

        localComponentChoose = new Label(grpClean, SWT.HORIZONTAL | SWT.SHADOW_NONE | SWT.CENTER);
        localComponentChoose.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        localComponentChoose.setEnabled(false);
        localComponentChoose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                LocalComponentDialog dialog = new LocalComponentDialog(shell);
                if (dialog.open() == Dialog.OK) {
                    dialog.persistChoices();
                }
            }
        });
        localComponentChoose.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/Plugin.png"));
        lblSelectLocalComponent = new Label(grpClean, SWT.NONE);
        lblSelectLocalComponent.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 3, 1));
        lblSelectLocalComponent.setEnabled(false);
        lblSelectLocalComponent.setText("Select local components");

        btnDropAllSessions = new Button(grpClean, SWT.CHECK);
        btnDropAllSessions.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setRunGC(btnDropAllSessions.getSelection());
            }
        });
        btnDropAllSessions.setText("Drop all sessions and run garbage collection");
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);

        btnStopTheEngine = new Button(grpClean, SWT.CHECK);
        btnStopTheEngine.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                launcher.setStopEngineNow(btnStopTheEngine.getSelection());
                btnCleanModelCache.setEnabled(!btnStopTheEngine.getSelection());
                btnCleanObservationDatabase.setEnabled(!btnStopTheEngine.getSelection());
                btnCleanWcswfsCache.setEnabled(!btnStopTheEngine.getSelection());
                btnRunCleaningCycle.setEnabled(!btnStopTheEngine.getSelection());
                btnDropAllSessions.setEnabled(!btnStopTheEngine.getSelection());
            }
        });
        btnStopTheEngine.setText("Stop the engine now (no restart)");
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);
        new Label(grpClean, SWT.NONE);
        browseCertFileButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                // User has selected to open multiple files
                FileDialog dlg = new FileDialog(fileName.getShell(), SWT.OPEN);

                // dlg.setFilterNames(new String[] { "im" });
                dlg.setFilterExtensions(new String[] { "*.cert" });
                dlg.setFilterPath(System.getProperty("user.home"));
                String fn = dlg.open();
                if (fn != null) {
                    String file = dlg.getFilterPath() + File.separator + dlg.getFileName();
                    launcher.setCertificate(new File(file));
                    certFilePath.setText(file);
                    checkOk();
                }
            }
        });

        setup();

        return fileName;
    }

    protected boolean checkForMaven() {

        if (System.getProperties().containsKey("maven.home")) {
            return true;
        }
        if (System.getenv("MAVEN_HOME") != null) {
            System.setProperty("maven.home", System.getenv("MAVEN_HOME"));
            return true;
        }

        return false;
    }

    protected void checkOk() {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {

                if (launcher.getEngineJar() == null) {
                    btnUseDownloadedDistribution.setEnabled(false);
                    lblOfficialDistribution.setEnabled(false);

                } else {
                    btnUseDownloadedDistribution.setEnabled(true);
                    lblOfficialDistribution.setEnabled(true);
                }

                if (groups.contains(IUser.GROUP_DEVELOPERS)) {
                    if (launcher.getMavenDirectory() == null) {
                        btnUseMavenInstallation.setEnabled(false);
                        lblLocalInstall.setEnabled(false);
                    } else {
                        btnUseMavenInstallation.setEnabled(true);
                        lblLocalInstall.setEnabled(true);
                    }
                }

                if (launcher.isUseDebug()) {
                    debugPort.setEnabled(true);
                }

                if (launcher.getBuildNumber() == null) {
                    btnDownload.setEnabled(false);
                    btnDownload.setToolTipText("cannot connect to download server");
                    btnDownload.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
                } else if (launcher.getEngineJar() == null) {
                    btnDownload.setEnabled(true);
                    btnDownload.setText("Download");
                    btnDownload.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
                    btnDownload
                            .setToolTipText("You don't have the engine distribution for these settings. Click to download.");
                } else if (launcher.canUpgrade()) {
                    btnDownload.setEnabled(true);
                    btnDownload.setText("Upgrade");
                    btnDownload.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
                    btnDownload
                            .setToolTipText("updated build " + launcher.getBuildNumber() + " is available");
                } else {
                    btnDownload.setEnabled(true);
                    btnDownload.setText("Reset");
                    btnDownload.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
                    btnDownload
                            .setToolTipText("You have the most current engine distribution. You can download it again clicking here");
                }

                if (getButton(IDialogConstants.OK_ID) != null) {
                    getButton(IDialogConstants.OK_ID).setEnabled(launcher.canLaunch());
                }
            }
        });
    }

    private void setup() {

        this.setTitle("k.LAB engine control panel.");

        // haveMaven = checkForMaven();
        //
        // if (!haveMaven && launcher.isUseLocal()) {
        // /*
        // * just disable and let the user find out when they try.
        // */
        // launcher.setUseLocal(false);
        // btnUseMavenInstallation.setSelection(false);
        // btnUseDownloadedDistribution.setSelection(true);
        // }

        if (groups.contains(IUser.GROUP_DEVELOPERS)) {
            if (launcher.getMavenDirectory() == null) {
                btnUseMavenInstallation.setEnabled(false);
                lblLocalInstall.setEnabled(false);
            } else {
                btnUseMavenInstallation.setEnabled(true);
                lblLocalInstall.setEnabled(true);
            }
        }

        if (launcher.getEngineJar() == null) {
            this.setMessage("Please download an engine and configure it as desired. If you have a developer\naccount you can also use a Maven distribution.");
            btnUseDownloadedDistribution.setEnabled(false);
            lblOfficialDistribution.setEnabled(false);
        } else {
            this.setMessage("Configure your engine as desired. If you have a developer account you can also\nuse a Maven distribution.");
            btnUseDownloadedDistribution.setEnabled(true);
            lblOfficialDistribution.setEnabled(false);
        }

        dataPath.setText(launcher.getDataDir().toString());
        certFilePath.setText(launcher.getCertificate().toString());
        if (launcher.getMavenDirectory() != null) {
            localInstallationPath.setText(launcher.getMavenDirectory().toString());
        }

        if (groups.contains(IUser.GROUP_DEVELOPERS)) {
            btnUseDeveloperNetwork.setEnabled(true);
            lblLocalInstall.setEnabled(true);
            localInstallationPath.setEnabled(true);
            browseMavenInstallButton.setEnabled(true);
            btnJavaDebugMode.setEnabled(true);
            lblSelectLocalComponent.setEnabled(true);
            localComponentChoose.setEnabled(true);
        }

        boolean isOnline = launcher.isOnline();

        btnStopTheEngine.setEnabled(isOnline);
        btnCleanModelCache.setEnabled(isOnline);
        btnCleanObservationDatabase.setEnabled(isOnline);
        btnCleanWcswfsCache.setEnabled(isOnline);
        btnDropAllSessions.setEnabled(isOnline);
        btnRunCleaningCycle.setEnabled(isOnline);
        btnJavaDebugMode.setSelection(launcher.isUseDebug());
        btnKlabDebugMode.setSelection(launcher.isKlabDebug());

        checkOk();
    }

    /**
     * Check if we should gather user preferences based on engine status and
     * configuration; if so, show the dialog and react appropriately; return only after an
     * engine is configured, up and running, or after user has given up.
     * 
     * @return true unless we don't have an engine or all attempts to get one have failed.
     */
    public static boolean initializeEngine(final EngineLauncher launcher, Set<String> userGroups) {

        boolean isOnline = launcher.isOnline();

        if (!isOnline && (!launcher.canLaunch() || !launcher.isLaunchAutomatically()
                || (launcher.isUpgradeAutomatically() && launcher.canUpgrade()))) {

            boolean skipDialog = false;
            if (!userGroups.contains(IUser.GROUP_DEVELOPERS) && launcher.isLaunchAutomatically()) {

                if (launcher.getEngineJar() == null) {
                    /*
                     * automatically download engine and reevaluate canLaunch
                     */
                    try {
                        downloadFile("Downloading " + (launcher.isUseDeveloper() ? " (development)" : "")
                                + "engine...", launcher, Eclipse
                                        .getShell(), new URL(launcher.isUseDeveloper() ? DEV_ENGINE_URL
                                                : ENGINE_URL), new File(KLAB.CONFIG.getDataPath("engine")
                                                        + File.separator
                                                        + (launcher.isUseDeveloper() ? "kmodeler_dev.jar"
                                                                : "kmodeler.jar")), ENGINE_SIZE_HINT, null);
                        skipDialog = launcher.canLaunch();
                    } catch (MalformedURLException e) {
                        // won't happen
                    }
                } else if (launcher.isUpgradeAutomatically() && launcher.canUpgrade()) {
                    try {
                        downloadFile("Upgrading " + (launcher.isUseDeveloper() ? " (development)" : "")
                                + " engine...", launcher, Eclipse
                                        .getShell(), new URL(launcher.isUseDeveloper() ? DEV_ENGINE_URL
                                                : ENGINE_URL), new File(KLAB.CONFIG.getDataPath("engine")
                                                        + File.separator
                                                        + (launcher.isUseDeveloper() ? "kmodeler_dev.jar"
                                                                : "kmodeler.jar")), ENGINE_SIZE_HINT, null);
                        skipDialog = launcher.canLaunch();
                    } catch (MalformedURLException e) {
                        // won't happen
                    }
                }
            }

            if (!skipDialog) {

                try {
                    Thread.sleep(1000l);
                } catch (InterruptedException e) {
                    //
                }

                EngineDialog dialog = new EngineDialog(Eclipse.getShell(), launcher, userGroups);
                if (dialog.open() != OK) {
                    return false;
                }
            }

            /*
             * store choices if requested
             */
            if (launcher.isRememberSettings()) {
                launcher.persistProperties();
            }

            if (isOnline && launcher.isStopEngineNow()) {
                try {
                    new ProgressMonitorDialog(Eclipse.getShell())
                            .run(true, true, new IRunnableWithProgress() {

                                @Override
                                public void run(IProgressMonitor monitor)
                                        throws InvocationTargetException, InterruptedException {
                                    monitor.beginTask("Stopping engine...", IProgressMonitor.UNKNOWN);
                                    launcher.shutdown(10);
                                    monitor.done();
                                }
                            });
                } catch (InvocationTargetException | InterruptedException e) {
                    Eclipse.handleException(e);
                }
            }
        }

        if (launcher.canLaunch() && !isOnline) {
            try {
                new ProgressMonitorDialog(Eclipse.getShell()).run(true, true, new IRunnableWithProgress() {

                    @Override
                    public void run(IProgressMonitor monitor)
                            throws InvocationTargetException, InterruptedException {
                        /*
                         * TODO improve
                         */
                        monitor.beginTask("Starting engine...", IProgressMonitor.UNKNOWN);
                        launcher.launch(true, new LaunchListener() {

                            @Override
                            public void launchSuccessful() {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void launchFailed(Throwable exception) {
                                Eclipse.alert("Engine start failed: exiting.");
                                System.exit(0);
                            }

                            @Override
                            public void compilationSuccessful() {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void compilationFailed() {
                                Eclipse.error("Maven engine compilation failed: exiting.");
                                System.exit(0);
                            }

                            @Override
                            public void shutdownStarted() {
                                // TODO Auto-generated method stub
                                // send event
                            }
                        });
                        monitor.done();
                    }
                });
            } catch (InvocationTargetException | InterruptedException e) {
                Eclipse.handleException(e);
            }
        }

        return true;
    }

    /*
     * using file - needs length hint
     */

    static class DownloadListener implements URLUtils.CopyListener, IRunnableWithProgress {

        IProgressMonitor       monitor = null;
        private long           size;
        private URL            url;
        private File           outfile;
        private int            total   = 0;
        private EngineLauncher launcher;
        private EngineDialog   dialog;
        String                 message;

        public DownloadListener(String message, EngineLauncher launcher, URL url, File outfile, long size,
                EngineDialog dialog) {
            this.size = size;
            this.url = url;
            this.outfile = outfile;
            this.launcher = launcher;
            this.dialog = dialog;
            this.message = message;
        }

        /**
         * Only call after finish to know if the failure to stop the server has
         * jeopardized the update (= a false return value).
         * 
         * @return
         */
        public boolean success() {
            return total >= 100;
        }

        @Override
        public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {

            this.monitor = monitor;
            monitor.setTaskName(message);
            monitor.subTask("Initializing...");
            onProgress(0);
            try {
                URLUtils.copy(url, outfile, this, size);
                /*
                 * save latest build number
                 */
                Integer build = launcher.getBuildNumber();
                if (build != null) {
                    if (launcher.isUseDeveloper()) {
                        KLAB.CONFIG.getProperties()
                                .setProperty(EngineLauncher.BUILD_DEV_PROPERTY, "" + build);
                    } else {
                        KLAB.CONFIG.getProperties().setProperty(EngineLauncher.BUILD_PROPERTY, "" + build);
                    }
                    KLAB.CONFIG.save();
                }
                launcher.rescanDistributions();
            } catch (KlabIOException e) {
                Eclipse.error(e);
                monitor.done();
            }
            if (dialog != null) {
                dialog.checkOk();
            }

        }

        @Override
        public void onProgress(int percent) {
            if (percent == 0) {
                monitor.beginTask("Downloading engine distribution"
                        + (launcher.isUseDeveloper() ? " (development)" : ""), 100);
            } else {
                if (percent > total) {
                    monitor.worked(1);
                    monitor.subTask(total + "% done");
                }
                total = percent;
                if (total >= 100) {
                    monitor.done();
                }
            }
        }
    }

    static boolean downloadFile(String message, EngineLauncher launcher, Shell shell, URL url, File file, long sizeHint, EngineDialog edialog) {
        final ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
        final DownloadListener downloader = new DownloadListener(message, launcher, url, file, sizeHint, edialog);
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    dialog.run(true, false, downloader);
                } catch (Exception e) {
                    Eclipse.handleException(e);
                }
                dialog.close();

            }
        });
        return downloader.success();
    }

    /*
     * using networked distribution - unused for now
     */

    class DownloadDistributionListener implements NetworkedDistribution.SyncListener, IRunnableWithProgress {

        int                   _total  = 0;
        int                   _sofar  = 0;
        IProgressMonitor      monitor = null;
        NetworkedDistribution sd      = null;
        boolean               ret     = true;

        public DownloadDistributionListener(NetworkedDistribution sd) {
            this.sd = sd;
        }

        /**
         * Only call after finish to know if the failure to stop the server has
         * jeopardized the update (= a false return value).
         * 
         * @return
         */
        public boolean success() {
            return ret;
        }

        @Override
        public void beforeDownload(String file) {
            monitor.setTaskName("Updating engine distribution: " + (_total - _sofar) + " files to download");
            monitor.subTask("downloading " + file);
            _sofar++;
            monitor.worked(1);
        }

        @Override
        public void beforeDelete(File localFile) {
            monitor.subTask("deleting " + localFile);
        }

        @Override
        public void notifyDownloadCount(int downloadFilecount, int deleteFileCount) {
            _total = downloadFilecount;
            monitor.beginTask("Updating server distribution: " + downloadFilecount
                    + " files to download", downloadFilecount);
        }

        @Override
        public void transferFinished() {
            monitor.done();
        }

        @Override
        public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
            this.monitor = monitor;
            sd.setListener(this);
            try {
                ret = sd.sync();
            } catch (KlabException e) {
                throw new InvocationTargetException(e);
            }
        }
    }

    protected boolean syncDistribution(NetworkedDistribution sd) {
        final ProgressMonitorDialog dialog = new ProgressMonitorDialog(Eclipse.getShell());
        final DownloadDistributionListener downloader = new DownloadDistributionListener(sd);
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                try {
                    dialog.run(true, false, downloader);
                } catch (Exception e) {
                    Eclipse.handleException(e);
                }
                dialog.close();
            }
        });
        return downloader.success();
    }

    public Label getLocalComponentChoose() {
        return localComponentChoose;
    }

    public Label getLblSelectLocalComponent() {
        return lblSelectLocalComponent;
    }
}
