/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.common.auth.KlabCertificate;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;

public class LocalComponentDialog extends TitleAreaDialog {

	protected KlabCertificate result;
	// protected Shell fileName;
	private TableViewer tableViewer;
	private List<Component> components = new ArrayList<>();
	private Table componentTable;
	private Button clearAllButton;
	
	class Component {
		File directory;
		String id;
		Properties properties;

		public boolean ok() {
			return properties != null;
		}

		Component(File file) {
			this.properties = MiscUtilities
					.readProperties(new File(file + File.separator + "src" + File.separator + "main" + File.separator
							+ "resources" + File.separator + "META-INF" + File.separator + "klab.properties"));
			if (this.properties != null) {
				this.id = this.properties
						.getProperty(org.integratedmodelling.common.project.Component.COMPONENT_ID_PROPERTY);
				if (this.id == null) {
					this.properties = null;
				}
				this.directory = file;
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Component other = (Component) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

		private LocalComponentDialog getOuterType() {
			return LocalComponentDialog.this;
		}

	}

	class ComponentLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getImage(Object element) {

			if (element instanceof Component) {
				ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/Plugin.png");
			}
			return null;
		}

		@Override
		public String getText(Object element) {
			return null;
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof Component) {
				if (columnIndex == 0) {
					return ((Component) element).id;
				}
				return ((Component) element).directory.toString();
			}
			return null;
		}

	}

	class ComponentContentProvider implements ITreeContentProvider {

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return getChildren(inputElement);
		}

		@Override
		public Object[] getChildren(Object parentElement) {
			if (parentElement instanceof List) {
				return components.toArray();
			}
			return null;
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Component) {
				return components;
			}
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			return element instanceof List && components.size() > 1;
		}

	}

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 */
	public LocalComponentDialog(Shell parent) {
		super(parent);
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public KlabCertificate getCertificate() {
		return result;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Components in development");
	}

	/**
	 * Create contents of the dialog.
	 */
	protected Control createDialogArea(Composite parent) {
		setTitleImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/logo_white_64.jpg"));

		Composite fileName = (Composite) super.createDialogArea(parent);
		fileName.setLayout(null);

		Button addButton = new Button(fileName, SWT.NONE);
		addButton.setBounds(359, 10, 75, 25);
		addButton.setText("Add...");
		addButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				DirectoryDialog dlg = new DirectoryDialog(fileName.getShell(), SWT.OPEN);
				String fn = dlg.open();
				if (fn != null) {
					String file = dlg.getFilterPath();
					addComponent(new File(file));
				}
				clearAllButton.setEnabled(components.size() > 0);
			}
		});

		Button removeButton = new Button(fileName, SWT.NONE);
		removeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				for (TableItem o : tableViewer.getTable().getSelection()) {
					components.remove((Component) o.getData());
				}
				tableViewer.setInput(components);
				clearAllButton.setEnabled(components.size() > 0);
				removeButton.setEnabled(false);
			}
		});
		removeButton.setEnabled(false);
		removeButton.setBounds(359, 41, 75, 25);
		removeButton.setText("Remove");

		clearAllButton = new Button(fileName, SWT.NONE);
		clearAllButton.setEnabled(false);
		clearAllButton.setBounds(359, 72, 75, 25);
		clearAllButton.setText("Clear all");
		clearAllButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				components.clear();
				tableViewer.setInput(components);
				clearAllButton.setEnabled(false);
			}
		});
		tableViewer = new TableViewer(fileName, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		componentTable = tableViewer.getTable();
		componentTable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				removeButton.setEnabled(componentTable.getSelection().length > 0);
			}
		});
		componentTable.setLinesVisible(true);
		componentTable.setHeaderVisible(true);
		componentTable.setBounds(10, 10, 343, 360);
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnNewColumn = tableViewerColumn.getColumn();
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("Component ID");

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnNewColumn_1 = tableViewerColumn_1.getColumn();
		tblclmnNewColumn_1.setWidth(220);
		tblclmnNewColumn_1.setText("Directory");

		tableViewer.setContentProvider(new ComponentContentProvider());
		tableViewer.setLabelProvider(new ComponentLabelProvider());

		this.setTitle("Identify component directories.");
		this.setMessage(
				"Add Maven installation directories for components. They will\nbe installed into the engine after restart.");

		String cinstalled = KLAB.CONFIG.getProperties().getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS);
		if (cinstalled != null) {
			String[] cc = cinstalled.split(",");
			for (String ccd : cc) {
				Component c = new Component(new File(ccd.trim()));
				if (c.ok()) {
					components.add(c);
				}
			}
			tableViewer.setInput(components);
			clearAllButton.setEnabled(components.size() > 0);
		}
		return fileName;
	}

	protected void addComponent(File file) {

		Component cdef = new Component(file);
		if (!cdef.ok()) {
			Eclipse.alert("This directory does not contain a k.LAB component.");
		} else {
			if (!this.components.contains(cdef)) {
				this.components.add(cdef);
				tableViewer.setInput(components);
			}
		}
	}

	public void persistChoices() {

		String cprops = "";
		for (Component c : components) {
			cprops += (cprops.isEmpty() ? "" : ",") + c.directory;
		}

		KLAB.CONFIG.getProperties().setProperty(IConfiguration.KLAB_LOCAL_COMPONENTS, cprops);
		KLAB.CONFIG.save();

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		getButton(IDialogConstants.OK_ID).setEnabled(true);
	}
}
