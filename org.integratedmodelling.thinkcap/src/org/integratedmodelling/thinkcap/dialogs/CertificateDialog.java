/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.common.auth.KlabCertificate;
import org.integratedmodelling.common.utils.FileUtils;

public class CertificateDialog extends TitleAreaDialog {

    protected KlabCertificate result;
    protected Shell           fileName;
    private Text              text;

    /**
     * Create the dialog.
     * 
     * @param parent
     */
    public CertificateDialog(Shell parent) {
        super(parent);
    }

    /**
     * Open the dialog.
     * 
     * @return the result
     */
    public KlabCertificate getCertificate() {
        return result;
    }

    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("No IM certificate");
    }

    /**
     * Create contents of the dialog.
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        setTitleImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));

        //        GC gc = new GC(parent);
//        gc.setFont(parent.getFont());
//        FontMetrics fm = gc.getFontMetrics();
//        Point extent = gc.textExtent("M");
        
//        int hm = extent.y + 2;
//        int wm = extent.x + 2;
        
        Composite fileName = (Composite) super.createDialogArea(parent);
        GridLayout gl_fileName = new GridLayout(1, false);
        gl_fileName.marginWidth = 3;
        gl_fileName.verticalSpacing = 0;
        gl_fileName.horizontalSpacing = 3;
        fileName.setLayout(gl_fileName);
        
        Composite composite = new Composite(fileName, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
        GridLayout gl_composite = new GridLayout(3, false);
        gl_composite.verticalSpacing = 0;
        gl_composite.marginWidth = 0;
        gl_composite.marginHeight = 0;
        composite.setLayout(gl_composite);
                //        new Label(fileName, SWT.NONE);
                //        new Label(fileName, SWT.NONE);
                        Label lblCertFile = new Label(composite, SWT.NONE);
                        lblCertFile.setText("Certificate file:");
                        text = new Text(composite, SWT.NONE);
                        text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
        Button btnNewButton = new Button(composite, SWT.NONE);
        btnNewButton.setText("Browse");
        btnNewButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                // User has selected to open multiple files
                FileDialog dlg = new FileDialog(fileName.getShell(), SWT.OPEN);

                dlg.setFilterExtensions(new String[] { "*.cert" });
                dlg.setFilterPath(System.getProperty("user.home"));
                String fn = dlg.open();
                if (fn != null) {

                    String file = dlg.getFilterPath() + File.separator + dlg.getFileName();
                    File certFile = new File(file);
                    if (certFile.exists() && certFile.isFile() && certFile.canRead()) {
                        try {
                            FileUtils.copyFile(certFile, KlabCertificate.getCertificateFile());
                        } catch (IOException e) {
                            setErrorMessage("Error copying certificate file to destination.");
                        }
                        result = new KlabCertificate(KlabCertificate.getCertificateFile());
                        if (!result.isValid()) {
                            setErrorMessage(result.getCause());
                        } else {
                        	setErrorMessage(null);
                            getShell().setText("Certificate is OK");
                            setTitle("Your IM certificate is valid and current.");
                            setMessage("Valid and current certificate installed for user "
                                    + result.getUserDescription()
                                    + ".");
                            getButton(IDialogConstants.OK_ID).setEnabled(true);
                        }
                    }

                    text.setText(result.getFile().toString());
                }
            }
        });

        this.setTitle("Please install your IM certificate.");
        this.setMessage("You do not have an integratedmodelling.org certificate installed. Please upload a current certificate.");

        return fileName;
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        super.createButtonsForButtonBar(parent);
        getButton(IDialogConstants.OK_ID).setEnabled(false);
    }

}
