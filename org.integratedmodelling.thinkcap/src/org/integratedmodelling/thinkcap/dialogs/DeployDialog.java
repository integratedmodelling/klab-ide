/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.thinkcap.dialogs;
//
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.MouseAdapter;
//import org.eclipse.swt.events.MouseEvent;
//import org.eclipse.swt.widgets.Button;
//import org.eclipse.swt.widgets.Combo;
//import org.eclipse.swt.widgets.Dialog;
//import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.Label;
//import org.eclipse.swt.widgets.Shell;
//import org.integratedmodelling.api.project.IProject;
//import org.integratedmodelling.api.runtime.IEngine;
//import org.integratedmodelling.common.engine.RemoteEngine;
//import org.integratedmodelling.thinkcap.Activator;
//import org.integratedmodelling.thinkcap.Eclipse;
//
//public class DeployDialog extends Dialog {
//
//    protected boolean  result = false;
//    protected Shell    shell;
//    protected Combo    combo;
//    protected IProject project;
//
//    /**
//     * Create the dialog.
//     * @param parent
//     * @param style
//     */
//    public DeployDialog(Shell parent, IProject project, int style) {
//        super(parent, style);
//        setText("SWT Dialog");
//        this.project = project;
//    }
//
//    /**
//     * Open the dialog.
//     * @return the result
//     */
//    public boolean open() {
//        createContents();
//        shell.open();
//        shell.layout();
//        Display display = getParent().getDisplay();
//        while (!shell.isDisposed()) {
//            if (!display.readAndDispatch()) {
//                display.sleep();
//            }
//        }
//        return result;
//    }
//
//    /**
//     * Create contents of the dialog.
//     */
//    private void createContents() {
//
//        shell = new Shell(getParent(), getStyle());
//        shell.setSize(450, 200);
//        shell.setText("Deploying project " + project.getId());
//
//        Button btnCancel = new Button(shell, SWT.NONE);
//        btnCancel.setBounds(289, 126, 75, 25);
//        btnCancel.setText("Cancel");
//        btnCancel.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseUp(MouseEvent e) {
//                shell.dispose();
//            }
//        });
//
//        Button btnContinue = new Button(shell, SWT.NONE);
//        btnContinue.setBounds(359, 126, 75, 25);
//        btnContinue.setText("Continue");
//        btnContinue.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseUp(MouseEvent e) {
//                result = deployProject(combo.getText());
//                shell.dispose();
//            }
//        });
//
//        Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
//        label.setBounds(10, 114, 424, 2);
//
//        combo = new Combo(shell, SWT.READ_ONLY);
//        combo.setBounds(125, 69, 309, 22);
//        for (IEngine server : Activator.client().getEngineClient().getEngines()) {
//            if (server instanceof RemoteEngine) {
//                combo.add(server.getId() + " (" + ((RemoteEngine) server).getUrl() + ")");
//            }
//        }
//        combo.select(0);
//
//        Label lblServerToDeploy = new Label(shell, SWT.NONE);
//        lblServerToDeploy.setBounds(10, 72, 109, 14);
//        lblServerToDeploy.setText("Server to deploy to:");
//
//        Label label_1 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
//        label_1.setBounds(10, 44, 424, 2);
//
//        Label lblDeployingProject = new Label(shell, SWT.NONE);
//        lblDeployingProject.setBounds(10, 24, 241, 14);
//        lblDeployingProject.setText("Deploying project " + project.getId());
//
//    }
//
//    private boolean deployProject(String serverId) {
//
//        for (IEngine server : Activator.client().getEngineClient().getEngines()) {
//            if (server instanceof RemoteEngine
//                    && serverId.equals(server.getId() + " (" + ((RemoteEngine) server).getUrl() + ")")) {
//                try {
//                    return server.deploy(project, false, true);
//                } catch (Exception e) {
//                    shell.dispose();
//                    Eclipse.handleException(e);
//                }
//            }
//        }
//        return false;
//    }
// }
