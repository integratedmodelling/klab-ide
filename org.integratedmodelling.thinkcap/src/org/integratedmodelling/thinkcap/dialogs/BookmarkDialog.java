/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class BookmarkDialog extends Dialog {

    private Text   text;
    private String bName;
    private String bDesc;
    private Text   description;

    /**
     * Create the dialog.
     * @param parentShell
     */
    public BookmarkDialog(Shell parentShell) {
        super(parentShell);
        setShellStyle(SWT.TITLE);
    }

    /**
     * Create contents of the dialog.
     * @param parent
     */
    @Override
    protected Control createDialogArea(Composite parent) {

        Composite container = (Composite) super.createDialogArea(parent);
        container.setLayout(null);

        Label lblSelectAName = new Label(container, SWT.NONE);
        lblSelectAName.setBounds(34, 23, 376, 15);
        lblSelectAName.setText("Enter the bookmark name. Separate folders with / (folder/bookmark):");

        text = new Text(container, SWT.BORDER);
        text.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                bName = text.getText();
            }
        });
        text.setBounds(34, 44, 376, 21);

        description = new Text(container, SWT.BORDER);
        description.setBounds(34, 99, 376, 21);
        description.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                bDesc = description.getText();
            }
        });

        Label lblBookmarkDescriptionoptional = new Label(container, SWT.NONE);
        lblBookmarkDescriptionoptional.setBounds(34, 78, 220, 15);
        lblBookmarkDescriptionoptional.setText("Bookmark description (optional):");

        return container;
    }

    /**
     * Create contents of the button bar.
     * @param parent
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
                true);
        createButton(parent, IDialogConstants.CANCEL_ID,
                IDialogConstants.CANCEL_LABEL, false);
    }

    /**
     * Return the initial size of the dialog.
     */
    @Override
    protected Point getInitialSize() {
        return new Point(450, 243);
    }

    public String getText() {
        return bName;
    }

    public String getDescription() {
        return bDesc;
    }
}
