/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.integratedmodelling.common.client.ReleaseNotes;

public class UpdateDialog extends Dialog {

    protected Shell shell;

    /**
     * Create the dialog.
     * @param parent
     */
    public UpdateDialog(Shell parent) {
        super(parent, SWT.TITLE);
    }

    /**
     * Open the dialog.
     */
    public void show() {
        createContents();
        shell.open();
        shell.layout();
        Display display = getParent().getDisplay();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    public static void open(Shell shell) {
        UpdateDialog dialog = new UpdateDialog(shell);
        dialog.show();
    }

    /**
     * Create contents of the dialog.
     */
    private void createContents() {

        shell = new Shell(getParent(), getStyle());
        shell.setSize(589, 625);
        shell.setText("Update available");
        Label lblYouDontHave = new Label(shell, SWT.NONE);
        lblYouDontHave.setBounds(10, 10, 563, 80);
        lblYouDontHave
                .setText("An update is available.\n\nPlease update using the Help -> Check for updates... menu action. Below you can read the "
                        + "release\nnotes for the update.");

        Button btnContinue = new Button(shell, SWT.NONE);
        btnContinue.setBounds(498, 562, 75, 25);
        btnContinue.setText("Continue");

        StyledText composite = new StyledText(shell, SWT.BORDER | SWT.V_SCROLL);
        composite.setAlwaysShowScrollBars(false);
        composite.setEditable(false);
        composite.setBounds(10, 85, 563, 467);
        String text = ReleaseNotes.get();
        composite.setText(text);

        btnContinue.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                shell.dispose();
            }
        });

    }
}
