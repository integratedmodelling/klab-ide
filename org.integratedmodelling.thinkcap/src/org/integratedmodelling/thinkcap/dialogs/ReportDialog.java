package org.integratedmodelling.thinkcap.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class ReportDialog extends Dialog {

    String html;
    
    public ReportDialog(Shell parentShell, String html) {
        super(parentShell);
        setShellStyle(SWT.RESIZE);
        this.html = html;
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        container.setSize(new Point(400, 700));
        Browser browser = new Browser(container, SWT.NONE);
        GridData gd_browser = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_browser.widthHint = 700;
        gd_browser.heightHint = 800;
        browser.setLayoutData(gd_browser);
        browser.setText(html);
        
        return container;
    }
    
    @Override
    protected void setShellStyle(int newShellStyle) {           
        super.setShellStyle(SWT.CLOSE | SWT.MODELESS| SWT.BORDER | SWT.TITLE | SWT.RESIZE);
        setBlockOnOpen(false);
    }
}
