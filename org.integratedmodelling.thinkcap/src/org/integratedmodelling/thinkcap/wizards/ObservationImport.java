/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;

public class ObservationImport extends WizardPage {

    private IConcept                   concept;
    private final FormToolkit          formToolkit = new FormToolkit(Display.getDefault());
    private Text                       txtNewText;
    private Table                      table;
    private Button                     btnImport;
    private List<IObservationMetadata> data;
    private Label                      infoLabel;
    private String                     idField     = null;
    private TableColumn                tblclmnNewColumn;
    private TableColumn                tblclmnNewColumn_1;
    private CheckboxTableViewer        checkboxTableViewer;
    private Text                       text;
    private Button                     btnUseConvexHull;
    private Button                     btnUseBoundingBox;

    class DataContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
            // TODO Auto-generated method stub

        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            // TODO Auto-generated method stub

        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof Map) {
                return ((Map<?, ?>) parentElement).entrySet().toArray();
            }
            return new Object[] {};
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof Map.Entry) {
                return data.get(0);
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return element instanceof List && ((List<?>) element).size() > 0;
        }

    }

    class DataLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {
            if (element instanceof Map.Entry) {
                switch (columnIndex) {
                case 0:
                    return ((Map.Entry<?, ?>) element).getKey().toString();
                case 1:
                    return ((Map.Entry<?, ?>) element).getValue().toString();
                }
            }
            return null;
        }
    }

    /**
     * Create the wizard.
     */
    public ObservationImport() {
        super("wizardPage");
        setTitle("Import observations");
        setDescription("This wizard allows you to select a file containing observations to import.");
    }

    /**
     * @wbp.parser.constructor
     */
    public ObservationImport(IConcept concept) {
        this();
        setImageDescriptor(ResourceManager
                .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));
        this.concept = concept;
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    @Override
    public void createControl(Composite parent) {

        // parent.setSize(new Point(550, 600));
        Composite container = new Composite(parent, SWT.NULL);
        setControl(container);

        Label lblFileToImport = formToolkit.createLabel(container, "File to import:", SWT.NONE);
        lblFileToImport.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
        lblFileToImport.setBounds(10, 39, 81, 15);

        txtNewText = formToolkit.createText(container, "New Text", SWT.NONE);
        txtNewText.setText("");
        txtNewText.setBounds(97, 36, 301, 21);

        Button btnBrowse = formToolkit.createButton(container, "Browse", SWT.NONE);
        btnBrowse.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
                fd.setText("Open file");
                fd.setFilterPath(System.getProperty("user.home"));
                String[] filterExt = { "*.shp", "*.csv", "*.*" };
                fd.setFilterExtensions(filterExt);
                String selected = fd.open();
                txtNewText.setText(selected);
                if (new File(selected).exists()) {
                    btnImport.setEnabled(true);
                }
            }
        });
        btnBrowse.setBounds(406, 34, 57, 25);

        btnImport = formToolkit.createButton(container, "Import", SWT.NONE);
        btnImport.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                importData(new File(txtNewText.getText()));
            }
        });
        btnImport.setEnabled(false);
        btnImport.setBounds(469, 34, 57, 25);

        checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER | SWT.FULL_SELECTION);
        table = checkboxTableViewer.getTable();
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.setBounds(10, 140, 516, 184);
        formToolkit.paintBordersFor(table);
        checkboxTableViewer.setLabelProvider(new DataLabelProvider());
        checkboxTableViewer.setContentProvider(new DataContentProvider());
        checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
            @Override
            public void checkStateChanged(CheckStateChangedEvent event) {
                Map.Entry<?, ?> kv = (Map.Entry<?, ?>) event.getElement();
                idField = kv.getKey().toString();
            }
        });
        tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
        tblclmnNewColumn_1.setWidth(100);
        tblclmnNewColumn_1.setText("Field");

        tblclmnNewColumn = new TableColumn(table, SWT.NONE);
        tblclmnNewColumn.setWidth(400);
        tblclmnNewColumn.setText("Value in first record");

        infoLabel = formToolkit.createLabel(container, "Importing as " + concept, SWT.NONE);
        infoLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
        infoLabel.setBounds(12, 119, 514, 15);

        Label label = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
        label.setBounds(10, 80, 516, 2);
        formToolkit.adapt(label, true, true);

        Button btnSimplifyPolygons = new Button(container, SWT.CHECK);
        btnSimplifyPolygons.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnSimplifyPolygons.setBounds(20, 332, 131, 16);
        // formToolkit.adapt(btnSimplifyPolygons, true, true);
        btnSimplifyPolygons.setText("Simplify polygons by");

        text = new Text(container, SWT.BORDER);
        text.setEnabled(false);
        text.setText("1.0");
        text.setBounds(157, 330, 66, 21);
        formToolkit.adapt(text, true, true);

        btnUseConvexHull = new Button(container, SWT.CHECK);
        btnUseConvexHull.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnUseConvexHull.setBounds(238, 332, 131, 16);
        // formToolkit.adapt(btnUseConvexHull, true, true);
        btnUseConvexHull.setText("Use convex hull");

        btnUseBoundingBox = new Button(container, SWT.CHECK);
        btnUseBoundingBox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnUseBoundingBox.setBounds(409, 332, 117, 16);
        // formToolkit.adapt(btnUseBoundingBox, true, true);
        btnUseBoundingBox.setText("Use perimeter");
    }

    private void importData(final File file) {

        Job job = new Job("Importing data...") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {
                try {
                    data = Activator.engine().importObservations(file);
                } catch (KlabException e) {
                    return Status.CANCEL_STATUS;
                }
                refresh();
                return Status.OK_STATUS;
            }
        };
        job.schedule();
    }

    private void refresh() {

        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                infoLabel.setText(data == null ? ""
                        : ("The import produced " + data.size() + " records." + (data.size() == 0 ? ""
                                : " Showing the first: please select a field to use as name.")));
                checkboxTableViewer.setInput(data == null ? null
                        : (data.size() > 0 ? data.get(0).getAttributes() : null));
            }
        });
    }

    public List<IObservationMetadata> getData() {
        return data;
    }

    public String getIdField() {
        return idField;
    }
}
