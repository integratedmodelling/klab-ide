/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;
import org.integratedmodelling.api.data.IExport;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.thinkcap.Eclipse;

public class ExportDataWizard extends Wizard {

    IContext   context;
    ExportData page;

    public ExportDataWizard(IContext context) {
        setWindowTitle("New Wizard");
        this.context = context;
    }

    @Override
    public void addPages() {
        addPage(page = new ExportData(context));
    }

    @Override
    public boolean performFinish() {

        final File file = page.getFile();

        boolean aggregate = page.aggregateTemporalCheckbox.getSelection();

        if (page.currentChoice > 0 && file != null) {

            Job job = new Job("Exporting data to " + file) {

                @Override
                protected IStatus run(IProgressMonitor monitor) {

                    IScale.Locator time = context.getLastTemporalTransition();
                    // if (time != null && time.getSlice() <= 0) {
                    // // FIXME review counting - 0 gives an error.
                    // time = TimeLocator.get(1);
                    // }

                    try {
                        switch (page.currentChoice) {
                        case ExportData.ALL_STATES:
                            context.persist(file, null, time);
                            break;
                        case ExportData.ALL_SUBJECTS:
                            context.persist(file, null, page.getSubjectType(), time);
                            break;
                        case ExportData.CONTEXT_BOUNDARIES:
                            context.persist(file, "/", page.getSubjectType());
                            break;
                        case ExportData.CURRENT_MOVIE:
                            context.persist(file, context.getPathFor(Environment.get().getSession()
                                    .getViewer().getVisibleSpatialState()), IExport.Format.VIDEO);
                            break;
                        case ExportData.CURRENT_STATE:
                            context.persist(file, context.getPathFor(Environment.get().getSession()
                                    .getViewer().getVisibleSpatialState()), time, (aggregate
                                            ? IExport.Aggregation.TOTAL : IExport.Aggregation.AVERAGE));
                            break;
                        }
                    } catch (Exception e) {
                        Eclipse.handleException(e);
                    }
                    return Status.OK_STATUS;
                }
            };

            job.schedule();

            return true;
        }

        return false;
    }
}
