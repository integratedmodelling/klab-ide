/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.thinkcap.Activator;

public class UploadToDatabase extends WizardPage {

    private Table      table;
    public Set<String> nodes = new HashSet<>();
    private Text text;

    /**
     * Create the wizard.
     */
    public UploadToDatabase() {
        super("wizardPage");
        setTitle("Publish observations");
        setDescription("Publish observations to local or remote repositories.");
    }

    /**
     * Create contents of the wizard.
     * @param parent
     */
    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);

        setControl(container);
        List<String> nodeNames = new ArrayList<>();
        nodeNames.add("Local engine");
        for (IServer n : KLAB.ENGINE.getNetwork().getNodes()) {
            if (Activator.engine().getResourceConfiguration()
                    .isAuthorized(StaticResource.OBSERVATION_SUBMIT, Activator.engine().getUser(), null)) {
                nodeNames.add(n.getId());
            }
        }

        CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER
                | SWT.FULL_SELECTION);
        table = checkboxTableViewer.getTable();
        table.setBounds(10, 31, 236, 185);
        checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {

            @Override
            public void checkStateChanged(CheckStateChangedEvent event) {
                // TODO Auto-generated method stub
                if (event.getChecked()) {
                    nodes.add(event.getElement().toString());
                } else {
                    nodes.remove(event.getElement().toString());
                }
            }
        });

        Combo lblYouCanUpload = new Combo(container, SWT.READ_ONLY);
        lblYouCanUpload.setBounds(10, 10, 236, 15);
        lblYouCanUpload.add("k.LAB observation storage");
        
        text = new Text(container, SWT.BORDER);
        text.setEditable(false);
        text.setBounds(40, 222, 206, 21);
        
        Label lblUrn = new Label(container, SWT.NONE);
        lblUrn.setBounds(10, 224, 24, 15);
        lblUrn.setText("URN");

        for (String s : nodeNames) {
            checkboxTableViewer.add(s);
        }
    }
}
