/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.progress.UIJob;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.EclipseWorkspace;
import org.integratedmodelling.thinkcap.events.ModelModifiedEvent;

public class NewScriptWizard extends Wizard {

    private NewScript page;
    private IProject  target;

    public NewScriptWizard() {
        setWindowTitle("New Thinklab Script");
    }

    public void setTargetProject(IProject o) {
        this.target = o;
    }

    @Override
    public void addPages() {
        addPage(this.page = new NewScript());
    }

    @Override
    public boolean performFinish() {

        final String nspc = page.getNamespace().getText().trim();
        final IProject p = EclipseWorkspace.getProjectForTarget(target);

        if (validate(nspc, p)) {

            Job job = new UIJob("Creating script " + nspc) {
                @Override
                public IStatus runInUIThread(IProgressMonitor monitor) {
                    try {
                        String f = p.getId() + File.separator + ".scripts" + File.separator + nspc
                                + ".groovy";
                        File sdir = new File(p.getLoadPath() + File.separator + ".scripts");
                        sdir.mkdir();
                        FileUtils.touch(new File(sdir + File.separator + nspc + ".groovy"));
                        Activator.getDefault().fireEvent(new ModelModifiedEvent(null));
                        Eclipse.openFile(f);
                    } catch (Exception e) {
                        page.setErrorMessage(e.getMessage());
                        return Status.CANCEL_STATUS;
                    }
                    return Status.OK_STATUS;
                }
            };

            job.schedule();

            return true;
        }

        return false;
    }

    private boolean validate(String nspc, IProject project) {
        /*
         * 1. check that namespace is not already there; set error on page if so
         * 2. check that source folder is valid (later - we only list valid ones)
         */

        for (File p : project.getScripts()) {
            if (MiscUtilities.getFileBaseName(p.toString()).equals(nspc)) {
                this.page.setErrorMessage("Script " + nspc + " already exists in project " + project.getId());
                return false;
            }
        }

        return true;
    }

    public IProject getProject() {
        return target;
    }

}
