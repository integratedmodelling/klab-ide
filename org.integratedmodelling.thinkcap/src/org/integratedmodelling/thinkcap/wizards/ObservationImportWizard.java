/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import org.eclipse.jface.wizard.Wizard;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.thinkcap.views.ObservationRecorder;

public class ObservationImportWizard extends Wizard {

    private ObservationRecorder recorder;
    private IConcept            concept;
    private ObservationImport   importer;

    public ObservationImportWizard(IConcept concept, ObservationRecorder recorder) {
        setWindowTitle("Import observation from an external source");
        this.recorder = recorder;
        this.concept = concept;
    }

    @Override
    public void addPages() {
        addPage(importer = new ObservationImport(concept));
    }

    @Override
    public boolean performFinish() {

        if (importer.getIdField() == null || importer.getData() == null) {
            return false;
        }

        if (importer.getData().size() > 0) {
            recorder.loadData(importer.getData(), importer.getIdField(), concept);
        }
        return true;
    }
}
