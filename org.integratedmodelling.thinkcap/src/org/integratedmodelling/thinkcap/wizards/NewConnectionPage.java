/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.thinkcap.wizards;
//
//import org.eclipse.jface.preference.DirectoryFieldEditor;
//import org.eclipse.jface.wizard.WizardPage;
//import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.SelectionAdapter;
//import org.eclipse.swt.events.SelectionEvent;
//import org.eclipse.swt.layout.GridData;
//import org.eclipse.swt.layout.GridLayout;
//import org.eclipse.swt.widgets.Combo;
//import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Label;
//import org.integratedmodelling.thinklab.common.connections.ConnectionFactory;
//import org.integratedmodelling.thinklab.common.connections.ConnectionFactory.ConnectionType;
//import org.eclipse.swt.widgets.Text;
//
//public class NewConnectionPage extends WizardPage {
//	
//	private DirectoryFieldEditor dirChooser;
//	private Combo combo;
//	private Text text;
//	private Composite serverArea;
//	private Composite folderArea;
//
//	/**
//	 * Create the wizard.
//	 */
//	public NewConnectionPage() {
//		super("wizardPage");
//		setTitle("New data connection");
//		setDescription("Create a new data connection to a local folder, dataset or service. The data sources provided will be available for drag/drop and to import into other connections.");
//	}
//
//	/**
//	 * Create contents of the wizard.
//	 * @param parent
//	 */
//	public void createControl(Composite parent) {
//		Composite container = new Composite(parent, SWT.NULL);
//
//		setControl(container);
//		container.setLayout(new GridLayout(1, false));
//		
//		Composite composite_1 = new Composite(container, SWT.NONE);
//		composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		composite_1.setLayout(new GridLayout(2, false));
//		
//		Label lblConnectionType = new Label(composite_1, SWT.NONE);
//		lblConnectionType.setText("Connection type: ");
//		
//		combo = new Combo(composite_1, SWT.READ_ONLY);
//		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		combo.setSize(564, 23);
//		combo.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				ConnectionType type = ConnectionType.values()[combo.getSelectionIndex()];
//				setInterface(type);
//			}
//		});
//		
//		folderArea = new Composite(container, SWT.NONE);
//		folderArea.setLayout(new GridLayout(2, false));
//		folderArea.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
//		
//		Label lblNewLabel = new Label(folderArea, SWT.NONE);
//		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
//		lblNewLabel.setText("Data folder: ");
//		
//
//		Composite importDirectoryBase = new Composite(folderArea, SWT.NONE);
//		importDirectoryBase.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		dirChooser = new DirectoryFieldEditor("", "", importDirectoryBase);
//		importDirectoryBase.setLayout(new GridLayout(dirChooser.getNumberOfControls(), false));
//		dirChooser.fillIntoGrid(importDirectoryBase, dirChooser.getNumberOfControls());
//		dirChooser.setEmptyStringAllowed(false);
//		
//		serverArea = new Composite(container, SWT.NONE);
//		serverArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		serverArea.setLayout(new GridLayout(2, false));
//		
//		Label lblServerUrl = new Label(serverArea, SWT.NONE);
//		lblServerUrl.setBounds(0, 0, 55, 15);
//		lblServerUrl.setText("Server URL: ");
//		
//		text = new Text(serverArea, SWT.BORDER);
//		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
//		text.setBounds(0, 0, 76, 21);
//
//		serverArea.setVisible(false);
//		
//		for (ConnectionType type : ConnectionType.values()) {
//			combo.add(type.getLabel());
//		}
//		combo.select(0);
//		
// 	}
//	
//	protected void setInterface(ConnectionType type) {
//
//		if (type.equals(ConnectionType.FOLDER)) {
//			serverArea.setVisible(false);
//			folderArea.setVisible(true);
//		} else {
//			serverArea.setVisible(true);
//			folderArea.setVisible(false);			
//		}
//	}
//
//	public ConnectionType getType() {
//		return ConnectionType.values()[combo.getSelectionIndex()];
//	}
//	
//	public String[] getParameters(ConnectionType type) {
//		String[] ret = new String[1];
//		if (type.equals(ConnectionType.FOLDER))
//			ret[0] = dirChooser.getStringValue();
//		else 
//			ret[0] = text.getText();
//		
//		return ret;
//	}
//}
//
