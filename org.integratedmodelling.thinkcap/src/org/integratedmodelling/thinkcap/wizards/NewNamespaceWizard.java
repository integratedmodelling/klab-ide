/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.progress.UIJob;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.common.project.Project.NamespaceGenerator;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.thinkcap.Eclipse;

public class NewNamespaceWizard extends Wizard {

    private NewNamespace       page;
    private IProject           target    = null;
    private NamespaceGenerator generator = null;
    private boolean            allowAppend;

    public NewNamespaceWizard(boolean allowAppend) {
        setWindowTitle("Thinklab Namespace");
        this.allowAppend = allowAppend;
    }

    public void setTargetProject(IProject o) {
        this.target = o;
    }

    public void setNamespaceGenerator(NamespaceGenerator generator) {
        this.generator = generator;
    }

    @Override
    public void addPages() {
        addPage(this.page = new NewNamespace(allowAppend));
    }

    @Override
    public boolean performFinish() {

        final String nspc = page.getNamespace().getText().trim();
        final IProject targetProject = KLAB.PMANAGER.getProject(page.getTargetProject().getText());
        final boolean isScenario = page.getCreateScenario().getSelection();
        final boolean isAppending = /* TODO true if appending to an existing ns */false;

        if (validate(nspc, targetProject)) {

            Job job = new UIJob("Creating namespace " + nspc) {
                @Override
                public IStatus runInUIThread(IProgressMonitor monitor) {
                    try {
                        String f = null;
                        if (generator == null) {
                            f =
                                    targetProject.getId() + File.separator +
                                            ((Project) targetProject).createNamespace(nspc, isScenario);
                        } else {
                            f = targetProject.getId()
                                    + File.separator
                                    +
                                    ((Project) targetProject)
                                            .createNamespace(nspc, isScenario, isAppending, generator);
                        }
                        Eclipse.openFile(f);

                    } catch (Exception e) {
                        page.setErrorMessage(e.getMessage());
                        return Status.CANCEL_STATUS;
                    }
                    return Status.OK_STATUS;
                }
            };

            job.schedule();

            return true;
        }

        return false;
    }

    private boolean validate(String nspc, IProject project) {

        if (project == null) {
            return false;
        }

        /*
         * 1. check that namespace is not already there; set error on page if so
         * 2. check that source folder is valid (later - we only list valid ones)
         */

        for (IProject p : ((ProjectManager) KLAB.PMANAGER).getProjects()) {
            for (INamespace n : p.getNamespaces()) {
                if (n.getId().equals(nspc)) {
                    this.page
                            .setErrorMessage("Namespace " + nspc + " already exists in project " + p.getId());
                    return false;
                }
            }
        }

        if (StringUtils.containsAny(nspc, StringUtils.UPPERCASE | StringUtils.WHITESPACE
                | StringUtils.NONLETTERS)) {
            page.setErrorMessage("namespace identifiers must contain only lowercase letters with no whitespace");
            return false;
        }

        return true;
    }

    public IProject getProject() {
        return target;
    }

}
