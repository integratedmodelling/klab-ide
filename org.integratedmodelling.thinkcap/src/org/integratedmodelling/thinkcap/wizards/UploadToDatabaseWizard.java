/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.model.ShapeRecord;
import org.integratedmodelling.thinkcap.views.ObservationRecorder;

public class UploadToDatabaseWizard extends Wizard {

    UploadToDatabase            page;
    private ObservationRecorder recorder;

    public UploadToDatabaseWizard(ObservationRecorder recorder) {
        setWindowTitle("Upload observations");
        this.recorder = recorder;
    }

    @Override
    public void addPages() {
        addPage(page = new UploadToDatabase());
    }

    @Override
    public boolean performFinish() {

        if (page.nodes.size() == 0) {
            return true;
        }

        Job job = new Job("Saving observations to database") {

            @Override
            protected IStatus run(IProgressMonitor monitor) {

                int nobs = recorder.countPublished();
                monitor.beginTask("Saving " + nobs + " observations to " + StringUtils.join(page.nodes, ", ")
                        + " observation database", nobs);
                int n = 0;

                for (ShapeRecord sr : recorder.getData()) {

                    if (monitor.isCanceled()) {
                        return Status.CANCEL_STATUS;
                    }
                    if (sr.publish) {

                        n++;

                        IDirectObserver sor = sr.makeSubjectObserver();
                        monitor.subTask("publishing " + sor.getId() + " (" + n + "/" + nobs + ")");

                        for (String node : page.nodes) {
                            try {
                                if (node.equals("local")) {
                                    Activator.engine().submitObservation(sor, true);
                                } else {
                                    IServer nd = KLAB.ENGINE.getNetwork().getNode(node);
                                    if (nd != null) {
                                        nd.submitObservation(sor, true);
                                    }
                                }
                            } catch (KlabException e) {
                                Eclipse.handleException(e);
                                return Status.CANCEL_STATUS;
                            } finally {
                                monitor.done();
                            }
                        }

                        monitor.worked(1);
                    }
                }
                monitor.done();
                recorder.clearPublished();
                return Status.OK_STATUS;
            }
        };
        job.setUser(true);
        job.schedule();

        return true;
    }
}
