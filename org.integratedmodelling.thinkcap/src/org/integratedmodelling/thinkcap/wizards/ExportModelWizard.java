package org.integratedmodelling.thinkcap.wizards;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import org.eclipse.jface.wizard.Wizard;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.EclipseWorkspace;

public class ExportModelWizard extends Wizard {

    private LocalExportResponse def;
    private ExportModelPage     page;

    public ExportModelWizard(LocalExportResponse def) {
        this.def = def;
        setWindowTitle("Export model");
    }

    @Override
    public void addPages() {
        addPage(this.page = new ExportModelPage(def));
    }

    @Override
    public boolean performFinish() {

        if (page.isExport()) {

            if (!KIM.validateProjectId(page.getProject())) {
                page.setErrorMessage("invalid project name");
                return false;
            }
            if (!KIM.validateNamespaceId(page.getProject())) {
                page.setErrorMessage("invalid namespace identifier");
                return false;
            }

            INamespace ns = KLAB.MMANAGER.getNamespace(page.getNamespace());

            if (ns != null
                    && (ns.getProject() == null
                            || !ns.getProject().getId().equals(page.getProject()))) {
                page.setErrorMessage("namespace " + page.getNamespace()
                        + " already exists in project " + ns.getProject().getId());
                return false;
            }

            IProject prj = KLAB.PMANAGER.getProject(page.getProject());
            if (prj == null) {
                prj = EclipseWorkspace.addProject(page.getProject());
            }

            if (ns == null) {
                try {
                    ((Project) prj).createNamespace(page.getNamespace(), false);
                    ns = KLAB.MMANAGER.getNamespace(page.getNamespace());
                } catch (KlabException e) {
                    page.setErrorMessage("error creating namespace "
                            + page.getNamespace());
                    return false;
                }
            }

            if (ns != null) {
                File file = ns.getLocalFile();
                if (file != null) {

                    /*
                     * export files to project directory
                     */
                    try {
                        ((ProjectManager) KLAB.PMANAGER).exportFilesToProject(def, prj);
                    } catch (KlabIOException e1) {
                        page.setErrorMessage("error exporting associated files");
                        return false;
                    }

                    /*
                     * append model
                     */
                    try {
                        Files.write(file.toPath(), ("\n" + page.getModelCode())
                                .getBytes(), StandardOpenOption.APPEND);
                    } catch (IOException e) {
                        page.setErrorMessage("error writing model text to namespace");
                        return false;
                    }

                    return true;
                }
            }

        } else if (!page.getModelCode().trim().isEmpty()) {
            Eclipse.copyToClipboard(page.getModelCode());
            return true;
        }

        return false;
    }

}
