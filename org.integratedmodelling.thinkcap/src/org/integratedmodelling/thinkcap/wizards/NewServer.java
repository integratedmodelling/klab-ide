/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.wb.swt.SWTResourceManager;

public class NewServer extends WizardPage {
	private Text text;
	private Text text_1;
	private Text text_2;
	private Text text_3;
	private Button btnSavePassword;

	/**
	 * Create the wizard.
	 */
	public NewServer() {
		super("newServer");
		setTitle("New Thinklab server");
		setDescription("Create a new Thinklab server description");
	}

	/**
	 * Create contents of the wizard.
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);

		setControl(container);
		container.setLayout(new FormLayout());
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.top = new FormAttachment(0, 41);
		fd_lblNewLabel.left = new FormAttachment(0, 98);
		lblNewLabel.setLayoutData(fd_lblNewLabel);
		lblNewLabel.setText("Server name");
		
		text = new Text(container, SWT.BORDER);
		fd_lblNewLabel.right = new FormAttachment(100, -411);
		FormData fd_text = new FormData();
		fd_text.left = new FormAttachment(0, 175);
		fd_text.top = new FormAttachment(lblNewLabel, -3, SWT.TOP);
		text.setLayoutData(fd_text);
		
		Label lblServerUrl = new Label(container, SWT.NONE);
		lblServerUrl.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblServerUrl.setText("Server URL");
		FormData fd_lblServerUrl = new FormData();
		fd_lblServerUrl.left = new FormAttachment(0, 107);
		fd_lblServerUrl.right = new FormAttachment(lblNewLabel, 0, SWT.RIGHT);
		lblServerUrl.setLayoutData(fd_lblServerUrl);
		
		text_1 = new Text(container, SWT.BORDER);
		fd_lblServerUrl.top = new FormAttachment(text_1, 3, SWT.TOP);
		fd_text.right = new FormAttachment(100, -122);
		FormData fd_text_1 = new FormData();
		fd_text_1.left = new FormAttachment(0, 175);
		fd_text_1.top = new FormAttachment(text, 21);
		fd_text_1.right = new FormAttachment(100, -122);
		text_1.setLayoutData(fd_text_1);
		
		Label lblUsername = new Label(container, SWT.NONE);
		lblUsername.setText("Username");
		FormData fd_lblUsername = new FormData();
		fd_lblUsername.left = new FormAttachment(0, 110);
		fd_lblUsername.top = new FormAttachment(lblServerUrl, 23);
		fd_lblUsername.right = new FormAttachment(lblNewLabel, 0, SWT.RIGHT);
		lblUsername.setLayoutData(fd_lblUsername);
		
		Label lblPassword = new Label(container, SWT.NONE);
		lblPassword.setText("Password");
		FormData fd_lblPassword = new FormData();
		fd_lblPassword.left = new FormAttachment(0, 113);
		fd_lblPassword.right = new FormAttachment(lblNewLabel, 0, SWT.RIGHT);
		lblPassword.setLayoutData(fd_lblPassword);
		
		text_2 = new Text(container, SWT.BORDER);
		FormData fd_text_2 = new FormData();
		fd_text_2.right = new FormAttachment(text, -1, SWT.RIGHT);
		fd_text_2.top = new FormAttachment(lblUsername, 0, SWT.TOP);
		fd_text_2.left = new FormAttachment(text, 0, SWT.LEFT);
		text_2.setLayoutData(fd_text_2);
		
		text_3 = new Text(container, SWT.BORDER);
		fd_lblPassword.top = new FormAttachment(text_3, 3, SWT.TOP);
		FormData fd_text_3 = new FormData();
		fd_text_3.right = new FormAttachment(text, -1, SWT.RIGHT);
		fd_text_3.left = new FormAttachment(text, 0, SWT.LEFT);
		fd_text_3.top = new FormAttachment(text_2, 19);
		text_3.setLayoutData(fd_text_3);
		
		btnSavePassword = new Button(container, SWT.CHECK);
		FormData fd_btnSavePassword = new FormData();
		fd_btnSavePassword.top = new FormAttachment(text_3, 21);
		fd_btnSavePassword.left = new FormAttachment(text, 0, SWT.LEFT);
		btnSavePassword.setLayoutData(fd_btnSavePassword);
		btnSavePassword.setText("Save password");
		
		Button btnConnectNow = new Button(container, SWT.CHECK);
		FormData fd_btnConnectNow = new FormData();
		fd_btnConnectNow.top = new FormAttachment(btnSavePassword, 0, SWT.TOP);
		fd_btnConnectNow.left = new FormAttachment(btnSavePassword, 6);
		btnConnectNow.setLayoutData(fd_btnConnectNow);
		btnConnectNow.setText("Connect now");
	}
	public Text getServerName() {
		return text;
	}
	public Text getServerURL() {
		return text_1;
	}
	public Text getUsername() {
		return text_2;
	}
	public Text getPassword() {
		return text_3;
	}
	public Button getBtnSavePassword() {
		return btnSavePassword;
	}
}
