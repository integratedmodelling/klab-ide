package org.integratedmodelling.thinkcap.wizards;

import java.text.NumberFormat;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.ui.EclipseViewer;

public class AddFeature extends WizardPage {
    private Text  text;
    private Text  text_1;

    EclipseViewer viewer;
    IConcept      concept;
    double        lon;
    double        lat;
    private String shape;

    /**
     * Create the wizard.
     */
    public AddFeature(EclipseViewer viewer, IConcept concept, double lon, double lat) {
        super("wizardPage");
        this.lat = lat;
        this.lon = lon;
        this.viewer = viewer;
        this.concept = concept;
        setImageDescriptor(ResourceManager
                .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));
        setTitle("Add a new " + NS.getDisplayName(concept));
        setDescription("Create a " + NS.getDisplayName(concept));
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);

        setControl(container);

        Label lblSpatialExtent = new Label(container, SWT.NONE);
        lblSpatialExtent.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
        lblSpatialExtent.setBounds(10, 10, 344, 23);
        lblSpatialExtent.setText("Spatial extent:");

        Button btnPoint = new Button(container, SWT.RADIO);
        btnPoint.setSelection(true);
        btnPoint.setBounds(79, 39, 275, 16);
        btnPoint.setText("Point @ " + NumberFormat.getInstance().format(lat) + ", "
                + NumberFormat.getInstance().format(lon));

        Button btnDrawAPolygon = new Button(container, SWT.RADIO);
        btnDrawAPolygon.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                /*
                 * set map in draw polygon mode and listen to shapes
                 */
                viewer.setContextEditMode(true);
                text_1.setText("");
            }
        });
        btnDrawAPolygon.setBounds(79, 61, 275, 16);
        btnDrawAPolygon.setText("Draw a polygonal feature");

        Button btnDrawALinear = new Button(container, SWT.RADIO);
        btnDrawALinear.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                /*
                 * set map in draw line mode, listen to shapes
                 */
                viewer.setContextEditMode(true);
                text_1.setText("");
            }
        });
        btnDrawALinear.setBounds(79, 83, 275, 16);
        btnDrawALinear.setText("Draw a linear feature");

        text = new Text(container, SWT.BORDER);
        text.setBounds(71, 119, 283, 21);
        text.setText(CamelCase.toLowerCase(NS.getDisplayName(concept), '-')
                + (Environment.get().getContextStructure() == null ? "-1"
                        : ("-" + (Environment.get().getContextStructure().count(concept) + 1))));

        Label lblName = new Label(container, SWT.NONE);
        lblName.setBounds(10, 122, 344, 15);
        lblName.setText("Name:");

        text_1 = new Text(container, SWT.BORDER);
        text_1.setEditable(false);
        text_1.setBounds(71, 146, 283, 116);
        text_1.setText(this.shape = "POINT (" + lat + " " + lon + ")");

        Label lblWkt = new Label(container, SWT.NONE);
        lblWkt.setBounds(10, 149, 344, 15);
        lblWkt.setText("WKT");
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
        text_1.setText(shape);
    }

    public String getFeatureName() {
        return text.getText();
    }
}
