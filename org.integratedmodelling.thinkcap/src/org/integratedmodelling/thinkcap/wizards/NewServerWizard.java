/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.wizard.Wizard;

public class NewServerWizard extends Wizard {

	public NewServerWizard() {
		setWindowTitle("New Thinklab server");
	}

	@Override
	public void addPages() {
		addPage(new NewServer());
	}

	@Override
	public boolean performFinish() {
		
		String server = ((NewServer)(getPages()[0])).getServerName().getText();
		String url = ((NewServer)(getPages()[0])).getServerURL().getText();
		String user = ((NewServer)(getPages()[0])).getUsername().getText();
		String password = ((NewServer)(getPages()[0])).getPassword().getText();
		
		boolean savePassword = ((NewServer)(getPages()[0])).getBtnSavePassword().getSelection();
		
		/*
		 * TODO validation
		 */
		if (validate(server, url, user, password, savePassword)) {

			Job job = new WorkspaceJob("Adding server " + server) {
			      public IStatus runInWorkspace(IProgressMonitor monitor) 
			         throws CoreException {

			    	  // TODO ADD SERVER, notify all views
			    	  
			    	  return Status.OK_STATUS;
			      }
			   };

			job.schedule();

			return true;
		}
		/*
		 * TODO return true if everything OK
		 */
		
		return false;
	}

	private boolean validate(String server, String url, String user,
			String password, boolean savePassword) {
		// TODO Auto-generated method stub
		return false;
	}

}
