/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.progress.UIJob;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.EclipseWorkspace;

public class NewProjectWizard extends Wizard implements INewWizard {

    private NewProject page;

    public NewProjectWizard() {
        setWindowTitle("New Thinklab project");
    }

    @Override
    public void addPages() {
        addPage(page = new NewProject());
    }

    @Override
    public boolean performFinish() {

        final String name = page.getProjectName().getText();
        final String nspc = page.getNamespace().getText();
        final ArrayList<String> deps = new ArrayList<String>();

        if (validate(name, nspc, deps)) {

            Job job = new UIJob("Creating project " + name) {
                @Override
                public IStatus runInUIThread(IProgressMonitor monitor) {
                    try {
                        IProject p = EclipseWorkspace.addProject(name);
                        String f =
                                p.getId() + File.separator +
                                        ((Project) p).createNamespace(nspc, false);
                        Eclipse.openFile(f);

                    } catch (Exception e) {
                        page.setErrorMessage(e.getMessage());
                        return Status.CANCEL_STATUS;
                    }
                    return Status.OK_STATUS;
                }
            };

            job.schedule();

            return true;
        }

        return false;
    }

    private boolean validate(String src, String nspc, ArrayList<String> deps) {

        // TODO do something sensible
        if (src.isEmpty())
            return false;
        if (nspc.isEmpty())
            return false;

        if (StringUtils.containsAny(src, StringUtils.UPPERCASE | StringUtils.WHITESPACE
                | StringUtils.NONLETTERS)) {
            page.setErrorMessage("project names must contain only lowercase letters with no whitespace");
            return false;
        }

        if (StringUtils.containsAny(src, StringUtils.UPPERCASE | StringUtils.WHITESPACE
                | StringUtils.NONLETTERS)) {
            page.setErrorMessage("namespace identifiers must contain only lowercase letters with no whitespace");
            return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection) {
        // TODO Auto-generated method stub

    }

}
