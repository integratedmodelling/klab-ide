/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.wizards;

import java.io.File;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.client.Environment;

public class ExportData extends WizardPage {

    private Text     text;
    private IContext context;
    String[]         filterExt          = { "*.zip", "*.shp", "*.*" };

    static final int CONTEXT_BOUNDARIES = 1;
    static final int ALL_SUBJECTS       = 2;
    static final int ALL_STATES         = 3;
    static final int CURRENT_STATE      = 4;
    static final int CURRENT_MOVIE      = 5;
    static final int FULL_DATASET       = 6;

    int              currentChoice      = 0;
    Button           aggregateTemporalCheckbox;

    /**
     * Create the wizard.
     */
    public ExportData(IContext context) {
        super("Export data");
        setImageDescriptor(ResourceManager
                .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));
        setTitle("Export data");
        setDescription("Export data from the current context");
        this.context = context;
    }

    public File getFile() {
        String fname = text.getText().trim();
        /*
         * TODO add extension if absent; make other obvious checks.
         */
        if (fname.isEmpty()) {
            return null;
        }
        return new File(fname);
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);

        setControl(container);

        Button btnCurrentContextBoundaries = new Button(container, SWT.RADIO);
        btnCurrentContextBoundaries.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                filterExt = new String[] { "*.shp" };
                currentChoice = CONTEXT_BOUNDARIES;
            }
        });
        btnCurrentContextBoundaries.setBounds(93, 54, 234, 16);
        btnCurrentContextBoundaries.setText("Current context boundaries");
        btnCurrentContextBoundaries.setEnabled(context != null);

        Button btnCurrentlySelectedState = new Button(container, SWT.RADIO);
        btnCurrentlySelectedState.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                filterExt = new String[] { "*.tif", "*.png" };
                currentChoice = CURRENT_STATE;
            }
        });
        btnCurrentlySelectedState.setBounds(93, 76, 234, 16);
        btnCurrentlySelectedState.setText("Current data for currently selected state");
        btnCurrentlySelectedState.setEnabled(context != null
                && Environment.get().getSession().getViewer().getVisibleSpatialState() != null);

        Button btnAllStatesIn = new Button(container, SWT.RADIO);
        btnAllStatesIn.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                filterExt = new String[] { "*.zip" };
                currentChoice = ALL_STATES;
            }
        });
        btnAllStatesIn.setBounds(93, 98, 207, 16);
        btnAllStatesIn.setText("Current data for all states in context (zip file)");
        btnAllStatesIn.setEnabled(context != null && context.getSubject().getStates().size() > 0);

        Button btnCurrentStateAs = new Button(container, SWT.RADIO);
        btnCurrentStateAs.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                filterExt = new String[] { "*.gif" };
                currentChoice = CURRENT_MOVIE;
            }
        });
        btnCurrentStateAs.setBounds(93, 120, 190, 16);
        btnCurrentStateAs.setText("Current state as a movie file");
        btnCurrentStateAs.setEnabled(context != null
                && Environment.get().getSession().getViewer().getVisibleSpatialState() != null
                // FIXME this is never true
                // && context.isFinished()
                && context.getSubject().getScale().isTemporallyDistributed()
                && context.getSubject().getScale().isSpatiallyDistributed());

        Label lblDefineWhatShould = new Label(container, SWT.NONE);
        lblDefineWhatShould.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
        lblDefineWhatShould.setBounds(30, 21, 270, 15);
        lblDefineWhatShould.setText("Select what should be exported:");

        Label lblChooseExportDestination = new Label(container, SWT.NONE);
        lblChooseExportDestination.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
        lblChooseExportDestination.setBounds(30, 232, 184, 15);
        lblChooseExportDestination.setText("Choose export destination:");

        text = new Text(container, SWT.BORDER);
        text.setBounds(30, 253, 423, 21);

        Button btnBrowse = new Button(container, SWT.NONE);
        btnBrowse.setBounds(460, 251, 75, 25);
        btnBrowse.setText("Browse");
        btnBrowse.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
                fd.setText("Open file");
                fd.setFilterPath(System.getProperty("user.home"));
                fd.setFilterExtensions(filterExt);
                String selected = fd.open();
                text.setText(selected);
            }
        });
        Button btnSubjectsOfClass = new Button(container, SWT.RADIO);
        btnSubjectsOfClass.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                filterExt = new String[] { "*.shp", "*.csv" };
                currentChoice = ALL_SUBJECTS;
            }
        });
        btnSubjectsOfClass.setBounds(93, 144, 131, 16);
        btnSubjectsOfClass.setText("All subjects of type:");
//        btnSubjectsOfClass.setEnabled(false);

        Combo combo = new Combo(container, SWT.READ_ONLY);
        combo.setBounds(230, 142, 223, 23);

        Button button = new Button(container, SWT.RADIO);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        button.setText("Full gridded dataset (NetCDF)");
        button.setEnabled(false);
        button.setBounds(93, 166, 234, 16);

        aggregateTemporalCheckbox = new Button(container, SWT.CHECK);
        aggregateTemporalCheckbox.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        aggregateTemporalCheckbox.setBounds(93, 203, 184, 15);
        aggregateTemporalCheckbox.setText("Aggregate temporal output");
    }

    public IConcept getSubjectType() {
        // TODO return the concept of the subjects we want to export.
        return null;
    }

}
