package org.integratedmodelling.thinkcap.wizards;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.thinkcap.Eclipse;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Link;

public class ExportModelPage extends WizardPage {

    private Combo       combo;
    private Label       lblInProject;
    private Combo       combo_1;
    private Table       table;
    private StyledText  styledText;

    Button              btnExportToNamespace;

    LocalExportResponse def;

    /**
     * Create the wizard.
     */
    public ExportModelPage(LocalExportResponse def) {
        super("exportModel");
        this.def = def;
        setImageDescriptor(ResourceManager
                .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/logo_white_64.jpg"));
        setTitle("Export model");
        setDescription("Export source code and associated files for k.LAB-generated model");
    }

    /**
     * Create contents of the wizard.
     * 
     * @param parent
     */
    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);

        setControl(container);

        styledText = new StyledText(container, SWT.BORDER);
        styledText.setBounds(10, 20, 554, 301);

        styledText.setText(def.getModelStatement());

        Group grpFiles = new Group(container, SWT.NONE);
        grpFiles.setText("Exported files");
        grpFiles.setBounds(10, 327, 554, 59);
        grpFiles.setLayout(null);

        table = new Table(grpFiles, SWT.BORDER | SWT.FULL_SELECTION);
        table.setBounds(8, 15, 536, 39);
        table.setLinesVisible(true);
        table.setEnabled(def.getFiles().size() > 0);

        for (String file : def.getFiles()) {
            TableItem item = new TableItem(table, SWT.NONE);
            item.setText(MiscUtilities.getFileName(file));
        }

        Button btnCopySourceTo = new Button(container, SWT.RADIO);
        btnCopySourceTo.setSelection(true);
        btnCopySourceTo.setBounds(10, 409, 152, 16);
        btnCopySourceTo.setText("Copy source to clipboard");

        btnExportToNamespace = new Button(container, SWT.RADIO);
        btnExportToNamespace.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                combo.setEnabled(btnExportToNamespace.getSelection());
                lblInProject.setEnabled(btnExportToNamespace.getSelection());
                combo_1.setEnabled(btnExportToNamespace.getSelection());
            }
        });
        btnExportToNamespace.setBounds(10, 437, 131, 16);
        btnExportToNamespace.setText("Export to namespace");

        combo = new Combo(container, SWT.NONE);
        combo.setEnabled(false);
        combo.setBounds(142, 435, 196, 23);

        lblInProject = new Label(container, SWT.NONE);
        lblInProject.setEnabled(false);
        lblInProject.setBounds(344, 438, 55, 15);
        lblInProject.setText("in project");

        combo_1 = new Combo(container, SWT.NONE);
        combo_1.setEnabled(false);
        combo_1.setBounds(405, 435, 159, 23);

        Link link = new Link(container, SWT.NONE);
        link.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                exportFiles();
            }
        });
        link.setBounds(168, 409, 131, 15);
        link.setText("<a>(Export files)</a>");
        link.setEnabled(def.getFiles().size() > 0);

        // combo_1.add("");
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            combo_1.add(p.getId());
        }
        combo_1.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                // TODO if project exists, load namespaces in ns combo
                String pid = combo_1.getText();
                if (!pid.trim().isEmpty()) {
                    IProject project = KLAB.PMANAGER.getProject(pid);
                    if (project != null) {
                        combo.removeAll();
                        // combo.add("");
                        for (INamespace ns : project.getNamespaces()) {
                            combo.add(ns.getId());
                        }
                    }
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                // TODO Auto-generated method stub

            }
        });
    }

    protected void exportFiles() {

        String[] fileType = new String[] {
                "*." + MiscUtilities.getFileExtension(def.getFiles().get(0))
        };
        
        FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
        dialog.setFilterExtensions(fileType);
        File path = new File(System.getProperty("user.home"));
        if (KLAB.CONFIG.getDefaultExportPath() != null) {
            path = KLAB.CONFIG.getDefaultExportPath();
        }
        dialog.setFilterPath(path.getPath());
        String ret = dialog.open();
        if (ret == null) {
            return;
        }

        String pth = MiscUtilities.getFilePath(ret);
        String prf = MiscUtilities.getFileBaseName(ret);
        for (String s : def.getFiles()) {
            String ext = MiscUtilities.getFileExtension(s);
            try {
                FileUtils.copyFile(new File(s), new File(pth + File.separator + prf + "." + ext));
            } catch (IOException e) {
                Eclipse.handleException(e);
                break;
            }
        }
    }

    public String getNamespace() {
        return combo.getText();
    }

    public String getProject() {
        return combo_1.getText();
    }

    public boolean isExport() {
        return btnExportToNamespace.getSelection();
    }

    public String getModelCode() {
        return styledText.getText() == null ? "" : styledText.getText();
    }
}
