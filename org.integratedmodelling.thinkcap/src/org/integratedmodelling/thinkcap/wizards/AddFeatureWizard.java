package org.integratedmodelling.thinkcap.wizards;

import org.eclipse.jface.wizard.Wizard;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.thinkcap.ui.EclipseViewer;

public class AddFeatureWizard extends Wizard {

    AddFeature            page;
    private EclipseViewer viewer;
    double                lat;
    double                lon;
    IConcept              concept;

    public AddFeatureWizard(EclipseViewer viewer, IConcept concept, double lat, double lon) {
        this.viewer = viewer;
        this.lat = lat;
        this.lon = lon;
        this.concept = concept;
        setWindowTitle("Add a new spatial subject");
    }

    @Override
    public void addPages() {
        addPage(page = new AddFeature(viewer, concept, lat, lon));
    }

    @Override
    public boolean performFinish() {
        
        if (page.getShape() != null) {
            return true;
        }
        
        return false;
    }

    public String getShape() {
        return page.getShape();
    }
    
    public void setShape(String shape) {
        page.setShape(shape);
    }

    public String getName() {
        return page.getFeatureName();
    }
}
