/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.thinkcap.wizards;
//
//import java.io.File;
//
//import org.eclipse.core.runtime.IProgressMonitor;
//import org.eclipse.core.runtime.IStatus;
//import org.eclipse.core.runtime.Status;
//import org.eclipse.core.runtime.jobs.Job;
//import org.eclipse.jface.wizard.Wizard;
//import org.eclipse.ui.progress.UIJob;
//import org.integratedmodelling.thinklab.api.connection.IConnection;
//import org.integratedmodelling.thinklab.api.project.IProject;
//import org.integratedmodelling.thinklab.common.connections.ConnectionFactory;
//import org.integratedmodelling.thinklab.common.connections.ConnectionFactory.ConnectionType;
//
//public class NewConnection extends Wizard {
//	
//	private NewConnectionPage page;
//	private IProject project;
//
//	public NewConnection() {
//		setWindowTitle("New data connection");
//	}
//
//	@Override
//	public void addPages() {
//		addPage(page = new NewConnectionPage());
//	}
//
//	@Override
//	public boolean performFinish() {
//		
//		final ConnectionType type = page.getType();
//		final String[] parameters = page.getParameters(type);
//		
//		if (validate(type, parameters)) {
//
//			Job job = new UIJob("Creating " + type.getLabel() + " connection") {
//				@Override
//				public IStatus runInUIThread(IProgressMonitor monitor) {
//					try {
//						IConnection connection = ConnectionFactory.get().getConnection(type, parameters);
//						if (connection == null) {
//							page.setErrorMessage("Error creating " + type + " connection file");
//						} else { 
//							File cfolder = new File(project.getLoadPath() + File.separator + ".connections");
//							cfolder.mkdirs();
//							connection.write(cfolder);
//						}
//					} catch (Exception e) {
//						page.setErrorMessage(e.getMessage());
//						return Status.CANCEL_STATUS;
//					}
//					return Status.OK_STATUS;
//				}
//			};
//
//			job.schedule();
//
//			return true;
//		}
//
//		return false;
//	}
//
//	private boolean validate(ConnectionType type, String[] parameters) {
//		return type != null && parameters != null && parameters.length > 0 &&
//			  parameters[0] != null && !parameters[0].trim().isEmpty();
//	}
//
//	public void setTargetProject(IProject tlProject) {
//		this.project = tlProject;
//	}
//
// }
