package org.integratedmodelling.thinkcap.perspectives;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.internal.e4.compatibility.ModeledPageLayout;
import org.integratedmodelling.thinkcap.views.DatasetView;

public class ExplorerPerspective implements IPerspectiveFactory {

    /**
     * Creates the initial layout for a page.
     */
    @Override
    public void createInitialLayout(IPageLayout layout) {
        String editorArea = layout.getEditorArea();
        addFastViews(layout);
        addViewShortcuts(layout);
        addPerspectiveShortcuts(layout);
        layout.addStandaloneView("org.integratedmodelling.thinkcap.views.ExplorerStatusBar", false, IPageLayout.BOTTOM, 0.96f, IPageLayout.ID_EDITOR_AREA);
        layout.addView("org.integratedmodelling.thinkcap.views.PaletteView", IPageLayout.LEFT, 0.33f, IPageLayout.ID_EDITOR_AREA);
        ((ModeledPageLayout)layout).stackView(DatasetView.ID, IPageLayout.ID_EDITOR_AREA, true);    
   }

    /**
     * Add fast views to the perspective.
     */
    private void addFastViews(IPageLayout layout) {
    }

    /**
     * Add view shortcuts to the perspective.
     */
    private void addViewShortcuts(IPageLayout layout) {
    }

    /**
     * Add perspective shortcuts to the perspective.
     */
    private void addPerspectiveShortcuts(IPageLayout layout) {
    }

}
