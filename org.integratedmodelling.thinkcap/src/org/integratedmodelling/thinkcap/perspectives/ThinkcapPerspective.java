/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.perspectives;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.internal.e4.compatibility.ModeledPageLayout;
import org.integratedmodelling.thinkcap.views.DatasetView;

public class ThinkcapPerspective implements IPerspectiveFactory {

    public static final String ID = "org.integratedmodelling.thinkcap.views.ThinkcapPerspective";

    /**
     * Creates the initial layout for a page.
     */
    @Override
    public void createInitialLayout(IPageLayout layout) {

        // String editorArea = layout.getEditorArea();
        layout.addView("org.integratedmodelling.thinkcap.views.ProvenanceView", IPageLayout.RIGHT, 0.79f, IPageLayout.ID_EDITOR_AREA);
        layout.addView("org.integratedmodelling.thinkcap.views.Servers", IPageLayout.BOTTOM, 0.5f, "org.integratedmodelling.thinkcap.views.ProvenanceView");

        addFastViews(layout);
        addViewShortcuts(layout);
        addPerspectiveShortcuts(layout);
        layout.addView("org.integratedmodelling.thinkcap.navigator.tlview", IPageLayout.LEFT, 0.35f, IPageLayout.ID_EDITOR_AREA);
        layout.addView("org.integratedmodelling.thinkcap.views.Scenarios", IPageLayout.BOTTOM, 0.69f, IPageLayout.ID_EDITOR_AREA);
        layout.addView("org.integratedmodelling.thinkcap.views.ContextView", IPageLayout.BOTTOM, 0.69f, "org.integratedmodelling.thinkcap.navigator.tlview");
        ((ModeledPageLayout)layout).stackView(DatasetView.ID, IPageLayout.ID_EDITOR_AREA, true);
        
        /*
         * TODO ensure map view and recorder appear where they should - not sure how to do it
         * with placeholders and ratios.
         */
        // layout.addPlaceholder(ObservationRecorder.ID, IPageLayout.BOTTOM, ratio, layout.getEditorArea());

    }

    /**
     * Add fast views to the perspective.
     */
    private void addFastViews(IPageLayout layout) {
    }

    /**
     * Add view shortcuts to the perspective.
     */
    private void addViewShortcuts(IPageLayout layout) {
    }

    /**
     * Add perspective shortcuts to the perspective.
     */
    private void addPerspectiveShortcuts(IPageLayout layout) {
    }

}
