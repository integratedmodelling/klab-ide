package org.integratedmodelling.thinkcap;

import java.util.logging.Level;

import org.eclipse.core.runtime.CoreException;
import org.integratedmodelling.common.client.ClientMonitor;
import org.integratedmodelling.thinkcap.events.NotificationEvent;

/**
 * Use the Eclipse infobus to send around notifications.
 * 
 * @author Ferd
 *
 */
public class EclipseMonitor extends ClientMonitor {

    @Override
    public void debug(Object o) {

        if (Activator.getDefault() != null && Activator.getDefault().isGUIActive()
                && Activator.getDefault().isDebugging()) {
            Activator.getDefault().fireEvent(new NotificationEvent(o.toString(), Level.FINE, this
                    .getSession(), this.getContext(), this.getTask()));
        } else {
            System.out.println("" + o);
        }
    }

    @Override
    public void error(Object error) {

        /*
         * CoreException happens (in its ResourceException incarnation) when Eclipse and Thinklab compete to
         * recompile the workspace. Should be fixed but for now just avoid the red marks in the log and
         * the screenshot emailing from the modeling monkeys.
         */
        if (Activator.getDefault() != null && Activator.getDefault().isGUIActive()
                && !(error instanceof CoreException)) {
            Activator.getDefault().fireEvent(new NotificationEvent(notify(error, Level.SEVERE, null)
                    .toString(), Level.SEVERE, this
                            .getSession(), this.getContext(), this.getTask()));
        } else {
            /*
             * TODO gather all errors and other messages and show them as
             * soon as the GUI is up.
             */
            Eclipse.alert("" + error);
        }
    }

    @Override
    public void info(Object info, String infoClass) {
        if (Activator.getDefault() != null && Activator.getDefault().isGUIActive()) {
            Activator.getDefault().fireEvent(new NotificationEvent(notify(info, Level.INFO, infoClass)
                    .toString(), infoClass, Level.INFO, this
                            .getSession(), this.getContext(), this.getTask()));
        } else {
            System.out.println("" + info);
        }
    }

    @Override
    public void warn(Object o) {
        if (Activator.getDefault() != null && Activator.getDefault().isGUIActive()) {
            if (o instanceof Throwable) {
                o = "exception: " + ((Throwable) o).getMessage();
            }
            Activator.getDefault().fireEvent(new NotificationEvent(notify(o, Level.WARNING, null)
                    .toString(), Level.WARNING, this
                            .getSession(), this.getContext(), this.getTask()));
        } else {
            System.out.println("" + o);
        }
    }

}
