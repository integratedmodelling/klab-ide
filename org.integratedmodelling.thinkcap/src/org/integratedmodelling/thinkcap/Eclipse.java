/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.equinox.internal.p2.core.helpers.ServiceHelper;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.operations.ProvisioningJob;
import org.eclipse.equinox.p2.operations.ProvisioningSession;
import org.eclipse.equinox.p2.operations.UpdateOperation;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mylyn.internal.commons.notifications.ui.popup.NotificationPopup;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.wizards.IWizardDescriptor;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.common.client.EngineController;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.network.Announcements.Announcement;
import org.integratedmodelling.common.utils.BrowserUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.thinkcap.dialogs.MapViewerDialog;
import org.integratedmodelling.thinkcap.dialogs.ProjectPrerequisiteSelectionDialog;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.interfaces.IThinklabProject;
import org.integratedmodelling.thinkcap.model.ThinklabModelManager;

public class Eclipse {

    // TODO notification:
    //
    // requires mylin (org.eclipse.mylin.commons.ui). Then just create
    // a notification object and call open() on it.

    @SuppressWarnings("restriction")
    public static class Notification extends NotificationPopup {

        private String title;
        private String message;
        private String link;

        public Notification(Shell display) {
            super(display);
        }

        public Notification(String title, String message, String link) {
            super(Eclipse.getShell());
            this.title = title;
            this.message = message;
            this.link = link;
        }

        @Override
        protected void createContentArea(Composite composite) {

            composite.setLayout(new GridLayout(1, true));

            /*
             * TODO needs title, date and text. Also should be able to use a list of
             * announcements and allow paging between them.
             */

            if (this.link != null) {
                Link linkGoogleNews = new Link(composite, 0);
                String googlenewsLink = "<a href=\"" + this.link + "\">Learn more...</a>";
                linkGoogleNews.setText(googlenewsLink);
                linkGoogleNews.setSize(400, 100);

                linkGoogleNews.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        try {
                            PlatformUI.getWorkbench().getBrowserSupport()
                                    .getExternalBrowser().openURL(new URL(link));
                        } catch (PartInitException e1) {
                            e1.printStackTrace();
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        protected String getPopupShellTitle() {
            return title;
        }

        @Override
        protected Image getPopupShellImage(int maximumHeight) {
            return null;
        }
    }

    public static void displayNotification(final String title, final String text, final String link) {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                Notification note = new Eclipse.Notification(title, text, link);
                note.open();
            }
        });
    }

    public static void displayNotification(final Announcement ann) {
        displayNotification(ann.getTitle(), ann.getText(), ann.getLink());
    }

    public static IStatus checkForUpdates(IProgressMonitor monitor)
            throws OperationCanceledException {

        @SuppressWarnings("restriction")
        final IProvisioningAgent agent = (IProvisioningAgent) ServiceHelper
                .getService(Activator.getDefault().getBundle()
                        .getBundleContext(), IProvisioningAgent.SERVICE_NAME);

        ProvisioningSession session = new ProvisioningSession(agent);
        // the default update operation looks for updates to the currently
        // running profile, using the default profile root marker. To change
        // which installable units are being updated, use the more detailed
        // constructors.
        UpdateOperation operation = new UpdateOperation(session);
        SubMonitor sub = SubMonitor.convert(monitor == null ? new NullProgressMonitor()
                : monitor, "Checking for application updates...", 200);
        IStatus status = operation.resolveModal(sub.newChild(100));
        if (status.getCode() == UpdateOperation.STATUS_NOTHING_TO_UPDATE) {
            return status;
        }
        if (status.getSeverity() == IStatus.CANCEL)
            throw new OperationCanceledException();

        if (status.getSeverity() != IStatus.ERROR) {
            // More complex status handling might include showing the user what updates
            // are available if there are multiples, differentiating patches vs. updates,
            // etc.
            // In this example, we simply update as suggested by the operation.
            ProvisioningJob job = operation.getProvisioningJob(null);
            status = job.runModal(sub.newChild(100));
            if (status.getSeverity() == IStatus.CANCEL)
                throw new OperationCanceledException();
        }

        alert("Please restart Thinkcap to finish the update.");

        return status;
    }

    // @SuppressWarnings("restriction")
    // public static void setRepositories(String updateSite) throws
    // InvocationTargetException {
    // try {
    // final MetadataRepositoryElement element = new MetadataRepositoryElement(null, new
    // URI(updateSite), true);
    // ElementUtils.updateRepositoryUsingElements(ProvisioningUI
    // .getDefaultUI(), new MetadataRepositoryElement[] { element });
    // } catch (URISyntaxException e) {
    // throw new InvocationTargetException(e);
    // }
    // }

    public static Image getConceptImage(IConcept concept, boolean isNothing) {

        Image ret = null;

        if (concept != null) {

            if (NS.isTrait(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, (isNothing ? "icons/t_error.png"
                        : "icons/t_blue.png"));
                if (concept.is(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
                    ret = ResourceManager.decorateImage(ret, ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "/icons/identity_decorator.gif"), SWTResourceManager.BOTTOM_RIGHT);
                } else if (concept.is(KLAB.c(NS.CORE_REALM_TRAIT))) {
                    ret = ResourceManager.decorateImage(ret, ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "/icons/realm_overlay.gif"), SWTResourceManager.BOTTOM_RIGHT);
                }
            }
            if (NS.isQuality(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, (isNothing ? "icons/q_error.png"
                        : "icons/q_green.png"));
            }
            if (NS.isProcess(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, (isNothing ? "icons/p_error.png"
                        : "icons/p_gold.png"));
            }
            if (NS.isThing(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, (isNothing ? "icons/s_error.png"
                        : "icons/s_orange.png"));
            }
            if (NS.isEvent(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, (isNothing ? "icons/e_error.png"
                        : "icons/e_yellow.png"));
            }
            if (NS.isConfiguration(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/DotsDown.png");
            }
            if (NS.isRole(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/group.gif");
            }
            if (NS.isDomain(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/domain.png");
            }
            if (NS.isRelationship(concept)) {
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/relationship.png");
            }
        }

        if (ret != null && concept != null && concept.isAbstract()) {
            ret = ResourceManager.decorateImage(ret, ResourceManager
                    .getPluginImage(Activator.PLUGIN_ID, "/icons/class_abs_decorator.gif"), SWTResourceManager.TOP_LEFT);
        }

        return ret == null ? ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/question.png") : ret;
    }

    // OK, this is now (2.3 onwards) the marker type that xtext editors will show. Not
    // sure how to
    // more properly handle this, as the type is not exposed by the UI plugin.
    public static String XTEXT_MARKER_TYPE = "org.eclipse.xtext.ui.check.normal";

    public static final class PlatformLogUtil {

        public static void logAsError(Plugin plugin, Object object) {
            IStatus status = StatusUtil.createErrorStatus(plugin, object);
            if (plugin != null) {
                plugin.getLog().log(status);
            } else {
                printStatus(status);
            }
        }

        public static void logAsWarning(Plugin plugin, Object object) {
            IStatus status = StatusUtil.createWarningStatus(plugin, object);
            if (plugin != null) {
                plugin.getLog().log(status);
            } else {
                printStatus(status);
            }
        }

        public static void logAsInfo(Plugin plugin, Object object) {
            IStatus status = StatusUtil.createInfoStatus(plugin, object);
            if (plugin != null) {
                plugin.getLog().log(status);
            } else {
                printStatus(status);
            }
        }

        private static void printStatus(IStatus status) {
            Assert.isNotNull(status);

            // TODO Provide a somehow more sophisticated implementation
            System.out.println(status.getMessage());
            Throwable exception = status.getException();
            if (exception != null) {
                exception.printStackTrace();
            }
        }
    }

    public static class ResourceDeltaFlagsAnalyzer {

        public final boolean ADDED;
        public final boolean ADDED_PHANTOM;
        public final boolean ALL_WITH_PHANTOMS;
        public final boolean CHANGED;
        public final boolean CONTENT;
        public final boolean COPIED_FROM;
        public final boolean DESCRIPTION;
        public final boolean ENCODING;
        public final boolean LOCAL_CHANGED;
        public final boolean MARKERS;
        public final boolean MOVED_FROM;
        public final boolean MOVED_TO;
        public final boolean NO_CHANGE;
        public final boolean OPEN;
        public final boolean REMOVED;
        public final boolean REMOVED_PHANTOM;
        public final boolean REPLACED;
        public final boolean SYNC;
        public final boolean TYPE;
        public final boolean ZERO;

        public ResourceDeltaFlagsAnalyzer(IResourceDelta delta) {
            int flags = delta.getFlags();
            ADDED = (flags & IResourceDelta.ADDED) != 0;
            ADDED_PHANTOM = (flags & IResourceDelta.ADDED_PHANTOM) != 0;
            ALL_WITH_PHANTOMS = (flags & IResourceDelta.ALL_WITH_PHANTOMS) != 0;
            CHANGED = (flags & IResourceDelta.CHANGED) != 0;
            CONTENT = (flags & IResourceDelta.CONTENT) != 0;
            COPIED_FROM = (flags & IResourceDelta.COPIED_FROM) != 0;
            DESCRIPTION = (flags & IResourceDelta.DESCRIPTION) != 0;
            ENCODING = (flags & IResourceDelta.ENCODING) != 0;
            LOCAL_CHANGED = (flags & IResourceDelta.LOCAL_CHANGED) != 0;
            MARKERS = (flags & IResourceDelta.MARKERS) != 0;
            MOVED_FROM = (flags & IResourceDelta.MOVED_FROM) != 0;
            MOVED_TO = (flags & IResourceDelta.MOVED_TO) != 0;
            NO_CHANGE = (flags & IResourceDelta.NO_CHANGE) != 0;
            OPEN = (flags & IResourceDelta.OPEN) != 0;
            REMOVED = (flags & IResourceDelta.REMOVED) != 0;
            REMOVED_PHANTOM = (flags & IResourceDelta.REMOVED_PHANTOM) != 0;
            REPLACED = (flags & IResourceDelta.REPLACED) != 0;
            SYNC = (flags & IResourceDelta.SYNC) != 0;
            TYPE = (flags & IResourceDelta.TYPE) != 0;
            ZERO = flags == 0;
        }

        @Override
        public String toString() {
            return ((ADDED ? " ADDED" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (ADDED_PHANTOM ? " ADDED_PHANTOM" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (CHANGED ? " CHANGED" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (CONTENT ? " CONTENT" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (COPIED_FROM ? " COPIED_FROM" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (DESCRIPTION ? " DESCRIPTION" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (ENCODING ? " ENCODING" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (LOCAL_CHANGED ? " LOCAL_CHANGED" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (MARKERS ? " MARKERS" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (MOVED_FROM ? " MOVED_FROM" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (MOVED_TO ? " MOVED_TO" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (NO_CHANGE ? " NO_CHANGE" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (OPEN ? " OPEN" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (REMOVED ? " REMOVED" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (REMOVED_PHANTOM ? " REMOVED_PHANTOM" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (REPLACED ? " REPLACED" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (SYNC ? " SYNC" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (TYPE ? " TYPE" : "") //$NON-NLS-1$ //$NON-NLS-2$
                    + (ZERO ? " ZERO" : "")).substring(1); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    public static List<?> reverse(List<?> orig) {
        ArrayList<Object> ret = new ArrayList<>();
        for (int i = orig.size() - 1; i >= 0; i--) {
            ret.add(orig.get(i));
        }
        return ret;
    }

    public static class StringStorage implements IStorage {

        private String string = "";
        private String name;

        StringStorage(String name) {
            this.name = name;
        }

        StringStorage(String name, String content) {
            this.name = name;
            this.string = content;
        }

        @Override
        public InputStream getContents() throws CoreException {
            return new ByteArrayInputStream(string.getBytes());
        }

        @Override
        public IPath getFullPath() {
            return null;
        }

        @Override
        @SuppressWarnings("rawtypes")
        public Object getAdapter(Class adapter) {
            return null;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public boolean isReadOnly() {
            return true;
        }

        public String getText() {
            return string;
        }
    }

    public static class StringInput implements IStorageEditorInput {

        private IStorage storage;

        StringInput(IStorage storage) {
            this.storage = storage;
        }

        @Override
        public boolean exists() {
            return true;
        }

        @Override
        public ImageDescriptor getImageDescriptor() {
            return null;
        }

        @Override
        public String getName() {
            return storage.getName();
        }

        @Override
        public IPersistableElement getPersistable() {
            return null;
        }

        @Override
        public IStorage getStorage() {
            return storage;
        }

        @Override
        public String getToolTipText() {
            return "String-based file: " + storage.getName();
        }

        @Override
        @SuppressWarnings("rawtypes")
        public Object getAdapter(Class adapter) {
            return null;
        }
    }

    /**
     * Open an editor used as a view - editing nothing but doing its things in the editor
     * area.
     * 
     * @param editorID
     */
    public static void dummyEdit(String fileId, String editorID, String... fileContent) {

        String s = "";
        if (fileContent != null) {
            for (String ss : fileContent) {
                s += ss;
            }
        }

        IStorage storage = new StringStorage(fileId, s);
        IStorageEditorInput input = new StringInput(storage);
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

        try {
            page.openEditor(input, editorID);
        } catch (Exception e) {
            handleException(e);
        }

    }

    public static class ThinklabStatus extends MultiStatus {

        public ThinklabStatus(int code, String message) {
            super(Activator.PLUGIN_ID, code, message, null);
            setSeverity(code);
        }

        public ThinklabStatus(int code, Throwable exception) {
            super(Activator.PLUGIN_ID, code, exception.getMessage(), exception);
            setSeverity(code);
        }
    }

    public static List<org.integratedmodelling.api.project.IProject> selectDependencies(IThinklabProject project, Shell shell) {

        ArrayList<org.integratedmodelling.api.project.IProject> ret = new ArrayList<>();

        ListSelectionDialog dlg = new ProjectPrerequisiteSelectionDialog(shell, project.getTLProject());

        if (dlg.open() == Window.OK) {
            for (Object o : dlg.getResult()) {
                ret.add((org.integratedmodelling.api.project.IProject) o);
            }
            return ret;
        }

        return null;
    }

    public static void handleException(Throwable e) {
        if (e instanceof CoreException) {
            StatusManager.getManager().handle((CoreException) e, Activator.PLUGIN_ID);
        } else if (e instanceof KlabException) {
            alert(e.getMessage());
            StatusManager.getManager().handle(new ThinklabStatus(IStatus.ERROR, e));
        } else {
            StatusManager.getManager().handle(new ThinklabStatus(IStatus.ERROR, e));
        }
    }

    public static void error(Object message) {
        if (message instanceof Throwable) {
            handleException((Throwable) message);
        } else {
            StatusManager.getManager().handle(new ThinklabStatus(IStatus.ERROR, message.toString()));
        }
    }

    public static void warningStatus(String message, String viewId) {
        if (viewId != null) {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(viewId)
                    .getViewSite().getActionBars().getStatusLineManager().setMessage(message);
        }
        StatusManager.getManager().handle(new ThinklabStatus(IStatus.WARNING, message));
    }

    public static void infoStatus(String message, String viewId) {
        if (viewId != null) {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView(viewId)
                    .getViewSite().getActionBars().getStatusLineManager().setMessage(message);
        }
        StatusManager.getManager().handle(new ThinklabStatus(IStatus.INFO, message));
    }

    public static void compileError(IResource subject, String message, int lineNumber) {
        StatusManager.getManager().handle(new ThinklabStatus(IStatus.INFO, message));
        /*
         * TODO set markers
         */
    }

    public static void openWizard(String id) {

        IWizardDescriptor descriptor = PlatformUI.getWorkbench().getNewWizardRegistry().findWizard(id);
        // If not check if it is an "import wizard".
        if (descriptor == null) {
            descriptor = PlatformUI.getWorkbench().getImportWizardRegistry().findWizard(id);
        }
        // Or maybe an export wizard
        if (descriptor == null) {
            descriptor = PlatformUI.getWorkbench().getExportWizardRegistry().findWizard(id);
        }
        try {
            // Then if we have a wizard, open it.
            if (descriptor != null) {
                IWizard wizard = descriptor.createWizard();
                WizardDialog wd = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                        .getShell(), wizard);
                wd.setTitle(wizard.getWindowTitle());
                wd.open();
            }
        } catch (CoreException e) {
            handleException(e);
        }
    }

    public static void openFile(String filename, int lineNumber) throws KlabException {

        /*
         * open as workspace file - otherwise xtext gives an exception
         */
        IFile file = null;
        if (filename.startsWith("file:")) {
            URL url = null;
            try {
                url = new URL(filename);
            } catch (MalformedURLException e) {
                throw new KlabIOException(e);
            }
            filename = url.getFile().toString();
        }
        File dfile = new File(filename);
        if (dfile.exists()) {
            // full file path
            IFile[] ff = ResourcesPlugin.getWorkspace().getRoot().findFilesForLocationURI(dfile.toURI());
            if (ff != null && ff.length > 0) {
                file = ff[0];
            }
        } else {
            Path path = new Path(filename);
            file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
        }
        IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

        try {
            if (lineNumber > 0) {
                HashMap<String, Object> map = new HashMap<>();
                map.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
                IMarker marker = file.createMarker(IMarker.TEXT);
                marker.setAttributes(map);
                IDE.openEditor(page, marker);
                marker.delete();
            } else {
                IDE.openEditor(page, file);
            }
        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }
    }

    public static void openFile(String filename) throws KlabException {
        openFile(filename, 0);
    }

    /**
     * Open the given view. Optionally fire the passed event after the view is shown.
     * 
     * @param id
     * @param events
     */
    public static void openView(final String id, final ThinklabEvent... events) {

        class Job extends UIJob {

            public Job() {
                super("");
            }

            @Override
            public IStatus runInUIThread(IProgressMonitor monitor) {
                try {
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(id);
                } catch (PartInitException e) {
                    Eclipse.handleException(e);
                }
                if (events != null) {
                    for (ThinklabEvent e : events) {
                        Activator.getDefault().fireEvent(e);
                    }
                }
                return Status.OK_STATUS;
            }
        }

        Job job = new Job();
        job.setUser(false);
        job.schedule();
        try {
            job.join();
        } catch (InterruptedException e) {
            handleException(e);
        }

    }

    public static void closeView(final String id) {

        class Job extends UIJob {

            public Job() {
                super("");
            }

            @Override
            public IStatus runInUIThread(IProgressMonitor monitor) {
                IViewPart view = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                        .findView(id);
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().hideView(view);
                return Status.OK_STATUS;
            }
        }

        new Job().schedule();
    }

    public static void copyToClipboard(String string) {

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection strSel = new StringSelection(string);
        clipboard.setContents(strSel, null);
        //
        // Clipboard clipboard = new Clipboard(Display.getCurrent());
        // TextTransfer textTransfer = TextTransfer.getInstance();
        // Transfer[] transfers = new Transfer[] { textTransfer };
        // Object[] data = new Object[] { defs };
        // clipboard.setContents(data, transfers);
        // clipboard.dispose();

    }

    /**
     * Add a Thinklab marker to the given file
     * 
     * @param file
     * @param message
     * @param lineNumber
     * @param severity
     */
    public static void addMarker(IFile file, String message, int lineNumber, int severity) {

        if (!file.exists())
            return;

        try {
            IMarker marker = file.createMarker(XTEXT_MARKER_TYPE);
            marker.setAttribute(IMarker.MESSAGE, message);
            marker.setAttribute(IMarker.SEVERITY, severity);
            if (lineNumber <= 0) {
                lineNumber = 1;
            }
            marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
        } catch (CoreException e) {
            Eclipse.handleException(e);
        }
    }

    public static File getInstallDirectory() {
        return new File(Platform.getInstallLocation().getURL().getFile());
    }

    public static File getInstallDirectory(String pluginId) {

        return new File(Platform.getInstallLocation().getURL().getFile());
    }

    /**
     * Give me a break.
     */
    public static Shell getShell() {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        if (window == null) {
            IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
            if (windows.length > 0) {
                return windows[0].getShell();
            }
        } else {
            return window.getShell();
        }
        return null;
    }

    public static void alert(String message) {
        try {
            IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
            Shell shell = window == null ? new Shell(new Display()) : window.getShell();
            MessageDialog.openError(shell, "Error", message);
        } catch (Throwable e) {
            // last resort
            System.out.println("ALERT: " + message);
        }
    }

    public static boolean confirm(String message) {
        Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
        return MessageDialog.openQuestion(shell, "Confirmation", message);
    }

    public static void warning(String message) {
        Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
        MessageDialog.openWarning(shell, "Warning", message);
    }

    public static void info(String message) {
        Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
        MessageDialog.openInformation(shell, "Information", message);
    }

    public static void beep() {
        PlatformUI.getWorkbench().getDisplay().beep();
    }

    /**
     * Get all relevant changed resources from a delta
     * 
     * @param d
     * @return changed resources we care about
     */
    public static Collection<IResource> getRelevantChangedResources(IResourceDelta d) {

        Set<IResource> ret = new HashSet<>();
        getRelevantChangedResources(d, ret);
        return ret;
    }

    public static Properties readProperties(File pfile) throws KlabIOException {
        Properties ret = new Properties();
        try (InputStream finp = new FileInputStream(pfile)) {
            ret.load(finp);
        } catch (Exception e) {
        }
        return ret;
    }

    /*
     * the things middle-aged PIs have to do
     */
    public static void printDelta(IResourceDelta d, int offset) {

        String s = MiscUtilities.spaces(offset);
        String type = "";

        if ((d.getFlags() & IResourceDelta.ADDED) != 0)
            type += " ADDED";
        if ((d.getFlags() & IResourceDelta.CHANGED) != 0)
            type += " CHANGED";
        if ((d.getFlags() & IResourceDelta.CONTENT) != 0)
            type += " CONTENT";
        if ((d.getFlags() & IResourceDelta.COPIED_FROM) != 0)
            type += " COPIED FROM";
        if ((d.getFlags() & IResourceDelta.LOCAL_CHANGED) != 0)
            type += " LOCAL CHANGED";
        if ((d.getFlags() & IResourceDelta.MARKERS) != 0)
            type += " MARKERS";
        if ((d.getFlags() & IResourceDelta.MOVED_FROM) != 0)
            type += " MOVED FROM";
        if ((d.getFlags() & IResourceDelta.MOVED_TO) != 0)
            type += " MOVED_TO";
        if ((d.getFlags() & IResourceDelta.OPEN) != 0)
            type += " OPEN";
        if ((d.getFlags() & IResourceDelta.REMOVED) != 0)
            type += " REMOVED";
        if ((d.getFlags() & IResourceDelta.REPLACED) != 0)
            type += " REPLACED";
        if ((d.getFlags() & IResourceDelta.SYNC) != 0)
            type += " SYNC";
        if ((d.getFlags() & IResourceDelta.TYPE) != 0)
            type += " TYPE";

        System.out.println(s + d.getFullPath() + type + " on "
                + d.getResource().getClass().getCanonicalName() + " " + d.getResource());
        for (IResourceDelta dd : d.getAffectedChildren())
            printDelta(dd, offset + 3);

    }

    private static void getRelevantChangedResources(IResourceDelta d, Set<IResource> ret) {

        if (ThinklabModelManager.get().isNamespaceFileExtension(d.getResource().getFileExtension())
                && (d.getFlags() & IResourceDelta.CONTENT) != 0) {
            ret.add(d.getResource());
        }

        for (IResourceDelta dd : d.getAffectedChildren()) {
            getRelevantChangedResources(dd, ret);
        }
    }

    public static boolean isProjectLevelChange(IResourceDelta delta) {

        boolean ret = false;
        for (IResourceDelta r : delta.getAffectedChildren()) {
            if (r.getResource() instanceof org.eclipse.core.resources.IProject
                    && (r.getFlags()
                            & (IResourceDelta.ADDED | IResourceDelta.REMOVED | IResourceDelta.OPEN)) != 0) {
                ret = true;
                break;
            }
        }

        System.out.println("Delta " + delta + " is " + (ret ? "" : "not ") + "a project level change.");
        printDelta(delta, 0);

        return ret;

    }

    // /**
    // * Check if updates are available. TODO use the URL in the same properties file.
    // Must pass a
    // * view ID if we want non-alert info and warnings to be displayed (thank Eclipse for
    // that).
    // *
    // * @param viewId
    // */
    // public static void checkForUpdates(String viewId) {
    //
    // File local = new File(Eclipse.getInstallDirectory() + File.separator +
    // "dist.properties");
    // try {
    //
    // if (local.exists()) {
    //
    // Properties dist = new Properties();
    // Properties remo = new Properties();
    //
    // // TODO read URL from properties, too
    // URL url = new URL("http://www.integratedmodelling.org/downloads/dist.properties");
    // InputStream rin = url.openStream();
    // FileInputStream inp = new FileInputStream(local);
    // dist.load(inp);
    // remo.load(rin);
    // inp.close();
    // rin.close();
    //
    // long dateLocal = Long.parseLong(dist.getProperty("build.date"));
    // long dateRemote = Long.parseLong(remo.getProperty("build.date"));
    // String curVer = dist.getProperty("thinkcap.version");
    // String remVer = remo.getProperty("thinkcap.version");
    //
    // String vldesc = " ";
    // String vrdesc = " ";
    // if (curVer != null)
    // vldesc = " (" + curVer + ") ";
    // if (remVer != null)
    // vrdesc = " (" + remVer + ") ";
    //
    // if (dateRemote > dateLocal) {
    // Eclipse.info("There is a new version" + vrdesc
    // + "of Thinkcap available. You are running " + curVer + ".");
    // } else {
    // Eclipse.info("The current Thinkcap version (" + vldesc
    // + ") is the most recent available.");
    // }
    //
    // } else {
    // Eclipse.warning("Cannot connect to check available versions.");
    // }
    // } catch (Exception e) {
    // Eclipse.warning("Cannot connect to check available versions.");
    // }
    //
    // }

    public static void updateMarkers(String namespaceId) {
        INamespace namespace = KLAB.MMANAGER.getNamespace(namespaceId);
        if (namespace != null) {
            try {
                updateMarkersForNamespace(namespace, getNamespaceIFile(namespaceId));
            } catch (CoreException e) {
                handleException(e);
            }
        }
    }

    public static void macOsAlert() {

        /**
         * TODO on a Mac, pop up a window telling to do this and provide a "don't show
         * this again" option.
         */
        if (KLAB.CONFIG.getOS() == OS.MACOS) {
            String xxx = "See (https://issues.jboss.org/browse/JGRP-1808) for\r\n" +
                    "sudo route add -net 224.0.0.0/5 127.0.0.1\r\n" +
                    "\r\n" +
                    "Check with netstat -nr that there is a line\r\n" +
                    "224.0.0/8 127.0.0.1 UGmS 0 0 lo0";

        }
    }

    /**
     * Add all error and warning markers from Thinklab logical errors. Use XText marker
     * types so they will be shown in editor. NOTE: only one marker per row is shown, and
     * error supersede warnings. We could just remove the check and have the multiple
     * markers thing, but at the moment errors may be reported more than once, and it's
     * questionable that seeing multiple markers is more useful than fixing one and seeing
     * the next afterwards.
     * 
     * @param ns
     * @param file
     * @throws CoreException
     */
    public static void updateMarkersForNamespace(final INamespace ns, final IFile file) throws CoreException {

        WorkspaceJob job = new WorkspaceJob("") {
            @Override
            public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {

                if (file == null) {
                    return Status.OK_STATUS;
                }

                if (file.exists()) {
                    file.deleteMarkers(XTEXT_MARKER_TYPE, true, IResource.DEPTH_ZERO);
                }

                HashSet<ICompileNotification> ln = new HashSet<>();

                for (ICompileNotification inot : ns.getCodeAnnotations()) {

                    if (ln.contains(inot)) {
                        continue;
                    }

                    ln.add(inot);

                    if (inot instanceof ICompileError) {
                        Eclipse.addMarker(file, inot.getMessage(), inot
                                .getLineNumber(), IMarker.SEVERITY_ERROR);
                    } else if (inot instanceof ICompileWarning) {
                        Eclipse.addMarker(file, inot.getMessage(), inot
                                .getLineNumber(), IMarker.SEVERITY_WARNING);
                    } else {
                        Eclipse.addMarker(file, inot.getMessage(), inot
                                .getLineNumber(), IMarker.SEVERITY_INFO);
                    }
                }
                return Status.OK_STATUS;
            }
        };
        job.setUser(false);
        job.schedule();
    }

    public static org.eclipse.core.resources.IProject getEclipseProject(String projectName) {
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        return root.getProject(projectName);
    }

    public static IWorkspaceRoot getWorkspaceRoot() {
        return ResourcesPlugin.getWorkspace().getRoot();
    }

    public static IWorkspace getWorkspace() {
        return ResourcesPlugin.getWorkspace();
    }

    public static Collection<org.eclipse.core.resources.IProject> getAllProjects() {

        ArrayList<org.eclipse.core.resources.IProject> ret = new ArrayList<>();
        for (org.eclipse.core.resources.IProject p : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
            if (p.isOpen()) {
                ret.add(p);
            }
        }
        return ret;
    }

    public static Collection<String> getAllProjectsInWorkspace() {
        ArrayList<String> ret = new ArrayList<>();
        for (IProject p : getAllProjects()) {
            ret.add(p.getName());
        }
        return ret;
    }

    public static IFile getNamespaceIFile(String namespace) {

        final INamespace ns = KLAB.MMANAGER.getNamespace(namespace);

        if (ns != null && ((KIMNamespace)ns).isProjectKnowledge()) {
            return  getEclipseProject(ns.getProject().getId()).getFile("/META-INF/knowledge.kim");
        }
        
        /*
         * happens when just deleted
         */
        if (ns == null || ns.getLocalFile() == null) {
            return null;
        }

        String rpath = namespace.replace('.', '/');
        rpath += "." + MiscUtilities.getFileExtension(ns.getLocalFile().toString());
        rpath = ns.getProject().getSourceRelativePath() + "/" + rpath;
        return getEclipseProject(ns.getProject().getId()).getFile(rpath);
    }

    public static Collection<String> getReferencedProjects(String projectId) {

        ArrayList<String> ret = new ArrayList<>();
        IProject proj = getEclipseProject(projectId);
        try {
            for (IProject pp : proj.getDescription().getReferencedProjects()) {
                ret.add(pp.getName());
            }
        } catch (CoreException e) {
        }
        return ret;
    }

    // debug
    public static void printAllMarkers() {

        try {
            for (org.eclipse.core.resources.IProject p : Eclipse.getAllProjects()) {
                System.out.println(p.getName());
                for (IMarker m : p.findMarkers(XTEXT_MARKER_TYPE, true, IResource.DEPTH_INFINITE)) {
                    System.out.println("   " + m.getAttribute(IMarker.MESSAGE) + " in " + m.getResource());
                }
            }
        } catch (CoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Converts a buffered image to SWT <code>ImageData</code>.
     *
     * @param bufferedImage the buffered image (<code>null</code> not permitted).
     *
     * @return The image data.
     */
    public static ImageData convertToSWT(BufferedImage bufferedImage) {
        if (bufferedImage.getColorModel() instanceof DirectColorModel) {
            DirectColorModel colorModel = (DirectColorModel) bufferedImage.getColorModel();
            PaletteData palette = new PaletteData(colorModel.getRedMask(), colorModel
                    .getGreenMask(), colorModel.getBlueMask());
            ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(), colorModel
                    .getPixelSize(), palette);
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[3];
            for (int y = 0; y < data.height; y++) {
                for (int x = 0; x < data.width; x++) {
                    raster.getPixel(x, y, pixelArray);
                    int pixel = palette.getPixel(new RGB(pixelArray[0], pixelArray[1], pixelArray[2]));
                    data.setPixel(x, y, pixel);
                }
            }
            return data;
        } else if (bufferedImage.getColorModel() instanceof IndexColorModel) {
            IndexColorModel colorModel = (IndexColorModel) bufferedImage.getColorModel();
            int size = colorModel.getMapSize();
            byte[] reds = new byte[size];
            byte[] greens = new byte[size];
            byte[] blues = new byte[size];
            colorModel.getReds(reds);
            colorModel.getGreens(greens);
            colorModel.getBlues(blues);
            RGB[] rgbs = new RGB[size];
            for (int i = 0; i < rgbs.length; i++) {
                rgbs[i] = new RGB(reds[i] & 0xFF, greens[i] & 0xFF, blues[i] & 0xFF);
            }
            PaletteData palette = new PaletteData(rgbs);
            ImageData data = new ImageData(bufferedImage.getWidth(), bufferedImage.getHeight(), colorModel
                    .getPixelSize(), palette);
            data.transparentPixel = colorModel.getTransparentPixel();
            WritableRaster raster = bufferedImage.getRaster();
            int[] pixelArray = new int[1];
            for (int y = 0; y < data.height; y++) {
                for (int x = 0; x < data.width; x++) {
                    raster.getPixel(x, y, pixelArray);
                    data.setPixel(x, y, pixelArray[0]);
                }
            }
            return data;
        }
        return null;
    }

    public static String namespaceIdFromIFile(IFile file) {
        String ns = "";
        String[] ps = file.getFullPath().segments();
        for (int i = 2; i < ps.length; i++) {
            String pp = ps[i];
            String ext = MiscUtilities.getFileExtension(pp);
            if (KIM.FILE_EXTENSIONS.contains(ext)) {
                pp = pp.substring(0, pp.length() - (ext.length() + 1));
            }
            ns += (ns.isEmpty() ? "" : ".") + pp;
        }
        return ns;
    }

    public static IProject isProjectConfigurationChange(IResourceDelta delta) {

        if (delta.getResource() instanceof IFile && delta.getResource().toString().endsWith(".project")) {
            return delta.getResource().getProject();
        }

        for (IResourceDelta dd : delta.getAffectedChildren()) {
            IProject p = isProjectConfigurationChange(dd);
            if (p != null)
                return p;
        }

        return null;
    }

    public static File findJarForPlugin(String string) {

        File f = new File(getInstallDirectory() + File.separator + "plugins");
        if (f.isDirectory()) {
            for (File ff : f.listFiles()) {
                if (ff.toString().contains(string) && ff.toString().endsWith(".jar")) {
                    return ff;
                }
            }
        }
        return null;
    }

    public static void displayNotifications(IUser user) {
        // TODO Auto-generated method stu
        // try {
        // for (Announcement ann : ((RESTUser)
        // user).getAuthClient().getNotifications().getNew()) {
        // displayNotification(ann);
        // }
        // } catch (KlabException e) {
        // // just do nothing.
        // }
    }

    /**
     * Get the icon path for the passed info category, which is most of the time null.
     * 
     * @param category
     * @return icon relative filepath
     */
    public static String getNotificationIcon(String category) {
        // TODO implement
        if (category != null) {
            switch (category) {
            case Messages.INFOCLASS_DOWNLOAD:
                return "icons/ArrowDownAlt.png";
            case Messages.INFOCLASS_UPLOAD:
                return "icons/ArrowUpAlt.png";
            case Messages.INFOCLASS_COMPLETED:
                return "icons/Accept.png";
            case Messages.INFOCLASS_NETWORK:
                return "icons/ShareThis.png";
            case Messages.INFOCLASS_COMPONENT:
                return "icons/Plugin.png";
            case Messages.INFOCLASS_LOCK:
                return "icons/LockSmall.png";
            case Messages.INFOCLASS_MODEL:
                return "icons/Cog.png";
            case Messages.INFOCLASS_TIME:
                return "icons/ClockSmall.png";
            case Messages.INFOCLASS_HAPPY:
                return "icons/SmileyHappy.png";
            case Messages.INFOCLASS_SAD:
                return "icons/SmileySad.png";
            case Messages.INFOCLASS_USER_OWN:
                return "icons/UserSmall.png";
            case Messages.INFOCLASS_USER_FOREIGN:
                return "icons/UserAlt.png";
            case Messages.INFOCLASS_STOP:
                return "icons/StopRed.png";
            }
        }
        return "icons/InfoAlt.png";
    }

    public static interface IActivatorListener {
        void onActivation() throws KlabException;
    }

    public static void addActivationListener(ViewPart view, IActivatorListener listener) {

        final IWorkbenchPage page = view.getSite().getPage();

        /*
         * TODO finish this appropriately (ideally, have all views implement an interface
         * with the same method to display the current environment and call this in a
         * generic way).
         */
        IPartListener2 pl = new IPartListener2() {

            @Override
            public void partVisible(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partOpened(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partInputChanged(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partHidden(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partDeactivated(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partClosed(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partBroughtToTop(IWorkbenchPartReference partRef) {
            }

            @Override
            public void partActivated(IWorkbenchPartReference partRef) {
                try {
                    listener.onActivation();
                } catch (KlabException e) {
                    handleException(e);
                }
            }
        };

        page.addPartListener(pl);

    }

    public static IWorkbenchPage getActivePage() {
        IWorkbench wb = PlatformUI.getWorkbench();
        IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
        return win.getActivePage();
    }

    /**
     * Lookup the definition of the passed concept and if found open it in the editor.
     * 
     * TODO open projects that are not in the workspace in read-only mode.
     * 
     * @param text
     * @return
     */
    public static boolean openConceptDefinition(IConcept concept) {
        return openRefDefinition(concept.toString());
    }

    /**
     * Lookup the definition of the passed concept ID and if found open it in the editor.
     * 
     * TODO open projects that are not in the workspace in read-only mode. TODO open URN
     * explorer if the conceptId is a URN.
     * 
     * @param conceptId
     * @return
     */
    public static boolean openRefDefinition(String conceptId) {

        if (conceptId.startsWith("http")) {
            BrowserUtils.startBrowser(conceptId);
            return true;
        }
        
        String[] spl = conceptId.split(":");
        if (spl.length > 2) {
            /*
             * try as a URN.
             */
            EngineController mclient = ((ModelingClient) KLAB.ENGINE).getCurrentEngineController();
            if (mclient != null) {
                Map<?, ?> desc = mclient.getURNMetadata(conceptId);
                
                if (desc != null) {

                    /*
                     * remove stuff that the user really doesn't want to see
                     */
                    desc.remove("serverKey");
                    desc.remove("id");
                    desc.remove("url");
                    
                    MapViewerDialog ptc = new MapViewerDialog(getShell(), conceptId, desc);
                    if (ptc.open() == Dialog.OK) {
                        return true;
                    }

                } else {
                    alert("URN is non-existent or user access is denied");
                }
            }
            return false;
        }

        IModelObject mo = KLAB.MMANAGER.findModelObject(conceptId);
        if (mo != null && mo.getNamespace() != null && mo.getNamespace().getLocalFile() != null) {
            HashMap<String, Object> map = new HashMap<>();
            IFile resourceToOpen = Eclipse.getNamespaceIFile(mo.getNamespace().getId());
            if (resourceToOpen != null) {
                map.put(IMarker.LINE_NUMBER, new Integer(mo.getFirstLineNumber()));
                try {
                    IMarker marker = resourceToOpen.createMarker(IMarker.TEXT);
                    marker.setAttributes(map);
                    IDE.openEditor(Eclipse.getActivePage(), marker);
                    marker.delete();
                } catch (CoreException e) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
