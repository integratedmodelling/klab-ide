/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Resource change reporter only cares about projects added, removed or closed. The rest
 * is taken care of by the k.LAB project manager file monitor.
 * 
 * @author ferdinando.villa
 *
 */
public class ResourceChangeReporter implements IResourceChangeListener {

    class DeltaConsumer implements IResourceDeltaVisitor {
        @Override
        public boolean visit(IResourceDelta delta) {
            IResource res = delta.getResource();
            boolean ret = true;
            switch (delta.getKind()) {
            case IResourceDelta.ADDED:
                if (res instanceof IProject) {
                    try {
                        File file = res.getLocation().toFile();
                        if (ProjectManager.isKlabProject(file)) {
                            KLAB.PMANAGER.registerProject(file);
                        }
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    ret = false;
                }
                break;
            case IResourceDelta.REMOVED:
                if (res instanceof IProject) {
                    try {
                        KLAB.PMANAGER.unregisterProject(res.getName());
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    ret = false;
                }
                break;
            case IResourceDelta.CHANGED:

                if ((delta.getFlags() & IResourceDelta.OPEN) != 0 && res instanceof IProject
                        && ((IProject) res).isOpen()) {
                    try {
                        File file = res.getLocation().toFile();
                        if (ProjectManager.isKlabProject(file)) {
                            ((ProjectManager) KLAB.PMANAGER).openAndRegisterProject(file);
                        }
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    ret = false;
                }
                break;
            }
            return ret;
        }
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        IResource res = event.getResource();

        try {
            switch (event.getType()) {
            case IResourceChangeEvent.PRE_CLOSE:
                File file = res.getLocation().toFile();
                if (ProjectManager.isKlabProject(file)) {
                    KLAB.PMANAGER.getProject(res.getName()).open(false);
                    KLAB.PMANAGER.unregisterProject(res.getName());
                }
                break;
            case IResourceChangeEvent.POST_CHANGE:
                event.getDelta().accept(new DeltaConsumer());
                break;
            }
        } catch (Exception e) {
            Eclipse.handleException(e);
        }
    }

}
