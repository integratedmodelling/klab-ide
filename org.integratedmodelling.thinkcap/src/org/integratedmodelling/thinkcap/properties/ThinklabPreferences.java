/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.properties;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.wb.swt.FieldLayoutPreferencePage;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.thinkcap.Activator;

public class ThinklabPreferences extends FieldLayoutPreferencePage implements
        IWorkbenchPreferencePage {

    public static final String ID = "org.integratedmodelling.thinkcap.preferences.page1";
    private Table              table;
    private Spinner            spinner;
    private String             _pval;

    /**
     * Create the preference page.
     */
    public ThinklabPreferences() {
        setTitle("Thinklab");
    }

    /**
     * Create contents of the preference page.
     * @param parent
     */
    @Override
    public Control createPageContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NULL);
        container.setLayout(new GridLayout());

        _pval = KLAB.CONFIG.getProperties().getProperty("thinklab.max.memory", "2048M");
        final int tmem = Integer.parseInt(_pval.substring(0, _pval.length() - 1));

        Group grpThinklabLocalServer = new Group(container, SWT.NONE);
        grpThinklabLocalServer.setText("Thinklab local server");
        grpThinklabLocalServer.setLayout(new GridLayout(1, false));
        grpThinklabLocalServer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        Composite composite_1 = new Composite(grpThinklabLocalServer, SWT.NONE);
        composite_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        composite_1.setLayout(new GridLayout(3, false));

        Label lblRamLimit = new Label(composite_1, SWT.NONE);
        lblRamLimit.setBounds(0, 0, 55, 15);
        lblRamLimit.setText("RAM limit");

        this.spinner = new Spinner(composite_1, SWT.BORDER);

        spinner.setMaximum(49152);
        spinner.setMinimum(2048);
        spinner.setIncrement(256);
        spinner.setPageIncrement(1024);
        spinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

        Label lblKb = new Label(composite_1, SWT.NONE);
        lblKb.setText("kB");

        Composite composite = new Composite(container, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        {
            DirectoryFieldEditor directoryFieldEditor = new DirectoryFieldEditor("id", "New DirectoryFieldEditor", composite);
            directoryFieldEditor.setEmptyStringAllowed(false);
            directoryFieldEditor.getLabelControl(composite).setText("Data export path");
            addField(directoryFieldEditor);
        }

        Group grpCacheManagement = new Group(container, SWT.NONE);
        grpCacheManagement.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        grpCacheManagement.setText("Cache management");
        grpCacheManagement.setLayout(new GridLayout(3, false));

        Button btnNewButton = new Button(grpCacheManagement, SWT.NONE);
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                // Activator.client().stopCurrentServer();
                // try {
                // FileUtils.deleteDirectory(new File(Env.CONFIG.getScratchArea() + File.separator + "kbox"
                // + File.separator + "thinklab"));
                // } catch (IOException e1) {
                // Activator.client().error(e1);
                // }
                // Activator.client().startCurrentServer();
            }
        });
        btnNewButton.setToolTipText("Clear stored model information (stops the server)");
        btnNewButton.setText("Clear model cache");

        Button btnNewButton_1 = new Button(grpCacheManagement, SWT.NONE);
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                // Activator.client().stopCurrentServer();
                // try {
                // FileUtils.deleteDirectory(new File(Env.CONFIG.getDataPath() + File.separator + "cache"));
                // } catch (IOException e1) {
                // Activator.client().error(e1);
                // }
            }
        });
        btnNewButton_1.setToolTipText("Clear remote service information (WCS, WFS etc). Stops the server.");
        btnNewButton_1.setText("Clear remote metadata");

        Button btnReindexKnowledge = new Button(grpCacheManagement, SWT.NONE);
        btnReindexKnowledge.setToolTipText("Rebuild ontology index");
        btnReindexKnowledge.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                KLAB.KM.getIndex().reindex();
            }
        });
        btnReindexKnowledge.setText("Reindex knowledge");

        Group grpThinklabProperties = new Group(container, SWT.NONE);
        grpThinklabProperties.setLayout(new GridLayout(1, false));
        grpThinklabProperties.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        grpThinklabProperties.setText("Thinklab properties");

        Composite composite_3 = new Composite(grpThinklabProperties, SWT.NONE);
        composite_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        composite_3.setBounds(0, 0, 64, 64);
        TableColumnLayout tcl_composite_3 = new TableColumnLayout();
        composite_3.setLayout(tcl_composite_3);

        TableViewer tableViewer = new TableViewer(composite_3, SWT.BORDER | SWT.FULL_SELECTION);
        table = tableViewer.getTable();
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn tblclmnPropert = tableViewerColumn.getColumn();
        tcl_composite_3.setColumnData(tblclmnPropert, new ColumnPixelData(143, true, true));
        tblclmnPropert.setText("Property");

        TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn tblclmnValue = tableViewerColumn_1.getColumn();
        tcl_composite_3.setColumnData(tblclmnValue, new ColumnPixelData(162, true, true));
        tblclmnValue.setText("Value");

        spinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                _pval = spinner.getSelection() + "M";
                setValid(true);
            }
        });

        spinner.setSelection(tmem);

        Label lblNewLabel = new Label(composite_1, SWT.NONE);
        lblNewLabel.setAlignment(SWT.CENTER);
        lblNewLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED));
        lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
        lblNewLabel.setText("Note: changes will be only effective after server restart.");

        return container;
    }

    /**
     * Initialize the preference page.
     */
    @Override
    public void init(IWorkbench workbench) {
    }

    @Override
    protected void initialize() {
        super.initialize();
    }

    @Override
    public boolean performOk() {

        super.performOk();
        KLAB.CONFIG.getProperties().setProperty("thinklab.max.memory", _pval);
        try {
            KLAB.CONFIG.save();
        } catch (Exception e) {
            Activator.engine().getMonitor().error(e);
        }
        return true;
    }
}
