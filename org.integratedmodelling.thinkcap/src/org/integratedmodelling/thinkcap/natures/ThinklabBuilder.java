/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.natures;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;

/*
 * micro-builder: only handles full build; everything else done by the navigator's content
 * provider.
 */
public class ThinklabBuilder extends IncrementalProjectBuilder {

    public static final String BUILDER_ID  = "org.integratedmodelling.thinkcap.builder";
    public static final String MARKER_TYPE = "org.integratedmodelling.thinklab.problem.compilation";

    @Override
    protected IProject[] build(int kind, @SuppressWarnings("rawtypes") Map args, IProgressMonitor monitor)
            throws CoreException {

//        IResourceDelta delta = getDelta(getProject());
//        if (delta != null) {
//            Eclipse.printDelta(delta, 0);
//        }
        
        if (Activator.getDefault().isInitializationFinished() && !Activator.getDefault().isRefreshing()) {
//            if (Eclipse.isProjectLevelChange(getDelta(getProject())) && kind == FULL_BUILD) {
//                Activator.getDefault().reloadProjects(true);
//            }
            Activator.getDefault().reloadProjects(false /* kind == FULL_BUILD*/);
        }
        return null;
    }

}
