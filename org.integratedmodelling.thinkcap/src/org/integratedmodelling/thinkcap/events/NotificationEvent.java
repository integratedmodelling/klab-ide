/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.events;

import java.util.logging.Level;

import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;

/**
 * Events fired when the monitor gets hit with a notification.
 * 
 * @author Ferd
 *
 */
@SuppressWarnings("javadoc")
public class NotificationEvent implements ThinklabEvent {

    public ITask    task;
    public IContext context;
    public ISession session;
    public Level    level;
    public String   body;
    public String   infoClass;

    public NotificationEvent(String body, Level level) {
        this(body, null, level, null, null, null);
    }

    public NotificationEvent(String body, String infoClass, Level level) {
        this(body, infoClass, level, null, null, null);
    }

    public NotificationEvent(String body, String infoClass,  Level level, ISession session) {
        this(body, infoClass, level, session, null, null);
    }
    
    public NotificationEvent(String body, Level level, ISession session, IContext context, ITask task) {
        this.body = body;
        this.level = level;
        this.session = session;
        this.context = context;
        this.task = task;
    }

    public NotificationEvent(String body, String infoClass, Level level, ISession session, IContext context,
            ITask task) {
        this.body = body;
        this.level = level;
        this.session = session;
        this.context = context;
        this.task = task;
        this.infoClass = infoClass;
    }
    
    @Override
    public String toString() {
        return "NotificationEvent [" + body + " " + level + "]";
    }

}
