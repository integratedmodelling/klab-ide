package org.integratedmodelling.thinkcap.events;

/**
 * Interface events: all views must be prepared to activate/deactivate
 * themselves upon these being received. Active means "responding to 
 * observation events".
 * 
 * @author ferdinando.villa
 *
 */
@SuppressWarnings("javadoc")
public class UiEvent implements ThinklabEvent {

    public static final int DEACTIVATE = 0;
    public static final int ACTIVATE   = 1;

    public int type;

    public UiEvent(int type) {
        this.type = type;
    }

}
