/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.events;

import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.network.Broadcaster.EngineStatus;

/**
 * Engine notifications and their types. Heartbeats will come very often,
 * the others hopefully not.
 * 
 * @author Ferd
 *
 */
@SuppressWarnings("javadoc")
public class EngineEvent implements ThinklabEvent {

    public static final int OFFLINE   = 0;
    public static final int ONLINE    = 1;
    public static final int HEARTBEAT = 2;
    public static final int WAITING = 3;
    public static final int LOCKED = 4;
    public static final int UNLOCKED = 5;

    public EngineData       engine;
    public EngineStatus     status;
    public int              type;

    public EngineEvent(EngineData engine, int message) {
        this.engine = engine;
        this.type = message;
    }

    public EngineEvent(EngineData engine, EngineStatus status, int message) {
        this.engine = engine;
        this.type = message;
        this.status = status;
    }
    
    @Override
    public String toString() {
        return "EngineEvent [" + type + "]";
    }

}
