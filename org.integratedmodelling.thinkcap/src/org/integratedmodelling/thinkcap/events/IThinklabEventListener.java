/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.events;

import org.integratedmodelling.thinkcap.Activator;

/**
 * Implemented by any object that wants to subscribe to the Thinklab event bus. 
 * 
 * This should be done in a standard Eclipse-endorsed way, spending the usual three 
 * weeks in study-try-fail cycles, but it's quite simple and for the time being 
 * it seems to work just fine as is.
 * 
 * @author Ferd
 *
 */
public interface IThinklabEventListener {

    /**
     * Handle whatever event comes from the event bus.
     * All events are sent through {@link Activator#fireEvent(ThinklabEvent)} and
     * nearly every event is triggered by the listeners set up in
     * engine.
     * @param event
     */
    public void handleThinklabEvent(ThinklabEvent event);
}
