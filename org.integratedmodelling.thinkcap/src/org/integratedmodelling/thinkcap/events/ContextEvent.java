/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.events;

import org.integratedmodelling.api.runtime.IContext;

/**
 * Events fired when something happens to a context.
 * FOCUS on a null context means we're resetting the context to
 * empty.
 * 
 * @author Ferd
 *
 */
@SuppressWarnings("javadoc")
public class ContextEvent implements ThinklabEvent {

    public static final int NEW      = 1;
    public static final int MODIFIED = 2;
    public static final int FOCUS    = 3;

    public IContext         context;
    public int              type;

    public ContextEvent(IContext context, int type) {
        this.context = context;
        this.type = type;
    }

    @Override
    public String toString() {
        return "ContextEvent " + (context == null ? "(null)" : (context + " (" + context.getId() + ")"));
    }
}
