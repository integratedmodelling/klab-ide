package org.integratedmodelling.thinkcap;

import java.io.File;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.TreeSelection;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.ITask.Status;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.EngineNotifier.EngineListener;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.client.referencing.Bookmark;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.monitoring.Notifiable;
import org.integratedmodelling.common.network.AbstractBaseNetwork;
import org.integratedmodelling.common.network.Broadcaster.EngineStatus;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.thinkcap.events.AuthenticationEvent;
import org.integratedmodelling.thinkcap.events.ContextEvent;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ModelModifiedEvent;
import org.integratedmodelling.thinkcap.events.NetworkEvent;
import org.integratedmodelling.thinkcap.events.NotificationEvent;
import org.integratedmodelling.thinkcap.events.SessionEvent;
import org.integratedmodelling.thinkcap.events.TaskEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.interfaces.IThinklabElement;
import org.integratedmodelling.thinkcap.interfaces.IThinklabModelObject;

/**
 * A modeling client specialized for operation within the Eclipse environment.
 * 
 * @author Ferd
 *
 */
public class EclipseEngine extends ModelingClient implements IThinklabEventListener {

    /**
     * 
     * @param certificate
     * @throws KlabException
     */
    public EclipseEngine(File certificate) throws KlabException {
        super(certificate, new EclipseMonitor());
    }

    @Override
    protected EngineData connectToEngine() throws KlabException {
        // TODO put up the fancy dialog unless a local engine is already
        // available.
        return super.connectToEngine();
    }

    @Override
    protected IWorkspace createWorkspace() {
        return new EclipseWorkspace(ResourcesPlugin.getWorkspace());
    }

    /**
     * Wire in listeners for all relevant engine events that fire events for our active
     * views to consume.
     */
    @Override
    protected void setupListeners() {

        super.setupListeners();

        /*
         * install engine listener to notify heartbeat and engine changes
         */
        this.engineNotifier.addListener(new EngineListener() {

            @Override
            public boolean engineOffline(EngineData ed) {
                if (Activator.getDefault() != null) {
                    Activator.getDefault().fireEvent(new EngineEvent(ed, EngineEvent.OFFLINE));
                }
                return true;
            }

            @Override
            public void engineHeartbeat(EngineData ed, EngineStatus status) {
                if (Activator.getDefault() != null) {
                    if (currentEngine != null && currentEngine.equals(ed)) {
                        Activator.getDefault().fireEvent(new EngineEvent(ed, status, EngineEvent.HEARTBEAT));
                    }
                }
            }

            @Override
            public void engineAvailable(EngineData ed) {
                if (Activator.getDefault() != null) {
                    Activator.getDefault().fireEvent(new EngineEvent(ed, EngineEvent.ONLINE));
                }
            }
        });

        /*
         * install modeling client listener to notify sessions
         */
        addListener(new Listener() {

            @Override
            public void sessionOpened(ISession session) {

                /*
                 * install session listener in engine to get notified of task and context
                 * events
                 */
                session.addListener(new ISession.Listener() {

                    @Override
                    public void taskEvent(ITask task, boolean isNew) {
//                        if (task.getStatus() == Status.FINISHED && !isNew) {
//                            return;
//                        }
                        Activator.getDefault()
                                .fireEvent(new TaskEvent(task, isNew ? TaskEvent.NEW : TaskEvent.MODIFIED));
                        if (task.getStatus() == Status.ERROR) {
                            Activator.getDefault()
                                    .fireEvent(new NotificationEvent("error in task: "
                                            + task.getDescription(), Level.SEVERE, task.getContext()
                                                    .getSession(), task.getContext(), task));
                        } else if (task.getStatus() == Status.RUNNING) {
                            Activator.getDefault()
                                    .fireEvent(new NotificationEvent("started "
                                            + task.getDescription(), Level.INFO, task.getContext()
                                                    .getSession(), task.getContext(), task));
                        } else if (task.getStatus() == Status.FINISHED) {
                            Activator.getDefault()
                                    .fireEvent(new NotificationEvent("finished "
                                            + task.getDescription(), Level.INFO, task.getContext()
                                                    .getSession(), task.getContext(), task));
                        }
                    }

                    @Override
                    public void contextEvent(IContext context, boolean isNew) {
                        if (isNew) {
                            Eclipse.openView("org.integratedmodelling.thinkcap.views.DatasetView", new ContextEvent(context, ContextEvent.NEW));
                        } else {
                            Activator.getDefault()
                                    .fireEvent(new ContextEvent(context, ContextEvent.MODIFIED));
                        }
                    }
                });

                /*
                 * each session may be for a different engine.
                 */
                ((ClientSession) session).getNetwork().addListener(new INetwork.Listener() {

                    @Override
                    public void online() {
                        Activator.getDefault().fireEvent(new NetworkEvent(NetworkEvent.ONLINE));
                    }

                    @Override
                    public void offline() {
                        Activator.getDefault().fireEvent(new NetworkEvent(NetworkEvent.OFFLINE));
                    }

                    @Override
                    public void nodeOnline(IServer node) {
                        Activator.getDefault().fireEvent(new NetworkEvent(node, NetworkEvent.NODE_ONLINE));
                    }

                    @Override
                    public void nodeOffline(IServer node) {
                        Activator.getDefault().fireEvent(new NetworkEvent(node, NetworkEvent.NODE_OFFLINE));
                    }
                });

                Activator.getDefault().fireEvent(new SessionEvent(session, SessionEvent.CREATED));
                Activator.getDefault().fireEvent(new AuthenticationEvent(session.getUser()));
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void sessionClosed(ISession session) {
                ((AbstractBaseNetwork) KLAB.ENGINE.getNetwork()).removeListeners();
                Activator.getDefault().fireEvent(new SessionEvent(session, SessionEvent.CLOSED));
            }

            @Override
            public void engineLocked() {
                // if (workspaceCleared) {
                // Activator.getDefault()
                // .fireEvent(new NotificationEvent("remote workspace was
                // cleared", null, Level.INFO,
                // getCurrentSession()));
                // }
                Activator.getDefault()
                        .fireEvent(new EngineEvent(EclipseEngine.this.currentEngine, EngineEvent.LOCKED));
            }

            @Override
            public void engineUnlocked() {
                Activator.getDefault()
                        .fireEvent(new EngineEvent(EclipseEngine.this.currentEngine, EngineEvent.UNLOCKED));
            }

            @Override
            public void engineUserAuthenticated(IUser user) {
                Activator.getDefault().fireEvent(new AuthenticationEvent(user));
            }
        });

        KLAB.PMANAGER.addListener(new IProjectLifecycleListener() {

            @Override
            public void projectUnregistered(IProject project) {
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void projectRegistered(IProject project) {
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void projectPropertiesModified(IProject project, File file) {
                /*
                 * FIXME this gets called periodically and I don't understand why.
                 */
                // Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void onReload(boolean full) {
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void namespaceModified(String ns, IProject project) {
                Eclipse.updateMarkers(ns);
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void namespaceDeleted(String ns, IProject project) {
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void namespaceAdded(String ns, IProject project) {
                Eclipse.updateMarkers(ns);
                Activator.getDefault().fireEvent(new ModelModifiedEvent());
            }

            @Override
            public void fileModified(IProject project, File file) {
                // TODO Auto-generated method stub
            }

            @Override
            public void fileDeleted(IProject project, File file) {
                // TODO Auto-generated method stub
            }

            @Override
            public void fileCreated(IProject project, File file) {
                // TODO Auto-generated method stub
            }
        });
    }

    /**
     * Descriptive label for UI
     * 
     * @return engine description
     */
    public String getDescription() {
        return "k.LAB engine v" + Version.CURRENT + " running as " + getName();
    }

    /**
     * User has "dropped" anything to observe, possibly choosing to add it to the current
     * context.
     * 
     * @param payload
     * @param isAddAction
     * 
     * @return true if the action has determined a context switch.
     */
    public boolean handleObservationAction(Object payload, boolean isAddAction) {

        String objectName = null;
        boolean ret = false;

        if (payload instanceof String) {
            if (((String)payload).startsWith("@PI|")) {
                payload = PaletteManager.get().getItem(((String)payload).substring(4));
            } else  if (((String)payload).startsWith("@BM|")) {
                payload = ((String)payload).substring(4);
            }
        }
        
        if (payload instanceof Bookmark) {
            objectName = ((Bookmark) payload).objectName;
        } else if (payload instanceof PaletteItem) {
            objectName = ((PaletteItem) payload).getModelObject();
        } else if (payload instanceof IModelObject) {
            objectName = ((IModelObject) payload).getName();
        } else if (payload instanceof String) {
            objectName = (String) payload;
        } else if (payload instanceof TreeSelection) {
            TreeSelection sel = (TreeSelection) payload;
            IThinklabElement o = (IThinklabElement) sel.getFirstElement();
            objectName = ((IThinklabModelObject) o).getModelObject().getName();
        }

        if (objectName != null) {

            IProject project = null;
            IContext context = Environment.get().getContext();
            Bookmark.Type type = null;
            IDirectObserver observer = null;
            Object object = null;
            
            /*
             * resolve object name to concept, model object or URN so we can validate the
             * observable.
             */
            if (StringUtils.countMatches(objectName, ":") <= 1) {
                object = KLAB.MMANAGER.findModelObject(objectName);
                if (object != null) {
                    project = ((IModelObject)object).getNamespace().getProject();
                    if (object instanceof IDirectObserver) {
                        observer = (IDirectObserver) object;
                        type = Bookmark.Type.SUBJECT;
                    } else if (object instanceof IModel) {
                        type = Bookmark.Type.MODEL;
                    }
                } else {
                    IConcept concept = KLAB.KM.getConcept(objectName);
                    if (concept != null) {
                        INamespace ns = KLAB.MMANAGER.getNamespace(concept.getConceptSpace());
                        if (ns != null) {
                            project = ns.getProject();
                        }
                        type = Bookmark.Type.CONCEPT;
                        object = concept;
                    }
                }
            } else {
                type = Bookmark.Type.RESOURCE;
            }

            if (project != null && project.hasErrors()
                    && !Activator.getDefault().getEngineLauncher().isKlabDebug()) {
                Eclipse.alert("The project contains errors. They should be fixed before models defined in this project are computed. Use debug mode to disable this check.");
            } else {

                if (context != null && ((Notifiable) context).getErrors().size() > 0
                        && type != Bookmark.Type.SUBJECT) {
                    Eclipse.alert("Previous observations generated errors. A new context should be established prior to making new observations.");
                } else if (context != null && context.isFinished()
                        && type != Bookmark.Type.SUBJECT) {
                    Eclipse.alert("Observation has been fully contextualized. A new context should be established prior to making new observations.");
                } else if (context != null && context.isRunning()) {
                    Eclipse.alert("Time is running. Making new observations during contextualization is currently unsupported.");
                } else if (object instanceof IModel && ((KIMModel)object).isAbstract()) {
                    Eclipse.alert("Models of abstract observables cannot be run directly.");
                } else if (object instanceof IModel && NS.isCountable(((IModel)object).getObservable()) && !((IModel)object).isInstantiator()) {
                    Eclipse.alert("Contextualizer models for countable observables cannot be run directly. Only instantiators are allowed.");
                } else {

                    try {

                        /*
                         * TODO if we're trying to observe a quality or anything that has
                         * a context associated, ensure compatibility before making the
                         * observation.
                         */
                        if (Environment.get().getSession() != null) {

                            if (observer != null && !isAddAction) {
                                Environment.get().defineLastContextObserved(observer);
                            }

                            /*
                             * if we have a context and we're adding, add instead of
                             * redefining.
                             */
                            if (isAddAction && context != null && observer != null) {
                                context.inScenario(Environment.get().getScenarioIds())
                                        .observe(observer);
                            } else {

                                ret = true;

                                if ((context == null && object != null)  || observer != null) {

                                    /*
                                     * if this is true, record the ROI to reset the observations if 
                                     * requested.
                                     */
                                    boolean implicitContext = (context == null && object != null);
                                    
                                    Environment.get().getSession()
                                            .observe(observer == null ? object : observer, Environment.get()
                                                    .getScenarios(), Environment.get()
                                                            .getSpatialForcing(), (Environment.get()
                                                                    .getTemporalForcing() != null
                                                                    && Environment.get()
                                                                            .getTemporalForcing()
                                                                            .isEmpty()) ? null
                                                                                    : Environment
                                                                                            .get()
                                                                                            .getTemporalForcing());
                                } else if (objectName != null && context != null) {
                                    context.inScenario(Environment.get().getScenarioIds())
                                            .observe(objectName);
                                }
                            }
                        } else {
                            Eclipse.alert("Error observing "
                                    + objectName
                                    + ": observation undefined or no context.");

                        }

                    } catch (KlabException e) {
                        if (e instanceof KlabValidationException) {
                            Eclipse.alert(e.getMessage());
                        } else {
                            Activator.engine().getMonitor().error(e);
                        }
                    }
                }
            }
        }

        return ret;
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {
        // TODO Auto-generated method stub

    }


}
