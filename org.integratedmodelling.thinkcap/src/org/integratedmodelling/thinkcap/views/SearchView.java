/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.indexing.KnowledgeIndex;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.events.OntologyFocusEvent;

public class SearchView extends ViewPart {

    public static final String ID                    = "org.integratedmodelling.thinkcap.views.SearchView"; //$NON-NLS-1$
    private final FormToolkit  toolkit               = new FormToolkit(Display.getCurrent());
    private Text               text;
    private Action             selectDataModels;
    private Action             selectAgentModels;
    private Action             selectSubjects;
    private Action             selectConcepts;
    private Action             action_4;
    private Action             action;
    Tree                       tree;
    TreeViewer                 treeViewer;
    ArrayList<Object>          matches               = new ArrayList<Object>();

    /*
     * filters. Default is to show only concepts from the im namespace and 
     * the projects.
     */
    protected boolean          _includeDataModels    = false;
    protected boolean          _includeAgentModels   = false;
    protected boolean          _includeSubjects      = false;
    protected boolean          _includeConcepts      = true;
    protected boolean          _includeInternal      = false;

    /*
     * these to support the community process. When either is
     * checked, everything else is set to false.
     */
    protected boolean          _showSharedTemporary  = false;
    protected boolean          _showPrivateTemporary = false;
    private Action             action_1;
    private Action             action_2;

    public SearchView() {
    }

    /*
     * Used to make the tree display what we want at 1+ levels 
     * tracking the parent propertly and without having to worry about the model.
     */
    class Child {

        static final int EQUIVALENCE = 1;
        static final int PARENT      = 2;
        static final int CHILD       = 3;

        Object           _parent;
        Object           _target;
        int              _type;

        Child(Object target, Object parent, int type) {
            _parent = parent;
            _target = target;
            _type = type;
        }

        String getType() {
            String ret = "";
            if (_parent instanceof IConcept) {
                ret = "C";
            } else if (_parent instanceof IModelObject) {
                ret = "M";
            } else if (_parent instanceof Child) {
                ret = _parent.toString();
            }
            return ret;
        }

        public Child(Object parent) {
            _parent = parent;
        }

        @Override
        public boolean equals(Object arg0) {
            return arg0 instanceof Child && arg0.toString().equals(toString());
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public String toString() {
            return getType() + "#" + _parent.toString();
        }

        public Image getImage() {
            switch (_type) {
            case CHILD:
                return ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", "icons/child_nav.gif");
            case EQUIVALENCE:
                return ResourceManager.getPluginImage("org.integratedmodelling.thinkcap", "icons/equal.gif");
            case PARENT:
                return ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", "icons/parent_nav.gif");
            }
            return null;
        }

        public boolean hasChildren() {
            // TODO Auto-generated method stub
            return false;
        }

        public Object[] getChildren() {
            // TODO Auto-generated method stub
            return null;
        }

    }

    class ContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
            // TODO Auto-generated method stub

        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            // TODO Auto-generated method stub

        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @SuppressWarnings("unchecked")
        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof Collection<?>) {
                return ((ArrayList<Object>) parentElement).toArray();
            } else if (parentElement instanceof IConcept) {
                return expandConcept((IConcept) parentElement);
            } else if (parentElement instanceof Child) {
                return ((Child) parentElement).getChildren();
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            if (!(element instanceof Collection<?>)) {
                return matches;
            } else if (element instanceof Child) {
                return ((Child) element)._parent;
            }
            return null;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean hasChildren(Object element) {
            if (element instanceof Collection<?>) {
                return ((ArrayList<Object>) element).size() > 0;
            } else if (element instanceof Child) {
                return ((Child) element).hasChildren();
            } else if (element instanceof IConcept) {
                return ((IConcept) element).getChildren().size() > 0 ||
                        ((IConcept) element).getAllParents().size() > 0;
            }

            return false;
        }

    }

    /**
     * TODO use ColumnLabelProvider for each column so we can control font and color and
     * make isNothing() concepts red.
     * 
     * @author fvilla
     *
     */
    class LabelProvider implements ITableLabelProvider {

        @Override
        public void addListener(ILabelProviderListener listener) {
            // TODO Auto-generated method stub

        }

        @Override
        public void dispose() {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean isLabelProperty(Object element, String property) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void removeListener(ILabelProviderListener listener) {
            // TODO Auto-generated method stub

        }

        @Override
        public Image getColumnImage(Object element, int columnIndex) {

            if (columnIndex == 0) {
                if (element instanceof IConcept) {
                    return ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/concept.gif");
                } else if (element instanceof IDirectObserver) {
                    return ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/observation.gif");
                } else if (element instanceof IModel) {
                    if (((IModel) element).getObserver() == null)
                        return ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/agent.gif");
                    else
                        return ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/model.gif");
                } else if (element instanceof Child) {
                    return ((Child) element).getImage();
                }
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {
            if (element instanceof IConcept) {
                if (columnIndex == 0) {
                    return ((IConcept) element).getLocalName();
                } else if (columnIndex == 1) {
                    return ((IConcept) element).getConceptSpace();
                } else if (columnIndex == 2) {
                    return ((IConcept) element).getMetadata().getString(IMetadata.DC_COMMENT);
                }
            } else if (element instanceof IModelObject) {
                if (columnIndex == 0) {
                    return ((IModelObject) element).getId();
                } else if (columnIndex == 1) {
                    return ((IModelObject) element).getNamespace().getId();
                } else if (columnIndex == 2) {
                    return ((IModelObject) element).getMetadata().getString(IMetadata.DC_COMMENT);
                }
            } else if (element instanceof Child) {
                return getColumnText(((Child) element)._target, columnIndex);
            }
            return null;
        }

    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new GridLayout(1, false));
        {
            text = new Text(container, SWT.BORDER);
            text.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    String t = text.getText();
                    search(t);
                }
            });
            text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            toolkit.adapt(text, true, true);
        }
        {
            Composite composite = new Composite(container, SWT.NONE);
            composite.setLayout(new FillLayout(SWT.HORIZONTAL));
            composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            toolkit.adapt(composite);
            toolkit.paintBordersFor(composite);
            {
                Composite normalSearchView = new Composite(composite, SWT.NONE);
                toolkit.adapt(normalSearchView);
                toolkit.paintBordersFor(normalSearchView);
                TreeColumnLayout tcl_normalSearchView = new TreeColumnLayout();
                normalSearchView.setLayout(tcl_normalSearchView);
                {
                    treeViewer = new TreeViewer(normalSearchView, SWT.BORDER);
                    treeViewer.addDoubleClickListener(new IDoubleClickListener() {
                        @Override
                        public void doubleClick(DoubleClickEvent event) {

                            Object o = ((TreeSelection) (event.getSelection())).getFirstElement();

                            if (o instanceof Child)
                                o = ((Child) o)._target;

                            showOntology(o);
                        }
                    });
                    tree = treeViewer.getTree();
                    tree.setHeaderVisible(true);
                    tree.setLinesVisible(true);
                    toolkit.adapt(tree);
                    toolkit.paintBordersFor(tree);
                    {
                        TreeColumn imageColumn = new TreeColumn(tree, SWT.NONE);
                        imageColumn.setText("Name");
                        tcl_normalSearchView.setColumnData(imageColumn, new ColumnPixelData(220, true, true));
                    }
                    {
                        TreeColumn namespaceColumn = new TreeColumn(tree, SWT.NONE);
                        namespaceColumn.addSelectionListener(new SelectionAdapter() {
                            @Override
                            public void widgetSelected(SelectionEvent e) {
                                text.setText("namespace:");
                                text.setSelection("namespace:".length());
                                text.forceFocus();
                            }
                        });
                        tcl_normalSearchView
                                .setColumnData(namespaceColumn, new ColumnPixelData(130, true, true));
                        namespaceColumn.setText("Namespace");
                    }
                    {
                        TreeColumn descriptionColumn = new TreeColumn(tree, SWT.NONE);
                        descriptionColumn.addSelectionListener(new SelectionAdapter() {
                            @Override
                            public void widgetSelected(SelectionEvent e) {
                                text.setText("description:");
                                text.setSelection("description:".length());
                                text.forceFocus();
                            }
                        });
                        tcl_normalSearchView
                                .setColumnData(descriptionColumn, new ColumnPixelData(400, true, true));
                        descriptionColumn.setText("Description");
                    }
                    {
                        treeViewer.setContentProvider(new ContentProvider());
                        treeViewer.setLabelProvider(new LabelProvider());
                        treeViewer.addDragSupport(DND.DROP_DEFAULT, new Transfer[] { TextTransfer
                                .getInstance() }, new DragSourceListener() {

                                    @Override
                                    public void dragStart(DragSourceEvent event) {
                                        // TODO Auto-generated method stub
                                        // System.out.println("ciao");
                                    }

                                    @Override
                                    public void dragSetData(DragSourceEvent event) {
                                        if (event.getSource() instanceof IConcept) {
                                            event.data = ((IConcept) event.getSource()).toString();
                                        } else if (event.getSource() instanceof IModelObject) {
                                            event.data = ((IModelObject) event.getSource()).getName();
                                        }
                                    }

                                    @Override
                                    public void dragFinished(DragSourceEvent event) {
                                        // TODO Auto-generated method stub
                                        // System.out.println("ciao");
                                    }
                                });

                    }
                }
            }
        }
        {
            Label lblNewLabel = new Label(container, SWT.NONE);
            lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
            toolkit.adapt(lblNewLabel, true, true);
            lblNewLabel.setText("No results");
        }

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    public Object[] expandConcept(IConcept parentElement) {

        Collection<IConcept> parents = parentElement.getAllParents();
        Collection<IConcept> children = parentElement.getChildren();

        /*
         * TODO add equivalence
         */
        if (parents.size() + children.size() > 0) {

            ArrayList<Child> ret = new ArrayList<Child>();
            for (IConcept c : parents) {
                ret.add(new Child(c, parentElement, Child.PARENT));
            }
            for (IConcept c : children) {
                ret.add(new Child(c, parentElement, Child.CHILD));
            }
            return ret.toArray();
        }
        return null;
    }

    @Override
    public void dispose() {
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
        {
            selectDataModels = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _includeDataModels = isChecked();
                    search(text.getText());
                }
            };
            selectDataModels.setToolTipText("Include data models in search");
            selectDataModels.setChecked(false);
            selectDataModels.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/model.gif"));
        }
        {
            selectAgentModels = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _includeAgentModels = isChecked();
                    search(text.getText());
                }
            };
            selectAgentModels.setToolTipText("Include agent models in search");
            selectAgentModels.setChecked(false);
            selectAgentModels.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/agent.gif"));
        }
        {
            selectSubjects = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _includeSubjects = isChecked();
                    search(text.getText());
                }
            };
            selectSubjects.setToolTipText("Include observed subjects in search");
            selectSubjects.setChecked(false);
            selectSubjects.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/observer.gif"));
        }
        {
            selectConcepts = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _includeConcepts = isChecked();
                    search(text.getText());
                }
            };
            selectConcepts.setToolTipText("Include concepts in search");
            selectConcepts.setChecked(true);
            selectConcepts.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/concept.gif"));
        }
        {
            action_4 = new Action("Force reindex") {
            };
            action_4.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/cleanup.gif"));
        }
        {
            action = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _includeInternal = !isChecked();
                    search(text.getText());
                }
            };
            action.setToolTipText("Filter out internal knowledge");
            action.setChecked(true);
            action.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/filter_internal_targets.gif"));
        }
        {
            action_1 = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _showSharedTemporary = isChecked();
                    search(text.getText());
                }
            };
            action_1.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Email.png"));
        }
        {
            action_2 = new Action("", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    _showPrivateTemporary = isChecked();
                    search(text.getText());
                }
            };
            action_2.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Percent.png"));
        }
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
        tbm.add(action_2);
        tbm.add(action_1);
        tbm.add(action);
        tbm.add(new Separator());
        tbm.add(selectConcepts);
        tbm.add(selectSubjects);
        tbm.add(selectAgentModels);
        tbm.add(selectDataModels);
        tbm.add(new Separator());
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
        manager.add(action_4);
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    protected void search(final String text) {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                IKnowledgeIndex i = KLAB.KM.getIndex();
                if (i != null) {
                    matches = new ArrayList<Object>();
                    if (text.length() > 1) {
                        try {
                            for (IMetadata m : i.search(text)) {
                                Object o = ((KnowledgeIndex) i).retrieveObject(m);
                                if (use(o))
                                    matches.add(o);
                            }
                        } catch (KlabException e) {
                            Activator.engine().getMonitor().error(e);
                        }
                    }
                } else {
                    // TODO say something
                }
                treeViewer.setInput(matches);
            }
        });

    }

    private void showOntology(Object o) {

        try {
            PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                    .showView("org.integratedmodelling.thinkcap.views.OntologyView");
            Activator.getDefault().fireEvent(new OntologyFocusEvent(o));
        } catch (PartInitException e) {
            Activator.engine().getMonitor().error(e);
        }

    }

    protected boolean use(Object o) {

        if (o == null)
            return false;

        /*
         * check against search settings
         */
        if (o instanceof IConcept) {

            if (!_includeConcepts)
                return false;

            if (!_includeInternal && ((Ontology) ((IConcept) o).getOntology()).isInternal())
                return false;

        } else if (o instanceof IDirectObserver) {

            if (!_includeSubjects)
                return false;

        } else if (o instanceof IModel) {

            if (((IModel) o).getObserver() == null && !_includeAgentModels)
                return false;

            if (((IModel) o).getObserver() != null && !_includeDataModels)
                return false;
        }

        return true;
    }

}
