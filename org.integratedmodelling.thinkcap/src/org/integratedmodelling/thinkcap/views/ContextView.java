/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.ContextEvent;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ScenariosSetEvent;
import org.integratedmodelling.thinkcap.events.TaskEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.ui.PopupSpaceChooser;
import org.integratedmodelling.thinkcap.ui.PopupTimeChooser;
import org.integratedmodelling.thinkcap.ui.PopupTreeChooser;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ContextView extends ViewPart implements IThinklabEventListener {

    public static final String ID                     = "org.integratedmodelling.thinkcap.views.ContextView"; //$NON-NLS-1$
    private final FormToolkit  toolkit                = new FormToolkit(Display
            .getCurrent());
    private Label              dropImage;
    private Composite          container;

    private CLabel             scenariosLabel;
    private Text               subjectLabel;
    private CLabel             spatialContext;
    private CLabel             temporalContext;
    private Table              queryResults;
    private SashForm           dropArea;
    private Button             btnNewButtonSp;
    private Button             btnNewButtonT;
    private Action             resetContext;
    private Action             previousContexts;
    private TableViewer        tableViewer;
    private Button             searchModeButton;
    private Action             action_1;
    private Action             interactiveMode;
    protected boolean          interactiveResolution  = false;

    private DateTimeFormatter  hhmm                   = DateTimeFormat
            .forPattern("hh:mm");

    /*
     * these are set if choices are made when a context isn't active
     */
    private Space              defaultSpatialForcing  = Environment.get()
            .getSpatialForcing();
    private Time               defaultTemporalForcing = Environment.get()
            .getTemporalForcing();
    private ITransition transition;

    class ContextContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            if (inputElement instanceof IContext) {
                return new Object[] { ((IContext) inputElement).getSubject() };
            } else if (inputElement instanceof ISubject) {
                return ((ISubject) inputElement).getSubjects().toArray();
            }
            return null;
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            return getElements(parentElement);
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof ISubject) {
                return ((ISubject) element).getContextObservation() == null
                        ? Environment.get().getContext()
                        : ((ISubject) element).getContextObservation();
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return element instanceof IContext
                    || (element instanceof ISubject
                            && ((ISubject) element).getSubjects().size() > 0);
        }

    }

    class ContextLabelProvider extends BaseLabelProvider implements ILabelProvider {

        @Override
        public Image getImage(Object element) {
            if (element instanceof ISubject) {
                return ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
            }
            return null;
        }

        @Override
        public String getText(Object element) {
            if (element instanceof ISubject) {
                return ((ISubject) element).getName();
            }
            return null;
        }

    }

    class ResultContentProvider implements ITreeContentProvider {

        // painful, but I really don't want to store this in the view. Cross
        // fingers.
        List<?> data = null;

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            if (inputElement instanceof List) {
                data = (List<?>) inputElement;
                return data.toArray();
            }
            return null;
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            return getElements(parentElement);
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof List) {
                data = (List<?>) element;
            }
            if (element instanceof ObservationMetadata) {
                return data;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            if (element instanceof List) {
                data = (List<?>) element;
            }
            return element instanceof List && data != null && data.size() > 0;
        }
    }

    class ResultLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (columnIndex == 0 && element instanceof ObservationMetadata) {
                return ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {
            if (element instanceof ObservationMetadata) {
                switch (columnIndex) {
                case 0:
                    return ((ObservationMetadata) element).id;
                case 1:
                    return ((ObservationMetadata) element).observableName;
                case 2:
                    return ((ObservationMetadata) element).namespaceId.equals(KLAB.NAME)
                            ? "Local database"
                            : ((ObservationMetadata) element).namespaceId;
                case 3:
                    return ((ObservationMetadata) element).description;
                }
            }
            return null;
        }

    }

    public ContextView() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    /**
     * Create contents of the view part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {

        container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        GridLayout gl_container = new GridLayout(1, false);
        gl_container.horizontalSpacing = 0;
        gl_container.verticalSpacing = 0;
        gl_container.marginHeight = 0;
        gl_container.marginWidth = 0;
        container.setLayout(gl_container);
        {
            Composite ccombo = new Composite(container, SWT.NONE);
            ccombo.setBackground(SWTResourceManager
                    .getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
            ccombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            GridLayout gl_ccombo = new GridLayout(3, false);
            gl_ccombo.marginWidth = 0;
            gl_ccombo.marginHeight = 0;
            ccombo.setLayout(gl_ccombo);

            searchModeButton = new Button(ccombo, SWT.TOGGLE);
            searchModeButton.setToolTipText("Query the network for a context ");
            searchModeButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseUp(MouseEvent e) {
                    searchMode(searchModeButton.getSelection());
                }
            });
            searchModeButton
                    .setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/Database.png"));
            toolkit.adapt(searchModeButton, true, true);
            {
                subjectLabel = new Text(ccombo, SWT.NONE);
                subjectLabel.setEnabled(false);
                subjectLabel.setEditable(false);
                subjectLabel
                        .setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
                subjectLabel.setForeground(SWTResourceManager
                        .getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
                subjectLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
                subjectLabel
                        .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                toolkit.adapt(subjectLabel, true, true);
                subjectLabel.setText("No context");
                subjectLabel.addListener(SWT.Traverse, new Listener() {
                    @Override
                    public void handleEvent(Event event) {
                        if (event.detail == SWT.TRAVERSE_RETURN) {
                            searchObservations(subjectLabel.getText());
                        }
                    }
                });
            }
            {
                final Button btnNewButton = new Button(ccombo, SWT.NONE);
                btnNewButton.setToolTipText("Choose target subject");
                btnNewButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseUp(MouseEvent e) {
                        if (Environment.get().getContext() != null) {
                            PopupTreeChooser ptc = new PopupTreeChooser(Eclipse
                                    .getShell(), new ContextLabelProvider(), new ContextContentProvider(), Environment
                                            .get().getContext()) {

                                @Override
                                protected void objectSelected(Object object) {
                                    if (object instanceof ISubject) {
                                        setObservationTarget((ISubject) object);
                                    }
                                    super.objectSelected(object);
                                }

                            };
                            ptc.show(btnNewButton.toDisplay(new Point(e.x, e.y)));
                        } else {
                            Eclipse.beep();
                        }
                    }
                });
                btnNewButton
                        .setImage(ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/Tree.png"));
                toolkit.adapt(btnNewButton, true, true);
            }
        }
        {
            dropArea = new SashForm(container, SWT.NONE);
            dropArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
            dropArea.setLayout(new FillLayout(SWT.HORIZONTAL));
            GridData gd_dropArea = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
            gd_dropArea.widthHint = 168;
            gd_dropArea.heightHint = 168;
            dropArea.setLayoutData(gd_dropArea);
            toolkit.adapt(dropArea);
            toolkit.paintBordersFor(dropArea);
            dropImage = new Label(dropArea, SWT.SHADOW_NONE | SWT.CENTER);
            dropImage.setBackground(SWTResourceManager
                    .getColor(SWT.COLOR_WIDGET_BACKGROUND));
            dropImage.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseDown(MouseEvent e) {
                    // if (_taskId >= 0) {
                    // showData(e.x, e.y);
                    // }
                }
            });
            dropImage.setToolTipText("Drop a subject to define the context.");
            dropImage.setImage(ResourceManager
                    .getPluginImage("org.integratedmodelling.thinkcap", "icons/ndrop.png"));
            toolkit.adapt(dropImage, true, true);
            DropTarget dropTarget = new DropTarget(dropImage, DND.DROP_MOVE
                    | DND.DROP_COPY | DND.DROP_LINK);
            dropTarget
                    .setTransfer(new Transfer[] {
                            // BookmarkTransfer.getInstance(),
                            TextTransfer.getInstance(),
                            LocalSelectionTransfer.getTransfer() });
            DragSource dragSource = new DragSource(dropImage, DND.DROP_MOVE
                    | DND.DROP_COPY);
            dragSource.setTransfer(new Transfer[] {
                    TextTransfer.getInstance()/*
                                               * , BookmarkTransfer.getInstance()
                                               */ });

            tableViewer = new TableViewer(dropArea, SWT.BORDER | SWT.FULL_SELECTION);
            queryResults = tableViewer.getTable();
            queryResults.setHeaderVisible(true);
            queryResults.setVisible(false);
            queryResults.setLinesVisible(true);
            queryResults.addMouseListener(new MouseListener() {

                @Override
                public void mouseUp(MouseEvent e) {
                }

                @Override
                public void mouseDown(MouseEvent e) {
                }

                @Override
                public void mouseDoubleClick(MouseEvent e) {
                    observeFromDatabase((ObservationMetadata) queryResults
                            .getSelection()[0].getData());
                    searchMode(false);
                }
            });
            toolkit.paintBordersFor(queryResults);

            TableColumn tblclmnNewColumn = new TableColumn(queryResults, SWT.NONE);
            tblclmnNewColumn.setWidth(200);
            tblclmnNewColumn.setText("Name");

            TableColumn tblclmnNewColumn_1 = new TableColumn(queryResults, SWT.NONE);
            tblclmnNewColumn_1.setWidth(160);
            tblclmnNewColumn_1.setText("Observable");

            TableColumn tblclmnNewColumn_2 = new TableColumn(queryResults, SWT.NONE);
            tblclmnNewColumn_2.setWidth(140);
            tblclmnNewColumn_2.setText("Namespace");

            TableColumn tblclmnNewColumn_3 = new TableColumn(queryResults, SWT.NONE);
            tblclmnNewColumn_3.setWidth(400);
            tblclmnNewColumn_3.setText("Description");
            dropArea.setWeights(new int[] { 100, 0 });
            tableViewer.setLabelProvider(new ResultLabelProvider());
            tableViewer.setContentProvider(new ResultContentProvider());

            Label label = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
            label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            toolkit.adapt(label, true, true);
            {
                Composite labelContainer = new Composite(container, SWT.NONE);
                labelContainer.setBackground(SWTResourceManager
                        .getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
                GridLayout gl_labelContainer = new GridLayout(3, false);
                gl_labelContainer.verticalSpacing = 0;
                gl_labelContainer.marginWidth = 0;
                gl_labelContainer.marginHeight = 0;
                labelContainer.setLayout(gl_labelContainer);
                labelContainer
                        .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                toolkit.adapt(labelContainer);
                toolkit.paintBordersFor(labelContainer);

                Label lblNewLabel = new Label(labelContainer, SWT.NONE);
                lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
                GridData gd_lblNewLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
                gd_lblNewLabel.widthHint = 62;
                lblNewLabel.setLayoutData(gd_lblNewLabel);
                lblNewLabel.setBounds(0, 0, 55, 15);
                toolkit.adapt(lblNewLabel, true, true);
                lblNewLabel.setText("Scenarios");
                {
                    scenariosLabel = new CLabel(labelContainer, SWT.NONE);
                    scenariosLabel.setFont(SWTResourceManager
                            .getFont("Segoe UI", 9, SWT.ITALIC));
                    scenariosLabel.setText("No scenarios active");
                    scenariosLabel
                            .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                    scenariosLabel.setAlignment(SWT.LEFT);
                    toolkit.adapt(scenariosLabel, true, true);
                }
                {
                    final Button btnNewButtonSC = new Button(labelContainer, SWT.NONE);
                    btnNewButtonSC.addSelectionListener(new SelectionAdapter() {
                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            Environment.get().clearScenarios();
                        }
                    });
                    btnNewButtonSC.setToolTipText("Reset all scenarios");
                    btnNewButtonSC
                            .setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
                    btnNewButtonSC.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                        }
                    });
                    btnNewButtonSC.setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/Player Record.png"));
                    toolkit.adapt(btnNewButtonSC, true, true);
                }
                Label lblNewLabel_1 = new Label(labelContainer, SWT.NONE);
                lblNewLabel_1
                        .setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
                GridData gd_lblNewLabel_1 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
                gd_lblNewLabel_1.widthHint = 62;
                lblNewLabel_1.setLayoutData(gd_lblNewLabel_1);
                toolkit.adapt(lblNewLabel_1, true, true);
                lblNewLabel_1.setText("Space");

                spatialContext = new CLabel(labelContainer, SWT.NONE);
                spatialContext
                        .setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
                spatialContext.setText(Environment.get().getSpatialForcing().toString());
                spatialContext.setForeground(SWTResourceManager
                        .getColor(SWT.COLOR_WIDGET_BORDER));
                spatialContext.setBackground(SWTResourceManager
                        .getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
                spatialContext
                        .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                toolkit.adapt(spatialContext, true, true);

                Menu menu = new Menu(spatialContext);
                spatialContext.setMenu(menu);

                MenuItem mntmSetThisAs = new MenuItem(menu, SWT.NONE);
                mntmSetThisAs.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        makeDefault(Environment.get().getSpatialForcing());
                    }
                });
                mntmSetThisAs.setText("Set this as default");

                MenuItem mntmResetDefaults = new MenuItem(menu, SWT.NONE);
                mntmResetDefaults.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        makeDefault(Space.getForcing(1, "km"));
                    }
                });
                mntmResetDefaults.setText("Reset defaults");
                {
                    btnNewButtonSp = new Button(labelContainer, SWT.NONE);
                    btnNewButtonSp
                            .setToolTipText("Choose default spatial representation");
                    btnNewButtonSp
                            .setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
                    btnNewButtonSp.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                            PopupSpaceChooser ptc = new PopupSpaceChooser(Eclipse
                                    .getShell(), SWT.BORDER, Environment.get()
                                            .getSpatialForcing(), ContextView.this);
                            ptc.show(btnNewButtonSp.toDisplay(new Point(e.x, e.y - 110)));
                        }
                    });
                    btnNewButtonSp.setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/Globe.png"));
                    toolkit.adapt(btnNewButtonSp, true, true);
                }

                Label lblNewLabel_3 = new Label(labelContainer, SWT.NONE);
                lblNewLabel_3
                        .setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
                GridData gd_lblNewLabel_3 = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
                gd_lblNewLabel_3.widthHint = 62;
                lblNewLabel_3.setLayoutData(gd_lblNewLabel_3);
                toolkit.adapt(lblNewLabel_3, true, true);
                lblNewLabel_3.setText("Time");

                temporalContext = new CLabel(labelContainer, SWT.NONE);
                temporalContext
                        .setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
                temporalContext.setText("No temporal context");
                temporalContext.setForeground(SWTResourceManager
                        .getColor(SWT.COLOR_WIDGET_BORDER));
                temporalContext.setBackground(SWTResourceManager
                        .getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
                temporalContext
                        .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                toolkit.adapt(temporalContext, true, true);

                Menu menu_1 = new Menu(temporalContext);
                temporalContext.setMenu(menu_1);

                MenuItem mntmSetThisAs_1 = new MenuItem(menu_1, SWT.NONE);
                mntmSetThisAs_1.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        makeDefault(Environment.get().getTemporalForcing());
                    }
                });
                mntmSetThisAs_1.setText("Set this as default");

                MenuItem mntmResetDefaults_1 = new MenuItem(menu_1, SWT.NONE);
                mntmResetDefaults_1.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        makeDefault(Time.getForcing(0, 0, 0));
                    }
                });
                mntmResetDefaults_1.setText("Reset defaults");
                {
                    btnNewButtonT = new Button(labelContainer, SWT.NONE);
                    btnNewButtonT
                            .setToolTipText("Choose default temporal representation");
                    btnNewButtonT
                            .setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
                    btnNewButtonT.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseUp(MouseEvent e) {
                            PopupTimeChooser ptc = new PopupTimeChooser(Eclipse
                                    .getShell(), SWT.BORDER, Environment.get()
                                            .getTemporalForcing(), ContextView.this);
                            ptc.show(btnNewButtonSp.toDisplay(new Point(e.x, e.y - 110)));
                        }
                    });
                    btnNewButtonT.setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/Clock.png"));
                    toolkit.adapt(btnNewButtonT, true, true);
                }
            }
            dropTarget.addDropListener(new DropTargetAdapter() {

                @Override
                public void drop(DropTargetEvent event) {

                    boolean addToContext = (event.detail
                            & DND.DROP_COPY) == DND.DROP_COPY;

                    if (Activator.engine()
                            .handleObservationAction(event.data, addToContext)) {

                        action_1.setEnabled(true);

                        /*
                         * reset forcings to default
                         */
                        setTimeForcing(defaultTemporalForcing);
                        setSpatialForcing(defaultSpatialForcing);
                    }
                }
            });
        }

        createActions();

        initializeToolBar();

        initializeMenu();

    }

    protected void makeDefault(IExtent forcing) {
        Environment.get().persistForcings();
    }

    protected void observeFromDatabase(ObservationMetadata data) {
        try {
            IDirectObserver observer = Activator.engine()
                    .retrieveObservation(data.getName(), data.getNodeId());
            if (Environment.get().getSession() != null && observer != null) {
                Environment.get().getSession().observe(observer, Environment.get()
                        .getSpatialForcing(), (Environment.get()
                                .getTemporalForcing() != null
                                && Environment.get().getTemporalForcing().isEmpty())
                                        ? null
                                        : Environment.get().getTemporalForcing());
                Environment.get().defineLastContextObserved(observer);
                action_1.setEnabled(true);
            } else {
                Eclipse.error(data.id
                        + " could not be observed: observable concepts unavailable or database out of date");
            }
        } catch (KlabException e) {
            Eclipse.handleException(e);
        }
    }

    private void setObservationTarget(ISubject object) {
        Environment.get().setTargetSubject(object);
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                subjectLabel.setText(object.getName());
            }
        });
    }

    protected void searchObservations(String text) {
        try {
            tableViewer.setInput(Activator.engine().queryObservations(text, false));
        } catch (KlabException e) {
            Eclipse.handleException(e);
        }

    }

    protected void searchMode(boolean on) {

        subjectLabel.setText(on ? ""
                : (Environment.get().getTargetSubject() == null ? "No context"
                        : Environment.get().getTargetSubject().getName()));
        subjectLabel.setEnabled(on);
        subjectLabel.setEditable(on);
        queryResults.setVisible(on);
        dropArea.setWeights(on ? new int[] { 0, 100 } : new int[] { 100, 0 });
        subjectLabel.setFocus();
        searchModeButton.setSelection(on);
        if (on) {
            tableViewer.setInput(null);
        }
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        toolkit.dispose();
        super.dispose();
    }

    public void choosePath(Display display, Shell shell, int x, int y) {

        Menu menu = new Menu(shell, SWT.POP_UP);
        MenuItem item = new MenuItem(menu, SWT.PUSH);
        item.setText("Menu Item");
        item.addListener(SWT.Selection, new Listener() {
            @Override
            public void handleEvent(Event e) {
                System.out.println("Item Selected");
            }
        });
        menu.setLocation(x, y);
        menu.setVisible(true);
        while (!menu.isDisposed() && menu.isVisible()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        menu.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {

        {
            resetContext = new Action("Reset context") {
                @Override
                public void run() {
                    if (Environment.get().getContext() == null) {
                        return;
                    }
                    Activator.getDefault()
                            .fireEvent(new ContextEvent(null, ContextEvent.FOCUS));
                }
            };
            resetContext.setToolTipText("Clear context");
            resetContext.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/target_red.png"));
        }
        {

            class ContextAction extends Action {

                IContext context;

                ContextAction(IContext context) {
                    super(context.getSubject().getName() + " ["
                            + new DateTime(context.getCreationTime()).toString(hhmm)
                            + "]");
                    this.context = context;
                    this.setImageDescriptor(ResourceManager
                            .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/observer.gif"));
                }

                @Override
                public void run() {
                    Job job = new Job("Switching contexts") {

                        @Override
                        protected IStatus run(IProgressMonitor monitor) {
                            Activator.getDefault()
                                    .fireEvent(new ContextEvent(context, ContextEvent.FOCUS));
                            return Status.OK_STATUS;
                        }
                    };
                    job.schedule();
                }
            }

            previousContexts = new Action("", SWT.DROP_DOWN) {

            };
            previousContexts.setMenuCreator(new IMenuCreator() {

                Menu fMenu = null;

                @Override
                public Menu getMenu(Menu parent) {
                    return null;
                }

                @Override
                public Menu getMenu(Control parent) {

                    if (fMenu != null) {
                        fMenu.dispose();
                    }

                    fMenu = new Menu(parent);
                    if (Environment.get().getSession() != null) {
                        for (IContext context : Environment.get().getSession()
                                .getContexts()) {
                            addActionToMenu(fMenu, new ContextAction(context));
                        }
                    }
                    return fMenu;
                }

                protected void addActionToMenu(Menu parent, Action action) {
                    ActionContributionItem item = new ActionContributionItem(action);
                    item.fill(parent, -1);
                }

                @Override
                public void dispose() {
                }
            });
            previousContexts.setToolTipText("Previous contexts");
            previousContexts.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/history_folder_green.png"));
        }
        {
            action_1 = new Action("") {

                @Override
                public void run() {
                    if (Environment.get().getSession() != null) {
                        try {
                            Environment.get().getSession()
                                    .observe(Environment.get()
                                            .reconstructLastContextObserved(), Environment.get()
                                                    .getScenarios(), Environment
                                                            .get()
                                                            .getSpatialForcing(), (Environment
                                                                    .get()
                                                                    .getTemporalForcing() != null
                                                                    && Environment.get()
                                                                            .getTemporalForcing()
                                                                            .isEmpty())
                                                                                    ? null
                                                                                    : Environment
                                                                                            .get()
                                                                                            .getTemporalForcing());
                        } catch (KlabException e) {
                            Eclipse.handleException(e);
                        }
                    }
                }

            };
            action_1.setEnabled(false);
            action_1.setToolTipText("Observe current context again");
            action_1.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/refresh.gif"));
        }

        {
            interactiveMode = new Action("Interactive mode", IAction.AS_CHECK_BOX) {
                @Override
                public void run() {
                    interactiveResolution = interactiveMode.isChecked();
                    Activator.engine().setInteractiveMode(interactiveResolution);
                }
            };
            interactiveMode.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/icon-edit.png"));
            interactiveMode.setToolTipText("Toggle interactive mode during resolution");
            interactiveMode.setEnabled(false);
        }

    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
        tbm.add(interactiveMode);
        tbm.add(action_1);
        tbm.add(resetContext);
        tbm.add(previousContexts);
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // TODO adjourn
    }

    @Override
    public void handleThinklabEvent(final ThinklabEvent event) {

        if (event instanceof EngineEvent) {

            EngineEvent se = (EngineEvent) event;

            if (Activator.engine().getCurrentEngine() != null
                    && Activator.engine().getCurrentEngine()
                            .equals(((EngineEvent) event).engine)) {
                if (se.type == EngineEvent.ONLINE) {
                    enable();
                } else if (se.type == EngineEvent.OFFLINE) {
                    disable(true);
                    Display.getDefault().asyncExec(new Runnable() {
                        @Override
                        public void run() {
                            reset();
                        }
                    });
                }
            }
        } else if (event instanceof TaskEvent) {

            TaskEvent teven = (TaskEvent) event;

            if (teven.type == TaskEvent.FOCUS) {
                // previousTask = this.task;
                // this.task = ((TaskEvent) event).task;
                rescan();
            } else /*
                    * TODO needs to check if the event produces any modification. Display
                    * is slow and during fast temporal transitions this enqueues too many
                    * redraws.
                    */ {
                boolean forceFinished = teven.task.getStatus() == ITask.Status.FINISHED;
//                ITransition transition = ((Task)teven.task).getCurrentTransition();
//                if (forceFinished || !(transition != null && this.transition != null)) {
                    display(forceFinished);
//                }
//                this.transition = transition;
            }

        } else if (event instanceof ContextEvent) {

            if (((ContextEvent) event).context == null) {
                setEmpty();
            } else if (((ContextEvent) event).type != ContextEvent.NEW) {
                display(false);
            }

            // } else {
            // /*
            // * switch context
            // */
            // this.context = ((ContextEvent) event).context;
            // if (context.getSubject() != null) {
            // Environment.get()
            // .setTemporalForcing(Time.getForcing(Environment.get().getTargetSubject().getScale().getTime()));
            // this.targetSubject = context.getSubject();
            // }
            // }
        } else if (event instanceof ScenariosSetEvent) {
            Display.getDefault().asyncExec(new Runnable() {

                @Override
                public void run() {
                    String label = "No scenarios active";
                    if (((ScenariosSetEvent) event).scenarios.size() > 0) {
                        label = "";
                        for (INamespace sc : ((ScenariosSetEvent) event).scenarios) {
                            label += (label.isEmpty() ? "" : ", ") + sc.getId();
                        }
                    }
                    scenariosLabel.setText(label);
                }

            });

        }
    }

    private void setEmpty() {
        Environment.get().setEmpty();
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                reset();
            }
        });
    }

    private void enable() {
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                container.setEnabled(true);
                interactiveMode.setEnabled(true);
            }
        });
    }

    private void disable(final boolean wholeComponent) {

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                if (wholeComponent)
                    container.setEnabled(false);
                else {
                    /*
                     * leave only the editor enabled
                     */
                }
                interactiveMode.setEnabled(false);
            }
        });
    }

    protected void display(boolean forceFinished) {

        /*
         * TODO extract to independent class to use here, in Provenance view and in
         * Archive view Add display of non-state data properties, extents, history and
         * model graphs Provide a path to each of those for invoking in editor from other
         * views.
         */
        Display.getDefault().asyncExec(new Runnable() {

            @Override
            public void run() {

                Environment env = Environment.get();
                
                if (Environment.get().getContext() == null) {
                    // EMPTY
                    dropImage
                            .setToolTipText("Drop or search for a subject to use as context.");
                    dropImage.setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/ndrop.png"));

                } else if (!forceFinished && (Environment.get().getTargetSubject() == null
                        || Environment.get().getContext().isRunning())) {

                    dropImage
                            .setToolTipText("Contextualization is ongoing. Please wait for it to finish.");
                    dropImage.setImage(ResourceManager
                            .getPluginImage("org.integratedmodelling.thinkcap", "icons/wrun.png"));
                    
                } else {

                    // keep the focal subject in the view.
                    IScale scale = Environment.get().getTargetSubject().getScale();
                    subjectLabel.setText(Environment.get().getTargetSubject().getName());

                    /*
                     * Only leave the scale forcing options available if the context does
                     * not already have a defined resolution.
                     */
                    boolean hasGrid = scale.getSpace() != null
                            && scale.getSpace().getMultiplicity() > 1;
                    // btnNewButtonSp.setEnabled(!hasGrid);
                    boolean hasSteps = scale.getTime() != null
                            && scale.getTime().getMultiplicity() > 1;
                    // btnNewButtonT.setEnabled(!hasSteps);

                    spatialContext
                            .setText(scale.getSpace() == null ? "No spatial context"
                                    : scale.getSpace().toString());
                    temporalContext
                            .setText(scale.getTime() == null ? "No temporal context"
                                    : scale.getTime().toString());

                    boolean dynamic = Environment.get().getTargetSubject().getScale()
                            .getTime() != null
                            && Environment.get().getTargetSubject().getScale().getTime()
                                    .isTemporallyDistributed();

                    if (Environment.get().getContext().isFinished()) {
                        dropImage
                                .setToolTipText("Contextualization is finished; no more observations are possible");
                        dropImage.setImage(ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/ocheck.png"));
                    } else if (Environment.get().getFocusTask() != null
                            && Environment.get().getFocusTask()
                                    .getStatus() == org.integratedmodelling.api.runtime.ITask.Status.ERROR) {
                        dropImage
                                .setToolTipText("Previous computations ended in errors. Drop a new subject to define the context.");
                        dropImage.setImage(ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/estop.png"));
                    } else if (Environment.get().getFocusTask() != null
                            && Environment.get().getFocusTask()
                                    .getStatus() == org.integratedmodelling.api.runtime.ITask.Status.RUNNING) {
                        // NO needs a wait image
                        dropImage
                                .setToolTipText("Contextualization is ongoing. Please wait for it to finish.");
                        dropImage.setImage(ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/wrun.png"));
                    } else { /*
                              * else if (_context.isInterrupted()) {
                              * spaceLabel.setToolTipText(
                              * "Drop a subject to define the context.");
                              * spaceLabel.setImage(ResourceManager .getPluginImage(
                              * "org.integratedmodelling.thinkcap", "icons/ostop.png")); }
                              */
                        // else {

                        dropImage.setToolTipText("Drop an observable to observe it in "
                                + Environment.get().getTargetSubject().getName()
                                + (dynamic ? " or start time contextualization." : "."));
                        dropImage.setImage(ResourceManager
                                .getPluginImage("org.integratedmodelling.thinkcap", "icons/odrop.png"));
                    }
                }
            }

        });

    }

    /*
     * next version
     */
    private void rescan() {

        if (Environment.get().getFocusTask() == null) {

            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    reset();
                }
            });
        }
    }

    public void resetForcings() {

    }

    public void setSpatialForcing(Space space) {

        if (Environment.get().getFocusTask() == null) {
            this.defaultSpatialForcing = space;
        }

        Environment.get().setSpatialForcing(space);
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                spatialContext.setText(Environment.get().getSpatialForcing().toString());
            }
        });
    }

    /*
     * must be run in UI thread
     */
    protected void reset() {

        disable(false);

        action_1.setEnabled(false);

        Environment.get().setEmpty();

        subjectLabel.setText("No context");
        dropImage.setToolTipText("Drop a subject to define the context.");
        dropImage.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/ndrop.png"));
        spatialContext.setText(Environment.get().getSpatialForcing() == null
                ? "No spatial context"
                : Environment.get().getSpatialForcing().toString());
        temporalContext.setText(Environment.get().getTemporalForcing() == null
                ? "No temporal context"
                : Environment.get().getTemporalForcing().toString());
        btnNewButtonSp.setEnabled(true);
        btnNewButtonT.setEnabled(true);
    }

    public void setTimeForcing(Time time) {

        if (Environment.get().getContext() == null) {
            this.defaultTemporalForcing = time;
        }

        Environment.get().setTemporalForcing(time);
        temporalContext.setText(Environment.get().getTemporalForcing() == null
                ? "No temporal context"
                : Environment.get().getTemporalForcing().toString());
    }

}
