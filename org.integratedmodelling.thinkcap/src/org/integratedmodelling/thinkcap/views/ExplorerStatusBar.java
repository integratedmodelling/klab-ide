package org.integratedmodelling.thinkcap.views;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;

public class ExplorerStatusBar extends ViewPart {

    public static final String ID      = "org.integratedmodelling.thinkcap.views.ExplorerStatusBar"; //$NON-NLS-1$
    private final FormToolkit  toolkit = new FormToolkit(Display.getCurrent());

    public ExplorerStatusBar() {
    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new GridLayout(1, false));
        
        Canvas canvas = new Canvas(container, SWT.NONE);
        GridData gd_canvas = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_canvas.heightHint = 80;
        canvas.setLayoutData(gd_canvas);
        canvas.setBounds(0, 0, 64, 64);
        toolkit.adapt(canvas);
        toolkit.paintBordersFor(canvas);
        
//        PaletteItemWidget btnNewButton = new PaletteItemWidget(canvas, null);
//        btnNewButton.setBounds(0, 0, 140, 30);
//        toolkit.adapt(btnNewButton, true, true);

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    @Override
    public void dispose() {
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }
}
