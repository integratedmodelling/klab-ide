/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.common.client.referencing.Bookmark;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ShapeCreatedEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.model.ShapeRecord;
import org.integratedmodelling.thinkcap.wizards.NewNamespaceWizard;
import org.integratedmodelling.thinkcap.wizards.ObservationImportWizard;
import org.integratedmodelling.thinkcap.wizards.UploadToDatabaseWizard;

public class ObservationRecorder extends ViewPart implements IThinklabEventListener {

    public static final String  ID             = "org.integratedmodelling.thinkcap.views.ObservationRecorder"; //$NON-NLS-1$
    private final FormToolkit   toolkit        = new FormToolkit(Display.getCurrent());
    private Table               table;
    private Text                text;
    private IConcept            currentConcept;

    private Set<String>         recentConcepts = new HashSet<>();

    List<ShapeRecord>           data           = new ArrayList<>();
    private CheckboxTableViewer checkboxTableViewer;
    private Combo               combo;
    private Button              btnCheckUncheckAll;
    private Button              btnRemoveAll;
    private Button              btnImport;
    private Button              btnSaveToDb;
    private Button              btnSaveToNamespace;
    private Button              btnCopyToClipboard;

    class ObservationContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
            // TODO Auto-generated method stub

        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            // TODO Auto-generated method stub

        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof List) {
                return ((List<?>) parentElement).toArray();
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof ShapeRecord) {
                return data;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return element instanceof List && ((List<?>) element).size() > 0;
        }

    }

    class ObservationLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (columnIndex == 0 && element instanceof ShapeRecord) {
                return ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, ((ShapeRecord) element).stored
                                ? "icons/database.gif"
                                : "icons/observer.gif");
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {
            if (element instanceof ShapeRecord) {
                switch (columnIndex) {
                case 0:
                    return ((ShapeRecord) element).getName();
                case 1:
                    return ((ShapeRecord) element).getShape();
                case 2:
                    return ((ShapeRecord) element).getConcept().toString();
                }
            }
            return null;
        }
    }

    class ObservationCheckStateProvider implements ICheckStateProvider {

        @Override
        public boolean isChecked(Object element) {
            return (element instanceof ShapeRecord && ((ShapeRecord) element).publish)
                    || ((ShapeRecord) element).isKnowledge();
        }

        @Override
        public boolean isGrayed(Object element) {
            return !(element instanceof ShapeRecord) || !isNameOk(((ShapeRecord) element).getName())
                    || ((ShapeRecord) element).isKnowledge();
        }

        private boolean isNameOk(String name) {
            return name != null;
            // TODO add whitespace and duplication check
        }

    }

    public ObservationRecorder() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    /**
     * Create contents of the view part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {

        loadRecentConcepts();

        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new GridLayout(1, false));

        Composite composite = new Composite(container, SWT.BORDER);
        composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        composite.setBounds(0, 0, 64, 64);
        toolkit.adapt(composite);
        toolkit.paintBordersFor(composite);
        composite.setLayout(new GridLayout(4, false));

        Label lblAnnotateAs = new Label(composite, SWT.NONE);
        lblAnnotateAs.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        toolkit.adapt(lblAnnotateAs, true, true);
        lblAnnotateAs.setText("Annotate as:");

        text = new Text(composite, SWT.BORDER);
        text.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                if (KLAB.KM != null) {
                    /*
                     * may be null if the view is shown at startup.
                     */
                    IConcept c = KLAB.KM.getConcept(text.getText());
                    if (c != null && NS.isThing(c)) {
                        currentConcept = c;
                        text.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));
                        addRecentConcept(c.toString());
                    } else {
                        text.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
                    }
                }
            }
        });
        text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        toolkit.adapt(text, true, true);

        Label lblRecent = new Label(composite, SWT.NONE);
        lblRecent.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        toolkit.adapt(lblRecent, true, true);
        lblRecent.setText("Recent: ");

        combo = new Combo(composite, SWT.READ_ONLY);
        combo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                text.setText(combo.getText());
            }
        });

        for (String s : recentConcepts) {
            combo.add(s);
        }

        GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
        gd_combo.widthHint = 180;
        combo.setLayoutData(gd_combo);
        combo.select(0);
        text.setText(combo.getText());
        if (KLAB.KM != null) {
            currentConcept = KLAB.KM.getConcept(text.getText());
        }
        toolkit.adapt(combo);
        toolkit.paintBordersFor(combo);

        checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER
                | SWT.FULL_SELECTION);
        checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
            @Override
            public void checkStateChanged(CheckStateChangedEvent event) {
                ShapeRecord sr = (ShapeRecord) event.getElement();
                sr.publish = event.getChecked();
            }
        });

        checkboxTableViewer.setLabelProvider(new ObservationLabelProvider());
        checkboxTableViewer.setContentProvider(new ObservationContentProvider());
        checkboxTableViewer.setCheckStateProvider(new ObservationCheckStateProvider());

        table = checkboxTableViewer.getTable();
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        table.setBounds(0, 0, 85, 85);
        toolkit.paintBordersFor(table);

        TableColumn tblclmnName = new TableColumn(table, SWT.NONE);
        tblclmnName.setWidth(419);
        tblclmnName.setText("Name");

        TableColumn tblclmnShape = new TableColumn(table, SWT.NONE);
        tblclmnShape.setWidth(168);
        tblclmnShape.setText("Shape");

        final TableEditor editor = new TableEditor(table);

        TableColumn tblclmnObservable = new TableColumn(table, SWT.NONE);
        tblclmnObservable.setWidth(160);
        tblclmnObservable.setText("Observable");

        // The editor must have the same size as the cell and must
        // not be any smaller than 50 pixels.
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = Math.max(tblclmnName.getWidth() - 50, 50);
        final int EDITABLECOLUMN = 0;

        table.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {

                Control oldEditor = editor.getEditor();
                if (oldEditor != null)
                    oldEditor.dispose();
                final TableItem item = (TableItem) e.item;
                if (item == null)
                    return;
                final ShapeRecord sr = (ShapeRecord) item.getData();

                final Text text = new Text(table, SWT.NONE);
                Listener textListener = new Listener() {
                    @Override
                    public void handleEvent(final Event e) {
                        switch (e.type) {
                        case SWT.FocusOut:
                            item.setText(EDITABLECOLUMN, text.getText());
                            String name = sanitize(text.getText());
                            if (!name.isEmpty()) {
                                sr.setName(name);
                            }
                            text.dispose();
                            refresh();
                            break;
                        case SWT.Traverse:
                            switch (e.detail) {
                            case SWT.TRAVERSE_RETURN:
                                item.setText(EDITABLECOLUMN, text.getText());
                                // FALL THROUGH
                            case SWT.TRAVERSE_ESCAPE:
                                name = sanitize(text.getText());
                                if (!name.isEmpty()) {
                                    sr.setName(name);
                                    sr.publish = true;
                                }
                                refresh();
                                text.dispose();
                                e.doit = false;
                            }
                            break;
                        }
                    }
                };
                text.addListener(SWT.FocusOut, textListener);
                text.addListener(SWT.Traverse, textListener);
                editor.setEditor(text, item, EDITABLECOLUMN);
                text.setText(item.getText(EDITABLECOLUMN));
                text.selectAll();
                text.setFocus();
            }
        });

        final DragSource source = new DragSource(table, DND.DROP_COPY | DND.DROP_MOVE
                | DND.DROP_DEFAULT);
        source.addDragListener(new DragSourceAdapter() {
            @Override
            public void dragSetData(DragSourceEvent event) {
                event.data = new Bookmark(((ShapeRecord) table.getSelection()[0].getData())
                        .makeSubjectObserver()).getSignature();
            }
        });
        source.setTransfer(new Transfer[] {
                /* BookmarkTransfer.getInstance(), */ TextTransfer.getInstance() });

        Composite composite_1 = new Composite(container, SWT.NONE);
        RowLayout rl_composite_1 = new RowLayout(SWT.HORIZONTAL);
        rl_composite_1.pack = false;
        composite_1.setLayout(rl_composite_1);
        composite_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        composite_1.setSize(297, 468);
        toolkit.adapt(composite_1);
        toolkit.paintBordersFor(composite_1);

        Button btnNewButton = new Button(composite_1, SWT.NONE);
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                queryLocal();
            }
        });
        toolkit.adapt(btnNewButton, true, true);
        btnNewButton.setText("Query existing");

        btnRemoveAll = new Button(composite_1, SWT.NONE);
        btnRemoveAll.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                removeCheckedFromDb();
                data.clear();
                refresh();
            }
        });

        toolkit.adapt(btnRemoveAll, true, true);
        btnRemoveAll.setText("Remove all");

        btnCheckUncheckAll = new Button(composite_1, SWT.NONE);
        btnCheckUncheckAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                toggleAll();
            }
        });
        toolkit.adapt(btnCheckUncheckAll, true, true);
        btnCheckUncheckAll.setText("Toggle all");

        btnImport = new Button(composite_1, SWT.NONE);
        btnImport.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                if (currentConcept == null) {
                    Eclipse.alert("Please define the concept to import as.");
                } else {
                    ObservationImportWizard wizard = new ObservationImportWizard(currentConcept, ObservationRecorder.this);
                    WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
                    dialog.setBlockOnOpen(true);
                    dialog.create();
                    dialog.open();
                }
            }
        });
        toolkit.adapt(btnImport, true, true);
        btnImport.setText("Import...");

        btnSaveToDb = new Button(composite_1, SWT.NONE);
        btnSaveToDb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                saveToDatabase();
            }
        });
        toolkit.adapt(btnSaveToDb, true, true);
        btnSaveToDb.setText("Save to database");

        btnSaveToNamespace = new Button(composite_1, SWT.NONE);
        btnSaveToNamespace.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnSaveToNamespace.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                saveToNamespace();
            }
        });
        toolkit.adapt(btnSaveToNamespace, true, true);
        btnSaveToNamespace.setText("Save to namespace");

        btnCopyToClipboard = new Button(composite_1, SWT.NONE);
        toolkit.adapt(btnCopyToClipboard, true, true);
        btnCopyToClipboard.setText("Copy to clipboard");
        btnCopyToClipboard.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                saveToClipboard();
            }
        });

        btnCheckUncheckAll.setEnabled(false);
        btnRemoveAll.setEnabled(false);
        btnSaveToDb.setEnabled(false);
        btnSaveToNamespace.setEnabled(false);
        btnCopyToClipboard.setEnabled(false);

        createActions();
        initializeToolBar();
        initializeMenu();

    }

    protected void toggleAll() {

        for (ShapeRecord sr : data) {
            if (sr.getName() != null && !sr.isKnowledge()) {
                sr.publish = !sr.publish;
            }
        }
        refresh();

    }

    protected void removeCheckedFromDb() {

        final ArrayList<String> toRemove = new ArrayList<>();
        for (ShapeRecord record : data) {
            if (record.stored && record.publish && record.getFqName() != null) {
                toRemove.add(record.getFqName());
            }
        }

        if (!toRemove.isEmpty()) {
            Job job = new Job("Deleting observations from database") {
                @Override
                protected IStatus run(IProgressMonitor monitor) {
                    try {
                        Activator.engine().removeObservations(toRemove);
                        for (Iterator<ShapeRecord> it = data.iterator(); it.hasNext();) {
                            ShapeRecord sh = it.next();
                            if (sh.stored && sh.publish) {
                                it.remove();
                            }
                        }
                        refresh();
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                    return Status.OK_STATUS;
                }
            };
            job.setUser(true);
            job.schedule();
        }
    }

    protected void queryLocal() {
        try {
            for (IObservationMetadata omd : Activator
                    .engine()
                    .queryObservations(text.getText().trim().isEmpty() ? combo.getText()
                            : text.getText(), true)) {
                data.add(new ShapeRecord(omd));
            }
            refresh();
        } catch (KlabException e) {
            Eclipse.handleException(e);
        }
    }

    public void clearPublished() {

        for (Iterator<ShapeRecord> it = data.iterator(); it.hasNext();) {
            if (it.next().publish) {
                it.remove();
            }
        }
        refresh();
    }

    private void loadRecentConcepts() {

        File f = new File(System.getProperty("user.home") + File.separator + ".or_concepts.txt");
        if (f.exists()) {
            try {
                for (String conc : FileUtils.readLines(f)) {
                    recentConcepts.add(conc);
                }
            } catch (IOException e) {
            }
        }
        recentConcepts.add("earth:Region");
    }

    private void addRecentConcept(final String s) {

        if (recentConcepts.contains(s)) {
            return;
        }

        recentConcepts.add(s);
        File f = new File(System.getProperty("user.home") + File.separator + ".or_concepts.txt");
        try {
            FileUtils.writeLines(f, recentConcepts);
        } catch (IOException e) {
        }
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                combo.removeAll();
                int selected = 0, i = 0;
                for (String ss : recentConcepts) {
                    combo.add(ss);
                    if (s.equals(ss)) {
                        selected = i;
                    }
                    i++;
                }
                combo.select(selected);
            }
        });

    }

    protected boolean saveToNamespace() {

        NewNamespaceWizard wizard = new NewNamespaceWizard(true);
        wizard.setNamespaceGenerator(new Project.NamespaceGenerator() {

            @Override
            public void write(String id, boolean isScenario, boolean isAppending, PrintWriter out)
                    throws Exception {
                out.print((isScenario ? "scenario " : "namespace ") + id);
                Collection<String> namespaces = collectNamespaces();

                /*
                 * preamble
                 */
                if (namespaces.size() == 0) {
                    out.println(";\n");
                } else {
                    out.print("\n   using");
                    int n = 0;
                    for (String s : namespaces) {
                        out.print("\n      " + s);
                        if (n == namespaces.size() - 1) {
                            out.println(";");
                        } else {
                            out.print(",");
                        }
                        n++;
                    }
                }

                for (ShapeRecord record : data) {
                    if (record.publish) {
                        out.println("\n" + record.getKIM());
                    }
                }

                clearPublished();
            }
        });
        WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
        dialog.setBlockOnOpen(true);
        dialog.create();
        return dialog.open() == Window.OK;
    }

    protected void saveToClipboard() {

        String defs = "";
        for (ShapeRecord record : data) {
            if (record.publish) {
                defs += record.getKIM() + "\n\n";
            }
        }

        Eclipse.copyToClipboard(defs);
    }

    protected Collection<String> collectNamespaces() {
        Set<String> ret = new HashSet<>();
        for (ShapeRecord sr : data) {
            if (sr.publish) {
                ret.add(sr.getConcept().getConceptSpace());
            }
        }
        return ret;
    }

    protected void saveToDatabase() {

        UploadToDatabaseWizard wizard = new UploadToDatabaseWizard(this);
        WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
        dialog.setBlockOnOpen(true);
        dialog.create();
        dialog.open();

    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    private String sanitize(String text) {
        text = text.trim();
        text = StringUtils.replaceWhitespace(text, "-");
        text = text.replace(".", "-");
        text = StringUtils.lowerCase(text);
        return text;
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {
        if (event instanceof ShapeCreatedEvent) {
            if (currentConcept == null) {
                Display.getDefault().syncExec(new Runnable() {
                    @Override
                    public void run() {
                        // regular Eclipse.alert won't work as this is called when the
                        // view isn't the active
                        // one.
                        MessageDialog
                                .openInformation(getSite()
                                        .getShell(), "Information", "Please set the type of subject that this shape will provide spatial context for.");
                    }
                });
            } else {
                data.add(new ShapeRecord(((ShapeCreatedEvent) event).shapeDef, currentConcept));
                refresh();
            }
        }
    }

    private void refresh() {
        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                checkboxTableViewer.setInput(data);
                boolean haveData = data.size() > 0;
                boolean haveChecked = haveChecked();
                btnCheckUncheckAll.setEnabled(haveData);
                btnRemoveAll.setEnabled(haveData);
                btnSaveToDb.setEnabled(haveChecked);
                btnSaveToNamespace.setEnabled(haveChecked);
                btnCopyToClipboard.setEnabled(haveChecked);
            }

        });
    }

    private boolean haveChecked() {
        for (ShapeRecord sr : data) {
            if (sr.publish) {
                return true;
            }
        }
        return false;
    }

    public void loadData(List<IObservationMetadata> importedData, String idField, IConcept concept) {

        for (IObservationMetadata zio : importedData) {

            if (zio.getName() != null && ((ObservationMetadata) zio).getGeometryWKB() != null) {
                ShapeRecord sr = new ShapeRecord(((ObservationMetadata) zio).getGeometryWKB(), concept);
                sr.setName(sanitize(idField != null ? zio.getAttributes().get(idField).toString()
                        : zio.getName()));
                sr.publish = true;
                data.add(sr);
            }
        }

        refresh();

    }

    public Collection<ShapeRecord> getData() {
        return data;
    }

    public int countPublished() {
        int ret = 0;
        for (ShapeRecord sr : data) {
            if (sr.publish) {
                ret++;
            }
        }
        return ret;
    }
}
