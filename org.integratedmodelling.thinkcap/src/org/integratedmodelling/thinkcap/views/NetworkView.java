/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.integratedmodelling.common.visualization.VisualizationFactory;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.NetworkEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.ui.GraphBrowser;

public class NetworkView extends ViewPart implements IThinklabEventListener {

    public static final String ID      = "org.integratedmodelling.thinkcap.views.NetworkView"; //$NON-NLS-1$
    private final FormToolkit  toolkit = new FormToolkit(Display.getCurrent());
    GraphBrowser               browser;

    public NetworkView() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new GridLayout(1, false));
        {
            SashForm sashForm = new SashForm(container, SWT.NONE);
            sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            toolkit.adapt(sashForm);
            toolkit.paintBordersFor(sashForm);
            {
                Composite composite = new Composite(sashForm, SWT.NONE);
                toolkit.adapt(composite);
                toolkit.paintBordersFor(composite);
            }

            browser = new GraphBrowser(sashForm, SWT.NONE);
            toolkit.adapt(browser);
            toolkit.paintBordersFor(browser);
            sashForm.setWeights(new int[] { 0, 1 });
        }

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    public void showNetwork() {

        Display.getDefault().syncExec(new Runnable() {
            @Override
            public void run() {
                browser.show(VisualizationFactory.visualizeNetwork());
            }
        });
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {
        // TODO Auto-generated method stub
        if (event instanceof NetworkEvent) {
            showNetwork();
        }
    }

}
