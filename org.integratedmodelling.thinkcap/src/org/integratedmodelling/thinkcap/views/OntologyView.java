/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.GridLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.visualization.VisualizationFactory;
import org.integratedmodelling.common.visualization.knowledge.KnowledgeGraph;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.OntologyFocusEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;

public class OntologyView extends ViewPart implements IThinklabEventListener {

    public static final String ID           = "org.integratedmodelling.thinkcap.views.OntologyView"; //$NON-NLS-1$
    private final FormToolkit  toolkit      = new FormToolkit(Display.getCurrent());
    private GraphViewer        graphViewer;
    private Table              table;
    private SashForm           sashForm;
    private TableViewer        tableViewer;
    private Group              composite_1;
    private int                currentDepth = 2;
    private Object             current      = null;

    private final static int MAX_DEPTH = 6;

    private final static int LAYOUT_SPRING = 0;
    private final static int LAYOUT_GRID   = 1;
    private final static int LAYOUT_HTREE  = 2;
    private final static int LAYOUT_VTREE  = 3;
    private final static int LAYOUT_RADIAL = 4;

    public OntologyView() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    class PropertyLabelProvider extends LabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            @SuppressWarnings("unchecked")
            Pair<String, Object> td = (Pair<String, Object>) element;

            if (columnIndex == 0) {
                return td.getFirst();
            } else if (columnIndex == 1) {
                return td.getSecond().toString();
            }
            return null;
        }
    }

    public class OntologyLabelProvider extends LabelProvider implements ISelfStyleProvider {
        @Override
        public String getText(Object element) {
            return element.toString();
        }

        @Override
        public Image getImage(Object element) {
            if (element instanceof IConcept) {
                return Eclipse.getConceptImage((IConcept) element, NS.isNothing((IConcept) element));
            }
            return null;
        }

        @Override
        public void selfStyleConnection(Object element, GraphConnection connection) {
        }

        @Override
        public void selfStyleNode(Object element, GraphNode node) {
        }
    }

    public class OntologyNodeContentProvider implements IGraphContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            if (inputElement instanceof KnowledgeGraph) {
                return ((KnowledgeGraph) inputElement).edgeSet().toArray();
            }
            return null;
        }

        @Override
        public Object getSource(Object rel) {
            if (rel instanceof KnowledgeGraph.Property) {
                return ((KnowledgeGraph.Property) rel).getSourceObject();
            }
            return null;
        }

        @Override
        public Object getDestination(Object rel) {
            if (rel instanceof KnowledgeGraph.Property) {
                return ((KnowledgeGraph.Property) rel).getTargetObject();
            }
            return null;
        }

    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {

        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new FillLayout(SWT.HORIZONTAL));

        sashForm = new SashForm(container, SWT.VERTICAL);
        toolkit.adapt(sashForm);
        toolkit.paintBordersFor(sashForm);

        Composite composite = new Composite(sashForm, SWT.NONE);
        toolkit.adapt(composite);
        toolkit.paintBordersFor(composite);
        composite.setLayout(new GridLayout(1, false));

        ToolBar toolBar = new ToolBar(composite, SWT.FLAT | SWT.RIGHT);
        toolBar.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
        toolkit.adapt(toolBar);
        toolkit.paintBordersFor(toolBar);

        ToolItem tltmRadioItem_1 = new ToolItem(toolBar, SWT.RADIO);
        tltmRadioItem_1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setLayoutEngine(LAYOUT_RADIAL);
            }
        });
        tltmRadioItem_1.setToolTipText("Radial layout");
        tltmRadioItem_1.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/radial_layout.gif"));

        ToolItem tltmRadioItem = new ToolItem(toolBar, SWT.RADIO);
        tltmRadioItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setLayoutEngine(LAYOUT_GRID);
            }
        });
        tltmRadioItem.setToolTipText("Grid layout");
        tltmRadioItem.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/grid_layout.gif"));

        ToolItem tltmNewItem_4 = new ToolItem(toolBar, SWT.RADIO);
        tltmNewItem_4.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setLayoutEngine(LAYOUT_SPRING);
            }
        });
        tltmNewItem_4.setToolTipText("Spring layout");
        tltmNewItem_4.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/spring_layout.gif"));

        ToolItem tltmNewItem_3 = new ToolItem(toolBar, SWT.RADIO);
        tltmNewItem_3.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setLayoutEngine(LAYOUT_VTREE);
            }
        });
        tltmNewItem_3.setToolTipText("Tree layout");
        tltmNewItem_3.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/tree_layout.gif"));

        ToolItem tltmNewItem_2 = new ToolItem(toolBar, SWT.RADIO);
        tltmNewItem_2.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                setLayoutEngine(LAYOUT_HTREE);
            }
        });
        tltmNewItem_2.setToolTipText("Horizontal tree layout");
        tltmNewItem_2.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/horizontal_tree_layout.gif"));

        ToolItem toolItem = new ToolItem(toolBar, SWT.SEPARATOR);

        ToolItem tltmNewItem_1 = new ToolItem(toolBar, SWT.NONE);
        tltmNewItem_1.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (current != null && currentDepth > 1) {
                    currentDepth--;
                    layout();
                }
            }
        });
        tltmNewItem_1.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/remove.gif"));

        ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
        tltmNewItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (current != null && currentDepth < MAX_DEPTH) {
                    currentDepth++;
                    layout();
                }

            }
        });
        tltmNewItem.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/add.gif"));

        graphViewer = new GraphViewer(composite, SWT.NONE);
        Control control = graphViewer.getControl();
        control.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
        control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        graphViewer.setLabelProvider(new OntologyLabelProvider());
        graphViewer.setContentProvider(new OntologyNodeContentProvider());
        graphViewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
        graphViewer.setNodeStyle(ZestStyles.NODES_NO_LAYOUT_RESIZE | ZestStyles.NODES_FISHEYE);
        setLayoutEngine(LAYOUT_SPRING);

        graphViewer.addDoubleClickListener(new IDoubleClickListener() {

            @Override
            public void doubleClick(DoubleClickEvent event) {
                Object o = ((IStructuredSelection) (event.getSelection())).getFirstElement();
                if (o != null) {
                    current = o;
                    layout();
                }
            }

        });

        graphViewer.addSelectionChangedListener(new ISelectionChangedListener() {

            @Override
            public void selectionChanged(SelectionChangedEvent event) {
                Object o = ((IStructuredSelection) (event.getSelection())).getFirstElement();
                showProperties(o);
            }

        });

        composite_1 = new Group(sashForm, SWT.NONE);
        toolkit.adapt(composite_1);
        toolkit.paintBordersFor(composite_1);
        composite_1.setLayout(new GridLayout(1, false));

        tableViewer = new TableViewer(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
        table = tableViewer.getTable();
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        toolkit.paintBordersFor(table);

        TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn propertyColumn = tableViewerColumn.getColumn();
        propertyColumn.setWidth(161);
        propertyColumn.setText("Property");

        TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
        TableColumn valueColumn = tableViewerColumn_1.getColumn();
        valueColumn.setWidth(600);
        valueColumn.setText("Value");

        tableViewer.setContentProvider(ArrayContentProvider.getInstance());
        tableViewer.setLabelProvider(new PropertyLabelProvider());

        sashForm.setWeights(new int[] { 7, 1 });

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    protected void showProperties(final Object o) {

        if (o != null) {
            Display.getDefault().syncExec(new Runnable() {

                @Override
                public void run() {
                    composite_1.setText("Metadata of " + o.toString());
                    sashForm.setWeights(new int[] { 7, 1 });
                    tableViewer.setInput(getMetadata(o));
                }
            });
        }
    }

    private List<Pair<String, Object>> getMetadata(Object o) {
        List<Pair<String, Object>> ret = new ArrayList<Pair<String, Object>>();

        if (o instanceof IConcept) {
            IMetadata md = ((IConcept) o).getMetadata();
            for (String key : md.getKeys()) {
                ret.add(new Pair<String, Object>(key, md.get(key)));
            }
        }

        return ret;
    }

    private void setLayoutEngine(int lcode) {

        LayoutAlgorithm layout = null;

        switch (lcode) {
        case LAYOUT_SPRING:
            layout = new SpringLayoutAlgorithm();
            break;
        case LAYOUT_VTREE:
            layout = new TreeLayoutAlgorithm();
            break;
        case LAYOUT_GRID:
            layout = new GridLayoutAlgorithm();
            break;
        case LAYOUT_HTREE:
            layout = new TreeLayoutAlgorithm();
            break;
        case LAYOUT_RADIAL:
            layout = new RadialLayoutAlgorithm();
            break;
        }

        graphViewer.setLayoutAlgorithm(layout, true);
        graphViewer.applyLayout();
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {
        if (event instanceof OntologyFocusEvent) {
            this.current = ((OntologyFocusEvent) event).focus;
            layout();
        }

    }

    private void layout() {

        if (this.current == null) {
            return;
        }

        /*
         * obtain the knowledge graph from this element, passing the current options
         */
        final KnowledgeGraph graph = VisualizationFactory
                .getKnowledgeGraph(this.current, currentDepth, KnowledgeGraph.ISA_PROPERTY);

        /*
         * display the graph if not null
         */
        if (graph != null) {
            Display.getDefault().syncExec(new Runnable() {

                @Override
                public void run() {
                    graphViewer.setInput(graph);
                }
            });
        }
    }
}
