package org.integratedmodelling.thinkcap.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.events.IExpansionListener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.client.palette.Palette;
import org.integratedmodelling.common.client.palette.PaletteFolder;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.data.DataManager;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.ui.Badge;
import org.integratedmodelling.thinkcap.ui.ObservationBox;
import org.integratedmodelling.thinkcap.ui.PaletteFolderViewer;

public class PaletteView extends ViewPart {

    public static final String ID                   = "org.integratedmodelling.thinkcap.views.PaletteView"; //$NON-NLS-1$
    private final FormToolkit  toolkit              = new FormToolkit(Display.getCurrent());

    Palette                    palette              = null;
    private Composite          container;
    private ObservationBox     searchBox;
    private Action             searchAction;
    private Set<PaletteFolder> currentlyExpandedSet = new HashSet<>();

    class ItemAction {

        String action;
        String target;

        public ItemAction(String s) {
            int n = s.indexOf(' ');
            this.action = s.substring(0, n).trim();
            this.target = s.substring(n).trim();
        }

        public void execute(boolean enabled) {
            switch (action) {
            case "show":
                // locate folder in target and set its visible status, then redraw
                PaletteFolder folder = getFolder(target);
                folder.setStatus(enabled ? "open" : "hidden");
                if (enabled) {
                    currentlyExpandedSet.add(folder);
                } else {
                    currentlyExpandedSet.remove(folder);
                }
                break;
            case "expand":
                // locate item in target and set its expanded status, then redraw folder
                break;
            }
        }

    }

    public PaletteView() {
    }

    private PaletteFolder getFolder(String target2) {
        for (PaletteFolder f : palette.getFolders()) {
            if (f.getName().equals(target2)) {
                return f;
            }
        }
        return null;
    }

    /**
     * Create contents of the view part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        {
            Composite composite = new Composite(parent, SWT.NONE);
            toolkit.adapt(composite);
            toolkit.paintBordersFor(composite);
            GridLayout gl_composite = new GridLayout(1, false);
            gl_composite.marginBottom = 5;
            gl_composite.marginHeight = 0;
            gl_composite.verticalSpacing = 0;
            gl_composite.horizontalSpacing = 0;
            composite.setLayout(gl_composite);

            ScrolledComposite scroll = new ScrolledComposite(composite, SWT.V_SCROLL);
            scroll.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
            scroll.setExpandVertical(true);
            scroll.setExpandHorizontal(true);
            this.container = toolkit.createComposite(scroll, SWT.NONE);
            // toolkit.paintBordersFor(container);
            container.setLayout(new GridLayout(1, false));
            {
                DropTarget dropTarget = new DropTarget(container, DND.DROP_MOVE);
                dropTarget.setTransfer(new Transfer[] {
                        FileTransfer.getInstance() });
                dropTarget.addDropListener(new DropTargetAdapter() {

                    @Override
                    public void drop(DropTargetEvent event) {
                        // file drop.
                        if (event.data instanceof String[] && ((String[]) event.data).length > 0) {
                            List<File> files = new ArrayList<>();
                            for (String f : ((String[]) event.data)) {
                                files.add(new File(f));
                            }
                            acceptFiles(files);
                        }
                    }

                });
            }

            // TODO adding this it does scroll, but not intelligently. Without this, no
            // scrolling happens
            Rectangle rr = parent.getClientArea();
            scroll.setMinSize(parent.computeSize(rr.width, rr.height));
            scroll.setContent(container);
            {
                Composite toolArea = new Composite(composite, SWT.NONE);
                toolArea.setBackground(SWTResourceManager
                        .getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
                GridData gd_toolArea = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
                gd_toolArea.heightHint = 30;
                toolArea.setLayoutData(gd_toolArea);
                // toolkit.adapt(toolArea);
                // toolkit.paintBordersFor(toolArea);
            }
            scroll.addControlListener(new ControlAdapter() {
                @Override
                public void controlResized(ControlEvent e) {
                    Rectangle r = scroll.getClientArea();
                    scroll.setMinSize(parent.computeSize(r.width, r.height));
                }
            });
        }

        if (PaletteManager.get().getPalettes().size() > 0) {
            this.palette = PaletteManager.get().getPalettes().get(0);
            draw();
        }

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    protected void acceptFiles(List<File> files) {

        /*
         * the drop should only have one main file. Refuse multiple files unless they're
         * related - for now, just check the file name is the same for all files.
         */
        List<Pair<File, Collection<File>>> payload = DataManager.arrangeFiles(files);
        if (payload.isEmpty()) {
            return;
        }
        if (payload.get(0).getFirst().equals(DataManager.UNKNOWN_FILE)) {
            Eclipse.alert("One or more files were not recognized as data. Please drop supported data files only.");
            return;
        }

        if (payload.size() > 1) {
            Eclipse.alert("Import of " + payload.size()
                    + " datasets requested. Please import one main dataset at a time.");
            return;
        }

        /*
         * TODO do it. This, if successful, should ultimately generate the .k sidecar file
         * and insert the file in both the toolkit UI and the toolkit namespace.
         */
        Eclipse.alert("Import of dataset " + MiscUtilities.getFileName(payload.get(0).getFirst())
                + " requested. Please wait until this function is implemented.");

    }

    public void draw() {

        /*
         * remove everything
         */
        for (Control control : container.getChildren()) {
            control.dispose();
        }

        this.setPartName(palette.getName());
        if (palette.getIcon() != null) {
            this.setTitleImage(ResourceManager.getPluginImage(Activator.PLUGIN_ID, palette.getIcon()));
        }

        /*
         * add invisible search box. TODO the box itself is unimplemented.
         */
        searchBox = new ObservationBox(container, this, SWT.NONE) {

            @Override
            protected Composite showResultArea(boolean show) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            protected void cancelSearch() {
                searchAction.setChecked(false);
            }
        };
        searchBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        ((GridData) searchBox.getLayoutData()).exclude = true;
        searchBox.setVisible(false);

        /*
         * draw title and description
         */
        CLabel titleLabel = new CLabel(container, SWT.NONE);
        titleLabel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NONE));
        titleLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        toolkit.adapt(titleLabel);
        // toolkit.paintBordersFor(titleLabel);
        titleLabel.setText(palette.getName() + " toolkit");

        if (palette.getDescription() != null && !palette.getDescription().trim().isEmpty()) {
            Badge descLabel = new Badge(container, Badge.CLOSEABLE
                    | Badge.MULTILINE | Badge.ROUNDED, SWT.NONE) {
                @Override
                protected void close() {
                    palette.setDescription(null);
                    draw();
                }
            };
            descLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GRAY));
            descLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
            descLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
            descLabel.setText(StringUtils.justifyLeft(palette.getDescription(), 70));
        }

        /*
         * for each section add a section widget
         */
        for (PaletteFolder folder : palette.getFolders()) {

            if (folder.getStatus().equals("hidden")) {
                continue;
            }

            Section folderSection = toolkit.createSection(container, Section.TWISTIE);
            folderSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
            folderSection.setText(folder.getName());
            folderSection.setClient(new PaletteFolderViewer(folderSection, this, folder));
            folderSection.setSeparatorControl(new Label(folderSection, SWT.HORIZONTAL | SWT.SEPARATOR));
            if (folder.getStatus() != null) {
                switch (folder.getStatus()) {
                case "closed":
                    folderSection.setExpanded(false);
                    break;
                case "open":
                    folderSection.setExpanded(true);
                    break;
                }
            }
            folderSection.addExpansionListener(new IExpansionListener() {

                @Override
                public void expansionStateChanging(ExpansionEvent e) {
                }

                @Override
                public void expansionStateChanged(ExpansionEvent e) {
                    folder.setStatus(folderSection.isExpanded() ? "open" : "closed");
                }
            });
            toolkit.paintBordersFor(folderSection);
        }
    }

    @Override
    public void dispose() {
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
        {
            searchAction = new Action("", Action.AS_CHECK_BOX) {

                @Override
                public void run() {
                    searchMode(this.isChecked());
                }

            };
            searchAction.setToolTipText("Search for tools to add");
            searchAction.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Search.png"));
        }
    }

    private void searchMode(boolean on) {
        ((GridData) searchBox.getLayoutData()).exclude = !on;
        searchBox.setVisible(on);
        searchBox.getParent().pack();
        if (on) {
            searchBox.searchMode(true);
        }
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
        tbm.add(searchAction);
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
        for (Palette palette : PaletteManager.get().getPalettes()) {
            manager.add(new Action(palette.getName()) {

                @Override
                public void run() {
                    PaletteView.this.palette = palette;
                    draw();
                }
            });
        }

        manager.add(new Separator("Toolkit operations"));

        manager.add(new Action("New toolkit...") {
        });

        manager.add(new Action("Add toolbox...") {
        });

        manager.add(new Separator("Save/restore"));

        manager.add(new Action("Save toolkit") {
        });

        manager.add(new Action("Reset toolkit") {

        });

        manager.add(new Action("Save as new toolkit...") {
        });

        manager.add(new Action("Delete current") {
        });
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    public void notifySelection(PaletteItem item, PaletteFolder folder, boolean enabled, boolean shiftPressed) {
        // TODO Auto-generated method stub
        draw();
    }

    public void notifyExpansion(PaletteItem item, PaletteFolder folder, boolean enabled, boolean shiftPressed) {
        if (item.getOnSelect() != null || item.getChildren().size() > 0) {
            if (!shiftPressed) {
                for (PaletteFolder expanded : currentlyExpandedSet) {
                    expanded.setStatus("hidden");
                }
                deactivateOtherItems(item);
            }
            for (ItemAction a : parseActions(item)) {
                a.execute(enabled);
            }
        }
        draw();
    }

    private void deactivateOtherItems(PaletteItem item) {
        for (PaletteFolder folder : palette.getFolders()) {
            for (PaletteItem it : folder.getItems()) {
                if (it.isActive() && !item.equals(it)) {
                    it.setActive(false);
                }
            }
        }
    }

    private List<ItemAction> parseActions(PaletteItem item) {
        List<ItemAction> ret = new ArrayList<>();
        if (item.getChildren().size() > 0) {
            ret.add(new ItemAction("expand " + item.getName()));
        }
        if (item.getOnSelect() != null) {
            String[] acs = item.getOnSelect().split(";");
            for (String s : acs) {
                ret.add(new ItemAction(s));
            }
        }
        return ret;
    }
}
