/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.ITask.Status;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.runtime.Dataflow;
import org.integratedmodelling.common.client.runtime.Resolution;
import org.integratedmodelling.common.monitoring.Notifiable;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.ContextEvent;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.IntrospectionEvent;
import org.integratedmodelling.thinkcap.events.SessionEvent;
import org.integratedmodelling.thinkcap.events.TaskEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.ui.GraphBrowser;

public class TaskView extends ViewPart implements IThinklabEventListener {

	public static final String ID = "org.integratedmodelling.thinkcap.views.ProvenanceView"; //$NON-NLS-1$
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	GraphVisualization pGraph = null;
	GraphVisualization wGraph = null;

	private Label statusImage;
	private ProgressBar progressBar;
	private Label interruptTask;
	private TableViewer treeViewer;
	private Label titleLabel;
	private Label commentLabel;
	private GraphBrowser resolutionBrowser;
	private GraphBrowser workflowBrowser;

	private Action copyItemAction;

	public TaskView() {
		Activator.getDefault().addThinklabEventListener(this);
	}

	class TaskLabelProvider extends LabelProvider implements ITableLabelProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (columnIndex == 0) {
				if (element instanceof ICompileError) {
					return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/CancelSmall.png");
				} else if (element instanceof ICompileWarning) {
					return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/AlertAlt2.png");
				} else if (element instanceof ICompileInfo) {
					return ResourceManager.getPluginImage(Activator.PLUGIN_ID,
							Eclipse.getNotificationIcon(((ICompileInfo) element).getCategory()));
				}
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (columnIndex == 1)
				return ((ICompileNotification) element).getMessage();
			return null;
		}
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = toolkit.createComposite(parent, SWT.NONE);
		toolkit.paintBordersFor(container);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		{
			TabFolder tabFolder = new TabFolder(container, SWT.NONE);
			toolkit.adapt(tabFolder);
			toolkit.paintBordersFor(tabFolder);
			{
				TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
				tbtmNewItem.setText("Task progress");
				{
					Composite composite = new Composite(tabFolder, SWT.NONE);
					tbtmNewItem.setControl(composite);
					toolkit.paintBordersFor(composite);
					composite.setLayout(new GridLayout(1, false));
					{
						Composite composite_1 = new Composite(composite, SWT.NONE);
						composite_1.setLayout(new GridLayout(3, false));
						composite_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
						toolkit.adapt(composite_1);
						toolkit.paintBordersFor(composite_1);

						statusImage = new Label(composite_1, SWT.NONE);
						GridData gd_statusImage = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 3);
						gd_statusImage.widthHint = 48;
						gd_statusImage.heightHint = 48;
						gd_statusImage.minimumWidth = 48;
						gd_statusImage.minimumHeight = 48;
						statusImage.setLayoutData(gd_statusImage);
						toolkit.adapt(statusImage, true, true);

						titleLabel = new Label(composite_1, SWT.NONE);
						titleLabel.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.BOLD));
						titleLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
						toolkit.adapt(titleLabel, true, true);
						titleLabel.setText("No task being executed.");
						new Label(composite_1, SWT.NONE);

						commentLabel = new Label(composite_1, SWT.NONE);
						commentLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
						commentLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
						toolkit.adapt(commentLabel, true, true);
						commentLabel.setText("Define a context to start.");
						new Label(composite_1, SWT.NONE);

						progressBar = new ProgressBar(composite_1, SWT.SMOOTH | SWT.INDETERMINATE);
						progressBar.setEnabled(false);
						progressBar.setVisible(false);
						GridData gd_progressBar = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
						gd_progressBar.heightHint = 4;
						progressBar.setLayoutData(gd_progressBar);
						toolkit.adapt(progressBar, true, true);

						interruptTask = new Label(composite_1, SWT.NONE);
						interruptTask.setToolTipText("Interrupt the current task");
						interruptTask.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseUp(MouseEvent e) {
								Environment.get().getFocusTask().interrupt();
							}
						});
						interruptTask.setImage(
								ResourceManager.getPluginImage("org.integratedmodelling.thinkcap", "icons/stop.gif"));
						toolkit.adapt(interruptTask, true, true);
						interruptTask.setVisible(false);
					}

					treeViewer = new TableViewer(composite, SWT.BORDER);
					final Table tree = treeViewer.getTable();
					tree.setLinesVisible(true);
					tree.setHeaderVisible(true);
					tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
					toolkit.paintBordersFor(tree);


					TableViewerColumn tableViewerColumn = new TableViewerColumn(treeViewer, SWT.NONE);
					TableColumn tblclmnNewColumn = tableViewerColumn.getColumn();
					tblclmnNewColumn.setAlignment(SWT.CENTER);
					tblclmnNewColumn.setMoveable(true);
					tblclmnNewColumn.setWidth(20);

					TableViewerColumn treeViewerColumn = new TableViewerColumn(treeViewer, SWT.NONE);
					TableColumn trclmnNewColumn = treeViewerColumn.getColumn();
					trclmnNewColumn.setWidth(360);
					trclmnNewColumn.setText("Notification");

					TableViewerColumn treeViewerColumn_1 = new TableViewerColumn(treeViewer, SWT.NONE);
					TableColumn trclmnNewColumn_1 = treeViewerColumn_1.getColumn();
					trclmnNewColumn_1.setWidth(100);
					trclmnNewColumn_1.setText("Time");

					treeViewer.setContentProvider(ArrayContentProvider.getInstance());
					treeViewer.setLabelProvider(new TaskLabelProvider());
	                treeViewer.addDoubleClickListener(new IDoubleClickListener() {

	                        @Override
	                        public void doubleClick(DoubleClickEvent event) {
	                            /*
	                             * open info dialog with the current selection
	                             */
	                            Object o = ((IStructuredSelection) (event.getSelection())).getFirstElement();
	                            if (o instanceof ICompileInfo) {
	                                Eclipse.info(((ICompileNotification)o).getMessage());
	                            } else if (o instanceof ICompileWarning) {
	                                Eclipse.warning(((ICompileNotification)o).getMessage());
	                            } else if (o instanceof ICompileError) {
	                                Eclipse.alert(((ICompileNotification)o).getMessage());
	                            }
	                        }
	                    });
				}
			}
			{
				TabItem provenanceTab = new TabItem(tabFolder, SWT.NONE);
				provenanceTab.setText("Resolution graph");
				{
					resolutionBrowser = new GraphBrowser(tabFolder, SWT.NONE);
					provenanceTab.setControl(resolutionBrowser);
					toolkit.paintBordersFor(resolutionBrowser);
				}
			}
			{
				TabItem workflowTab = new TabItem(tabFolder, SWT.NONE);
				workflowTab.setText("Dataflow");
				{
					workflowBrowser = new GraphBrowser(tabFolder, SWT.NONE);
					workflowTab.setControl(workflowBrowser);
					toolkit.paintBordersFor(workflowBrowser);
				}
			}
		}

		createActions();
		hookGlobalActions();
		initializeToolBar();
		initializeMenu();
	}

	@Override
	public void dispose() {
		Activator.getDefault().removeThinklabEventListener(this);
		toolkit.dispose();
		super.dispose();
	}

	private void hookGlobalActions() {
		IActionBars bars = getViewSite().getActionBars();
		bars.setGlobalActionHandler(ActionFactory.COPY.getId(), copyItemAction);
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions

		copyItemAction = new Action("Copy...") {
			@Override
			public void run() {
				StringBuilder sb = new StringBuilder();
				for (org.eclipse.swt.widgets.TableItem ti : treeViewer.getTable().getItems()) {
					Notification notification = (Notification) ti.getData();
					sb.append(notification.getBody() + "\n");
				}

				Clipboard clipboard = new Clipboard(Display.getCurrent());
				TextTransfer textTransfer = TextTransfer.getInstance();
				Transfer[] transfers = new Transfer[] { textTransfer };
				Object[] data = new Object[] { sb.toString() };
				clipboard.setContents(data, transfers);
				clipboard.dispose();
			}
		};

	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager manager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	@Override
	public void handleThinklabEvent(ThinklabEvent event) {

		if (event instanceof TaskEvent) {

			TaskEvent teven = (TaskEvent) event;
			final ITask task = ((TaskEvent) event).task;
			if (teven.type == TaskEvent.FOCUS) {
				setContext(task.getContext());
				setTask(task);
			} else if (task.getContext().equals(Environment.get().getContext())) {
				setTask(task);
			}

		} else if (event instanceof IntrospectionEvent) {
			rescan();
		} else if (event instanceof EngineEvent) {

			if (Activator.engine().getCurrentEngine() != null
					&& Activator.engine().getCurrentEngine().equals(((EngineEvent) event).engine)) {
				if (((EngineEvent) event).type == EngineEvent.OFFLINE) {
					setTask(null);
				}
			}

		} else if (event instanceof SessionEvent) {

			SessionEvent seven = (SessionEvent) event;

			// if (seven.type == SessionEvent.CLOSED) {
			setTask(null);
			// } else {
			// /*
			// * FOCUS or CREATED: TODO load most recent context? Probably not -
			// just in
			// * chooser Context view
			// */
			// }

		} else if (event instanceof ContextEvent) {

			ContextEvent ceven = (ContextEvent) event;
			if (ceven.type == ContextEvent.NEW || ceven.type == ContextEvent.FOCUS) {
				setContext(ceven.context);
			}
		}
	}

	private void setContext(IContext context) {
		setTask(null);
	}

	public void setTask(ITask task) {
		pGraph = null;
		wGraph = null;
		if (task != null) {
			if (task.getDataflow() instanceof Dataflow) {
				wGraph = ((Dataflow) task.getDataflow()).visualize();
			}
			if (task.getResolution() instanceof Resolution) {
				pGraph = ((Resolution) task.getResolution()).visualize();
			}
		}
		rescan();
	}

	private void rescan() {

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				if (Environment.get().getFocusTask() != null) {

					/*
					 * reset event list
					 */
					treeViewer.setInput(((Notifiable) Environment.get().getFocusTask()).getNotifications());

					/*
					 * set progress bar and controls according to status
					 */
					if (Environment.get().getFocusTask().getStatus() == ITask.Status.FINISHED) {
						progressBar.setVisible(false);
						interruptTask.setVisible(false);
					} else {
						progressBar.setVisible(true);
						interruptTask.setVisible(true);
					}

					/*
					 * labels
					 */
					String description = Environment.get().getFocusTask().getDescription();
					if (description == null) {
						description = "";
					}
					titleLabel.setText(description);

					/*
					 * set main status display
					 */
					Image simg = null;

					switch (Environment.get().getFocusTask().getStatus()) {
					case FINISHED:
						simg = ResourceManager.getPluginImage("org.integratedmodelling.thinkcap",
								"icons/status-finished.png");
						break;
					case INTERRUPTED:
					case ERROR:
						simg = ResourceManager.getPluginImage("org.integratedmodelling.thinkcap",
								"icons/status-error.png");
						break;
					case INITIALIZING:
					case RUNNING:
					case QUEUED:
						simg = ResourceManager.getPluginImage("org.integratedmodelling.thinkcap",
								"icons/status-wait.png");
					}

					statusImage.setImage(simg);
					statusImage.setVisible(true);

					commentLabel.setText(Environment.get().getContext().isFinished() ? "Context is finalized."
							: (Environment.get().getFocusTask().getStatus() == Status.FINISHED
									? "Ready for further observations" : "Please wait for task to complete"));

				} else {
					titleLabel.setText("No task being executed.");
					commentLabel.setText(Environment.get().getContext() == null ? "Define a context to start."
							: "Ready for further observations");
					statusImage.setVisible(false);
					progressBar.setVisible(false);
					treeViewer.setInput(null);
				}

				workflowBrowser.show(wGraph);
				resolutionBrowser.show(pGraph);

			}
		});

	}
}
