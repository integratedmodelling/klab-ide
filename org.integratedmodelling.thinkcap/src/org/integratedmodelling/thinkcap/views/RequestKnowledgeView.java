/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.SWTResourceManager;

public class RequestKnowledgeView extends ViewPart {

	public static final String ID = "org.integratedmodelling.thinkcap.views.RequestKnowledgeView"; //$NON-NLS-1$
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text conceptId;
	private Text descriptionArea;
	private Text text;
	private Table table;

	public RequestKnowledgeView() {
	}

	public class RelationshipEditingSupport extends EditingSupport {

		ColumnViewer viewer = null;
		
		public RelationshipEditingSupport(ColumnViewer viewer) {
			super(viewer);
			this.viewer = viewer;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return element.toString();
		}

		@Override
		protected void setValue(Object element, Object value) {
			viewer.update(element, null);
		}
		
	}
	
	public class ConceptEditingSupport extends EditingSupport {

		TableViewer viewer = null;		

		public ConceptEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return new TextCellEditor(viewer.getTable());		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return element.toString();
		}

		@Override
		protected void setValue(Object element, Object value) {
			viewer.update(element, null);
		}
		
	}
	
	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = toolkit.createComposite(parent, SWT.NONE);
		toolkit.paintBordersFor(container);
		container.setLayout(new GridLayout(2, false));
		{
			Label lblConceptId = new Label(container, SWT.NONE);
			toolkit.adapt(lblConceptId, true, true);
			lblConceptId.setText("Concept ID");
		}
		{
			StyledText styledText = new StyledText(container, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP);
			styledText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
			GridData gd_styledText = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4);
			gd_styledText.minimumWidth = 180;
			styledText.setLayoutData(gd_styledText);
			toolkit.adapt(styledText);
			toolkit.paintBordersFor(styledText);
		}
		{
			conceptId = new Text(container, SWT.BORDER);
			conceptId.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			toolkit.adapt(conceptId, true, true);
		}
		{
			Label lblDescription = new Label(container, SWT.NONE);
			toolkit.adapt(lblDescription, true, true);
			lblDescription.setText("Description");
		}
		{
			descriptionArea = new Text(container, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
			GridData gd_descriptionArea = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
			gd_descriptionArea.minimumWidth = 420;
			gd_descriptionArea.heightHint = 222;
			gd_descriptionArea.minimumHeight = 280;
			descriptionArea.setLayoutData(gd_descriptionArea);
			toolkit.adapt(descriptionArea, true, true);
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			Label lblClosestNamespace = new Label(container, SWT.NONE);
			lblClosestNamespace.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
			toolkit.adapt(lblClosestNamespace, true, true);
			lblClosestNamespace.setText("Closest Namespace");
		}
		{
			text = new Text(container, SWT.BORDER);
			text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			toolkit.adapt(text, true, true);
		}
		{
			Label lblClosestConcepts = new Label(container, SWT.NONE);
			lblClosestConcepts.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			toolkit.adapt(lblClosestConcepts, true, true);
			lblClosestConcepts.setText("Closest Concepts");
		}
		{
			TableViewer tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION);
			table = tableViewer.getTable();
			table.setLinesVisible(true);
			GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
			gd_table.minimumHeight = 90;
			table.setLayoutData(gd_table);
			toolkit.paintBordersFor(table);
			{
				TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				TableColumn tblclmnConcept = tableViewerColumn.getColumn();
				tblclmnConcept.setWidth(300);
				tblclmnConcept.setText("Concept");
				tableViewerColumn.setEditingSupport(new ConceptEditingSupport(tableViewer));
			}
			{
				TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
				TableColumn tblclmnRelationship = tableViewerColumn.getColumn();
				tblclmnRelationship.setWidth(500);
				tblclmnRelationship.setText("Relationship");
				tableViewerColumn.setEditingSupport(new RelationshipEditingSupport(tableViewer));
			}
		}
		{
			Composite composite = new Composite(container, SWT.NONE);
			composite.setLayout(new GridLayout(3, false));
			composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
			toolkit.adapt(composite);
			toolkit.paintBordersFor(composite);
			
			Label lblNewLabel = new Label(composite, SWT.NONE);
			lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			toolkit.adapt(lblNewLabel, true, true);
			lblNewLabel.setText("(undefined)");
			
			Button btnNewButton = new Button(composite, SWT.NONE);
			toolkit.adapt(btnNewButton, true, true);
			btnNewButton.setText("Submit Request");
			
			Button btnNewButton_1 = new Button(composite, SWT.NONE);
			toolkit.adapt(btnNewButton_1, true, true);
			btnNewButton_1.setText("Cancel Request");
		}
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		{
			Label lblProvisionalConceptTo = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
			lblProvisionalConceptTo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
			lblProvisionalConceptTo.setText("Provisional concept to use");
			toolkit.adapt(lblProvisionalConceptTo, true, true);
		}

		createActions();
		initializeToolBar();
		initializeMenu();
	}

	public void dispose() {
		toolkit.dispose();
		super.dispose();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager manager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}
}
