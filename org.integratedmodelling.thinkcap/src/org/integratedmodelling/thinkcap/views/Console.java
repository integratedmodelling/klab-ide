/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.VerifyKeyListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.client.IConsole;
import org.integratedmodelling.common.client.cli.CommandHistory;
import org.integratedmodelling.common.client.cli.CommandProcessor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.Eclipse.IActivatorListener;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;

/**
 * Command console with styles, history and easy API based on sucky beanshell thing and
 * redesigned with SWT StyledText.
 * 
 * 1. define command terminator (optionally tied to specific command prefix) 2. callback
 * to execute command 3. initialization and termination callback 4. break handler linked
 * to ctrl-C 5. autocompletion handler linked to ctrl-space
 * 
 * @author Ferd
 *
 */
public class Console extends ViewPart implements VerifyKeyListener, IThinklabEventListener, IConsole {

    public static final String ID                = "org.integratedmodelling.thinkcap.console.Console"; //$NON-NLS-1$

    private final FormToolkit  toolkit           = new FormToolkit(Display.getCurrent());

    private int                cmdStart          = 0;
    private String             currentCommand    = "";
    private String             currentTerminator = null;
    private StyledText         text;
    private int                histLine;
    private String             prompt            = "k#> ";
    private HashSet<String>    keywords          = null;

    private boolean            _running          = false;
    private boolean            _commandMode      = true;

    private CommandProcessor   commandProcessor;
    private CommandHistory     history;

    ToolItem                   startButton       = null;
    ToolItem                   stopButton        = null;
    ToolItem                   refreshButton     = null;
    ToolItem                   vacuumButton      = null;

    String                     ZEROS             = "000";
    private ToolItem           commandMode;

    private CommandListener    commandListener;

    private String             oldPrompt;

    private String             endCommand;

    private boolean            initialized;

    public Console() {
        Activator.getDefault().addThinklabEventListener(this);
        this.commandProcessor = new CommandProcessor(this, Activator.engine().getMonitor());
        this.history = new CommandHistory();
    }

    /**
     * Define what terminates a command. If null (default), enter sends commmand.
     * @param terminator
     */
    protected void setCommandTerminator(String terminator) {
        currentTerminator = terminator;
    }

    /**
     * Pass a collection of keywords if you want them highlighted as the user types.
     * 
     * @param keywords
     */
    protected void setKeywords(Collection<String> keywords) {

    }

    /**
     * Callback invoked on Ctrl-C
     */
    protected void interruptLastCommand() {

    }

    /**
     * Define the prompt (default empty string).
     * @param prompt
     */
    protected void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public void printStatusMessage() {

        println("ThinkLab command shell v" + Version.getCurrent());
        println("Workspace: " + KLAB.WORKSPACE.getDefaultFileLocation());
        println();
    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        toolkit.paintBordersFor(container);
        container.setLayout(new GridLayout(1, false));
        text = new StyledText(container, SWT.BORDER | SWT.WRAP | SWT.MULTI | SWT.V_SCROLL);
        text.addVerifyKeyListener(this);
        text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        toolkit.adapt(text);
        toolkit.paintBordersFor(text);

        /*
         * make it scroll to the end automatically
         */
        text.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                text.setTopIndex(text.getLineCount() - 1);

            }
        });

        Eclipse.addActivationListener(this, new IActivatorListener() {
            @Override
            public void onActivation() throws KlabException {
                initialize();
            }
        });
        
        createActions();
        initializeToolBar();
        initializeMenu();

        text.setEnabled(false);
    }

    @Override
    public void dispose() {
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        initialize();
        text.setFocus();
    }

    private synchronized void type(VerifyEvent e) {
        switch (e.keyCode) {
        case (SWT.CR):
            enter();
            resetCommandStart();
            text.setCaretOffset(cmdStart);
            consume(e);
            text.redraw();
            break;

        case (SWT.ARROW_UP):
            historyUp();
            consume(e);
            break;

        case (SWT.ARROW_DOWN):
            historyDown();
            consume(e);
            break;

        case (SWT.ARROW_LEFT):
        case (SWT.BS):
        case (SWT.DEL):
            if (text.getCaretOffset() <= cmdStart) {
                // This doesn't work for backspace.
                // See default case for workaround
                consume(e);
            }
            break;

        case (SWT.ARROW_RIGHT):
            forceCaretMoveToStart();
            break;

        case (SWT.HOME):
            text.setCaretOffset(cmdStart);
            consume(e);
            break;

        // case ( KeyEvent.VK_U ): // clear line
        // if ( ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
        // replaceRange( "", cmdStart, textLength());
        // histLine = 0;
        // e.consume();
        // }
        // break;

        case (SWT.ALT):
        case (SWT.CAPS_LOCK):
        case (SWT.CONTROL):
        case (SWT.SHIFT):
        case (SWT.PRINT_SCREEN):
        case (SWT.SCROLL_LOCK):
        case (SWT.INSERT):
        case (SWT.F1):
        case (SWT.F2):
        case (SWT.F3):
        case (SWT.F4):
        case (SWT.F5):
        case (SWT.F6):
        case (SWT.F7):
        case (SWT.F8):
        case (SWT.F9):
        case (SWT.F10):
        case (SWT.F11):
        case (SWT.F12):
        case (SWT.ESC):

            // only modifier pressed
            break;

        // TODO add Control-Space for completion

        // Control-C
        case ('c'):
            if (text.getSelectionText() == null) {
                if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
                    /*
                     * send interrupt hook
                     */
                    interruptLastCommand();
                    // append("^C");
                }
                consume(e);
            }
            break;

        case (SWT.TAB):

            String part = text.getText().substring(cmdStart);
            doCommandCompletion(part);
            consume(e);
            break;

        default:
            if ((e.stateMask & (SWT.CTRL | SWT.ALT)) == 0) {
                // plain character
                forceCaretMoveToEnd();
            }
            break;
        }
    }

    private void append(String string) {
        text.append(string);
    }

    private void resetCommandStart() {
        cmdStart = textLength();
    }

    private void enter() {

        String s = getCmd();
        append("\n");
        histLine = 0;

        /*
         * TODO if ends with current terminator, call acceptLine and print prompt. Else
         * add to current command.
         */
        if (currentTerminator == null || s.endsWith(currentTerminator)) {
            currentCommand += s;
            acceptLine(currentCommand);
            currentCommand = "";
            print(prompt);
        } else {
            currentCommand += s;
        }
        text.redraw();
    }

    private String getCmd() {
        int tl = textLength() - cmdStart - 1;
        return tl <= 0 ? "" : text.getText(cmdStart, cmdStart + tl);
    }

    private void acceptLine(String line) {

        /*
         * submit as command
         */
        if (_commandMode) {
            if (commandListener != null) {
                if (line.equals(endCommand)) {
                    endCommand = null;
                    commandListener = null;
                    setPrompt(oldPrompt);
                } else {
                    Object ret = commandListener.execute(line);
                    outputResult(line, ret);
                }
            } else if (commandProcessor != null) {
                commandProcessor.processCommand(line);
            }
        }
    }

    private void historyDown() {

        String text = history.getPrevious();

        if (text == null) {
            text = "";
        }
        if ((this.text.getText().length() - cmdStart) > 0) {
            this.text.replaceTextRange(cmdStart, this.text.getText().length() - cmdStart, "");
        }
        append(text);
        this.text.setCaretOffset(textLength());
    }

    private void historyUp() {
        String text = history.getNext();

        if (text == null) {
            text = "";
        }
        if ((this.text.getText().length() - cmdStart) > 0) {
            this.text.replaceTextRange(cmdStart, this.text.getText().length() - cmdStart, "");
        }
        append(text);
        this.text.setCaretOffset(textLength());
    }

    public void println(Object o) {
        String[] ss = o.toString().split("\\r?\\n");
        for (String s : ss) {
            print(s + "\n");
            text.redraw();
        }
    }

    public void print(final Object o) {

        Activator.getDefault().getWorkbench().getDisplay().syncExec(new Runnable() {
            @Override
            public void run() {
                append(String.valueOf(o));
                resetCommandStart();
                text.setCaretOffset(cmdStart);
            }
        });
    }

    /**
     * Prints "\\n" (i.e. newline)
     */
    public void println() {
        print("\n");
        text.redraw();
    }

    private int textLength() {
        String it = text.getText();
        return text.getText().length();
    }

    private String replaceRange(Object s, int start, int end) {

        String st = s.toString();
        // text.setSelection(start, end);
        text.replaceTextRange(start, st.length(), st);
        // text.repaint();
        return st;
    }

    private void consume(KeyEvent e) {
        e.doit = false;
    }

    private void doCommandCompletion(String part) {
        // TODO Auto-generated method stub
    }

    private void forceCaretMoveToEnd() {
        if (text.getCaretOffset() < cmdStart) {
            text.setCaretOffset(textLength());
        }
        text.redraw();
    }

    private void forceCaretMoveToStart() {
        if (text.getCaretOffset() < cmdStart) {
            // move caret first!
        }
        text.redraw();
    }

    @Override
    public void verifyKey(VerifyEvent event) {
        type(event);
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {
        // TODO
    }

    @Override
    public void grabCommandLine(String prompt, String endCommand, CommandListener listener) {
        this.commandListener = listener;
        this.oldPrompt = this.getPrompt();
        this.endCommand = endCommand;
        println("Type '" + endCommand + "' to return to normal operation\n");
        this.setPrompt(prompt);
    }

    private String getPrompt() {
        return prompt;
    }

    @Override
    public void error(Object e) {
        // TODO Auto-generated method stub
        println("error: " + e.toString());
    }

    @Override
    public void warning(Object e) {
        println("warning: " + e.toString());
    }

    @Override
    public void info(Object e, String infoClass) {
        println(e.toString());
    }

    @Override
    public void echo(Object string) {
        print(string.toString());
    }

    @Override
    public void outputResult(String input, Object ret) {

        if (ret == null) {
            return;
        }

        if (ret instanceof Map) {
            for (Object o : ((Map<?, ?>) ret).keySet()) {
                println(o + " = " + ((Map<?, ?>) ret).get(o));
            }
        } else if (ret instanceof Iterable) {
            for (Iterator<?> it = ((Iterable<?>) ret).iterator(); it.hasNext();) {
                println(it.next());
            }
        } else {
            println(ret);
        }
    }

    @Override
    public void reportCommandResult(String input, boolean ok) {
        // add to history
        if (history != null) {
            history.append(input);
        }
    }

    public void initialize() {
        if (!this.initialized) {
            this.initialized = true;
            printStatusMessage();
            print(prompt);
            text.setEnabled(true);
        }
    }

}
