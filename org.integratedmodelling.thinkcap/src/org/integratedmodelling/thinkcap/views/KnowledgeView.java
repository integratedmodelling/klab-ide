/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.navigator.IDescriptionProvider;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.HTMLStyledTextParser;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.factories.IModelManager;
import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.visualization.ILegend;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.referencing.Bookmark;
import org.integratedmodelling.common.client.referencing.BookmarkGraph;
import org.integratedmodelling.common.client.referencing.BookmarkManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.authority.AuthorityFactory;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.events.ArchiveEvent;
import org.integratedmodelling.thinkcap.events.AuthenticationEvent;
import org.integratedmodelling.thinkcap.events.BookmarksModifiedEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.ModelModifiedEvent;
import org.integratedmodelling.thinkcap.events.ScenariosSetEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.ui.KnowledgeBrowser;

public class KnowledgeView extends ViewPart implements IThinklabEventListener {

    public static final String  ID                    = "org.integratedmodelling.thinkcap.views.Scenarios"; //$NON-NLS-1$
    private final FormToolkit   toolkit               = new FormToolkit(Display
            .getCurrent());
    private Table               table;
    private CheckboxTableViewer checkboxTableViewer;
    private TreeViewer          issuesTree;
    private TreeViewer          bookmarksTree;
    private TreeViewer          testTree;
    private TreeViewer          resultViewer;
    private Combo               authList;

    /*
     * Search matches
     */
    private List<Bookmark>      matches               = new ArrayList<>();

    // private Action action;

    /*
     * filters. Default is to show only concepts from the im namespace and the projects.
     */
    protected boolean           _includeDataModels    = false;
    protected boolean           _includeAgentModels   = false;
    protected boolean           _includeSubjects      = false;
    protected boolean           _includeConcepts      = true;
    protected boolean           _includeInternal      = false;

    /*
     * these to support the community process. When either is checked, everything else is
     * set to false.
     */
    protected boolean           _showSharedTemporary  = false;
    protected boolean           _showPrivateTemporary = false;

    /*
     * current authority ID
     *
     */
    String                      _curAuth              = null;

    // show parent hierarchy instead of children in knowledge tree
    private boolean             showParents           = false;

    class BookmarkNode extends Bookmark {

        IConcept concept;

        BookmarkNode(IModelObject o) {
            super(o);
        }

        BookmarkNode(IConcept o) {
            IModelObject obj = KLAB.MMANAGER.findModelObject(o.toString());
            objectName = id = o.toString();
            lineNumber = obj == null ? -1 : obj.getFirstLineNumber();
            file = obj == null ? null : obj.getNamespace().getLocalFile().toString();
            namespace = o.getOntology().getConceptSpace();
            inheritance = new String[] { o.toString() };
            objectType = Type.CONCEPT;
            concept = o;
        }

        private static final long serialVersionUID = -957881730982613445L;

        List<BookmarkNode>        children         = new ArrayList<>();
        BookmarkNode              parent           = null;
    }

    /*
     * the model for the tree
     */
    public static class Archive {

        Archive(List<IContext> data) {
            archives = data;
        }

        List<IContext> archives;
    }

    Archive            archive;
    // private Text text;
    private Text       authSearch;
    private SashForm   authDisplay;
    private TableViewer authResultsTree;
    private StyledText authDescription;
    private Action     reasonerAction;

    // private Action action;

    private Image getBookmarkImage(Bookmark element, String bclass) {

        IConcept concept = null;

        if (bclass.equals(BookmarkManager.CLASS_CHECK)) {
            // TODO proper gif
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
        } else if (bclass.equals(BookmarkManager.CLASS_CHECK)) {
            // TODO proper gif
            return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
        } else {
            switch (element.objectType) {
            case CONCEPT:
                concept = KLAB.KM.getConcept(element.objectName);
                return Eclipse.getConceptImage(concept, NS.isNothing(concept));
            case MODEL:
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, element.agent ? "/icons/agent.gif"
                        : "/icons/model.gif");
            case SUBJECT:
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
            default:
                break;
            }
        }

        return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark.gif");
    }

    public class BookmarkContentProvider implements ITreeContentProvider {

        String bclass;

        BookmarkContentProvider(String bclass) {
            this.bclass = bclass;
        }

        private BookmarkGraph getGraph() {
            return ((BookmarkManager) KLAB.KM.getBookmarkManager()).getBookmarks(bclass);
        }

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {

            Object[] ret = null;
            if (parentElement instanceof BookmarkGraph) {
                ret = ((BookmarkGraph) parentElement).getTopLevelBookmarks().toArray();
            } else if (parentElement instanceof Bookmark) {
                ret = getGraph().childrenOf((Bookmark) parentElement);
            }

            return ret;
        }

        @Override
        public Object getParent(Object element) {
            Object ret = null;
            if (element instanceof Bookmark) {
                ret = getGraph().getParent((Bookmark) element);
            }
            return ret;
        }

        @Override
        public boolean hasChildren(Object element) {

            boolean ret = false;
            if (element instanceof BookmarkGraph) {
                ret = ((BookmarkGraph) element).vertexSet().size() > 0;
            } else if (element instanceof Bookmark) {
                ret = getGraph().hasChildren((Bookmark) element);
            }
            return ret;
        }
    }

    class AuthResultContentProvider implements ITreeContentProvider {

        Collection<?> root = null;

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            return parentElement instanceof Collection<?> ? ((Collection<?>) parentElement).toArray() : null;
        }

        @Override
        public Object getParent(Object element) {
            return element instanceof IMetadata ? root : null;
        }

        @Override
        public boolean hasChildren(Object element) {
            if (element instanceof Collection<?>) {
                root = (Collection<?>) element;
                return ((Collection<?>) element).size() > 0;
            }
            return false;
        }

    }

    class AuthResultLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (element instanceof AuthorityConcept) {
                if (columnIndex == 0) {
                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/copy.png");
                }
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            if (element instanceof AuthorityConcept) {
                switch (columnIndex) {
                case 1:
                    return ((AuthorityConcept) element).getId();
                case 2:
                    return ((AuthorityConcept) element).getLabel();
                default:
                    return "";
                }
            }
            return null;
        }
    }

    class BookmarkLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        String bclass;

        private BookmarkGraph getGraph() {
            return ((BookmarkManager) KLAB.KM.getBookmarkManager()).getBookmarks(bclass);
        }

        public BookmarkLabelProvider(String bclass) {
            this.bclass = bclass;
        }

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (columnIndex == 0 && element instanceof Bookmark) {
                return getGraph().hasChildren((Bookmark) element)
                        ? ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark_folder.gif")
                        : getBookmarkImage((Bookmark) element, bclass);
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            if (element instanceof Bookmark) {
                return columnIndex == 0 ? ((Bookmark) element).name : ((Bookmark) element).message;
            }
            return null;
        }
    }

    class TestBookmarkLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        private BookmarkGraph getGraph() {
            return ((BookmarkManager) KLAB.KM.getBookmarkManager()).getBookmarks(BookmarkManager.CLASS_TEST);
        }

        public TestBookmarkLabelProvider() {
        }

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (element instanceof Bookmark) {
               /* if (columnIndex == 2) {
                    if (!((BookmarkManager) KLAB.KM.getBookmarkManager())
                            .getBookmarks(BookmarkManager.CLASS_TEST)
                            .hasChildren((Bookmark) element)) {
                        
                         * TODO set in launch handler
                         
                        return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/launch_run.gif");
                    }
                } else */if (columnIndex == 0) {
                    return getGraph().hasChildren((Bookmark) element)
                            ? ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/bookmark_folder.gif")
                            : getBookmarkImage((Bookmark) element, BookmarkManager.CLASS_TEST);
                }
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            if (element instanceof Bookmark) {

                switch (columnIndex) {
                case 0:
                    return ((Bookmark) element).name;
                case 1:
                    return ((Bookmark) element).message;
                }
            }
            return null;
        }
    }

    public class KnowledgeBookmarkContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parent) {
            if (parent instanceof List<?>) {
                return ((List<?>) parent).toArray();
            } else if (parent instanceof BookmarkNode) {

                if (((BookmarkNode) parent).objectType == Bookmark.Type.CONCEPT) {

                    // FIXME we don't really need to store children
                    IConcept c = ((BookmarkNode) parent).concept;
                    ((BookmarkNode) parent).children.clear();
                    if (c != null) {
                        if (showParents) {
                            for (IConcept p : c.getParents()) {
                                BookmarkNode bp = new BookmarkNode(p);
                                ((BookmarkNode) parent).children.add(bp);
                                bp.parent = (BookmarkNode) parent;
                            }
                        } else {
                            for (IConcept p : c.getChildren()) {
                                BookmarkNode bp = new BookmarkNode(p);
                                ((BookmarkNode) parent).children.add(bp);
                                bp.parent = (BookmarkNode) parent;
                            }
                        }
                        return ((BookmarkNode) parent).children.toArray();
                    }
                }
                /*
                 * TODO see what we want to do with other things.
                 */
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {

            if (element instanceof BookmarkNode) {
                if (((BookmarkNode) element).parent == null)
                    return matches;
                return ((BookmarkNode) element).parent;
            }

            return null;
        }

        @Override
        public boolean hasChildren(Object element) {

            if (element instanceof List<?>) {
                return ((List<?>) element).size() > 0;
            } else if (element instanceof BookmarkNode
                    && ((BookmarkNode) element).objectType == Bookmark.Type.CONCEPT) {

                IConcept c = ((BookmarkNode) element).concept;
                return c != null && c.getChildren().size() > 0;
            }
            return false;
        }

    }

    class KnowledgeBookmarkLabelProvider extends BaseLabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {
            if (columnIndex == 0 && element instanceof BookmarkNode) {
                return getBookmarkImage((Bookmark) element, BookmarkManager.CLASS_BOOKMARK);
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            if (element instanceof BookmarkNode) {
                if (((BookmarkNode) element).concept != null) {
                    return columnIndex == 0 ? ((BookmarkNode) element).concept.getLocalName()
                            : ((BookmarkNode) element).concept.getConceptSpace();
                }
                return columnIndex == 0 ? ((BookmarkNode) element).objectName
                        : ((BookmarkNode) element).message;
            }
            return null;
        }
    }

    public class ArchiveContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {

            if (parentElement instanceof Archive) {
                return ((Archive) parentElement).archives.toArray();
            }
            // else if (parentElement instanceof Visualization) {
            // Visualization vis = ((Visualization) parentElement);
            // ArrayList<Object> ch = new ArrayList<Object>();
            // ch.add(vis.contextMedia);
            //
            // for (Pair<GraphVisualization, GraphVisualization> w :
            // vis.resolutionStrategy) {
            // ch.add(w.getFirst());
            // ch.add(w.getSecond());
            // }
            //
            // for (IStateVisualization zio : vis.states) {
            // ch.add(zio);
            // }
            // for (IVisualization zio : vis.subjects) {
            // ch.add(zio);
            // }
            // return ch.toArray();
            // } else if (parentElement instanceof IStateVisualization) {
            // return new Object[] { ((StateVisualization) parentElement).legend };
            // }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            /*
             * if (element instanceof Visualization) { return ((Visualization)
             * element).parent == null ? archive : ((Visualization) element).parent; }
             * else if (element instanceof StateVisualization) { return
             * ((StateVisualization) element).parent; } else if (element instanceof
             * ILegend) { return ((Legend) element).parent; } else
             */if (element instanceof GraphVisualization) {
                return ((GraphVisualization) element).parent;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            if (element instanceof Archive) {
                return ((Archive) element).archives.size() > 0;
            } /*
               * else if (element instanceof Visualization) { return true; } else if
               * (element instanceof IStateVisualization) { return !((StateVisualization)
               * element).isContext; }
               */
            return false;
        }
    }

    class ArchiveLabelProvider extends BaseLabelProvider implements ILabelProvider {

        @Override
        public Image getImage(Object element) {

            /*
             * if (element instanceof Visualization) { return
             * ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/dataset.gif"); }
             * else if (element instanceof StateVisualization) { return
             * ResourceManager.getImage(((StateVisualization)
             * element).iconFile.toString()); } else
             */if (element instanceof ILegend) {
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/colormap.gif");
            } else if (element instanceof GraphVisualization) {
                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "icons/tree_layout.gif");
            }
            return null;
        }

        @Override
        public String getText(Object element) {

            /*
             * if (element instanceof Visualization) { return ((Visualization)
             * element).getName(); } else if (element instanceof StateVisualization) {
             * return ((StateVisualization) element).isContext ? "Context" :
             * ((StateVisualization) element).legend.getDescription(ILegend.LABEL); } else
             */if (element instanceof ILegend) {
                return "Palette (TODO)";
            } else if (element instanceof GraphVisualization) {
                return ((GraphVisualization) element).name;
            }
            return null;
        }
    }

    public KnowledgeView() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    public class ScenarioLabelProvider extends LabelProvider implements ILabelProvider, IDescriptionProvider {

        @Override
        public Image getImage(Object element) {

            if (element instanceof INamespace) {
                if (((INamespace) element).hasErrors())
                    return ResourceManager.decorateImage(ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "/icons/scenario.gif"), ResourceManager
                                    .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/error_co.gif"), SWTResourceManager.BOTTOM_LEFT);
                if (((INamespace) element).hasWarnings())
                    return ResourceManager.decorateImage(ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "/icons/scenario.gif"), ResourceManager
                                    .getPluginImage("org.eclipse.ui.navigator.resources", "/icons/full/ovr16/warning_co.gif"), SWTResourceManager.BOTTOM_LEFT);

                return ResourceManager.getPluginImage(Activator.PLUGIN_ID, "/icons/scenario.gif");
            }

            return null;
        }

        @Override
        public String getText(Object element) {
            if (element instanceof INamespace) {
                return ((INamespace) element).getId();
            }
            return null;
        }

        @Override
        public String getDescription(Object anElement) {
            // TODO Auto-generated method stub
            return null;
        }

    }

    class ScenarioContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parentElement) {
            if (parentElement instanceof IModelManager) {
                List<INamespace> nss = ((IModelManager) parentElement).getScenarios();
                Collections.sort(nss, new Comparator<INamespace>() {

                    @Override
                    public int compare(INamespace arg0, INamespace arg1) {
                        return arg0.getId().compareTo(arg1.getId());
                    }

                });
                return nss.toArray();
            }
            return null;
        }

        @Override
        public Object getParent(Object element) {
            if (element instanceof INamespace) {
                return KLAB.MMANAGER;
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            if (element instanceof IModelManager) {
                return ((IModelManager) element).getScenarios().size() > 0;
            }
            return false;
        }

    }

    /**
     * Create contents of the view part.
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        parent.setLayout(new FillLayout(SWT.HORIZONTAL));
        {
            final TabFolder tabFolder = new TabFolder(parent, SWT.NONE);
            tabFolder.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {

                    TabItem selected = tabFolder.getSelection()[0];
                    resetActions(selected.getText());
                }
            });
            toolkit.adapt(tabFolder);
            toolkit.paintBordersFor(tabFolder);
            {
                TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
                tbtmNewItem.setText("Bookmarks");
                {
                    Composite composite = new Composite(tabFolder, SWT.NONE);
                    tbtmNewItem.setControl(composite);
                    toolkit.paintBordersFor(composite);
                    composite.setLayout(new FillLayout(SWT.HORIZONTAL));
                    {
                        bookmarksTree = new TreeViewer(composite, SWT.BORDER);
                        final Tree tree = bookmarksTree.getTree();
                        tree.setHeaderVisible(true);
                        tree.setLinesVisible(true);
                        tree.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseDoubleClick(MouseEvent e) {
                                Object o = ((TreeSelection) (bookmarksTree.getSelection())).getFirstElement();
                                if (o instanceof Bookmark && ((Bookmark) o).lineNumber > 0
                                        && ((Bookmark) o).file != null) {
                                    try {
                                        Bookmark bookmark = (Bookmark) o;

                                        IModelObject mo = KLAB.MMANAGER.findModelObject(bookmark.objectName);
                                        File file = null;
                                        int nline = -1;
                                        if (mo != null) {
                                            file = mo.getNamespace().getLocalFile();
                                            nline = mo.getFirstLineNumber();
                                        }

                                        if (nline > 0 && file != null && file.exists()) {
                                            Eclipse.openFile(file.toString(), nline);
                                        } else {
                                            Eclipse.beep();
                                            Eclipse.warningStatus("Bookmark " + bookmark.id
                                                    + " is out of sync with the model base", ID);
                                        }

                                    } catch (KlabException e1) {
                                        Activator.engine().getMonitor().error(e1);
                                    }
                                }
                            }
                        });
                        final Menu menu = new Menu(tree);
                        tree.setMenu(menu);
                        menu.addMenuListener(new MenuAdapter() {
                            @Override
                            public void menuShown(MenuEvent e) {

                                Object o = tree.getSelection()[0].getData();
                                final Bookmark bookmark = o instanceof Bookmark ? (Bookmark) o : null;

                                if (bookmark == null || !bookmark.isPermanent()) {
                                    return;
                                }

                                // Get rid of existing menu items
                                MenuItem[] items = menu.getItems();
                                for (int i = 0; i < items.length; i++) {
                                    items[i].dispose();
                                }
                                // Add menu items for current selection
                                MenuItem newItem = new MenuItem(menu, SWT.NONE);
                                newItem.setText("Delete bookmark " + tree.getSelection()[0].getText());
                                newItem.addSelectionListener(new SelectionListener() {

                                    @Override
                                    public void widgetSelected(SelectionEvent e) {
                                        try {
                                            KLAB.KM.getBookmarkManager().deleteBookmark(bookmark);
                                            Activator.getDefault()
                                                    .fireEvent(new BookmarksModifiedEvent(bookmark));
                                        } catch (KlabIOException e1) {
                                            Eclipse.handleException(e1);
                                        }
                                    }

                                    @Override
                                    public void widgetDefaultSelected(SelectionEvent e) {
                                        //
                                    }
                                });
                            }
                        });
                        toolkit.paintBordersFor(tree);
                        final DragSource source = new DragSource(tree, DND.DROP_COPY | DND.DROP_MOVE
                                | DND.DROP_DEFAULT);
                        source.addDragListener(new DragSourceAdapter() {
                            @Override
                            public void dragSetData(DragSourceEvent event) {
                                event.data = ((TreeSelection) (bookmarksTree.getSelection()))
                                        .getFirstElement();
                                if (event.data instanceof Bookmark) {
                                    event.data = ((Bookmark) event.data).getSignature();
                                }
                            }
                        });
                        source.setTransfer(new Transfer[] { /*
                                                             * BookmarkTransfer.
                                                             * getInstance(),
                                                             */ TextTransfer.getInstance() });
                        {
                            TreeColumn trclmnNewColumn = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn.setText("Bookmark");
                            trclmnNewColumn.setWidth(330);
                        }
                        {
                            TreeColumn trclmnNewColumn_1 = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn_1.setText("Description");
                            trclmnNewColumn_1.setWidth(480);
                        }
                        bookmarksTree
                                .setContentProvider(new BookmarkContentProvider(BookmarkManager.CLASS_BOOKMARK));
                        bookmarksTree
                                .setLabelProvider(new BookmarkLabelProvider(BookmarkManager.CLASS_BOOKMARK));

                        bookmarksTree.addDropSupport(DND.DROP_COPY | DND.DROP_MOVE
                                | DND.DROP_DEFAULT, new Transfer[] {
                                        /* BookmarkTransfer.getInstance(), */ TextTransfer
                                                .getInstance() }, new DropTargetAdapter() {

                                                    @Override
                                                    public void drop(DropTargetEvent event) {
                                                        // System.out.println("merde: " +
                                                        // event.data);
                                                    }

                                                });
                    }
                }
            }
            TabItem tbtmNewItem_5 = new TabItem(tabFolder, SWT.NONE);
            tbtmNewItem_5.setText("Knowledge Dictionary");
            KnowledgeBrowser composite_1 = new KnowledgeBrowser(tabFolder, SWT.NONE);
            tbtmNewItem_5.setControl(composite_1);
            toolkit.paintBordersFor(composite_1);
            composite_1.setLayout(new GridLayout(1, false));
            {
                TabItem tbtmNewItem_6 = new TabItem(tabFolder, SWT.NONE);
                tbtmNewItem_6.setText("Authorities");
                {
                    Composite composite_2 = new Composite(tabFolder, SWT.NONE);
                    tbtmNewItem_6.setControl(composite_2);
                    toolkit.paintBordersFor(composite_2);
                    composite_2.setLayout(new GridLayout(1, false));
                    {
                        Composite composite_3 = new Composite(composite_2, SWT.NONE);
                        composite_3.setLayout(new GridLayout(3, false));
                        composite_3
                                .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                        toolkit.adapt(composite_3);
                        toolkit.paintBordersFor(composite_3);
                        {
                            authList = new Combo(composite_3, SWT.READ_ONLY);
                            GridData gd_authList = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
                            gd_authList.widthHint = 100;
                            authList.setLayoutData(gd_authList);
                            authList.addSelectionListener(new SelectionAdapter() {
                                @Override
                                public void widgetSelected(SelectionEvent e) {
                                    selectAuthority(authList.getText());
                                }
                            });
                            toolkit.adapt(authList);
                            toolkit.paintBordersFor(authList);
                        }
                        {
                            authSearch = new Text(composite_3, SWT.BORDER);
                            authSearch.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyReleased(KeyEvent e) {
                                    if (e.keyCode == SWT.CR) {
                                        String query = authSearch.getText().trim();
                                        if (!query.isEmpty() && _curAuth != null) {
                                            searchAuthority(query);
                                        }
                                    }
                                }
                            });
                            authSearch
                                    .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
                            toolkit.adapt(authSearch, true, true);
                        }
                        {
                            Button btnNewButton_1 = new Button(composite_3, SWT.NONE);
                            btnNewButton_1.setEnabled(false);
                            toolkit.adapt(btnNewButton_1, true, true);
                            btnNewButton_1.setText("Copy selected");
                        }
                    }
                    {
                        authDisplay = new SashForm(composite_2, SWT.NONE);
                        authDisplay
                                .setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
                        toolkit.adapt(authDisplay);
                        toolkit.paintBordersFor(authDisplay);
                        {
                            authResultsTree = new TableViewer(authDisplay, SWT.BORDER);
                            Table tree_1 = authResultsTree.getTable();
                            tree_1.setHeaderVisible(true);
                            tree_1.setLinesVisible(true);
                            toolkit.paintBordersFor(tree_1);
                            authResultsTree
                                    .setContentProvider(new AuthResultContentProvider());
                            authResultsTree
                                    .setLabelProvider(new AuthResultLabelProvider());
                            {
                                TableColumn trclmnNewColumn_6 = new TableColumn(tree_1, SWT.CENTER);
                                trclmnNewColumn_6.setResizable(false);
                                trclmnNewColumn_6.setWidth(18);
                            }
                            {
                                TableColumn authIDColumn = new TableColumn(tree_1, SWT.LEFT);
                                authIDColumn.setWidth(120);
                                authIDColumn.setText("Identifier");
                            }
                            {
                                TableColumn authDescriptionColumn = new TableColumn(tree_1, SWT.LEFT);
                                authDescriptionColumn.setWidth(500);
                                authDescriptionColumn.setText("Description");
                            }
                        }
                        {
                            authDescription = new StyledText(authDisplay, SWT.BORDER
                                    | SWT.READ_ONLY | SWT.WRAP);
                            authDescription.setTopMargin(2);
                            authDescription.setRightMargin(2);
                            authDescription.setLeftMargin(2);
                            authDescription.setBottomMargin(2);
                            toolkit.adapt(authDescription);
                            toolkit.paintBordersFor(authDescription);
                            authDescription.setBackground(SWTResourceManager
                                    .getColor(SWT.COLOR_INFO_BACKGROUND));
                        }
                        authDisplay.setWeights(new int[] { 288, 285 });
                    }
                }
            }
            {
                TabItem tbtmNewItem_2 = new TabItem(tabFolder, SWT.NONE);
                tbtmNewItem_2.setText("Scenarios");
                Composite container = toolkit.createComposite(tabFolder, SWT.NONE);
                tbtmNewItem_2.setControl(container);
                toolkit.paintBordersFor(container);
                container.setLayout(new FillLayout(SWT.HORIZONTAL));

                checkboxTableViewer = CheckboxTableViewer.newCheckList(container, SWT.BORDER
                        | SWT.FULL_SELECTION);
                checkboxTableViewer.addCheckStateListener(new ICheckStateListener() {
                    @Override
                    public void checkStateChanged(CheckStateChangedEvent event) {
                        addNamespace((INamespace) event.getElement(), event.getChecked());
                    }
                });
                table = checkboxTableViewer.getTable();
                table.setLinesVisible(true);
                toolkit.paintBordersFor(table);
                checkboxTableViewer.setLabelProvider(new ScenarioLabelProvider());
                checkboxTableViewer.setContentProvider(new ScenarioContentProvider());
                checkboxTableViewer.setInput(KLAB.MMANAGER);
            }
            {
                TabItem tbtmNewItem_1 = new TabItem(tabFolder, SWT.NONE);
                tbtmNewItem_1.setText("My Issues");
                {
                    Composite composite = new Composite(tabFolder, SWT.NONE);
                    tbtmNewItem_1.setControl(composite);
                    toolkit.paintBordersFor(composite);
                    composite.setLayout(new FillLayout(SWT.HORIZONTAL));
                    {
                        issuesTree = new TreeViewer(composite, SWT.BORDER);
                        Tree tree = issuesTree.getTree();
                        tree.setHeaderVisible(true);
                        tree.setLinesVisible(true);
                        tree.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseDoubleClick(MouseEvent e) {
                                Object o = ((TreeSelection) (issuesTree.getSelection())).getFirstElement();
                                if (o instanceof Bookmark && ((Bookmark) o).lineNumber > 0
                                        && ((Bookmark) o).file != null) {
                                    try {
                                        Eclipse.openFile(((Bookmark) o).file, ((Bookmark) o).lineNumber);
                                    } catch (KlabException e1) {
                                        Activator.engine().getMonitor().error(e1);
                                    }
                                }
                            }
                        });
                        toolkit.adapt(tree);
                        toolkit.paintBordersFor(tree);
                        {
                            TreeColumn trclmnNewColumn_2 = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn_2.setWidth(330);
                            trclmnNewColumn_2.setText("Issue");
                        }
                        {
                            TreeColumn trclmnNewColumn_5 = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn_5.setText("Description");
                            trclmnNewColumn_5.setWidth(480);
                        }
                        issuesTree
                                .setContentProvider(new BookmarkContentProvider(BookmarkManager.CLASS_CHECK));
                        issuesTree.setLabelProvider(new BookmarkLabelProvider(BookmarkManager.CLASS_CHECK));
                    }
                }
            }
            {
                TabItem tbtmNewItem_4 = new TabItem(tabFolder, SWT.NONE);
                tbtmNewItem_4.setText("Tests");
                {
                    Composite composite = new Composite(tabFolder, SWT.NONE);
                    tbtmNewItem_4.setControl(composite);
                    toolkit.paintBordersFor(composite);
                    composite.setLayout(new FillLayout(SWT.HORIZONTAL));
                    {
                        testTree = new TreeViewer(composite, SWT.BORDER);
                        Tree tree = testTree.getTree();
                        tree.setHeaderVisible(true);
                        tree.setLinesVisible(true);
                        tree.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseDoubleClick(MouseEvent e) {
                                Object o = ((TreeSelection) (testTree.getSelection())).getFirstElement();
                                if (o instanceof Bookmark && ((Bookmark) o).lineNumber > 0
                                        && ((Bookmark) o).file != null) {
                                    try {
                                        Eclipse.openFile(((Bookmark) o).file, ((Bookmark) o).lineNumber);
                                    } catch (KlabException e1) {
                                        Activator.engine().getMonitor().error(e1);
                                    }
                                }
                            }
                        });
                        final DragSource source = new DragSource(tree, DND.DROP_COPY | DND.DROP_MOVE
                                | DND.DROP_DEFAULT);
                        source.addDragListener(new DragSourceAdapter() {
                            @Override
                            public void dragSetData(DragSourceEvent event) {
                                event.data = ((TreeSelection) (testTree.getSelection()))
                                        .getFirstElement();
                                if (event.data instanceof Bookmark) {
                                    event.data = ((Bookmark) event.data).getSignature();
                                }
                            }
                        });
                        source.setTransfer(new Transfer[] { /*
                                 * BookmarkTransfer.
                                 * getInstance(),
                                 */ TextTransfer.getInstance() });
                        toolkit.adapt(tree);
                        toolkit.paintBordersFor(tree);
                        {
                            TreeColumn trclmnNewColumn_3 = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn_3.setText("Test");
                            trclmnNewColumn_3.setWidth(330);
                        }
                        {
                            TreeColumn trclmnNewColumn_4 = new TreeColumn(tree, SWT.NONE);
                            trclmnNewColumn_4.setText("Description");
                            trclmnNewColumn_4.setWidth(580);
                        }
//                        {
//                            TreeColumn runButtonColumn = new TreeColumn(tree, SWT.NONE);
//                            runButtonColumn.setText("Run controls");
//                            runButtonColumn.setWidth(180);
//                        }
//                        {
//                            TabItem tbtmNewItem_3 = new TabItem(tabFolder, SWT.NONE);
//                            tbtmNewItem_3.setText("Results");
//                            {
//                                resultViewer = new TreeViewer(tabFolder, SWT.BORDER);
//                                resultViewer.addDoubleClickListener(new IDoubleClickListener() {
//                                    @Override
//                                    public void doubleClick(DoubleClickEvent event) {
//
//                                        Object o = ((TreeSelection) (event.getSelection())).getFirstElement();
//                                        /*
//                                         * if (o instanceof Visualization) { try {
//                                         * PlatformUI.getWorkbench().
//                                         * getActiveWorkbenchWindow().getActivePage()
//                                         * .showView(
//                                         * "org.integratedmodelling.thinkcap.views.DatasetView"
//                                         * );
//                                         * Client.get().fireEvent(ArchiveEvent.display(((
//                                         * Visualization) o))); } catch (PartInitException
//                                         * e) { Eclipse.handleException(e); } }
//                                         */}
//                                });
//                                Tree tree_1 = resultViewer.getTree();
//                                tbtmNewItem_3.setControl(tree_1);
//                                tree_1.setLinesVisible(true);
//                                toolkit.paintBordersFor(tree_1);
//                                resultViewer.setContentProvider(new ArchiveContentProvider());
//                                resultViewer.setLabelProvider(new ArchiveLabelProvider());
//                            }
//                            resultViewer.setInput(buildArchive());
//                        }
//                        {
//                            {
//                            }
//                        }
                        testTree.setContentProvider(new BookmarkContentProvider(BookmarkManager.CLASS_TEST));
                        testTree.setLabelProvider(new TestBookmarkLabelProvider());
                    }
                }
            }
        }

        initializeAuthorityView();

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    private void initializeAuthorityView() {

        for (String s : AuthorityFactory.get().getAuthorityViewIds()) {
            authList.add(s);
        }
        if (authList.getItemCount() > 0) {
            authList.select(0);
            selectAuthority(authList.getItem(0));
        }
    }

    private void selectAuthority(String auth) {
        _curAuth = auth;
        IAuthority<?> autho = null;
        try {
            autho = AuthorityFactory.get().getAuthorityView(auth);
        } catch (KlabValidationException e1) {
        }

        if (autho == null) {
            return;
        }

        final IAuthority<?> authority = autho;

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                if (authority.canSearch()) {
                    authDisplay.setWeights(new int[] { 1, 1 });
                    authSearch.setEnabled(true);
                } else {
                    authDisplay.setWeights(new int[] { 0, 1 });
                    authSearch.setEnabled(false);
                }
                authDescription.setText(authority.getDescription());
                try {
                    new HTMLStyledTextParser(authDescription).parse();
                } catch (IOException e) {
                }
                authResultsTree.setInput(null);
                authSearch.setText("");
            }
        });
    }

    private void searchAuthority(String query) {

        ISearchResult<AuthorityConcept> ret = null;

        if (query != null) {

            IAuthority<AuthorityConcept> authority = null;
            try {
                authority = AuthorityFactory.get().getAuthorityView(_curAuth);
            } catch (KlabValidationException e) {
                // proceed
            }
            if (authority != null) {
                ret = authority.search(query, authList.getItem(authList.getSelectionIndex()));
            }
        }

        if (ret != null) {
            final List<AuthorityConcept> rr = ret.getMatches();
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    authResultsTree.setInput(rr);
                }
            });
        }
    }

    protected void resetActions(final String text) {

        final IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();

        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {

                tbm.removeAll();

                // if (text.startsWith("Knowledge")) {
                // tbm.add(action);
                // tbm.add(selectConcepts);
                // tbm.add(selectSubjects);
                // tbm.add(selectAgentModels);
                // tbm.add(selectDataModels);
                // }

                tbm.update(true);
            }
        });
    }

    protected void addNamespace(final INamespace element, final boolean checked) {

        /*
         * add scenario to current choice; reset checkmarks appropriately
         */
        Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                checkboxTableViewer.setAllChecked(false);
                Environment.get().setScenario(element, checked);
                for (INamespace n : Environment.get().getScenarios()) {
                    checkboxTableViewer.setChecked(n, true);
                }
                Activator.getDefault().fireEvent(new ScenariosSetEvent(Environment.get().getScenarios()));
            }
        });
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        toolkit.dispose();
        super.dispose();
    }

    /**
     * Create the actions.
     */
    private void createActions() {
        // Create the actions
        // {
        // selectDataModels = new Action("", IAction.AS_CHECK_BOX) {
        // @Override
        // public void run() {
        // _includeDataModels = isChecked();
        // search(text.getText());
        // }
        // };
        // selectDataModels.setToolTipText("Include data models in search");
        // selectDataModels.setChecked(false);
        // selectDataModels.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/model.gif"));
        // }
        // {
        // selectAgentModels = new Action("", IAction.AS_CHECK_BOX) {
        // @Override
        // public void run() {
        // _includeAgentModels = isChecked();
        // search(text.getText());
        // }
        // };
        // selectAgentModels.setToolTipText("Include agent models in search");
        // selectAgentModels.setChecked(false);
        // selectAgentModels.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/agent.gif"));
        // }
        // {
        // selectSubjects = new Action("", IAction.AS_CHECK_BOX) {
        // @Override
        // public void run() {
        // _includeSubjects = isChecked();
        // search(text.getText());
        // }
        // };
        // selectSubjects.setToolTipText("Include observed subjects in search");
        // selectSubjects.setChecked(false);
        // selectSubjects.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/observer.gif"));
        // }
        // {
        // selectConcepts = new Action("", IAction.AS_CHECK_BOX) {
        // @Override
        // public void run() {
        // _includeConcepts = isChecked();
        // search(text.getText());
        // }
        // };
        // selectConcepts.setToolTipText("Include concepts in search");
        // selectConcepts.setChecked(true);
        // selectConcepts.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/concept.gif"));
        // }
        // {
        // action_4 = new Action("Force reindex") {
        // };
        // action_4.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/cleanup.gif"));
        // }
        // {
        // action = new Action("", IAction.AS_CHECK_BOX) {
        // @Override
        // public void run() {
        // _includeInternal = !isChecked();
        // search(text.getText());
        // }
        // };
        // action.setToolTipText("Filter out internal knowledge");
        // action.setChecked(true);
        // action.setImageDescriptor(ResourceManager.getPluginImageDescriptor(
        // "org.integratedmodelling.thinkcap", "icons/filter_internal_targets.gif"));
        // }
        {
            reasonerAction = new Action("", IAction.AS_DROP_DOWN_MENU) {
            };
            reasonerAction.setToolTipText("Reasoner is stopped");
            reasonerAction.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/dot_grey.png"));
        }
    }

    /**
     * Initialize the toolbar.
     */
    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
        tbm.add(reasonerAction);
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    @Override
    public void handleThinklabEvent(final ThinklabEvent event) {

        if (event instanceof ModelModifiedEvent || event instanceof BookmarksModifiedEvent) {
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (event instanceof ModelModifiedEvent) {
                        checkboxTableViewer.setInput(KLAB.MMANAGER);
                        issuesTree.setInput(((BookmarkManager) KLAB.KM.getBookmarkManager())
                                .getBookmarks(BookmarkManager.CLASS_CHECK));
                        testTree.setInput(((BookmarkManager) KLAB.KM.getBookmarkManager())
                                .getBookmarks(BookmarkManager.CLASS_TEST));
                    }
                    bookmarksTree.setInput(((BookmarkManager) KLAB.KM.getBookmarkManager())
                            .getBookmarks(BookmarkManager.CLASS_BOOKMARK));

                }
            });
        } else if (event instanceof ArchiveEvent && ((ArchiveEvent) event).archive != null) {
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    resultViewer.setInput(buildArchive());
                }
            });
        } else if (event instanceof ScenariosSetEvent) {
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    checkboxTableViewer.setInput(KLAB.MMANAGER);
                }
            });
        } else if (event instanceof AuthenticationEvent) {

            /*
             * TODO improve logics
             */
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    initializeAuthorityView();
                }
            });
        }
    }

    private Archive buildArchive() {

        ArrayList<IContext> ret = new ArrayList<>();
        // File adir = Env.CONFIG.getWorkspace("archive");
        //
        // for (File f : adir.listFiles()) {
        //
        // if (!f.isDirectory())
        // continue;
        //
        // File idx = new File(f + File.separator + "index.xml");
        // if (!idx.exists())
        // continue;
        //
        // try {
        // ret.add(Visualization.read(f.toString()));
        // } catch (ThinklabIOException e) {
        // Eclipse.handleException(e);
        // }
        // }
        //
        // /*
        // * sort most recent first
        // */
        // Collections.sort(ret, new Comparator<Visualization>() {
        //
        // @Override
        // public int compare(Visualization o1, Visualization o2) {
        // if (o1.getLastModified() == o2.getLastModified())
        // return 0;
        // if (o1.getLastModified() < o2.getLastModified())
        // return 1;
        // return -1;
        // }
        // });
        //
        this.archive = new Archive(ret);

        return this.archive;
    }

    // protected void search(final String text) {
    //
    // Display.getDefault().asyncExec(new Runnable() {
    // @Override
    // public void run() {
    // IKnowledgeIndex i = KLAB.KM.getIndex();
    // if (i != null) {
    // matches = new ArrayList<Bookmark>();
    // if (text.length() > 1) {
    // try {
    // for (IMetadata m : i.search(text)) {
    // Object o = ((KnowledgeIndex) i).retrieveObject(m);
    // Bookmark b = use(o);
    // if (b != null)
    // matches.add(b);
    // }
    // } catch (KlabException e) {
    // Activator.engine().getMonitor().error(e);
    // }
    // }
    // } else {
    // // TODO say something
    // }
    // // searchViewer.setInput(matches);
    // }
    // });
    // }

    // private void showRequestForm(String cId) {
    //
    // try {
    // PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
    // .showView(AnnotationAssistant.ID);
    // // Client.get().fireEvent(new OntologyFocusEvent(o));
    // } catch (PartInitException e) {
    // Eclipse.handleException(e);
    // }
    //
    // }

    // private void showOntology(Object o) {
    //
    // try {
    // PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(OntologyView.ID);
    // Client.get().fireEvent(new OntologyFocusEvent(o));
    // } catch (PartInitException e) {
    // Eclipse.handleException(e);
    // }
    //
    // }

    protected Bookmark use(Object o) {

        if (o == null)
            return null;

        BookmarkNode ret = null;

        /*
         * check against search settings
         */
        if (o instanceof IConcept) {

            if (!_includeConcepts)
                return null;

            if (!_includeInternal && ((Ontology) ((IConcept) o).getOntology()).isInternal())
                return null;

            ret = new BookmarkNode((IConcept) o);

        } else if (o instanceof IDirectObserver) {

            if (!_includeSubjects)
                return null;

            ret = new BookmarkNode((IDirectObserver) o);

        } else if (o instanceof IModel) {

            if (((IModel) o).getObserver() == null && !_includeAgentModels)
                return null;

            if (((IModel) o).getObserver() != null && !_includeDataModels)
                return null;

            ret = new BookmarkNode((IModel) o);
        }

        return ret;
    }

}
