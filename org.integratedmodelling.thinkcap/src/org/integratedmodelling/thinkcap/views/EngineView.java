/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.launcher.EngineLauncher;
import org.integratedmodelling.common.launcher.EngineLauncher.Source;
import org.integratedmodelling.common.network.Broadcaster.EngineStatus;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.Eclipse.IActivatorListener;
import org.integratedmodelling.thinkcap.dialogs.EngineDialog;
import org.integratedmodelling.thinkcap.events.AuthenticationEvent;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.NetworkEvent;
import org.integratedmodelling.thinkcap.events.NotificationEvent;
import org.integratedmodelling.thinkcap.events.SessionEvent;
import org.integratedmodelling.thinkcap.events.TaskEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class EngineView extends ViewPart implements IThinklabEventListener {

    public static final String ID                   = "org.integratedmodelling.thinkcap.views.Servers"; //$NON-NLS-1$
    private Table              tableTasks;

    private Label              verLabel;
    private Label              memLabel;
    private Label              upLabel;
    private Label              userLabel;
    private Label              serverLabel;

    private Group              taskArea;
    private TableViewer        taskViewer;

    private Action             showNetwork;
    private Action             stopServer;
    private Action             cleaningCycle;
    private Action             startServer;
    private Action             updateThinkcap;
    private Group              grpMessages;
    private Table              tableMessages;
    private TableViewer        tableViewer;
    private TableColumn        tblclmnNewColumn;
    private TableViewerColumn  tableViewerColumn;
    private TableColumn        tblclmnNewColumn_1;
    private TableViewerColumn  tableViewerColumn_1;

    ArrayList<Notification>    globalNotifications  = new ArrayList<>();
    private Action             copyItemAction;
    private Set<EngineData>    engines              = new HashSet<>();

    /*
     * if this is true, any engine online event received from a local engine will cause a
     * session to be opened.
     */
    private boolean            automaticConnectMode = true;

    PeriodFormatter            yearsAndMonths       = new PeriodFormatterBuilder()
            .printZeroNever()
            .appendYears().appendSuffix(" y ")
            .appendMonths().appendSuffix(" m ").appendDays().appendSuffix(" d ")
            .appendHours()
            .appendSuffix(" h ")
            .appendMinutes().printZeroAlways().appendSuffix(" min ").appendSeconds()
            .appendSuffix(" sec")
            .toFormatter();
    private boolean            isRunning;
    private Action             action_1;
    private SashForm sashForm;

    class SwitchEngineAction extends Action {

        EngineData engine;

        SwitchEngineAction(EngineData engine) {
            super(engine.getName() + " (" + engine.getIp() + ":" + engine.getPort()
                    + ")");
            this.engine = engine;
            this.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/engine-icon.png"));
        }

        @Override
        public void run() {
            Job job = new Job("Switching engine") {

                @Override
                protected IStatus run(IProgressMonitor monitor) {
                    /*
                     * TODO! and only if user is admin
                     */
                    // Activator.getDefault().fireEvent(new
                    // ContextEvent(context, ContextEvent.FOCUS));
                    return Status.OK_STATUS;
                }
            };
            job.schedule();
        }
    }

    class TaskLabelProvider extends LabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {

            ITask td = (ITask) element;

            if (columnIndex == 0) {

                switch (td.getStatus()) {
                case ERROR:
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/error_obj.gif");
                case FINISHED:
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/complete_status.gif");
                case INTERRUPTED:
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/model_stopped.gif");
                case INITIALIZING:
                case RUNNING:
                case QUEUED:
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/wait.gif");
                }
            }
            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            ITask td = (ITask) element;

            if (columnIndex == 1) {
                return td.getDescription();
            } else if (columnIndex == 2) {
                if (td.getStatus() == org.integratedmodelling.api.runtime.ITask.Status.FINISHED) {
                    return ((td.getEndTime() - td.getStartTime()) / 1000) + "s";
                } else {
                    return ((new Date().getTime() - td.getStartTime()) / 1000) + "s";
                }
            }
            return null;
        }

    }

    class ServerLabelProvider extends LabelProvider implements ITableLabelProvider {

        @Override
        public Image getColumnImage(Object element, int columnIndex) {

            if (columnIndex == 0) {
                if (element instanceof ICompileError) {
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/CancelSmall.png");
                } else if (element instanceof ICompileWarning) {
                    return ResourceManager
                            .getPluginImage(Activator.PLUGIN_ID, "icons/AlertAlt2.png");
                } else if (element instanceof ICompileInfo) {
                    return ResourceManager.getPluginImage(Activator.PLUGIN_ID, Eclipse
                            .getNotificationIcon(((ICompileInfo) element).getCategory()));
                }
            }

            return null;
        }

        @Override
        public String getColumnText(Object element, int columnIndex) {

            ICompileNotification td = (ICompileNotification) element;

            if (columnIndex == 1) {
                return td.getMessage();
            }
            /*
             * TODO add date
             */
            return null;
        }

    }

    public EngineView() {

        Activator.getDefault().addThinklabEventListener(this);

    }

    /**
     * Create contents of the view part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {
        parent.setLayout(new GridLayout(1, false));

        GridLayout gl_grpServers = new GridLayout(2, false);
        gl_grpServers.marginWidth = 0;
        {
            GC gc = new GC(parent);
            gc.setFont(parent.getFont());
            FontMetrics fm = gc.getFontMetrics();
            Point extent = gc.textExtent("M");

            int hb = extent.y + 8;
            int hm = extent.y + 2;
            int wm = extent.x + 2;
            int ht = extent.y + 6;

            Group grpStatus = new Group(parent, SWT.NONE);
            GridLayout gl_grpStatus = new GridLayout(7, false);
            gl_grpStatus.verticalSpacing = 3;
            gl_grpStatus.marginWidth = 2;
            gl_grpStatus.marginHeight = 0;
            gl_grpStatus.horizontalSpacing = 2;
            grpStatus.setLayout(gl_grpStatus);
            GridData gd_grpStatus = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
            gd_grpStatus.heightHint = hm * 3 + 4;
            grpStatus.setLayoutData(gd_grpStatus);
            grpStatus.setText("Engine Status");

            new Label(grpStatus, SWT.NONE);
            serverLabel = new Label(grpStatus, SWT.NONE);
            serverLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
            GridData gd_serverLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1);
            gd_serverLabel.heightHint = 16;
            serverLabel.setLayoutData(gd_serverLabel);
            serverLabel.setText("connecting...");
            new Label(grpStatus, SWT.NONE);

            Label lblVersion = new Label(grpStatus, SWT.NONE);
            lblVersion.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            lblVersion
                    .setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
            lblVersion.setText("Version:");

            verLabel = new Label(grpStatus, SWT.NONE);
            verLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            GridData gd_verLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_verLabel.widthHint = 96;
            verLabel.setLayoutData(gd_verLabel);

            Label lblMemory = new Label(grpStatus, SWT.NONE);
            lblMemory.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            lblMemory
                    .setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
            lblMemory.setText("Memory:");

            memLabel = new Label(grpStatus, SWT.NONE);
            memLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            GridData gd_memLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_memLabel.widthHint = 85;
            memLabel.setLayoutData(gd_memLabel);

            Label lblSince = new Label(grpStatus, SWT.NONE);
            lblSince.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            lblSince.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
            lblSince.setText("Up:");

            upLabel = new Label(grpStatus, SWT.NONE);
            upLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.NORMAL));
            GridData gd_upLabel = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
            gd_upLabel.widthHint = 96;
            upLabel.setLayoutData(gd_upLabel);
            new Label(grpStatus, SWT.NONE);

            userLabel = new Label(grpStatus, SWT.NONE);
            userLabel
                    .setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 6, 1));

        }

        /*
         * I would actually prefer a single click here, but OK
         */
        
        sashForm = new SashForm(parent, SWT.VERTICAL);
        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        
                taskArea = new Group(sashForm, SWT.NONE);
                taskArea.setText("Tasks");
                taskArea.setLayout(new FillLayout(SWT.HORIZONTAL));
                
                        taskViewer = new TableViewer(taskArea, SWT.BORDER | SWT.FULL_SELECTION);
                        tableTasks = taskViewer.getTable();
                        tableTasks.setHeaderVisible(true);
                        tableTasks.setLinesVisible(true);
                        
                                TableColumn taskStatus = new TableColumn(tableTasks, SWT.NONE);
                                taskStatus.setResizable(false);
                                taskStatus.setWidth(29);
                                
                                        TableColumn taskCommand = new TableColumn(tableTasks, SWT.LEFT);
                                        taskCommand.setWidth(255);
                                        taskCommand.setText("Observable");
                                        
                                                TableColumn taskInterrupt = new TableColumn(tableTasks, SWT.NONE);
                                                taskInterrupt.setResizable(false);
                                                taskInterrupt.setWidth(48);
                                                
                                                        grpMessages = new Group(sashForm, SWT.NONE);
                                                        grpMessages.setText("Log");
                                                        grpMessages.setLayout(new FillLayout(SWT.HORIZONTAL));
                                                        
                                                                tableViewer = new TableViewer(grpMessages, SWT.BORDER | SWT.FULL_SELECTION);
                                                                tableMessages = tableViewer.getTable();
                                                                tableMessages.setLinesVisible(true);
                                                                
                                                                        tableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
                                                                        tblclmnNewColumn = tableViewerColumn.getColumn();
                                                                        tblclmnNewColumn.setAlignment(SWT.CENTER);
                                                                        tblclmnNewColumn.setResizable(false);
                                                                        tblclmnNewColumn.setWidth(22);
                                                                        
                                                                                tableViewerColumn_1 = new TableViewerColumn(tableViewer, SWT.NONE);
                                                                                tblclmnNewColumn_1 = tableViewerColumn_1.getColumn();
                                                                                tblclmnNewColumn_1.setWidth(500);
                                                                                
                                                                                        tableViewer.setContentProvider(ArrayContentProvider.getInstance());
                                                                                        tableViewer.setLabelProvider(new ServerLabelProvider());
                                                                                        
                                                                                                taskViewer.setContentProvider(ArrayContentProvider.getInstance());
                                                                                                taskViewer.setLabelProvider(new TaskLabelProvider());
                                                                                                taskViewer.addDoubleClickListener(new IDoubleClickListener() {

                                                                                                    @Override
                                                                                                    public void doubleClick(DoubleClickEvent event) {
                                                                                                        Object o = ((StructuredSelection) (event.getSelection()))
                                                                                                                .getFirstElement();
                                                                                                        Activator.getDefault()
                                                                                                                .fireEvent(new TaskEvent((ITask) o, TaskEvent.FOCUS));
                                                                                                    }
                                                                                                });
                                                                                                sashForm.setWeights(new int[] {1, 1});

        Eclipse.addActivationListener(this, new IActivatorListener() {
            @Override
            public void onActivation() throws KlabException {
                redisplay();
            }
        });

        createActions();
        hookGlobalActions();
        initializeToolBar();
        initializeMenu();

    }

    private void hookGlobalActions() {
        IActionBars bars = getViewSite().getActionBars();
        bars.setGlobalActionHandler(ActionFactory.COPY.getId(), copyItemAction);
    }

    /**
     * Create the actions.
     */
    private void createActions() {

        {
            copyItemAction = new Action("Copy...") {
                @Override
                public void run() {
                    StringBuilder sb = new StringBuilder();
                    for (org.eclipse.swt.widgets.TableItem ti : tableViewer.getTable()
                            .getItems()) {
                        Notification notification = (Notification) ti.getData();
                        sb.append(notification.getBody() + "\n");
                    }

                    Clipboard clipboard = new Clipboard(Display.getCurrent());
                    TextTransfer textTransfer = TextTransfer.getInstance();
                    Transfer[] transfers = new Transfer[] { textTransfer };
                    Object[] data = new Object[] { sb.toString() };
                    clipboard.setContents(data, transfers);
                    clipboard.dispose();
                }
            };
        }
        {
            showNetwork = new Action("Set online status", IAction.AS_CHECK_BOX) {

                @Override
                public void run() {
                    Activator.engine().setOnlineStatus(showNetwork.isChecked());
                }
            };
            showNetwork.setToolTipText("Connect or disconnect from the IM network");
            showNetwork.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Globe.png"));
            showNetwork.setChecked(!KLAB.CONFIG.isOffline());
            // TODO move to a menu with confirmation; switch back to the previous "show network" behavior
            showNetwork.setEnabled(false);
        }
        {
            cleaningCycle = new Action("Local engine operations", IAction.AS_DROP_DOWN_MENU) {
                @Override
                public void run() {
                    EngineLauncher launcher = Activator.getDefault().getEngineLauncher();
                    Source previous = launcher.getActiveSource();
                    EngineDialog ptc = new EngineDialog(Eclipse
                            .getShell(), launcher, Activator.getDefault()
                                    .getCertificate().getGroups());
                    if (ptc.open() == Dialog.OK) {

                        if (launcher.isRememberSettings()) {
                            launcher.persistProperties();
                        }

                        boolean isOnline = launcher.isOnline();

                        if (isOnline && launcher.isStopEngineNow()) {
                            try {
                                new ProgressMonitorDialog(Eclipse.getShell())
                                        .run(true, true, new IRunnableWithProgress() {

                                            @Override
                                            public void run(IProgressMonitor monitor)
                                                    throws InvocationTargetException,
                                                    InterruptedException {
                                                monitor.beginTask("Stopping engine...", IProgressMonitor.UNKNOWN);
                                                launcher.shutdown(10);
                                                monitor.done();
                                            }
                                        });
                            } catch (InvocationTargetException | InterruptedException e) {
                                Eclipse.handleException(e);
                            }

                        } else if (launcher.getActiveSource().requiresRestart(previous)
                                || launcher.isRestartRequested()) {
                            try {
                                new ProgressMonitorDialog(Eclipse.getShell())
                                        .run(true, true, new IRunnableWithProgress() {

                                            @Override
                                            public void run(IProgressMonitor monitor)
                                                    throws InvocationTargetException,
                                                    InterruptedException {
                                                monitor.beginTask("Stopping engine...", IProgressMonitor.UNKNOWN);
                                                launcher.shutdown(10);
                                                monitor.done();
                                            }
                                        });
                            } catch (InvocationTargetException | InterruptedException e) {
                                Eclipse.handleException(e);
                            }
                            if (launcher.isRestartRequested()) {
                                try {
                                    new ProgressMonitorDialog(Eclipse.getShell())
                                            .run(true, true, new IRunnableWithProgress() {

                                                @Override
                                                public void run(IProgressMonitor monitor)
                                                        throws InvocationTargetException,
                                                        InterruptedException {
                                                    monitor.beginTask("Cleaning up...", IProgressMonitor.UNKNOWN);
                                                    launcher.runCleaningCycle();
                                                    monitor.done();
                                                }
                                            });
                                } catch (InvocationTargetException
                                        | InterruptedException e) {
                                    Eclipse.handleException(e);
                                }
                            }
                            try {

                                /*
                                 * we manage this.
                                 */
                                automaticConnectMode = false;

                                new ProgressMonitorDialog(Eclipse.getShell())
                                        .run(true, true, new IRunnableWithProgress() {

                                            @Override
                                            public void run(IProgressMonitor monitor)
                                                    throws InvocationTargetException,
                                                    InterruptedException {
                                                monitor.beginTask("Restarting engine...", IProgressMonitor.UNKNOWN);
                                                launcher.launch(true);
                                                monitor.done();
                                            }
                                        });

                                /*
                                 * wait for connection
                                 */
                                Job init = new Job("Rebooting k.LAB engine") {

                                    @Override
                                    public IStatus run(IProgressMonitor monitor) {
                                        try {
                                            Activator.engine().boot();
                                        } catch (KlabException e) {
                                            Eclipse.handleException(e);
                                            return Status.CANCEL_STATUS;
                                        } finally {
                                            automaticConnectMode = true;
                                        }
                                        return Status.OK_STATUS;
                                    }
                                };

                                init.schedule();

                            } catch (Exception e) {
                                Eclipse.handleException(e);
                            }
                        }

                        /*
                         * TODO check if launcher has changes: 1. stop engine called -
                         * stop the engine; 2. changes requested that require restart
                         * (clean other than GC, switch to other source) - restart engine
                         * 3. GC requested - run cleanup
                         */
                    }
                }
            };
            cleaningCycle.setMenuCreator(new IMenuCreator() {

                Menu fMenu = null;

                @Override
                public Menu getMenu(Menu parent) {
                    return null;
                }

                @Override
                public Menu getMenu(Control parent) {

                    if (fMenu != null) {
                        fMenu.dispose();
                    }

                    fMenu = new Menu(parent);
                    if (KLAB.ENGINE != null) {
                        for (EngineData engine : Activator.engine().getEngines()) {
                            if (!engine.equals(Activator.engine().getCurrentEngine())) {
                                addActionToMenu(fMenu, new SwitchEngineAction(engine));
                            }
                        }
                    }
                    return fMenu;
                }

                protected void addActionToMenu(Menu parent, Action action) {
                    ActionContributionItem item = new ActionContributionItem(action);
                    item.fill(parent, -1);
                }

                @Override
                public void dispose() {
                }
            });
            cleaningCycle.setToolTipText("Stop, clean or reconfigure the local engine");
            cleaningCycle.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Gear.png"));
        }

        {
            stopServer = new Action("Stop engine") {
                @Override
                public void run() {
                    statusStarting("stopping");
                    try {
                        Activator.engine().stop();
                    } catch (Exception e) {
                        Activator.engine().getMonitor().error(e);
                    }
                }
            };
            stopServer.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/stop.gif"));
        }

        {
            startServer = new Action("Start engine") {
                @Override
                public void run() {
                    statusStarting("starting");
                    try {
                        Activator.engine().start();
                    } catch (Exception e) {
                        Activator.engine().getMonitor().error(e);
                    }
                }
            };
            startServer.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/start.gif"));
        }
        {
            action_1 = new Action("Show network graph") {

                @Override
                public void run() {
                    try {
                        PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                                .getActivePage()
                                .showView(NetworkView.ID);
                        Activator.getDefault()
                                .fireEvent(new NetworkEvent(NetworkEvent.VIEW_REQUEST));
                    } catch (Exception e) {
                        Activator.engine().getMonitor().error(e);
                    }
                }

            };
        }
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager toolbarManager = getViewSite().getActionBars()
                .getToolBarManager();
        if (updateThinkcap != null) {
            toolbarManager.add(updateThinkcap);
        }
        toolbarManager.add(cleaningCycle);
        toolbarManager.add(showNetwork);
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
        menuManager.add(action_1);
    }

    @Override
    public void setFocus() {
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        super.dispose();
    }

    @Override
    public void handleThinklabEvent(final ThinklabEvent event) {

        /*
         * we check the server status at each event
         */
        // checkServer();

        if (event instanceof TaskEvent) {
            final TaskEvent taskEvent = (TaskEvent) event;
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    taskViewer.setInput(taskEvent.task.getContext().getTasks());
                }
            });
        } else if (event instanceof EngineEvent) {

            if (((EngineEvent) event).type == EngineEvent.HEARTBEAT) {

                if (Activator.engine().getCurrentEngine() != null
                        && Activator.engine().getCurrentEngine()
                                .equals(((EngineEvent) event).engine)) {

                    statusRunning();

                    final EngineStatus status = ((EngineEvent) event).status;

                    Display.getDefault().asyncExec(new Runnable() {

                        @Override
                        public void run() {

                            String mem = status.getFreeMemory() + "/"
                                    + status.getTotalMemory() + " MB";
                            memLabel.setText(mem);
                            Object version = status.getVersion();
                            long uptime = status.getLastEngineTime()
                                    - status.getBootTime();
                            verLabel.setText(version == null ? "?" : version.toString());
                            upLabel.setText(yearsAndMonths.print(new Period(uptime)));
                        }
                    });
                }
            } else if (((EngineEvent) event).type == EngineEvent.ONLINE) {

                if (automaticConnectMode && Activator.engine().getCurrentSession() == null
                        && ((EngineEvent) event).engine.isLocal()) {
                    try {
                        Activator.engine().connect(((EngineEvent) event).engine);
                        statusRunning();
                    } catch (KlabException e) {
                        Eclipse.handleException(e);
                    }
                }

                this.engines.add(((EngineEvent) event).engine);

            } else if (((EngineEvent) event).type == EngineEvent.OFFLINE) {

                if (Activator.engine().getCurrentEngine() != null
                        && Activator.engine().getCurrentEngine()
                                .equals(((EngineEvent) event).engine)) {
                    statusStopped();
                }
                this.engines.remove(((EngineEvent) event).engine);

            } else if (((EngineEvent) event).type == EngineEvent.WAITING) {
                if (Activator.engine().getCurrentEngine() != null
                        && Activator.engine().getCurrentEngine()
                                .equals(((EngineEvent) event).engine)) {
                    statusStarting("working");
                }
            }

        } else if (event instanceof NetworkEvent) {
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {
                    boolean green = ((NetworkEvent) event).type != NetworkEvent.OFFLINE;
                    showNetwork
                            .setToolTipText(green ? "Primary server is online"
                                    : "Primary server is offline");
                    showNetwork.setImageDescriptor(green
                            ? ResourceManager
                                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/GreenGlobe.png")
                            : ResourceManager
                                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/RedGlobe.png"));
                }
            });
        } else if (event instanceof NotificationEvent) {

            NotificationEvent neven = (NotificationEvent) event;
            if (neven.session == null || neven.task == null) {
                Display.getDefault().asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        tableViewer.setInput(Activator.engine().getNotifications());
                    }
                });
            }

        } else if (event instanceof SessionEvent) {

            // SessionEvent seven = (SessionEvent) event;
            statusEmpty();

        } else if (event instanceof AuthenticationEvent) {

            final AuthenticationEvent ev = (AuthenticationEvent) event;
            Display.getDefault().asyncExec(new Runnable() {
                @Override
                public void run() {

                    serverLabel.setText(Activator.engine().getDescription());

                    String userStatus = "";

                    if (ev.user != null) {
                        switch (ev.user.getOnlineStatus()) {
                        case ONLINE:
                            userStatus = " (online)";
                            break;
                        case OFFLINE:
                            userStatus = " (offline)";
                            break;
                        case UNKNOWN:
                            userStatus = " (logging in...)";
                            break;
                        }

                        if (KLAB.CONFIG.isOffline()) {
                            userStatus = " (sandbox mode)";
                        }

                        userLabel.setForeground(Display.getDefault()
                                .getSystemColor(SWT.COLOR_BLACK));
                        if (ev.user.getOnlineStatus() == IUser.Status.ONLINE) {
                            if (ev.user.getGroups().contains("ADMIN")) {
                                userLabel.setForeground(Display.getDefault()
                                        .getSystemColor(SWT.COLOR_DARK_RED));
                            } else {
                                userLabel.setForeground(Display.getDefault()
                                        .getSystemColor(SWT.COLOR_DARK_GREEN));
                            }

                        } else if (ev.user.getOnlineStatus() == IUser.Status.UNKNOWN) {
                            userLabel.setForeground(Display.getDefault()
                                    .getSystemColor(SWT.COLOR_DARK_GRAY));
                        } else if (!ev.user.isAnonymous()) {
                            userLabel.setForeground(Display.getDefault()
                                    .getSystemColor(SWT.COLOR_RED));
                        }

                        if (KLAB.CONFIG.isOffline()) {
                            userLabel.setForeground(Display.getDefault()
                                    .getSystemColor(SWT.COLOR_DARK_BLUE));
                        }

                        String prefix = ev.user.isAnonymous() ? "Unregistered user: "
                                : (ev.user.getOnlineStatus() == IUser.Status.ONLINE
                                        ? (ev.user.getGroups().contains("ADMIN")
                                                ? "Administering as "
                                                : "Connected as ")
                                        : "");

                        if (KLAB.CONFIG.isOffline()) {
                            prefix = "";
                        }

                        userLabel.setText(prefix + ev.user.toString() + userStatus);

                        cleaningCycle.setEnabled(!ev.user.isAnonymous());
                    }
                }
            });
        }

    }

    private void redisplay() {
        if (Activator.engine().isRunning()) {
            statusRunning();
        } else {
            statusStopped();
        }
    }

    private void statusStarting(final String what) {

        this.isRunning = false;

        Display.getDefault().syncExec(new Runnable() {

            @Override
            public void run() {

                startServer.setEnabled(false);
                stopServer.setEnabled(false);
                serverLabel
                        .setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
                serverLabel.setText(what + "...");
            }
        });
    }

    private void statusRunning() {

        if (this.isRunning) {
            return;
        }

        this.isRunning = true;

        Display.getDefault().syncExec(new Runnable() {

            @Override
            public void run() {
                serverLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
                serverLabel.setText(Activator.engine().getDescription());
                startServer.setEnabled(false);
                stopServer.setEnabled(true);
                cleaningCycle.setImageDescriptor(ResourceManager
                        .getPluginImageDescriptor("org.integratedmodelling.thinkcap", Activator
                                .getDefault()
                                .getEngineLauncher().canUpgrade()
                                        ? "icons/YellowGear.png"
                                        : "icons/GreenGear.png"));
            }
        });

    }

    private void statusStopped() {

        this.isRunning = false;

        Display.getDefault().syncExec(new Runnable() {

            @Override
            public void run() {
                startServer.setEnabled(true);
                stopServer.setEnabled(false);
                verLabel.setText("");
                memLabel.setText("");
                upLabel.setText("");
                serverLabel
                        .setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
                serverLabel.setText("Stopped.");
                taskViewer.setInput(null);
                tableViewer.setInput(null);
                cleaningCycle.setImageDescriptor(ResourceManager
                        .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Gear.png"));
            }
        });

    }

    private void statusEmpty() {
        //
        // this.status = 0;
        this.isRunning = false;

        Display.getDefault().syncExec(new Runnable() {

            @Override
            public void run() {
                taskViewer.setInput(null);
                tableViewer.setInput(null);
            }
        });

    }
}
