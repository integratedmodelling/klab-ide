/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.thinkcap.views;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.integratedmodelling.api.data.IExport;
import org.integratedmodelling.api.data.IExport.Aggregation;
import org.integratedmodelling.api.data.IExport.Format;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.IHistogram;
import org.integratedmodelling.api.modelling.visualization.ILegend;
import org.integratedmodelling.api.modelling.visualization.IViewer.Display;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask.Status;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.beans.responses.ValueSummary;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.referencing.BookmarkManager;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.runtime.Provenance;
import org.integratedmodelling.common.client.runtime.Task;
import org.integratedmodelling.common.client.viewer.ContextStructure.Folder;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.ExportableArtifact;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.knowledge.ObservationGroup;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Subject;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.image.ImageUtil;
import org.integratedmodelling.common.visualization.ColorMap;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Observations;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.thinkcap.Activator;
import org.integratedmodelling.thinkcap.Eclipse;
import org.integratedmodelling.thinkcap.dialogs.MapViewerDialog;
import org.integratedmodelling.thinkcap.dialogs.ReportDialog;
import org.integratedmodelling.thinkcap.events.ContextEvent;
import org.integratedmodelling.thinkcap.events.EngineEvent;
import org.integratedmodelling.thinkcap.events.IThinklabEventListener;
import org.integratedmodelling.thinkcap.events.SessionEvent;
import org.integratedmodelling.thinkcap.events.ShapeCreatedEvent;
import org.integratedmodelling.thinkcap.events.TaskEvent;
import org.integratedmodelling.thinkcap.events.ThinklabEvent;
import org.integratedmodelling.thinkcap.events.TransitionEvent;
import org.integratedmodelling.thinkcap.ui.DataButtonPanel;
import org.integratedmodelling.thinkcap.ui.DataButtonPanel.Interaction;
import org.integratedmodelling.thinkcap.ui.DataButtonPanel.Run;
import org.integratedmodelling.thinkcap.ui.EclipseViewer;
import org.integratedmodelling.thinkcap.ui.GraphBrowser;
import org.integratedmodelling.thinkcap.ui.ObservationBox;
import org.integratedmodelling.thinkcap.ui.TimeBar;
import org.integratedmodelling.thinkcap.ui.TimeBar.Listener;
import org.integratedmodelling.thinkcap.wizards.ExportDataWizard;
import org.integratedmodelling.thinkcap.wizards.ExportModelWizard;

/**
 * @author ferdinando.villa
 */
public class DatasetView extends ViewPart implements IThinklabEventListener {

    /**
     * ID of view in Eclipse config.
     */
    public static final String ID                             = "org.integratedmodelling.thinkcap.views.DatasetView"; //$NON-NLS-1$

    private final FormToolkit  toolkit                        = new FormToolkit(org.eclipse.swt.widgets.Display
            .getCurrent());

    private CheckboxTreeViewer treeViewer;
    private GraphBrowser       graphBrowser;
    private EclipseViewer      dataViewer;

    private boolean            startWithObservationBarVisible = false;
    private boolean            startWithTreeVisible           = true;

    // private Tree tree;
    private Tree               scenarioTree;
    private SashForm           dataArea;

    Interaction                infoMode                       = Interaction.PAN;

    private Composite          mapMenuArea;
    private Label              dataNameLabel;
    private Font               nFont;
    private Font               bFont;
    private Font               iFont;
    private Font               biFont;
    private TreeViewer         scenarioViewer;
    // FIXME obsolete
    private boolean            isMapVisible                   = true;
    private Label              histogram;
    private Label              colormap;
    private TimeBar            timebar;
    private Button             editShapes;
    private Label              dataValue;
    private Label              showMapMenu;
    private Slider             slider;

    private DataButtonPanel    ctrlButtons;
    private Composite          histogramDisplayArea;
    private Composite          composite;
    private Composite          composite_2;

    private Composite          composite_3;
    private Label              dataHistogram;
    private Button             externalBrowser;
    // private String contextId;
    private Action             showNamesAction;
    private Action             searchAction;
    private Action             showTreeAction;
    private ObservationBox     searchBox;
    private SashForm           sashForm;
    private Composite          visualizerArea;
    private Composite          parent;

    class ObjInfo {
        String info;
        Object parent;

        ObjInfo(String info, Object parent) {
            this.info = info;
            this.parent = parent;
        }
    }

    class ResultCheckStateProvider implements ICheckStateProvider {

        @Override
        public boolean isChecked(Object element) {
            return (element instanceof IObservation
                    && dataViewer.getView().isShown(((IObservation) element)))
                    || (element instanceof Folder
                            && Environment.get().getContextStructure()
                                    .containsFolder(((Folder) element)));
        }

        @Override
        public boolean isGrayed(Object element) {
            return element instanceof IProcess;
        }

    }

    public class ResultDropListener extends ViewerDropAdapter {

        protected ResultDropListener(Viewer viewer) {
            super(viewer);
        }

        @Override
        public boolean performDrop(Object data) {
            Object target = getCurrentTarget();
            if (!(target instanceof IObservation)) {
                return false;
            }
            Object toDrop = data;
            if (toDrop instanceof String && (((String) toDrop).startsWith("@BM|"))
                    || ((String) toDrop).startsWith("@PI|")) {

                IConcept role = BookmarkManager.getConcept((String) toDrop);

                if (role != null && NS.isRole(role)) {
                    Collection<IConcept> applicable = Observables
                            .getApplicableObservables(role);
                    if (applicable.isEmpty()
                            || NS.isContained(((IObservation) target).getObservable()
                                    .getType(), applicable)) {
                        ((AbstractContext) Environment.get().getContext()).getScenario()
                                .addRoleAttribution((IObservation) target, role);

                        /*
                         * set the role in the observation and update the icon.
                         */
                        ((Observation) target).getExplicitRoles().add(role);

                        treeViewer.update(target, null);

                    } else {
                        Eclipse.alert("Role " + role
                                + " can not be attributed to observable "
                                + ((IObservation) target).getObservable().getType());
                    }
                } else {
                    Eclipse.alert("Drop action is used only to set roles for observed objects");
                }
            }
            return false;
        }

        @Override
        public boolean validateDrop(Object target, int operation, TransferData transferType) {

            /*
             * only good to drop right above observations
             */
            int loc = getCurrentLocation();
            if (loc == LOCATION_BEFORE || loc == LOCATION_AFTER) {
                return false;
            }

            if (Environment.get().getContext() != null
                    && target instanceof IObservation) {
                /*
                 * check we're dropping a role - apparently we can't
                 */
                return true;
            }
            return false;
        }

    }

    public class ResultContentProvider implements ITreeContentProvider {

        @Override
        public void dispose() {
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            return Environment.get().getContextStructure().getChildren(inputElement);
        }

        @Override
        public Object[] getChildren(Object parent) {
            return Environment.get().getContextStructure().getChildren(parent);
        }

        @Override
        public Object getParent(Object element) {
            if (Environment.get().getContextStructure() != null) {
                return Environment.get().getContextStructure().getParent(element);
            }
            return null;
        }

        @Override
        public boolean hasChildren(Object element) {
            return Environment.get().getContextStructure().hasChildren(element);
        }
    }

    class ResultLabelProvider extends BaseLabelProvider implements ILabelProvider {

        @Override
        public Image getImage(Object element) {

            Image ret = null;

            if (element instanceof ISubject) {
                ret = ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/observer.gif");
            } else if (element instanceof Folder) {
                ret = ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, ((Folder) element)
                                .isRelationshipFolder()
                                        ? "icons/expand-tree.gif" : "icons/group16.png");
            } else if (element instanceof IRelationship) {
                ret = ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/Link.png");
            } else if (element instanceof IProcess) {
                ret = ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/Gear.png");
            } else if (element instanceof IState) {

                // TODO make thumbnail from latest data if available
                if (((IState) element).isSpatiallyDistributed()) {
                    ret = ((IState) element).isDynamic()
                            ? ResourceManager.decorateImage(ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/raster.png"), ResourceManager
                                            .getPluginImage(Activator.PLUGIN_ID, "icons/timeangrp_ovr.gif"), SWTResourceManager.TOP_RIGHT)
                            : ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/raster.png");
                } else if (((IState) element).isTemporallyDistributed()) {
                    ret = ((IState) element).isDynamic()
                            ? ResourceManager.decorateImage(ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/Stats2.png"), ResourceManager
                                            .getPluginImage(Activator.PLUGIN_ID, "icons/timeangrp_ovr.gif"), SWTResourceManager.TOP_RIGHT)
                            : ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/Stats2.png");
                } else {
                    ret = ((IState) element).isDynamic()
                            ? ResourceManager.decorateImage(ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/Poll.png"), ResourceManager
                                            .getPluginImage(Activator.PLUGIN_ID, "icons/timeangrp_ovr.gif"), SWTResourceManager.TOP_RIGHT)
                            : ResourceManager
                                    .getPluginImage(Activator.PLUGIN_ID, "icons/Poll.png");
                }

            } else if (element instanceof ILegend) {
                // TODO make thumbnail for legend
                ret = ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "icons/colormap.gif");
            } else if (element instanceof ExportableArtifact) {

                ExportableArtifact a = (ExportableArtifact) element;
                ret = ResourceManager.getPluginImage(Activator.PLUGIN_ID, a.isModel()
                        ? "icons/model.gif"
                        : "icons/Dataset.gif");
            }

            /*
             * set a green decorator if we have given this any explicit role.
             */
            if (element instanceof Observation
                    && !((Observation) element).getExplicitRoles().isEmpty()) {
                ret = ResourceManager.decorateImage(ret, ResourceManager
                        .getPluginImage(Activator.PLUGIN_ID, "/icons/dot_green_decorator.png"), SWTResourceManager.TOP_RIGHT);
            }

            return ret;
        }

        @Override
        public String getText(Object element) {

            if (element instanceof ISubject) {
                return dataViewer.getLabel((Observation) element) + " ["
                        + NS.getDisplayName(((Observation) element).getObservable()
                                .getType())
                        + "]";
            } else if (element instanceof IState) {
                ISubject ss = ((IState) element).getObservingSubject();
                return dataViewer.getLabel((Observation) element)
                        + (ss == null ? "" : (" (per " + ss.getName() + ")"));
            } else if (element instanceof Observation) {
                return dataViewer.getLabel((Observation) element);
            } else if (element instanceof Folder) {
                return ((Folder) element).isRelationshipFolder()
                        ? ("Relationships [" + ((Folder) element).size() + "]")
                        : (NS.getDisplayName(((Folder) element).getObservable().getType())
                                + " ["
                                + ((Folder) element).size() + "]");
            } else if (element instanceof ExportableArtifact) {
                return ((ExportableArtifact) element).getName();
            }

            return null;
        }
    }

    public DatasetView() {
        Activator.getDefault().addThinklabEventListener(this);
    }

    public void showTree(boolean show, boolean setControl) {
        sashForm.setMaximizedControl(show ? null : visualizerArea);
        if (setControl) {
            showTreeAction.setChecked(show);
        }
    }

    public void pack() {
        this.parent.pack();
        this.parent.layout(true);
    }

    /**
     * Create contents of the view part.
     * 
     * @param parent
     */
    @Override
    public void createPartControl(Composite parent) {

        parent.setLayout(new GridLayout(2, false));

        this.parent = parent;

        sashForm = new SashForm(parent, SWT.NONE);
        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        final Composite treeArea = new Composite(sashForm, SWT.BORDER);
        GridLayout gl_composite_1 = new GridLayout(1, false);
        gl_composite_1.verticalSpacing = 2;
        gl_composite_1.marginWidth = 2;
        gl_composite_1.marginHeight = 2;
        treeArea.setLayout(gl_composite_1);

        treeViewer = new CheckboxTreeViewer(treeArea, SWT.BORDER);
        final Tree tree_1 = treeViewer.getTree();
        tree_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        createFontVariants(tree_1.getFont());

        treeViewer.setContentProvider(new ResultContentProvider());
        treeViewer.setLabelProvider(new ResultLabelProvider());
        treeViewer.setCheckStateProvider(new ResultCheckStateProvider());
        treeViewer.addCheckStateListener(new ICheckStateListener() {

            @Override
            public void checkStateChanged(CheckStateChangedEvent event) {
                display(event.getElement(), event.getChecked());
            }
        });

        /*
         * drop roles on observations
         */
        treeViewer.addDropSupport(DND.DROP_COPY
                | DND.DROP_MOVE, new Transfer[] {
                        // BookmarkTransfer.getInstance(),
                        TextTransfer.getInstance(),
                        LocalSelectionTransfer
                                .getTransfer() }, new ResultDropListener(treeViewer));

        MenuManager menuMgr = new MenuManager();
        menuMgr.setRemoveAllWhenShown(true);
        menuMgr.addMenuListener(new IMenuListener() {
            @Override
            public void menuAboutToShow(IMenuManager manager) {
                fillObservationMenu(manager);
            }
        });
        Menu menu = menuMgr.createContextMenu(treeViewer.getControl());
        treeViewer.getControl().setMenu(menu);
        treeViewer.addDoubleClickListener(new IDoubleClickListener() {
            @Override
            public void doubleClick(DoubleClickEvent event) {
                TreeItem item = tree_1.getSelection()[0];
                Object obs = item.getData();
                if (obs instanceof ExportableArtifact) {
                    ((ClientSession) Activator.engine().getCurrentSession())
                            .getEngineController()
                            .editArtifact(Environment.get()
                                    .getContext(), (ExportableArtifact) obs);
                } else if (obs instanceof ISubject) {
                    if (Environment.get().getContextStructure()
                            .hasSubjectiveObservations((ISubject) obs)) {
                        Environment.get().setPovSubject((ISubject) obs);
                        treeViewer.setInput(Environment.get().getContextStructure());
                        showChecked();
                    }
                    if (((IDirectObservation) obs).getScale().getSpace() != null) {
                        dataViewer.focus((IDirectObservation) obs);
                    }
                } else if (obs instanceof IDirectObservation
                        && ((IDirectObservation) obs).getScale().getSpace() != null) {
                    dataViewer.focus((IDirectObservation) obs);
                }
            }
        });

        getSite().registerContextMenu(menuMgr, treeViewer);

        Section scenarioArea = toolkit
                .createSection(treeArea, ExpandableComposite.TWISTIE);
        scenarioArea.setToolTipText("All scenarios run on this context");
        scenarioArea.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        toolkit.paintBordersFor(scenarioArea);
        scenarioArea.setText("Scenarios");

        scenarioViewer = new TreeViewer(scenarioArea, SWT.BORDER | SWT.FULL_SELECTION);
        scenarioTree = scenarioViewer.getTree();
        scenarioTree.setLinesVisible(true);
        scenarioViewer.addDoubleClickListener(new IDoubleClickListener() {
            @Override
            public void doubleClick(DoubleClickEvent event) {
                TreeItem item = scenarioTree.getSelection()[0];
                Object obs = item.getData();
                if (obs instanceof GraphVisualization) {
                    showGraph((GraphVisualization) obs);
                }
            }
        });
        toolkit.paintBordersFor(scenarioTree);
        scenarioArea.setClient(scenarioTree);
        // scenarioViewer.setContentProvider(new HistoryContentProvider());
        // scenarioViewer.setLabelProvider(new HistoryLabelProvider());

        visualizerArea = new Composite(sashForm, SWT.NONE);
        visualizerArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        toolkit.adapt(visualizerArea);
        toolkit.paintBordersFor(visualizerArea);
        GridLayout gl_composite_4 = new GridLayout(1, false);
        gl_composite_4.horizontalSpacing = 1;
        gl_composite_4.marginHeight = 0;
        gl_composite_4.marginWidth = 0;
        gl_composite_4.verticalSpacing = 0;
        visualizerArea.setLayout(gl_composite_4);
        GridLayout gl_dataArea = new GridLayout(2, false);
        gl_dataArea.verticalSpacing = 0;
        gl_dataArea.marginWidth = 0;
        gl_dataArea.marginHeight = 0;
        gl_dataArea.horizontalSpacing = 0;
        GridData gd_graphBrowser = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
        gd_graphBrowser.exclude = true;

        GridLayout gl_composite = new GridLayout(1, false);
        gl_composite.marginWidth = 0;
        gl_composite.marginHeight = 0;
        gl_composite.verticalSpacing = 0;
        gl_composite.horizontalSpacing = 1;

        composite = new Composite(visualizerArea, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        toolkit.adapt(composite);
        toolkit.paintBordersFor(composite);
        GridLayout gl_composite2 = new GridLayout(2, false);
        gl_composite2.verticalSpacing = 0;
        gl_composite2.marginWidth = 0;
        gl_composite2.horizontalSpacing = 0;
        gl_composite2.marginHeight = 0;
        composite.setLayout(gl_composite2);

        dataArea = new SashForm(composite, SWT.VERTICAL);
        dataArea.setSashWidth(0);
        dataArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        dataArea.setLayout(gl_dataArea);
        dataArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));

        // for some reason the graph browser does not get resized with the area.
        dataArea.addControlListener(new ControlListener() {
            @Override
            public void controlResized(ControlEvent e) {
                if (!isMapVisible) {
                    graphBrowser.setSize(dataArea.getSize());
                } else {
                    dataViewer.setSize(dataArea.getSize());
                }
            }

            @Override
            public void controlMoved(ControlEvent e) {
            }
        });

        if (!startWithTreeVisible) {
            sashForm.setMaximizedControl(dataArea);
        }

        composite_2 = new Composite(dataArea, SWT.NONE);
        toolkit.adapt(composite_2);
        toolkit.paintBordersFor(composite_2);
        GridLayout gl_composite_2 = new GridLayout(1, false);
        gl_composite_2.verticalSpacing = 0;
        gl_composite_2.marginWidth = 0;
        gl_composite_2.marginHeight = 0;
        composite_2.setLayout(gl_composite_2);
        composite_2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

        class SearchObservationBox extends ObservationBox {

            public SearchObservationBox(Composite parent, ViewPart view, int style) {
                super(parent, view, style);
            }

            @Override
            protected Composite showResultArea(boolean show) {
                Composite ret = dataViewer.showSearchResults(show, dataArea.getSize());
                if (!show) {
                    dataViewer.setSize(dataArea.getSize());
                    dataViewer.pack();
                }
                return ret;
            }

            @Override
            protected void cancelSearch() {
                showResultArea(false);
                showObservationBar(false);
                searchAction.setChecked(false);
            }

        }
        ;

        searchBox = new SearchObservationBox(composite_2, this, SWT.NONE);
        searchBox.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        if (!startWithObservationBarVisible) {
            ((GridData) searchBox.getLayoutData()).exclude = true;
            searchBox.setVisible(false);
        }

        dataViewer = new EclipseViewer(composite_2, 0, SWT.NONE) {

            @Override
            protected void recordShape(String shape) {
                if (featureWizard != null) {
                    featureWizard.setShape(shape);
                } else {
                    DatasetView.this.recordShape(shape);
                }
            }

            @Override
            protected void handleLocationClick(double lat, double lon) {
                if (featureWizard != null) {
                    featureWizard.setShape("POINT (" + lat + " " + lon + ")");
                } else {
                    DatasetView.this.handleLocationClick(lat, lon);
                }
            }

        };
        // dataViewer.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true,
        // true, 1, 1));
        dataViewer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
        toolkit.adapt(dataViewer);
        toolkit.paintBordersFor(dataViewer);

        graphBrowser = new GraphBrowser(dataArea, SWT.NONE);
        GridLayout gridLayout = (GridLayout) graphBrowser.getLayout();
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        gridLayout.verticalSpacing = 0;
        // graphBrowser.setEnabled(true);
        graphBrowser.setLayoutData(gd_graphBrowser);
        toolkit.adapt(graphBrowser);
        toolkit.paintBordersFor(graphBrowser);

        mapMenuArea = new Composite(composite, SWT.NONE);
        GridLayout gl_mapMenuArea = new GridLayout(1, false);
        gl_mapMenuArea.marginBottom = 1;
        gl_mapMenuArea.marginTop = 1;
        gl_mapMenuArea.marginRight = 1;
        gl_mapMenuArea.marginLeft = 1;
        gl_mapMenuArea.horizontalSpacing = 0;
        gl_mapMenuArea.verticalSpacing = 3;
        gl_mapMenuArea.marginWidth = 0;
        gl_mapMenuArea.marginHeight = 0;
        mapMenuArea.setLayout(gl_mapMenuArea);
        mapMenuArea.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, false, true, 1, 1));
        ((GridData) (mapMenuArea.getLayoutData())).widthHint = 0;
        // toolkit.adapt(mapMenuArea);
        // toolkit.paintBordersFor(mapMenuArea);
        mapMenuArea.setVisible(false);
        mapMenuArea.setEnabled(false);

        Button cycleMaps = new Button(mapMenuArea, SWT.NONE);
        cycleMaps.setToolTipText("Cycle background maps");
        cycleMaps.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                dataViewer.cycleView();
            }
        });
        cycleMaps.setSize(28, 26);
        cycleMaps.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/Globe.png"));

        editShapes = new Button(mapMenuArea, SWT.TOGGLE);
        editShapes.setToolTipText("Start recording spatial objects");
        editShapes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                editMode(editShapes.getSelection());
            }
        });
        editShapes.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/Write3.png"));

        final boolean useExternal = KLAB.CONFIG.getProperties()
                .getProperty("klab.browser.external", "false")
                .equals("true");
        externalBrowser = new Button(mapMenuArea, SWT.TOGGLE);
        externalBrowser.setToolTipText("Use external browser (requires restart)");
        externalBrowser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                KLAB.CONFIG.getProperties()
                        .setProperty("klab.browser.external", externalBrowser
                                .getSelection() ? "true" : "false");
                KLAB.CONFIG.save();
                Eclipse.info("New settings will be applied after k.LAB is restarted.");
            }
        });
        externalBrowser
                .setImage(ResourceManager
                        .getPluginImage("org.integratedmodelling.thinkcap", "icons/Player Eject.png"));
        externalBrowser.setSelection(useExternal);

        slider = new Slider(mapMenuArea, SWT.VERTICAL);
        slider.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, false, false, 1, 1));
        slider.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (e.detail != SWT.DRAG) {
                    setTransparency(slider.getSelection());
                }
            }
        });
        slider.setSelection(100);
        slider.setToolTipText("Set transparency of top layer");

        dataArea.setWeights(new int[] { 1, 0 });

        timebar = new TimeBar(visualizerArea, SWT.NONE);
        GridData gd_timebar = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
        gd_timebar.heightHint = 8;
        timebar.setLayoutData(gd_timebar);
        toolkit.paintBordersFor(timebar);
        timebar.addListener(new Listener() {

            @Override
            public void timeSelected(long time) {
                if (Environment.get().getContext() != null) {
                    if (Environment.get().getContext().isRunning()) {
                        /*
                         * TODO remove restriction when we figure out how to avoid race
                         * conditions or EOF errors on engine when reading datasets being
                         * written to.
                         */
                        Eclipse.beep();
                    } else {
                        TimeLocator locator = TimeLocator
                                .get(Environment.get().getContext(), time);
                        dataViewer.locate(locator);
                        updateTime(locator, time);
                    }
                }
            }
        });

        Composite controls = new Composite(visualizerArea, SWT.BORDER);
        GridLayout gl_controls = new GridLayout(5, false);
        gl_controls.marginWidth = 0;
        gl_controls.verticalSpacing = 1;
        gl_controls.marginHeight = 0;
        controls.setLayout(gl_controls);
        controls.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        controls.setBounds(0, 0, 64, 64);
        toolkit.adapt(controls);
        toolkit.paintBordersFor(controls);

        ctrlButtons = new DataButtonPanel(controls, SWT.NONE, new DataButtonPanel.Listener() {

            @Override
            public void onStopSelected() {
                if (Environment.get().getFocusTask() != null
                        && Environment.get().getFocusTask()
                                .getStatus() == Status.RUNNING) {
                    Environment.get().getFocusTask().interrupt();
                }
            }

            @Override
            public void onRunSelected(Run run) {
                if (run == Run.RUN) {
                    try {
                        Environment.get().getContext().run();
                        // Activator.client().fireEvent(new
                        // SessionEvent(_context));
                    } catch (KlabException e) {
                        Activator.engine().getMonitor().error(e);
                    }
                }
            }

            @Override
            public void onInteractionChanged(Interaction interaction) {
                infoMode = interaction;
                showValue((String) null);
            }

            @Override
            public void onReportSelected() {

                ReportDialog dialog = new ReportDialog(getSite()
                        .getShell(), ((ClientSession) Activator.engine()
                                .getCurrentSession())
                                        .getEngineController()
                                        .getContextReport(Environment.get()
                                                .getContext()));
                dialog.open();

            }

            @Override
            public void onDisplayChanged(Display[] displays) {

                boolean hasGeomapper = false;
                boolean hasGraph = false;

                for (Display v : displays) {
                    if (v != null) {
                        if (v == Display.MAP || v == Display.TIMEPLOT
                                || v == Display.FLOWS) {
                            hasGeomapper = true;
                        } else if (v == Display.STRUCTURE || v == Display.PROVENANCE) {
                            hasGraph = true;
                        }
                    }
                }

                int gsize = hasGeomapper ? 1 : 0;
                int graph = hasGraph ? 1 : 0;

                dataArea.setWeights(new int[] { gsize, graph });

                ArrayList<Display> mapVis = new ArrayList<>();

                for (Display v : displays) {
                    if (v != null) {
                        switch (v) {
                        case FLOWS:
                            mapVis.add(v);
                            dataViewer.show(Environment.get().getContext().getSubject(), v);
                            break;
                        case MAP:
                            mapVis.add(v);
                            break;
                        case PROVENANCE:
                            if (Environment.get().getContext() != null) {
                                Provenance provenance = ((ClientSession) Activator
                                        .engine()
                                        .getCurrentSession())
                                                .getEngineController()
                                                .getProvenance(Environment.get()
                                                        .getContext());

                                graphBrowser.show(provenance.visualize());
                            }
                            break;
                        case STRUCTURE:
                            graphBrowser
                                    .show(Environment.get().getContext().getSubject());
                            break;
                        case TIMEPLOT:
                            mapVis.add(v);
//                            dataViewer.show();
                            break;
                        }
                    }
                    // only the first.
                    break;
                }
            }

            @Override
            public void onAdditionalMapClick() {
                dataViewer.cycleView();
            }

            @Override
            public void onMapDoubleClick() {
                dataViewer.resetView();
            }

        });
        GridData gd_ctrlButtons = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_ctrlButtons.widthHint = 120;
        ctrlButtons.setLayoutData(gd_ctrlButtons);
        toolkit.adapt(ctrlButtons);
        toolkit.paintBordersFor(ctrlButtons);

        dataNameLabel = new Label(controls, SWT.SHADOW_NONE);
        GridData gd_dataNameLabel = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_dataNameLabel.widthHint = 188;
        dataNameLabel.setLayoutData(gd_dataNameLabel);
        dataNameLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.BOLD));
        toolkit.adapt(dataNameLabel, true, true);

        histogramDisplayArea = new Composite(controls, SWT.BORDER);
        histogramDisplayArea
                .setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        GridLayout gl_histogramDisplayArea = new GridLayout(1, false);
        gl_histogramDisplayArea.horizontalSpacing = 0;
        gl_histogramDisplayArea.marginHeight = 0;
        gl_histogramDisplayArea.marginWidth = 0;
        gl_histogramDisplayArea.verticalSpacing = 0;
        histogramDisplayArea.setLayout(gl_histogramDisplayArea);
        toolkit.adapt(histogramDisplayArea);
        toolkit.paintBordersFor(histogramDisplayArea);

        histogram = new Label(histogramDisplayArea, SWT.SHADOW_NONE);
        histogram.setAlignment(SWT.CENTER);
        GridData gd_histogram = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
        gd_histogram.widthHint = 40;
        gd_histogram.heightHint = 16;
        histogram.setLayoutData(gd_histogram);
        toolkit.adapt(histogram, true, true);

        colormap = new Label(histogramDisplayArea, SWT.SHADOW_NONE);
        colormap.setAlignment(SWT.CENTER);
        GridData gd_colormap = new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1);
        gd_colormap.widthHint = 40;
        gd_colormap.heightHint = 4;
        colormap.setLayoutData(gd_colormap);
        toolkit.adapt(colormap, true, true);

        composite_3 = new Composite(controls, SWT.NONE);
        composite_3.setLayout(new GridLayout(2, false));
        composite_3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        toolkit.adapt(composite_3);
        toolkit.paintBordersFor(composite_3);

        dataValue = new Label(composite_3, SWT.NONE);
        dataValue.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        dataValue.setAlignment(SWT.CENTER);
        dataValue.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        dataValue.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.BOLD));
        toolkit.adapt(dataValue, true, true);

        dataHistogram = new Label(composite_3, SWT.NONE);
        GridData gd_dataHistogram = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_dataHistogram.widthHint = 40;
        dataHistogram.setVisible(false);
        dataHistogram.setLayoutData(gd_dataHistogram);
        toolkit.adapt(dataHistogram, true, true);

        showMapMenu = new Label(controls, SWT.NONE);
        showMapMenu.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", "icons/stackbars_hor.png"));
        GridData gd_showMapMenu = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
        gd_showMapMenu.widthHint = 16;
        showMapMenu.setLayoutData(gd_showMapMenu);
        showMapMenu.setAlignment(SWT.CENTER);
        showMapMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseUp(MouseEvent e) {
                toggleMapMenu();
            }
        });
        showMapMenu.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        showMapMenu.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.BOLD));
        toolkit.adapt(showMapMenu, true, true);

        sashForm.setWeights(new int[] { 176, 405 });
        new Label(parent, SWT.NONE);

        createActions();
        initializeToolBar();
        initializeMenu();
    }

    class ExportArtifactAction extends Action {

        private IObservation observation;
        private boolean      enab = true;
        ExportableArtifact   artifact;

        ExportArtifactAction(String name, ExportableArtifact artifact) {
            super(name);
            this.artifact = artifact;
        }

        @Override
        public boolean isEnabled() {
            return enab;
        }

        public ExportArtifactAction enable(boolean b) {
            enab = b;
            return this;
        }

        @Override
        public void run() {

            File output = null;

            LocalExportResponse result = ((ClientSession) Activator.engine()
                    .getCurrentSession())
                            .getEngineController()
                            .exportArtifact(Environment.get()
                                    .getContext(), artifact, output);

            if (result != null && artifact.isModel() && result.getModelStatement() != null
                    && !result.getModelStatement().isEmpty()) {

                /*
                 * open wizard with model and export options
                 */
                ExportModelWizard wizard = new ExportModelWizard(result);
                WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
                dialog.setBlockOnOpen(true);
                dialog.create();
                if (dialog.open() == Window.OK) {
                    /*
                     * export
                     */
                }
            } else if (result != null && !artifact.isModel()
                    && result.getFiles().size() > 0) {

                String[] fileType = new String[] {
                        "*." + MiscUtilities.getFileExtension(result.getFiles().get(0)) };

                FileDialog dialog = new FileDialog(getSite().getShell(), SWT.SAVE);
                dialog.setFilterExtensions(fileType);
                boolean hadPath = false;
                File path = new File(System.getProperty("user.home"));
                if (KLAB.CONFIG.getDefaultExportPath() != null) {
                    path = KLAB.CONFIG.getDefaultExportPath();
                    hadPath = true;
                }
                dialog.setFilterPath(path.getPath());
                String ret = dialog.open();
                if (ret == null) {
                    return;
                }

                String pth = MiscUtilities.getFilePath(ret);
                String prf = MiscUtilities.getFileBaseName(ret);
                for (String s : result.getFiles()) {
                    String ext = MiscUtilities.getFileExtension(s);
                    try {
                        FileUtils.copyFile(new File(s), new File(pth + File.separator
                                + prf + "." + ext));
                    } catch (IOException e) {
                        Eclipse.handleException(e);
                        break;
                    }
                }

            }
        }
    };

    class ExportAction extends Action {

        private Format             format;
        private Aggregation        aggregation;
        private String[]           fileType;
        private IObservation       observation;
        private Collection<IState> observations;
        private boolean            enab = true;

        ExportAction(String name, IObservation observation, IExport.Format format,
                IExport.Aggregation aggregation,
                String... fileType) {
            super(name);
            this.format = format;
            this.aggregation = aggregation;
            this.fileType = fileType;
            this.observation = observation;
        }

        @Override
        public boolean isEnabled() {
            return enab;
        }

        public ExportAction enable(boolean b) {
            enab = b;
            return this;
        }

        @Override
        public void run() {

            if (this.format == Format.KBOX) {

                // TODO this should produce a menu of destination servers with
                // Local
                // preselected, then
                // do the export from inside the dialog.

                String urn = ((ClientSession) Activator.engine().getCurrentSession())
                        .getEngineController()
                        .storeObservation(Environment.get()
                                .getContext(), this.observation);
                if (urn != null) {
                    Eclipse.info("Observation saved with URN " + urn);
                }
                return;
            }

            /*
             * get output file
             */
            FileDialog dialog = new FileDialog(getSite().getShell(), SWT.SAVE);
            dialog.setFilterExtensions(fileType);
            boolean hadPath = false;
            File path = new File(System.getProperty("user.home"));
            if (KLAB.CONFIG.getDefaultExportPath() != null) {
                path = KLAB.CONFIG.getDefaultExportPath();
                hadPath = true;
            }
            dialog.setFilterPath(path.getPath());
            String result = dialog.open();
            if (result == null) {
                return;
            }
            File file = new File(result);
            try {

                if (observation != null) {
                    Environment.get().getContext()
                            .persist(file, Environment.get().getContext()
                                    .getPathFor(observation), Environment
                                            .get().getCurrentTime(), format, aggregation);
                } else {
                    // TODO
                }

                /*
                 * remember path if we didn't have it
                 */
                if (!hadPath) {
                    KLAB.CONFIG.getProperties()
                            .setProperty("klab.export.path", file.getPath());
                    KLAB.CONFIG.save();
                }
            } catch (KlabException e) {
                Eclipse.handleException(e);
            }
        }
    };

    protected void fillObservationMenu(IMenuManager menu) {

        TreeItem item = treeViewer.getTree().getSelection()[0];
        final Object obs = item.getData();

        if (!(obs instanceof ExportableArtifact)) {

            MenuManager roleMenu = new MenuManager("Set role");

            roleMenu.add(new Action("New...") {

                @Override
                public boolean isEnabled() {
                    // TODO Auto-generated method stub
                    return false;
                }
            });

            /*
             * TODO add separator and compatible previously used roles
             */
            roleMenu.add(new Separator());

            // TODO Auto-generated method stub
            menu.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));

            menu.add(new Action("Show in viewer") {
                @Override
                public void run() {
                    treeViewer.setChecked(obs, !treeViewer.getChecked(obs));
                    display(obs, treeViewer.getChecked(obs));
                }
            });

            if (obs instanceof IObservation && ((IObservation) obs).getMetadata() != null
                    && !((IObservation) obs).getMetadata().isEmpty()) {
                menu.add(new Action("View medatata") {
                    @Override
                    public void run() {
                        MapViewerDialog ptc = new MapViewerDialog(Eclipse
                                .getShell(), new ResultLabelProvider()
                                        .getText(obs), ((IObservation) obs).getMetadata()
                                                .getDataAsMap());
                        if (ptc.open() == Dialog.OK) {
                            return;
                        }
                    }
                });
            }

            menu.add(roleMenu);
        }

        if (obs instanceof IObservation /* as it should always be */) {

            if (!(obs instanceof ExportableArtifact)
                    && !(obs instanceof ObservationGroup)) {

                if (NS.isQuality(((IObservation) obs).getObservable().getType())) {
                    Action provideData = new Action("Provide own data..") {
                        @Override
                        public void run() {
                            // TODO wizard
                        }
                    };
                    provideData.setEnabled(false);
                    menu.add(provideData);
                }

                if (NS.isCountable(((IObservation) obs).getObservable().getType())) {
                    Action copyObservation = new Action("Copy definition") {
                        @Override
                        public void run() {
                            String def = Observations
                                    .getKIMDefinition((IDirectObservation) obs);
                            if (def != null) {
                                Eclipse.copyToClipboard(def);
                            }
                        }
                    };
                    menu.add(copyObservation);
                }
            }

            /*
             * export, man
             */
            menu.add(getExportMenu((IObservation) obs));
        }
    }

    /**
     * The complex export menu. We will want to categorize export types (as an array of
     * possible option sets with labels) in the vis factory and just create the menus
     * here, so we can export from other clients and APIs more easily.
     * 
     * @param observation
     * @return
     */
    protected MenuManager getExportMenu(IObservation observation) {

        MenuManager ret = new MenuManager("Export");

        boolean isSpatial = observation.getScale().isSpatiallyDistributed();
        boolean isRaster = isSpatial
                && observation.getScale().getSpace().getGrid() != null;
        boolean isTemporal = observation.getScale().isTemporallyDistributed();
        boolean hasAllStates = Environment.get().getContext().isFinished();

        String timeString = isTemporal
                ? (Environment.get().getCurrentTime() == null ? " @ init"
                        : (" @ " + Environment.get().getCurrentTime()))
                : "";

        if (observation instanceof IState) {

            if (isSpatial) {
                if (isRaster) {

                    ret.add(new ExportAction("Raster file (geotiff)"
                            + timeString, observation, IExport.Format.SCIENTIFIC_DATASET, IExport.Aggregation.AVERAGE, "*.tif", "*.tiff"));

                    if (isTemporal) {
                        // movie
                        ret.add(new ExportAction("Video file (gif)", observation, IExport.Format.VIDEO, IExport.Aggregation.AVERAGE, "*.gif")
                                .enable(hasAllStates));
                        // aggregated raster
                        ret.add(new ExportAction("Aggregated data (geotiff)", observation, IExport.Format.SCIENTIFIC_DATASET, IExport.Aggregation.TOTAL, "*.tif", "*.tiff")
                                .enable(hasAllStates));
                    }

                } else {
                    ret.add(new ExportAction("Vector file (shp)"
                            + timeString, observation, IExport.Format.SCIENTIFIC_DATASET, IExport.Aggregation.AVERAGE, "*.shp")
                                    .enable(false));
                }
            } else if (isTemporal) {
                ret.add(new ExportAction("Timeseries data (csv)", observation, IExport.Format.SCIENTIFIC_DATASET, IExport.Aggregation.AVERAGE, "*.csv")
                        .enable(hasAllStates));
            }

        } else if (observation instanceof ISubject || observation instanceof IProcess
                || observation instanceof IEvent) {

            if (isSpatial) {
                ret.add(new ExportAction("Vector file (shp)"
                        + timeString, observation, IExport.Format.STATE_COLLECTION_FILE, IExport.Aggregation.AVERAGE, "*.shp")
                                .enable(false));
            }

            if (((IDirectObservation) observation).getStates().size() > 0) {
                if (isSpatial) {
                    ret.add(new ExportAction("All states (zip)"
                            + timeString, observation, IExport.Format.STATE_COLLECTION_FILE, IExport.Aggregation.AVERAGE, "*.zip"));
                    if (isTemporal) {
                        ret.add(new ExportAction("All states (netcdf)"
                                + timeString, observation, IExport.Format.NETCDF, IExport.Aggregation.AVERAGE, "*.nc")
                                        .enable(false /* TODO hasAllStates */));
                        ret.add(new ExportAction("Aggregated states (zip)", observation, IExport.Format.STATE_COLLECTION_FILE, IExport.Aggregation.TOTAL, "*.zip")
                                .enable(false /* TODO hasAllStates */));
                    }
                } else {
                    ret.add(new ExportAction("State values (csv)"
                            + timeString, observation, IExport.Format.CSV, IExport.Aggregation.AVERAGE, "*.csv")
                                    .enable(false));
                    if (isTemporal) {
                        ret.add(new ExportAction("Aggregated state values (csv)", observation, IExport.Format.CSV, IExport.Aggregation.TOTAL, "*.csv")
                                .enable(false /* TODO hasAllStates */));
                    }
                }
            }

            /*
             * subject things
             */
            if (observation instanceof ISubject) {
                if (observation.equals(Environment.get().getContext().getSubject())) {
                    ret.add(new Separator());
                    ret.add(new ExportAction("Provenance graph (graphML)"
                            + timeString, observation, IExport.Format.GRAPH, IExport.Aggregation.AVERAGE, "*.gml")
                                    .enable(false));
                    ret.add(new ExportAction("All knowledge (OWL)"
                            + timeString, observation, IExport.Format.OWL, IExport.Aggregation.AVERAGE, "*.owl")
                                    .enable(false));
                    if (hasAllStates) {
                        ret.add(new ExportAction("Contextualization report (pdf)", observation, IExport.Format.PDF, IExport.Aggregation.AVERAGE, "*.pdf")
                                .enable(false /* TODO hasAllStates */));
                    }
                    ret.add(new Separator());
                }
                ret.add(new ExportAction("Full dataset (zip)"
                        + timeString, observation, IExport.Format.EVERYTHING, IExport.Aggregation.AVERAGE, "*.zip")
                                .enable(false));

                /*
                 * export to CKAN and others, only for root context.
                 */
                if (observation.equals(Environment.get().getContext().getSubject())) {
                    ret.add(new Separator());
                    ret.add(new ExportAction("Publish..."
                            + timeString, observation, IExport.Format.SITE, IExport.Aggregation.AVERAGE, "*.zip")
                                    .enable(false));
                }
            }

        } else if (observation instanceof Folder) {

            // assume all objects are equally spatial or not
            IObservation o = ((Folder) observation).iterator().next();
            boolean spatialObjs = o.getScale().getSpace() != null;

            /*
             * multi-objects: shapefile, csv, timeline
             */
            if (NS.isThing(observation.getObservable())) {
                // shapefile, csv
                if (spatialObjs) {
                    ret.add(new ExportAction("Vector file (shp)", observation, IExport.Format.OBJECT_COLLECTION_FILE, IExport.Aggregation.AVERAGE, "*.shp")/*
                                                                                                                                                            * .enable
                                                                                                                                                            * (
                                                                                                                                                            * false)
                                                                                                                                                            */);
                } else {
                    ret.add(new ExportAction("Comma-separated data (csv)", observation, IExport.Format.CSV, IExport.Aggregation.AVERAGE, "*.csv")/*
                                                                                                                                                  * .enable
                                                                                                                                                  * (
                                                                                                                                                  * false)
                                                                                                                                                  */);
                }

            } else if (NS.isEvent(observation.getObservable())) {
                if (spatialObjs) {
                    ret.add(new ExportAction("Event coverage (shp)", observation, IExport.Format.OBJECT_COLLECTION_FILE, IExport.Aggregation.AVERAGE, "*.shp")
                            .enable(false));
                } else {
                    ret.add(new ExportAction("Event timeline (csv)", observation, IExport.Format.CSV, IExport.Aggregation.AVERAGE, "*.csv")
                            .enable(false));
                }
            }
        } else if (observation instanceof ExportableArtifact) {

            ret.add(new ExportArtifactAction("Save "
                    + ((ExportableArtifact) observation).getLabel()
                    + "...", (ExportableArtifact) observation));

        }

        if (!(observation instanceof Folder)) {
            // TODO this should produce a menu of destination servers with Local
            // preselected, then
            // do the export from inside the dialog.
            ret.add(new ExportAction("Save and display URN", observation, IExport.Format.KBOX, IExport.Aggregation.AVERAGE, "")
                    .enable(true));

        }

        return ret;
    }

    protected boolean export() {
        ExportDataWizard wizard = new ExportDataWizard(Environment.get().getContext());
        WizardDialog dialog = new WizardDialog(getSite().getShell(), wizard);
        dialog.setBlockOnOpen(true);
        dialog.create();
        return dialog.open() == Window.OK;
    }

    protected void recordShape(String shapeDef) {
        Activator.getDefault().fireEvent(new ShapeCreatedEvent(shapeDef));
    }

    private Iterable<Locator> getLocators() {
        ArrayList<Locator> ret = new ArrayList<>();
        if (Environment.get().getContext() != null) {
            IScale scale = Environment.get().getContext().getSubject().getScale();
            if (scale.getSpace() != null) {
                ret.add(SpaceLocator.all());
            }
            if (dataViewer.getView()
                    .getTime() != null /* && state.isDynamic() */) {
                ret.add(dataViewer.getView().getTime());
            }
        }
        return ret;
    }

    protected void updateTime(TimeLocator time, final long millis) {

        Environment.get().setCurrentTime(time);

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                // timeLabel.setText("" + new Date(millis));
                if (dataViewer.getVisibleSpatialStates().size() > 0) {
                    // FIXME add this back - just a repaint of the states with
                    // the new
                    // locators
                    // dataViewer.show(dataViewer.getVisibleSpatialStates(),
                    // getLocators());
                    ctrlButtons.setForState(dataViewer.getVisibleSpatialState());
                    dataValue.setText("");

                    if (dataViewer
                            .getVisibleSpatialState() != null /*
                                                               * which is really should
                                                               * not be...
                                                               */) {
                        try {
                            Pair<IColormap, IHistogram> hm = dataViewer
                                    .getStateSummary(dataViewer.getVisibleSpatialState());
                            IHistogram hist = hm == null ? null : hm.getSecond();
                            IColormap cmap = hm == null ? null : hm.getFirst();
                            if (hist == null) {
                                histogram.setImage(null);
                            } else {
                                Image image = new Image(org.eclipse.swt.widgets.Display
                                        .getCurrent(), Eclipse
                                                .convertToSWT((BufferedImage) hist
                                                        .getImage(40, 16)));
                                histogram.setImage(image);
                                histogram.setToolTipText(hist.getDescription());
                            }
                            if (cmap == null) {
                                colormap.setImage(null);
                            } else {
                                // the SWT conversion fails with some images.
                                File f = ((ColorMap) cmap).saveImage(40, 4);
                                colormap.setImage(ResourceManager.getImage(f.toString()));
                            }
                        } catch (KlabException e) {
                            Activator.engine().getMonitor().error(e);
                        }
                    }
                }
            }
        });

    }

    /**
     * Report coordinates; if in info mode, also request data value at location and
     * display it.
     * 
     * @param lat
     * @param lon
     */
    protected void handleLocationClick(final double lat, final double lon) {

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {

                if (infoMode == Interaction.INFO && Environment.get().getContext() != null
                        && dataViewer.getVisibleSpatialState() != null) {
                    ArrayList<Locator> locators = new ArrayList<>();
                    if (Environment.get().getContext().getSubject().getScale()
                            .getTime() != null
                            && Environment.get().getContext().getSubject().getScale()
                                    .getTime()
                                    .getMultiplicity() > 1) {

                        Locator timeLocator = dataViewer.getVisibleSpatialState()
                                .isDynamic()
                                        ? dataViewer.getView().getTime()
                                        : TimeLocator.initialization();

                        locators.add(timeLocator);

                    }
                    locators.add(SpaceLocator.get(lon, lat));

                    long idx = Environment.get().getContext().getSubject().getScale()
                            .locate(locators.toArray(new Locator[locators.size()]));

                    if (idx >= 0) {
                        showValue(dataViewer.getValueDescriptor(dataViewer
                                .getVisibleSpatialState(), idx));
                    }

                } else if (infoMode == Interaction.COORDINATES) {

                    String locdesc = NumberFormat.getInstance().format(lat) + ", "
                            + NumberFormat.getInstance().format(lon);
                    showValue(locdesc);

                } else {
                    showValue((String) null);
                }
            }
        });
    }

    protected void showValue(ValueSummary value) {
        if (value == null) {
            dataValue.setText("");
            dataValue.setToolTipText("");
            dataHistogram.setVisible(false);
        } else if (value.getDistribution() != null) {
            String description = value == null ? "" : value.getDescription();
            if (value.getMostLikelyClass() != null) {
                IKnowledge c = Knowledge.parse(value.getMostLikelyClass());
                description = c.getLocalName();
                dataValue.setToolTipText("Most likely category");
            }
            dataHistogram.setVisible(true);
            dataValue.setText(description);
            dataValue.setSize(new Point(description.length() * 6, dataValue.getSize().y));
            dataHistogram.setToolTipText(value == null ? "" : value.getDescription());
            dataHistogram.setImage(new Image(org.eclipse.swt.widgets.Display
                    .getCurrent(), Eclipse
                            .convertToSWT((BufferedImage) ImageUtil
                                    .drawHistogram(value.getDistribution(), 30, 20))));

        } else if (value.getDescription() != null) {

            dataValue.setText(value == null ? "" : value.getDescription());
            dataValue.setToolTipText(value == null ? "" : value.getDescription());
            dataHistogram.setVisible(false);
        }
        dataValue.getParent().getParent().layout();
    }

    protected void showValue(String value) {
        dataValue.setText(value == null ? "" : value.toString());
        dataValue.setToolTipText(value == null ? "" : value.toString());
        dataHistogram.setVisible(false);
        dataValue.getParent().getParent().layout();
    }

    public void showObservationBar(boolean show) {
        ((GridData) searchBox.getLayoutData()).exclude = !show;
        searchBox.setVisible(show);
        searchBox.getParent().pack();
        if (show) {
            searchBox.searchMode(true);
        }
        dataViewer.setSize(dataArea.getSize());
    }

    /*
     * Properly highlight the item position in view stack in the observation tree.
     * @param path
     */
    protected void showChecked() {

        ArrayList<TreeItem> items = new ArrayList<>();
        collectItems(treeViewer.getTree().getItems(), items);

        for (TreeItem item : items) {

            Object o = item.getData();

            boolean isSubjectiveState = false;
            boolean isCurrentlyObservedState = true;
            boolean isSubjectiveObserver = false;
            boolean isCurrentObserver = false;

            if (Environment.get().getContext() != null && o instanceof Observation) {

                isSubjectiveObserver = o instanceof ISubject && Environment.get()
                        .getContextStructure().hasSubjectiveObservations((ISubject) o);

                ISubject pov = Environment.get().getPovSubject();
                if (pov != null
                        && !pov.equals(Environment.get().getContext().getSubject())) {

                    if (o instanceof ISubject) {
                        isCurrentObserver = pov.equals(o);
                    } else if (o instanceof IState) {
                        isSubjectiveState = ((Observation) o)
                                .getObservingSubject() != null;
                        if (isSubjectiveState
                                && !((Observation) o).getObservingSubject().equals(pov)) {
                            isCurrentlyObservedState = false;
                        }
                    }
                }
            }

            if (o instanceof IState) {

                if (!isCurrentlyObservedState) {
                    item.setGrayed(true);
                } else {

                    String pth = Environment.get().getContext()
                            .getPathFor((IObservation) o);
                    if (Environment.get().getContext() != null
                            && dataViewer.getView()
                                    .isShown(Environment.get().getContext().get(pth))) {
                        item.setFont(bFont);
                        if (dataViewer.getVisibleSpatialState() != null
                                && dataViewer.getVisibleSpatialState().equals(o)) {
                            item.setForeground(org.eclipse.swt.widgets.Display
                                    .getCurrent()
                                    .getSystemColor(isSubjectiveState
                                            ? SWT.COLOR_DARK_GREEN : SWT.COLOR_DARK_RED));
                        }
                    } else {
                        item.setForeground(org.eclipse.swt.widgets.Display
                                .getCurrent()
                                .getSystemColor(isSubjectiveState ? SWT.COLOR_DARK_GREEN
                                        : SWT.COLOR_BLACK));
                    }
                }
            } else {
                item.setFont(isCurrentObserver ? bFont : nFont);
                item.setForeground(org.eclipse.swt.widgets.Display.getCurrent()
                        .getSystemColor(isSubjectiveObserver ? SWT.COLOR_DARK_BLUE
                                : SWT.COLOR_BLACK));
            }
        }
    }

    private void collectItems(TreeItem[] items, ArrayList<TreeItem> coll) {
        for (TreeItem i : items) {
            coll.add(i);
            collectItems(i.getItems(), coll);
        }
    }

    private void createFontVariants(Font font) {

        nFont = font;
        FontData[] fd = new FontData[font.getFontData().length];
        int i = 0;
        for (FontData f : font.getFontData()) {
            FontData rf = new FontData();
            rf.setHeight(f.getHeight());
            rf.setLocale(f.getLocale());
            rf.setName(f.getName());
            rf.setStyle(SWT.BOLD);
            fd[i++] = rf;
        }
        bFont = new Font(font.getDevice(), fd);
        FontData[] fd2 = new FontData[font.getFontData().length];
        i = 0;
        for (FontData f : font.getFontData()) {
            FontData rf = new FontData();
            rf.setHeight(f.getHeight());
            rf.setLocale(f.getLocale());
            rf.setName(f.getName());
            rf.setStyle(SWT.ITALIC);
            fd2[i++] = rf;
        }
        iFont = new Font(font.getDevice(), fd);
        FontData[] fd3 = new FontData[font.getFontData().length];
        i = 0;
        for (FontData f : font.getFontData()) {
            FontData rf = new FontData();
            rf.setHeight(f.getHeight());
            rf.setLocale(f.getLocale());
            rf.setName(f.getName());
            rf.setStyle(SWT.ITALIC | SWT.BOLD);
            fd3[i++] = rf;
        }
        biFont = new Font(font.getDevice(), fd);
    }

    protected void setTransparency(int selection) {
        if (Environment.get().getContext() != null
                && dataViewer.getVisibleSpatialState() != null) {
            dataViewer.setOpacity(dataViewer.getVisibleSpatialState(), selection / 100.0);
        }
    }

    protected void editMode(boolean on) {

        if (on) {
            try {
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
                        .showView(ObservationRecorder.ID);
            } catch (PartInitException e) {
                Eclipse.handleException(e);
            }
        }
        editShapes.setToolTipText(on ? "Stop recording spatial objects"
                : "Start recording spatial objects");
        dataViewer.setContextEditMode(on);
    }

    protected void toggleMapMenu() {

        boolean vis = !mapMenuArea.isVisible();
        showMapMenu.setImage(ResourceManager
                .getPluginImage("org.integratedmodelling.thinkcap", vis
                        ? "icons/stackbars_ver.png" : "icons/stackbars_hor.png"));
        ((GridData) (mapMenuArea.getLayoutData())).widthHint = (vis ? 28 : 0);
        mapMenuArea.setEnabled(vis);
        mapMenuArea.setVisible(vis);
        composite.layout();
    }

    // true, show graph browser; false, show geomapper
    protected void showBrowser(boolean graph) {

        if (graph && !isMapVisible || !graph && isMapVisible)
            return;

        ((GridData) graphBrowser.getLayoutData()).exclude = graph;
        ((GridData) dataViewer.getLayoutData()).exclude = !graph;
        graphBrowser.setVisible(graph);
        graphBrowser.setEnabled(graph);
        dataViewer.setVisible(!graph);
        dataViewer.setEnabled(!graph);
        if (graph) {
            graphBrowser.setSize(dataArea.getSize());
        }
        dataArea.layout();
        isMapVisible = !graph;
    }

    protected void display(final Object o, boolean checked) {

        showBrowser(false);

        if (o instanceof Folder) {

            /*
             * check all objects in folder and display them
             */
            for (IObservation obs : ((Folder) o)) {
                treeViewer.setChecked(obs, checked);
                display(obs, checked);
            }

        } else if (o instanceof IObservation) {

            if (checked) {
                dataViewer.show((IObservation) o);
            } else {
                dataViewer.hide((IObservation) o);
            }

            if (o instanceof IState) {

                ctrlButtons.setForState(dataViewer.getVisibleSpatialState());

                if (dataViewer.getVisibleSpatialState() == null) {

                    histogram.setImage(null);
                    colormap.setImage(null);
                    dataNameLabel.setText("");
                    dataViewer.clearAllStates();
                    slider.setEnabled(false);

                } else {

                    dataNameLabel.setText(dataViewer.getVisibleSpatialState()
                            .getObservable().getType()
                            .toString());
                    // FIXME change to observer description
                    // dataObserverLabel.setText(_currentState.getObservable().getObservationType().getLocalName());
                    // dataViewer.show(getDisplayedStates(), getLocators());
                    slider.setEnabled(true);
                    // stateExportButton.setVisible(true);
                    boolean hasColormap = false;
                    try {
                        Pair<IColormap, IHistogram> hm = dataViewer
                                .getStateSummary(((IState) o));
                        IHistogram hist = hm == null ? null : hm.getSecond();
                        IColormap cmap = hm == null ? null : hm.getFirst();
                        if (hist == null) {
                            histogram.setImage(null);
                        } else {
                            Image image = new Image(org.eclipse.swt.widgets.Display
                                    .getCurrent(), Eclipse
                                            .convertToSWT((BufferedImage) hist
                                                    .getImage(40, 16)));
                            histogram.setImage(image);
                            histogram.setToolTipText(hist.getDescription());
                            hasColormap = true;
                        }
                        if (cmap == null) {
                            colormap.setImage(null);
                        } else {
                            // the SWT conversion fails with some images.
                            File f = ((ColorMap) cmap).saveImage(40, 4);
                            colormap.setImage(ResourceManager.getImage(f.toString()));
                            hasColormap = true;
                        }
                    } catch (KlabException e) {
                        Activator.engine().getMonitor().error(e);
                    }

                    if (hasColormap) {
                        /*
                         * TODO set the border for the histogram area - which I don't know
                         * how to do, so it has a border even when empty now.
                         */
                    }
                }

                dataArea.redraw();
            }
            showChecked();
        }
    }

    protected void showGraph(final GraphVisualization o) {

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                showBrowser(true);
                graphBrowser.show(o);
            }
        });
    }

    @Override
    public void dispose() {
        Activator.getDefault().removeThinklabEventListener(this);
        bFont.dispose();
        toolkit.dispose();
        super.dispose();
    }

    private void createActions() {
        {
            showNamesAction = new Action("", Action.AS_CHECK_BOX) {

                @Override
                public void run() {
                    dataViewer.showNames(this.isChecked());
                }

            };
            showNamesAction.setToolTipText("Show subject names");
            showNamesAction.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/TextPlus.png"));
            showNamesAction.setChecked(true);
        }
        {
            searchAction = new Action("", Action.AS_CHECK_BOX) {

                @Override
                public void run() {
                    ((GridData) searchBox.getLayoutData()).exclude = !this.isChecked();
                    searchBox.setVisible(this.isChecked());
                    searchBox.getParent().pack();
                    if (this.isChecked()) {
                        searchBox.searchMode(true);
                    }
                    dataViewer.setSize(dataArea.getSize());
                }

            };
            searchAction.setToolTipText("Specify observations");
            searchAction.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Search.png"));
            searchAction.setChecked(startWithObservationBarVisible);
        }
        {
            showTreeAction = new Action("", Action.AS_CHECK_BOX) {

                @Override
                public void run() {
                    showTree(this.isChecked(), false);
                }

            };
            showTreeAction.setToolTipText("Specify observations");
            showTreeAction.setImageDescriptor(ResourceManager
                    .getPluginImageDescriptor("org.integratedmodelling.thinkcap", "icons/Tree.png"));
            showTreeAction.setChecked(startWithTreeVisible);
        }
    }

    /**
     * Initialize the toolbar.
     */
    private void initializeToolBar() {
        IToolBarManager tbm = getViewSite().getActionBars().getToolBarManager();
        tbm.add(showNamesAction);
        tbm.add(searchAction);
        tbm.add(showTreeAction);
    }

    /**
     * Initialize the menu.
     */
    private void initializeMenu() {
        IMenuManager manager = getViewSite().getActionBars().getMenuManager();
    }

    @Override
    public void setFocus() {
        // Set the focus
    }

    @Override
    public void handleThinklabEvent(ThinklabEvent event) {

        if (event instanceof SessionEvent) {
            SessionEvent seven = (SessionEvent) event;
            show(null, false);
            dataViewer.loadSession(seven.type == SessionEvent.CLOSED ? null
                    : (ClientSession) seven.session);
        } else if (event instanceof ContextEvent) {
            ContextEvent ceven = (ContextEvent) event;
            show(ceven.context, ceven.type == ContextEvent.NEW);
            if (ceven.context != null && ceven.context.isFinished()) {
                ctrlButtons.runFinished();
            }
        } else if (event instanceof EngineEvent) {

            if (Activator.engine().getCurrentEngine() != null
                    && Activator.engine().getCurrentEngine()
                            .equals(((EngineEvent) event).engine)) {
                if (((EngineEvent) event).type == EngineEvent.OFFLINE) {
                    reset();
                }
            }
        } else if (event instanceof TransitionEvent) {
            stepForward((TransitionEvent) event);
        } else if (event instanceof TaskEvent) {
            ITransition transition = ((Task) ((TaskEvent) event).task)
                    .getCurrentTransition();
            if (transition != null) {
                timebar.setComputedTime(transition.getTime().getEnd().getMillis());
            }
        }
    }

    /**
     * show passed context (initialized or finished)
     * 
     * @param context
     */
    public void show(final IContext context, boolean isNew) {

        // String contextId = Environment.get().getTargetSubject() == null ?
        // null
        // : Environment.get().getTargetSubject().getName();
        //
        boolean emptyContext = context == null || context.isEmpty();
        //
        // final boolean newContext = (emptyContext && contextId != null)
        // || (contextId == null && !emptyContext)
        // || (!emptyContext && !contextId.equals(context.getId()));
        //
        if (context == null) {
            reset();
        }

        //
        // this.contextId = context == null ? null : context.getId();

        if (!emptyContext && context != null
                && Environment.get().getCurrentTime() != null) {
            dataViewer.locate(Environment.get().getCurrentTime());
        }

        if (!emptyContext) {
            dataViewer.show(context);
        }

        ctrlButtons.setForContext(context, (context == null || context.isEmpty()
                || dataViewer == null
                || dataViewer.getView() == null) ? null
                        : dataViewer.getView().getTime());

        if (context == null || context.isEmpty()) {
            return;
        }

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                ISubject s = context.getSubject();
                setPartName(context.getSubject().getName());
                if (s.getScale().getSpace() != null) {
                    dataViewer.show(context);
                }
                if (s.getScale().getTime() != null) {
                    // if (isNew) {
                    // don't reset the scale unless it's a new context.
                    timebar.setScale(s.getScale().getTime());
                    // }
                }
                treeViewer.setInput(Environment.get().getContextStructure());
                if (((Subject) context.getSubject()).hasChildren()) {
                    treeViewer.expandToLevel(2);
                }
                showChecked();
            }
        });
    }

    private void stepForward(final TransitionEvent event) {

        // if (this.contextId == null ||
        // !event.context.getId().equals(this.contextId)) {
        // return;
        // }
        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {

                timebar.setComputedTime(event.transition.getTime().getEnd().getMillis());

                /*
                 * TODO get the list of modified observations from the event and only
                 * react if anything in the display list has changed. If things have
                 * changed that are not in the display list, adjust the tree and show
                 * something to indicate that what was there already has changed.
                 */

                /*
                 * let's say that for now, we just display what the user clicks on, and
                 * there's no automatic update. Breakpoints are only OK for debugging,
                 * otherwise way too slow and we should think of a timed update for
                 * displayed data that's independent of the run time.
                 */
                // if (_showOrder.size() > 0) {
                // /*
                // * if we have anything displayed, the engine is waiting for us
                // to
                // * display it and call restart.
                // */
                // for (String s : _showOrder) {
                // System.out.println("redisplaying " + s);
                // }
                //
                // /*
                // * restart
                // */
                // try {
                // Activator
                // .client()
                // .getEngineClient()
                // .send("context", "cmd", "breakpoint", "ctx",
                // _context.getId(),
                // "action", "start");
                // } catch (ThinklabException e) {
                // Eclipse.handleException(e);
                // }
                // }
            }
        });
    }

    private void reset() {

        Environment.get().setCurrentTime(null);

        org.eclipse.swt.widgets.Display.getDefault().asyncExec(new Runnable() {
            @Override
            public void run() {
                showBrowser(false);
                // contextId = null;
                histogram.setImage(null);
                colormap.setImage(null);
                dataNameLabel.setText("");
                dataViewer.clear();
                slider.setEnabled(false);
                treeViewer.setInput(null);
                graphBrowser.show(null);
                timebar.setScale(null);
                dataValue.setText("");
                ctrlButtons.setForContext(null);
            }
        });
    }

}
