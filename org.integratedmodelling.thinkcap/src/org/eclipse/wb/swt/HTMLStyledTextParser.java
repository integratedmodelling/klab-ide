/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.eclipse.wb.swt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;

/**
 * Instances of this class are used to convert HTML content of a styled text
 * into style ranges
 */
public class HTMLStyledTextParser {

    private final StyledText styledText;

    /**
     * Constructor
     * 
     * @param styledText styled text to analyze
     */
    public HTMLStyledTextParser(final StyledText styledText) {
        this.styledText = styledText;
    }

    /**
     * Parse the content, build the list of style ranges and apply them to the
     * styled text widget
     * 
     * @throws IOException
     */
    public void parse() throws IOException {
        if (this.styledText == null || "".equals(this.styledText.getText().trim())) {
            return;
        }
        final String text = this.styledText.getText().trim();
        int currentPosition = 0;
        final StringBuilder output = new StringBuilder();
        final List<StyleRange> listOfStyles = new ArrayList<StyleRange>();
        final LinkedList<StyleRange> stack = new LinkedList<StyleRange>();
        final int max = text.length();
        for (int i = 0; i < max; i++) {
            String currentTag;
            String currentClosingTag;
            String currentBRTag;
            if (i <= max - 4) {
                currentTag = text.substring(i, i + 3).toLowerCase();
            } else {
                currentTag = null;
            }

            if (i <= max - 5) {
                currentClosingTag = text.substring(i, i + 4).toLowerCase();
            } else {
                currentClosingTag = null;
            }

            if (i <= max - 5) {
                currentBRTag = text.substring(i, i + 5).toLowerCase();
            } else {
                currentBRTag = null;
            }

            if (currentTag != null) {
                if ("<b>".equalsIgnoreCase(currentTag)) {
                    final StyleRange currentStyleRange = new StyleRange();
                    currentStyleRange.start = currentPosition;
                    currentStyleRange.length = 0;
                    currentStyleRange.fontStyle = SWT.BOLD;
                    stack.push(currentStyleRange);
                    i += 2;
                    continue;
                }
                if ("<i>".equalsIgnoreCase(currentTag)) {
                    final StyleRange currentStyleRange = new StyleRange();
                    currentStyleRange.start = currentPosition;
                    currentStyleRange.length = 0;
                    currentStyleRange.fontStyle = SWT.ITALIC;
                    stack.push(currentStyleRange);
                    i += 2;
                    continue;
                }
                if ("<u>".equalsIgnoreCase(currentTag)) {
                    final StyleRange currentStyleRange = new StyleRange();
                    currentStyleRange.start = currentPosition;
                    currentStyleRange.length = 0;
                    currentStyleRange.fontStyle = SWT.NONE;
                    currentStyleRange.underline = true;
                    stack.push(currentStyleRange);
                    i += 2;
                    continue;
                }
            }

            if (currentClosingTag != null) {
                if ("</b>".equalsIgnoreCase(currentClosingTag)) {
                    final StyleRange currentStyleRange = stack.pop();
                    if ((currentStyleRange.fontStyle & SWT.BOLD) == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Error at position #").append(currentPosition)
                                .append(" - closing </b> tag found but ");
                        if ((currentStyleRange.fontStyle & SWT.ITALIC) != 0) {
                            sb.append("</i>");
                        } else {
                            sb.append("</u>");
                        }
                        sb.append(" tag expected !");
                        throw new RuntimeException(sb.toString());
                    }
                    currentStyleRange.length = currentPosition - currentStyleRange.start + 1;
                    listOfStyles.add(currentStyleRange);
                    i += 3;
                    continue;
                }

                if ("</i>".equalsIgnoreCase(currentClosingTag)) {
                    final StyleRange currentStyleRange = stack.pop();
                    if ((currentStyleRange.fontStyle & SWT.ITALIC) == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Error at position #").append(currentPosition)
                                .append(" - closing </i> tag found but ");
                        if ((currentStyleRange.fontStyle & SWT.BOLD) != 0) {
                            sb.append("</b>");
                        } else {
                            sb.append("</u>");
                        }
                        sb.append(" tag expected !");
                        throw new RuntimeException(sb.toString());
                    }
                    currentStyleRange.length = currentPosition - currentStyleRange.start + 1;
                    listOfStyles.add(currentStyleRange);
                    i += 3;
                    continue;
                }

                if ("</u>".equalsIgnoreCase(currentClosingTag)) {
                    final StyleRange currentStyleRange = stack.pop();
                    if (!currentStyleRange.underline) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Error at position #").append(currentPosition)
                                .append(" - closing </u> tag found but ");
                        if ((currentStyleRange.fontStyle & SWT.ITALIC) != 0) {
                            sb.append("</i>");
                        } else {
                            sb.append("</b>");
                        }
                        sb.append(" tag expected !");
                        throw new RuntimeException(sb.toString());
                    }
                    currentStyleRange.length = currentPosition - currentStyleRange.start + 1;
                    listOfStyles.add(currentStyleRange);
                    i += 3;
                    continue;
                }
            }

            if (currentBRTag != null && "<br/>".equalsIgnoreCase(currentBRTag)) {
                currentPosition++;
                output.append("\n");
                i += 4;
                continue;
            }
            currentPosition++;
            output.append(text.substring(i, i + 1));
        }
        this.styledText.setText(output.toString());
        this.styledText.setStyleRanges(listOfStyles.toArray(new StyleRange[listOfStyles.size()]));
    }
}
