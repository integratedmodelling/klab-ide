* When switching Eclipse releases for the Maven build

1. Most problems will come from explicit version numbers (not 0.0.0) being surreptitiously inserted into the klab.product (in the updatesite package) and the feature definition (in the sdk package). This happens when using Update in the Eclipse product editor. Maybe it also happens by itself, given the number of problems I had with these.
2. Do not remove the -SNAPSHOT qualifier from the Maven version in klab-ide and children as it's tied to the .qualifier version modifier in the plugins and the update process depend on them. 
3. The version needs to be changed also in the build.site project, wherever it appears (should be 2 places, one non-obvious).
4. Build a test release with the klab-ide/scripts/build.sh after ensuring ssh access to bc3@integratedmodelling.org.

* To debug a built release:

1. Copy the klab.ini in this folder in the base folder of a built distribution, replacing the existing;
2. Run k.LAB and attach a debugger to port 5005.

* Issues with the splash screen not showing:

Eclipse wants the splash screen to be BMP 24bit; on Windows, it needs to be saved with no color information. EVEN doing so, Windows currently does not show it (if saved by GIMP). So far the only way I found has been to install ImageMagick and convert the GIMP-saved BMP with

> magick convert splash_orig.bmp BMP2:splash.bmp

